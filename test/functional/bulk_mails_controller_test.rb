require 'test_helper'

class BulkMailsControllerTest < ActionController::TestCase
  setup do
    @bulk_mail = bulk_mails(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bulk_mails)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bulk_mail" do
    assert_difference('BulkMail.count') do
      post :create, bulk_mail: @bulk_mail.attributes
    end

    assert_redirected_to bulk_mail_path(assigns(:bulk_mail))
  end

  test "should show bulk_mail" do
    get :show, id: @bulk_mail
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bulk_mail
    assert_response :success
  end

  test "should update bulk_mail" do
    put :update, id: @bulk_mail, bulk_mail: @bulk_mail.attributes
    assert_redirected_to bulk_mail_path(assigns(:bulk_mail))
  end

  test "should destroy bulk_mail" do
    assert_difference('BulkMail.count', -1) do
      delete :destroy, id: @bulk_mail
    end

    assert_redirected_to bulk_mails_path
  end
end
