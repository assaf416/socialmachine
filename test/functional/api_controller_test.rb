require 'test_helper'

class ApiControllerTest < ActionController::TestCase
  test "should get scan_plan" do
    get :scan_plan
    assert_response :success
  end

  test "should get scan" do
    get :scan
    assert_response :success
  end

end
