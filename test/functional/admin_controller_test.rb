require 'test_helper'

class AdminControllerTest < ActionController::TestCase
  test "should get licenses" do
    get :licenses
    assert_response :success
  end

  test "should get background_jobs" do
    get :background_jobs
    assert_response :success
  end

  test "should get users" do
    get :users
    assert_response :success
  end

  test "should get posts" do
    get :posts
    assert_response :success
  end

  test "should get comments" do
    get :comments
    assert_response :success
  end

  test "should get websites" do
    get :websites
    assert_response :success
  end

end
