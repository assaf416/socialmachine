require 'test_helper'

class AutorespondersControllerTest < ActionController::TestCase
  setup do
    @autoresponder = autoresponders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:autoresponders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create autoresponder" do
    assert_difference('Autoresponder.count') do
      post :create, autoresponder: @autoresponder.attributes
    end

    assert_redirected_to autoresponder_path(assigns(:autoresponder))
  end

  test "should show autoresponder" do
    get :show, id: @autoresponder
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @autoresponder
    assert_response :success
  end

  test "should update autoresponder" do
    put :update, id: @autoresponder, autoresponder: @autoresponder.attributes
    assert_redirected_to autoresponder_path(assigns(:autoresponder))
  end

  test "should destroy autoresponder" do
    assert_difference('Autoresponder.count', -1) do
      delete :destroy, id: @autoresponder
    end

    assert_redirected_to autoresponders_path
  end
end
