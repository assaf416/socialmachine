require 'test_helper'

class TropoControllerTest < ActionController::TestCase
  test "should get callback" do
    get :callback
    assert_response :success
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get recordings" do
    get :recordings
    assert_response :success
  end

end
