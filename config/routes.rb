Store::Application.routes.draw do
  
  
  
  resources :calls

  get "tropo/callback"
  post "tropo/callback"
  get "tropo/index"

  get "tropo/recordings"

#  require 'sidekiq/web'
#  mount Sidekiq::Web, at: '/sidekiq'
#  
  resources :autoresponders
  
  post "api/parse_google_search"
  get "api/pending_emails"
  post "api/process_emails"
  post "api/process_scan"
  post "api/process_website"
  get "api/process_website"
  get "api/channel_scanned"
  get "api/email_status"
  get "api/export_brand"
  get "api/brands"
  get "api/scan_plan"
  post "api/scan"
  post "api/logs"
  get "api/scan"
  get "api/monitor"

  get 'trackers/load'
  resources :trackers
  resources :scores

  get "emails/delete_all"
  get "emails/send_email"
  resources :emails

  get "feed_entries/show"
  get "feed_entries/deploy"

  resources :tutorials

  post "bulk_mails/add_articles"
  post "bulk_mails/configure"
  get "bulk_mails/move_article"
  get "bulk_mails/stats"
  get "bulk_mails/configure"
  post "bulk_mails/import"
  get "bulk_mails/import"
  get "bulk_mails/duplicate"
  get "bulk_mails/check"
  get "bulk_mails/stop"
  get "bulk_mails/start"
  get "bulk_mails/preview"
  resources :bulk_mails

  resources :forum_posts

  resources :topics

  resources :colors

  resources :mentions

  resources :tickets
  resources :feed_entries

  
  
  get "posts/email_action"
  get "posts/post_to"
  get "posts/repost"
  get "posts/preview"
  get "posts/deploy_to_facebook"
  get "posts/add_to_delayed"
  get "posts/retweet"
  get "posts/follow_user"
  
  get "brands/templates"
  get "brands/search"
  get "brands/searches"
  get "brands/recommendations"
  get "brands/connections"
  get "brands/disconnect"
  get "brands/map"
  get "brands/reading"
  get "brands/exposure"
  get "brands/twitter_search"
  get "brands/twitter_mentions"
  get "brands/repost_rss_items"
  
  
  
  
  get "admin/find_people"
  get "admin/page_preview"
  get "admin/website_templates"
  get "admin/email_templates"
  get "admin/licenses"
  get "admin/licenses"
  get "admin/test"
  get "admin/bgc"

  get "admin/background_jobs"

  get "admin/timeline_json"
  get "admin/reader"
  get "admin/index"
  get "admin/users"
  get "admin/refresh_rss"
  get "admin/delete_all_jobs"
  get "admin/delete_job"
  get "admin/posts"
  get "admin/comments"
  get "admin/websites"

  
  get "slides/create"

  get "slides/edit"

  get "slides/destroy"

  get "slides/index"

  get "slides/update"

  get "slides/new"

  get "channels/create_short_link"
  get "channels/remove_feed"
  get "channels/add_feed"
  get "channels/register"
  post "channels/register"
  get "channels/ignore"
  get "channels/scan"
  get "channels/index"
  get "channels/show"
  post "channels/update"

  get "/users/reset_user"
  get "/users/delete_user"
  
  resources :slides
  resources :channels
  resources :posts

  resources :deployments

  resources :messages

  resources :groups

  get "analysis/index"
  get "analysis/callback"
  get "analysis/fans"
  get "analysis/social_networks"
  get "analysis/mentions"
  get "analysis/website"
  post "analysis/website"
  get "analysis/mailing_list"

  get "inquieries/reply"
  resources :inquieries

  resources :feeds

  get "logout" => "sessions#destroy", :as => "logout"
  get "login" => "sessions#new", :as => "login"
  get "sign_up" => "users#new", :as => "sign_up"
  resources :users
  resources :sessions  

  
  
  post "public/form_post"
  get "public/campaign"
  get "public/newsletter"
  get "public/event"
  get "public/email_verification"
  get "public/unsubscribe_from_email"
  get "public/elections"
  get "public/index_heb"
  get "public/meetup"
  get "public/event"
  get "public/facebook_page_public"
  get "public/facebook_page_private"
  get "public/index"
  get "public/tour"
  get "public/blog"
  get "public/prices"
  get "public/beta"
  get "public/store"
  get "public/product"
  get "public/article"
  get "public/register"
  get "public/login"
  get "public/brands"
  get "public/brand"

  get "brands/timeline"
  get "brands/stream"
  get "brands/scan_brand"
  get "brands/social_dashboard"
  get "brands/social_login_callback"
  get "brands/social_login_failure"
  
  match "/auth/:provider/callback", to: "brands#social_login_callback"
  match "/auth/failure", to: "brands#social_login_failure"

  get "page_partials/row_settings"
  get "page_partials/images"
  get "/page_partials/replace_with"
  post "/page_partials/quick_update"
  resources :page_partials

  get "pages/copy_template"
  get "pages/preview_inner"
  get "pages/make_template"
  get "pages/save_colors"
  get "pages/create_page"
  get "pages/add_line"
  get "pages/set_css_template"
  get "pages/export"
  get "pages/add_line"
  get "pages/hero_row"
  get "pages/move_row_top"
  get "pages/move_row_bottom"
  get "pages/move_row_up"
  get "pages/move_row_down"
  get "pages/remove_row"
  get "pages/preview"
  get "pages/css"
  get "pages/layout"
  get "pages/js"
  resources :pages
  
  get "websites/move_page"
  get "websites/create_page"
  get "websites/reset_custom_colors"
  get "websites/apply_color_patch"
  get "websites/css"
  get "websites/stats"
  resources :websites

  get "research/index"
  get "research/rss"
  get "research/people"
  get "research/news"
  get "research/competitors"
  get "research/groups"
  get "research/twitter"

  get "research/groups"
  get "articles/new_article"
  get "articles/publish"
  get "articles/deploy_to_twitter"
  get "articles/deploy_to_facebook_page"
  get "articles/deploy_to_blog_via_email"

  resources :bookmarks

  get "/templates/duplicate"
  get "/templates/preview"
  resources :templates

  resources :email_listings
  get "competitors/scan"
  match '/competitors/:id/scan' => "competitors#scan"
  
  
  get "events/publish"
  get "events/deploy"
  
  post "fans/import"
  get "fans/import"
  get "fans/invite"
  get "fans/ignore" 
  get "fans/search" 
  get "fans/scan" 
  get "fans/follow" 
  get "fans/unfollow" 
  get "fans/competitor" 
  get "fans/charts" 
  
  
  get "products/charts" 
  get "events/charts" 
  
  
  match '/events/:id/publish' => "events#publish"
  match '/events/:id/deploy' => "events#deploy"
  resources :events
  resources :competitors
  resources :comments
  resources :fans
  resources :users
  resources :articles
  resources :brands
  resources :products
  
  get "channels/scan"
  
  match '/channels/:id/scan' => "channels#scan"
  match 'blog/:id' => "public#blog"
  match 'store/:id' => "public#store"
  match 'site/:id' => "public#brand"
  #  mount Resque::Server, :at => "/resque"
  
  #  mount Resque::Server, :at => "/resque"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'public#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  match ':controller(/:action(/:id))(.:format)'
end
