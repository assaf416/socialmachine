# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
#every 5.minutes do
#  command  "tail -n 1500  ~/socialmachine/log/development.log | grep FAIL  > ~/logs/failuers.log"
#  command  'ps -fe | grep "ruby\|Passenger"  | tail -100 | sed "s/^/$(date) /" >> ~/logs/processes.log'
#  command  'top -n1 | grep "Passenger\|ruby\|mysql" | tail -100 | sed "s/^/$(date), /" >> ~/logs/top.log'
#  command  "mysqlshow --user=root --password=clipper --status socialmachine > ~/logs/db.log"
#  command  "cat /opt/nginx/logs/access.log | grep this-is > ~/logs/trackers.log"
#end
  

every 5.minutes do
  command " cd /Users/assafgoldstein/Desktop/Projects/socialmachine/system/scrtips; echo 'trying to load ruby scanner' >> ../logs/scanner.actions.log "
  command " cd /Users/assafgoldstein/Desktop/Projects/socialmachine/system/scrtips; ruby brands_production.rb  >> ../logs/brands.log "
  command " cd /Users/assafgoldstein/Desktop/Projects/socialmachine/system/scrtips; ruby main.rb  >> ../logs/dev.log "
end

every 24.hours do
#  command " cd ~/socialmachine; sudo thor scan brands http://23.21.17.104 /home/ubuntu/scanner >> ~/logs/thor.log"
  #      command  "tail -n 500  ~/socialmachine/log/development.log | grep  > ~/logs/failuers.log"
#  command  'rm -f ~/logs/processes.log'
#  command  'rm -f ~/logs/top.log'
end

# update using: whenever --update-crontab socialmachine
# Learn more: http://github.com/javan/whenever
