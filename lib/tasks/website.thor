#encoding: UTF-8  
#Encoding.filesystem = "UTF-8"
class Scan < Thor
 
  require 'uri'
  require 'cgi'
  require 'nokogiri'
  require 'open-uri'
  
  
  
  #
  # Website Scan
  # 
  
  desc "website [USER] [PWD] [OUTPUT-YML-FILE]", "scanning website"
  def website(brand, url, out_file)
    
    def page_views(page_name)
      # run cat | grep to trackers.log and load the page's trackers
    end
    
    
    def page_meta_data(the_url)
      
      if the_url.include? "http"
      else
        the_url = "http://" + the_url
      end
      
      puts " STATUS : READING #{the_url}"
      # grab page
      begin
        doc =  Nokogiri::HTML( open(URI.encode(the_url)))
      rescue Exception => e
        logger "FAILED TO LOAD PAGE FOR ANALYSIS: #{the_url} , #{e.message}"
      end
    

      # keywords
      keywords =[]
      begin
        doc.css('meta[name="keywords"]').each do |meta_tag|
          keywords << meta_tag['content'] 
        end
      rescue Exception => e
        puts "!! -- EXEPTION IN KEYWORDS_DESCRIPTION #{e.message} " 
      end
    
    
      # META DESCRIPTION
      meta = ""
      begin
        doc.css('meta[name="description"]').each do |meta_tag|
          meta << meta_tag['content'] 
        end
      rescue Exception => e
        puts "!! -- EXEPTION IN META_DESCRIPTION #{e.message} " 
      end
      
      
      return { :url => the_url, :keywords => keywords, :description => meta}
      
      
    end
   
    
    
    puts "SCANNING WEBSITE"
    site_info = {}
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      puts "FAILED THE WEBSITE SCAN #{e.message}"
      exit
    end
      
    website_url = plan[0][:website_url]
  
    
    
    site_info[:meta] = page_meta_data(website_url)
    keywords = site_info[:meta][:keywords]
    keyword_searches = google_keywords(keywords,website_url) unless keywords.nil?
    site_info[:searches] = keyword_searches
    
    #    puts site_info.inspect
    #    File.open(out_file, 'w') { |file| file.write(site_info.to_yaml) }
    File.open(out_file, 'w:UTF-8') { |file| file.write(site_info.to_json) }
  end

  
  
  desc "google_keywords [keywords] [url]", "RETURN YML WITH GOOGLE KEYWORD SEARCH "
  def google_keywords(keywords,url)
    puts " GOOGLE SEARCHING FOR #{keywords} of SITE:#{url}"
   
    results = []
    
    for what in keywords[0].split(",") do    
      
      puts "WHAT: #{what}"
      found_locations =[]
      new_search = CGI::escape(what)
      begin    
        doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{new_search}&num=100"))

        ar_1 = []
        ar_2 = []
        ar_3 = []

        doc.css('h3.r a').each do |link|
          begin
            ar_1 << link.content.encode
          rescue 
            next
          end
        end

        doc.search("cite").each do |cite|
          begin
            link = cite.inner_text.encode
            ar_2 << link
          rescue
            next
          end
        end

        doc.css("li.g").each do |x|
          begin
            ar_3 << x.inner_text.encode
          rescue
            next
          end
        end

        res_google = []
        i = 0
        for item in ar_1 do
          begin
            description = ar_3[i]
            res_google << {:title => ar_1[i], :description => description, :url => ar_2[i]} 
            i = i + 1
          rescue
            next
          end
        end
    
        i = 0
        for item in res_google do 
          i = i + 1
        end
      
      
      
        puts " ===  REMOVE DUPLICATIONS AND IGNORED SITES AND GETTINH LOCATIONS"
        filtered = {}
        i = 0
        for item in res_google do
          i = i + 1
          found_locations << i if item[:url].include? url
          
          begin
            link =  item[:url]
            if link.include? "/"
              pos = link.index("/")
              link = link[0..pos-1]
              #            puts " LINK : #{link}"
            end
          
            
            
            if filtered["#{link}"].nil?
              if link =~ /facebook.com|twitter.com|linkedin.co|.org|wikipedia|.pdf/i then
                #             puts "IGNORING SOCIAL NETOWRK:  #{link}"
              else
                #            puts "ADDED #{i}: #{link}"
                meta = ""
                begin
                  meta  =  page_meta_data(item[:url])
                rescue Exception => e
                  puts " FAILED TO GET META DATA FOR #{item[:url]} REASON: #{e.message}"
                  next
                end
                filtered["#{link}"] =  {:location => i, :host => link , :title => item[:title], :description => item[:description] , :meta => meta}
              end
            end
  
          rescue Exception => e
            puts "ERROR #{e.message}"
            next
          end
        end
      
        filtered.keys.each{| k|  
          puts "#{filtered[k][:location]} ) #{filtered[k][:host]} | #{filtered[k][:title]}"
        }
          
        results << {:search => what, :locations => found_locations ,:resutls => filtered}
      rescue Exception => e
        puts "ERROR *** #{e.message}"
        return results
      end
    
    end
    return results
  end
  
  


  desc "logger [VALUE]", "Logging stuff"
  def logger(s)
    puts " #{Time.now}  STATUS: #{s}"
  end
  
  
end