
class Scan < Thor
  # encoding: UTF-8 
  require 'koala'
  require 'yaml'
  require 'nokogiri'
  
  
  #
  # FACEBOOK  Scan
  # 
  
  desc "facebook [BRAND_ID] [HOST] [OUT_YAML]", "scanning facbook "
  def facebook(brand, url, file)

    t00 = Time.now
    
    
    def fb_page_views(page_name)
      # run cat | grep to trackers.log and load the page's trackers
    end
    
    def fb_messages(graph,rest)
      ret = []
      messages = graph.get_connections("me", "threads")
      for item in messages do 
        ret << { :from => item['senders'] , :posted_at => item["updated_time"] , :message => item["snippet"] , :from_user_id => "" , :messages_count => item["message_count"], :uid => item["id"]}
      end
  
      return ret
    end


    def fb_fan_pages_posts(graph,rest)
      fan_pages = []
      accounts = graph.get_connections("me", "accounts")
      for account in accounts do
        unless account['category'].include? "Application"
      
      
          comments = []
      
      
          page = graph.get_object(account["id"])
          page_posts = graph.get_connections(account["id"], "feed")
      
          for post in page_posts do 
            if post['comments']['count'] > 0 then
              for comment in post['comments']['data'] do
                comments << { :from_uid => comment['from']['id'] ,:from_name => comment['from']['name'] , :posted_at => comment['created_time'] ,  :message => comment['message'] }
              end
            end
          end
          fan_pages << {:name => page['name'], 
            :page => page, :general_info => account,
            :posts => page_posts,
            :comments => comments , 
            :stats => {:posts => page_posts.size , :comments => comments.size , :likes => page["likes"] }}
        end
      end
      return fan_pages
    end




    def fb_group_posts(graph,rest)
  
      res = []
      groups = graph.get_connections("me", "groups")
      for group in groups
        posts = []
        users = []
        begin
          group_feed = graph.get_connections(group["id"], "feed")
          for item in group_feed do 
            posts << item
          end
        rescue Exception => ex
          logger.error "FAILED ON  fb_posts_and_comments. Error: #{ex.message}"
          next
        end
        res << {:group => group , :posts => posts , :posts_count => posts.size}
      end
      return res
    end


    def fb_groups_find(graph,rest,search)
      ids = []
      pages = []
      for q in search.split(',') do 
        begin
          search_res = graph.search("#{q}",{:type => "group"})
          for item in search_res do 
            begin
              ids << item['id'] 
            rescue Exception => e
              puts "FAILED on fb_groups_find #{e.message}"
              next
            end
          end
      
          idss ="0" ; ids.each{|id| idss << ",#{id}"}
          fql = "SELECT gid, name, pic,group_type,email,update_time, description,website  FROM group WHERE  gid IN(#{idss})"
          users = []
          profiles = rest.fql_query(fql)
          for profile in profiles do 
            begin
              users << profile
            rescue Exception => e2
              log_error " FAILED on fb_groups_find #{e2.message}"
            end
          end
          return users
        rescue Exception => e
          log_error " FAILED on fb_groups_find  (2) #{e.message}"
          return users
        end
      end
    end


    def fb_mentions_find(graph,rest,search)
      ids = []
      posts = []
      for q in search.split(',') do 
        begin
          search_res = graph.search("#{q}",{:type => "post"})
          for item in search_res do 
            begin
              posts << item
              ids << item['from']['id'] 
            rescue Exception => e
              log_error "FAILED on fb_mentions_find #{e.message}"
              next
            end
          end
      
          idss ="0" ; ids.each{|id| idss << ",#{id}"}
          fql = fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
          users = []
          profiles = rest.fql_query(fql)
          for profile in profiles do 
            begin
              users << profile
            rescue Exception => e2
              log_error " FB_MENTIONS_FIND  #{e2.message}"
            end
          end
          return {:search => search, :users => users , :posts => posts , :posts_found => posts.size,  :users_found => users.size }
        rescue Exception => e
          log_error "FB_MENTIONS_FIND 2 #{e.message}"
          return {:search => search,  :posts => posts, :users => users }
        end
      end
    end

    def fb_pages_find(graph,rest,search)
      ids = []
      pages = []
      for q in search.split(',') do 
        begin
          search_res = graph.search("#{q}",{:type => "page"})
          for item in search_res do 
            begin
              ids << item['id'] 
            rescue Exception => e
              log_error "FAILED on fb_pages_find #{e.message}"
              next
            end
          end
      
          idss ="0" ; ids.each{|id| idss << ",#{id}"}
          fql = "SELECT  page_id ,name, username, description, page_url,website,phone , fan_count FROM page WHERE  page_id IN(#{idss})"
          users = []
          profiles = rest.fql_query(fql)
          for profile in profiles do 
            begin
              users << profile
            rescue Exception => e2
              puts " +++++++++++++ #{e2.message}"
            end
          end
          return {:search => search, :users => users }
        rescue Exception => e
          puts " !!!!!!!!!!!!!!!!!!!! #{e.message}"
          return {:search => search, :users => users }
        end
      end
    end




    def short_profile(q,profile)
      return {
        :uid => profile["uid"],
        :name => profile["name"],
        :image_url => profile["pic_small"],
        :website => profile["website"],
        :home_url => "",
        :email => "",
        :friends_count => "",
        :bio => "",
        :search_key => q,
        :wall_count => profile["wall_count"],
        :friend_count => profile["friend_count"],
        :likes_count => profile["likes_count"],
        :location => profile["hometown_location"]
      }
    end

    def fb_search_users(graph,rest,search,kind)
 
      ids = []
      users = []
      for q in search.split(',') do 
        begin
          search_res = graph.search("#{q}",{:type => "post"})
    
          for item in search_res do 
            begin
              #puts " ----> #{item.to_yaml}"
          
              #          puts " POST USER : #{item['from']}   ID: #{item['from']['id']}"
              ids << item['from']['id'] 
          
              #          users << {  :uid => "#{item['id']}",  :name => "#{item['name']}", :image_url => "https://graph.facebook.com/#{item['id']}/picture", :kind => kind   }
            rescue Exception => e
              puts "FAILED on fb_search_users #{e.message}"
              next
            end

          end
      
          idss ="0" ; ids.each{|id| idss << ",#{id}"}
          fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
      
          users = []
          profiles = rest.fql_query(fql)
          for profile in profiles do 
            begin
              #              profile["search"] = q
              users << short_profile(q,profile)
              #          puts "#{kind} profile ::#{profile.inspect}"
            rescue Exception => e2
              puts " +++++++++++++ #{e2.message}"
            end
          end
          return users
        rescue Exception => e
          puts " !!!!!!!!!!!!!!!!!!!! #{e.message}"
          return users
        end
      end
  
    end

    
    def fb_friends(graph,rest , uids)
      #
      # Load Friends
      #
  
      ids2 = []
      ids = []
      res_friends = []
      res_new_friends = []
      friends = graph.get_connections("me", "friends")
      for friend in friends do
        ids << friend["id"]
        ids2 << friend["id"] unless uids.include? friend["id"]
      end
  
      idss = ids.to_s.gsub!("[","").gsub!("]","")
      fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
 
      idss2 = ids2.to_s.gsub!("[","").gsub!("]","")
      fql2 = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss2})"
 
      profiles = rest.fql_query(fql)
      new_profiles = rest.fql_query(fql2)
 
      for profile in profiles do
        res_friends << profile
      end
  
      for profile in new_profiles do
        res_new_friends << profile
      end
      return res_friends, res_new_friends
    end



    def fb_profile(graph,rest)
      fbp = graph.get_object("me")
      profile = rest.fql_query("SELECT friend_count , about_me, name, contact_email,pic_with_logo , username, website , hometown_location , status ,political   FROM user WHERE uid=#{fbp['id']}")
      p = profile[0]
      res_profile =  { :name => p["name"] , :bio => p["about_me"] , :website => p["website"] , :email => p["contact_email"]}
    end
    
    



    
    # globals
    token = "" 
    secret = ""
    profile = ""
    uids     = ""
    search_1,search_2,search_3,search_4 = ""
  
    
    
    puts " LOADING PARAMETERS.."
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
      
      
      
      token = plan[0][:facebook_token]
      secret = plan[0][:facebook_secret]
      uids = plan[0][:facebook_fan_uids]
      profile = plan[0][:facebook_profile_name]
      search_1 = plan[0][:facebook_search_1]
      search_2 = plan[0][:facebook_search_2]
      search_3 = plan[0][:facebook_search_3]
      search_4 = plan[0][:facebook_search_4]

      channel_id = plan[0][:fb_cid]
      puts " STARTING #{token}|secret:#{secret}:  uids:#{uids}"
      
      
      channel_info = {}
      res_friends = []
      res_pages = []
      res_groups = []
      res_profile = {}
  
      ids = []
      @graph = Koala::Facebook::GraphAPI.new(token)
      @rest = Koala::Facebook::RestAPI.new(token)


      notfications = []
      fbp = @graph.get_object("me")
      fql = "SELECT notification_id, sender_id, title_text, body_text,object_type,icon_url, href, created_time ,is_unread  FROM notification WHERE recipient_id=#{fbp['id']}"
      @rest.fql_query(fql).each{|n| notfications << n}
  
  
      friends , new_friends = fb_friends(@graph,@rest, uids)
  
  
  
      
      
      channel_info[:profile] = fb_profile(@graph,@rest)
      channel_info[:notifications] = notfications
      channel_info[:friends] = friends
      channel_info[:new_friends] = new_friends
      channel_info[:recommended_competitors] = fb_pages_find(@graph,@rest,search_1)
      channel_info[:recommended_groups] = fb_pages_find(@graph,@rest,search_2)
      channel_info[:fan_pages] = fb_fan_pages_posts(@graph,@rest)
      channel_info[:groups] = fb_group_posts(@graph,@rest)
      channel_info[:messages] = fb_messages(@graph,@rest)
      channel_info[:prospects] = fb_mentions_find(@graph,@rest,search_4)
     
      
     
      pages_stats = []
      total_posts = 0
      total_comments = 0
      total_likes = 0
      for page in channel_info[:fan_pages]
        total_posts = total_posts + page[:stats][:posts] unless page[:stats][:posts].nil?
        total_comments = total_comments + page[:stats][:comments] unless page[:stats][:comments].nil?
        total_likes = total_likes + page[:stats][:likes] unless page[:stats][:likes].nil?
       
        pages_stats << { 
          :posts => page[:stats][:posts],
          :comments => page[:stats][:comments],
          :likes => page[:stats][:likes],
        }
      end
        
      
      stats_block = {
        :posts => total_posts ,
        :comments => total_comments ,
        :likes => total_likes, :pages_stats => pages_stats}
        
      
      channel_info[:scan] = { 
        :scan_start_time => t00, :scan_end_time => Time.now, :channel => channel_id,
        :new_friends => new_friends.size,
        :total_friends => friends.size,
        :no_longer_friends => 0,
        :fan_pages_stats => stats_block
      }  
         
      
      
      puts " ------------ RESULT ---------"
      #  puts channel_info.to_yaml
  
       
      #
      # Update the Channel Status
      #
      puts " UPDATING THE SOCIAL MACHINE .."
      params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
      x = Net::HTTP.post_form(URI.parse("#{url}/api/process_scan"), params)
      puts " POSTED YAML TO SOCIALMACHINE"

      
      File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
      puts "done with facebook!"
      
      
      
     
      
      
      #      channel_id = plan[0][:fb_cid]
      #      uri = URI.parse("#{url}/api/channel_scanned?id=#{channel_id}")
      #      response = Net::HTTP.get_response(uri)
      #      res = Net::HTTP.get(uri)
      
    rescue Exception => e
      puts "ERROR RUNIING SCAN : #{e.message}"
      exit
    end
    
    
   
    
    
    
    
  end

 


  desc "logger [VALUE]", "Logging stuff"
  def logger(s)
    puts " #{Time.now}  STATUS: #{s}"
  end
  
  
  
  
  


  #   -------------------------------------------------------------------------
  #
  #    FACEBOOK 
  #
  #   -------------------------------------------------------------------------



 
end