
class Scan < Thor
  
  #
  # Yelp Scan
  # 
  
  desc "yelp [USER] [PWD] [OUTPUT-YML-FILE]", "scanning yelp user"
  def yelp(brand, url, file)
    
    #    client = yelp::Client.new('y1ifejqt5oss', '0EI8fpBTNyOf1G0U')
    
    puts "SCANNING yelp"
    
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      puts "FAILED THE yelp SCAN #{e.message}"
      exit
    end
      
    token   = plan[0][:yelp_token]
    client = yelp2::Client.new(:oauth_token => token)
    profile = client.user('self')
    competitors = []
    for item in  client.search_venues(:ll => '32.3705,34.8654')["groups"]
       item["items"].each{|i|competitors << { :info =>  i  } }
#        :id => item["id"],        :name => item["name"],        :location => item["location"]      }
    end
    
    friends_checkins = client.recent_checkins
    
#    close_users = client.search_users(:ll => '32.3705,34.8654')
    
    #    competitors_2  = client.search_venues(:ll => '34.8654,32.3705')
   
    
    channel_info = { :profile => profile , :competitors => competitors, :friends_checkins => friends_checkins}
    
    
    
    
    puts channel_info.to_yaml
    
    
    File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
    puts "Done loading yelp  "
    
  
  end
end