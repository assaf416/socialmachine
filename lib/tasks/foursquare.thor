
class Scan < Thor
  require 'foursquare2'
  # encoding: UTF-8 
  #
  # Foursquare Scan
  # 
  
  desc "foursquare [USER] [PWD] [OUTPUT-YML-FILE]", "scanning foursquare user"
  def foursquare(brand, url, file)
    
    
    t00 = Time.now
    #    client = foursquare::Client.new('y1ifejqt5oss', '0EI8fpBTNyOf1G0U')
    
    puts "SCANNING foursquare"
    
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      puts "FAILED THE FOURSQUARE SCAN #{e.message}"
      exit
    end
      
    token   = plan[0][:foursquare_token]
    client = Foursquare2::Client.new(:oauth_token => token)
    profile = client.user('self')
   
    friends_checkins = client.recent_checkins
   
    
    ar_friends = []
    ar_venues = []
    ar_specials = []




    specials = client.search_specials(:ll => '36.142064,-86.816086')
    for item in specials do 
      ar_specials << item
      puts "-- -- -- -- "
      puts " SPECIAL : #{item.inspect}"
      puts "-- -- -- -- "
    end


    friends = client.user_friends("18806450")
    puts "friends"
    friends.each {|f|
      if  f[1].class == Array
        for i in f[1] do 
          ar_friends << i
          #      puts " ITEM : #{i} "
        end
      end
    }  



    venues  = client.search_venues(:ll => '36.142064,-86.816086', :query => 'Starbucks')
    venues.each {|f|
      if  f[1].class == Array
        for i in f[1] do 
          for venue in i["items"] do 
            puts " ------ ----------- ------------"
            ar_venues << {  :id =>  venue.id , 
              :name =>  venue.name , 
              :stats => venue.stats, 
              :info => venue.contact, 
              :specials => venue.specials, 
              :visitors => venue.beenHere,
              :location => venue.location}
            puts " VENUE : #{venue.to_yaml} "
            puts " ------ ----------- ------------"
          end
          #      ar_venues << i
      
        end
      end
    }  
  
  
    channel_info = {
      :scan => { :scan_start_time => t00, :scan_end_time => Time.now, :channel => plan[0][:fs_cid]},
      :profile => profile ,  :friends_checkins => friends_checkins,
      :friends => { :count => ar_friends.count , :list => ar_friends} , 
      :specials => {:count => ar_specials.count,  :list => ar_specials},  
      :venues => {:count => ar_venues.count,  :list => ar_venues}}  
    
    #
    # Update the Channel Status
    #
    puts " UPDATING THE SOCIAL MACHINE .."
    params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
    x = Net::HTTP.post_form(URI.parse("#{url}/api/process_scan"), params)
    puts " POSTED YAML TO SOCIALMACHINE"
    
    
    File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
    puts "Done loading foursquare  "
    
    
  end
end