
class Scan < Thor
  require 'google_plus'
  
  #
  # google_plus Scan
  # 
  
  desc "google_plus [USER] [PWD] [OUTPUT-YML-FILE]", "scanning google_plus user"
  def google_plus(brand, url, file)
    
    #    client = google_plus::Client.new('y1ifejqt5oss', '0EI8fpBTNyOf1G0U')
    
    puts "SCANNING google_plus"
    
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      puts "FAILED THE google_plus SCAN #{e.message}"
      exit
    end
      
    
    token = plan[0][:google_plus_token]
    profile_id = plan[0][:google_plus_profile_id]
    
    GooglePlus.api_key = 'AIzaSyBkHTrd0iEB84Ije_1VehUnBWTx4j_CCk8'
    person = GooglePlus::Person.get(profile_id)#, :access_token => token)
    puts person.display_name
    activites = person.list_activities.items
    
    channel_info = { :profile => person, :activities => activites}
    
    puts channel_info.to_yaml
    
    File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
    puts "Done loading google_plus  "
  end
end