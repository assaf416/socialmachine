require "google_reader_api"
# encoding: UTF-8 
class Scan < Thor
 
  #
  # Google Reader Scan
  # 
  
  
  desc "google_reader [USER] [PWD] [OUTPUT-YML-FILE]", "scanning google reader"
  def google_reader(brand, url, file_name)
    puts "SCANNING GOOGLE READER"
    t00 =Time.now
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      puts "FAILED THE RSS  SCAN #{e.message}"
      exit
    end
      
    email = plan[0][:gmail_email]
    pwd = plan[0][:gmail_secret]
    search = plan[0][:rss_search]
    channel_id = plan[0][:rs_cid]
  
    channel_info = {:name => "RSS"}
    feeds = []
    failed = []
    recommended_articles = []
    unread_articles = []
    unread_articles2 = {}
  

    user = GoogleReaderApi::User.new(:email => email, :password => pwd)

    for feed in user.feeds do 
      begin 
        feeds << {:title => feed.title , :url => feed.url }
        for x in feed.unread_items(10) do 
          #        x.entry.toggle_read
          unread_articles <<  x.entry
          unless x.entry.content.nil?
            ssss = { 
              :message => x.entry.content.content ,
              :posted_at => x.entry.published.content,
              :link => x.entry.link.href,
              :title  => x.entry.title.to_s,
              :feed => { :name => feed.title , :url => feed.url}
            } 
            unread_articles2[x.entry.link.href] = ssss
            for keyword in search.split(",") do 
              recommended_articles <<  ssss if ssss[:message].include? keyword
            end
          end
        end
      rescue Exception => e
        failed << {:feed => feed.title, :error => e.message}
        #puts "FAILED ON GOOGLE READER LOADING  #{feed.title}  #{e.message}"
        next
      end
      
      
      new_articles = []
      unread_articles2.each{ | key, value | new_articles << value }
    end

    channel_info[:scan] = { :scan_start_time => t00, :scan_end_time => Time.now, :channel => channel_id}
    channel_info[:feeds] = {  :total => feeds.size, :feeds => feeds}
    channel_info[:recommended] = {:search => search, :total => recommended_articles.size, :articles => recommended_articles}
    channel_info[:unread] = { :total => new_articles.size , :articles => new_articles}
    channel_info[:failed] = failed

    #
    # Update the Channel Status
    #
    puts " UPDATING THE SOCIAL MACHINE .."
    params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
    x = Net::HTTP.post_form(URI.parse("#{url}/api/process_scan"), params)
    puts " POSTED YAML TO SOCIALMACHINE"

  
    File.open(file_name, "w") { |file| file.write(channel_info.to_yaml) }
    puts "WROTE THE RSS FILE!"
  end

 


  desc "logger [VALUE]", "Logging stuff"
  def logger(s)
    puts " #{Time.now}  STATUS: #{s}"
  end
  
  
end