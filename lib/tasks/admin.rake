desc "Social Update of brands"
  task :social_scan => :environment do
    for brand in Brand.all do 
      brand.social_scan
    end
  end
  
  task :fans_scan => :environment do
    for fan in Fan.all do 
      fan.scan
    end
  end
