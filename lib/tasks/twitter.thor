class Scan < Thor
  require 'yaml'
  require 'nokogiri'
  require 'twitter'
  # encoding: UTF-8 
  
  desc "twitter [BRAND_ID] [HOST] [OUT_YAML]", "scanning twitter channel "
  def twitter( brand, url, file)
    t00 = Time.now
    # globals
    token = "" 
    secret = ""
    profile = ""
    uids     = ""
    search_1,search_2,search_3,search_4 = ""
  
    
  
    def fb_page_views(page_name)
      # run cat | grep to trackers.log and load the page's trackers
    end
    
    
  
    #
    # Twitter Scan
    # 
  
    def clean_string(s)
      return s.gsub(/\s+/, ' ').strip
      return s.strip
    end

    def short_post(post)
      { :id => post["id"], 
        :created_at => post["created_at"], 
        :from_user_id => post["user"]["id"],
        :from_user_name => post["from_user_name"],
        :from_user_profile_image_url => post["profile_image_url"],
        :source => post["source"],
        :message => post["text"],
      }
    end

    def short_user(user)
      {
        :id => user["id"] ,
        :friends => user["friends_count"],
        :description => clean_string(user["description"]),
        :profile_image_url => user["profile_image_url"],
        :name => user["name"],
        :lang => user["lang"],
        :profile_background_image_url => user["profile_background_image_url"],
        :followers_count => user["followers_count"],
        :screen_name => user["screen_name"],
        :url => user["url"],
        :statuses_count => user["statuses_count"],
        :location => user["location"],
        :time_zone => user["time_zone"],
        :retweet_count => user["retweet_count"]
      }
    end

  
    def user_from_list(id,list)
      for item in list do 
        if item[:id] == id.to_s
          return item 
        end
      end
    end
  
  
  
  
  
    #def twitter_scan(brand_id,token, secret , id , content_search,competitors_search,prospect_search,mentions_search, uids)

    puts " LOADING PARAMETERS.."
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
      
      
      token = plan[0][:twitter_token]
      secret = plan[0][:twitter_secret]
      uids = plan[0][:twitter_fan_uids]
      profile = plan[0][:twitter_profile_name]
      search_1 = plan[0][:twitter_search_1]
      search_2 = plan[0][:twitter_search_2]
      search_3 = plan[0][:twitter_search_3]
      search_4 = plan[0][:twitter_search_4]
      channel_id = plan[0][:tw_cid]

    rescue Exception => e
      puts "ERROR RUNIING SCAN : #{e.message}"
      exit
    end
    
    
    channel_info = {}
  
    api = Twitter.configure do |config|
      config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
      config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
      config.oauth_token = token
      config.oauth_token_secret = secret
    end
  
 
    puts " TWITTER SCAN!"
  
    my_followers   = []
    new_followers  = []
    not_following  = []
 
    #
    # Add my followers
    #
    begin
      user_ids = "-1,"
      api.follower_ids.collection.each{|p | user_ids << ",#{p}"}
      search_url =  "http://api.twitter.com/1/users/lookup.json?user_id=#{user_ids}&include_entities=true"
      uri = URI.parse(search_url)
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      json = JSON.parse(res)
      json.each{|p| my_followers << short_user(p) } 
    rescue
      puts "FAILED TO RUN  TWITTER FOLLOWER CALL"
    end

    puts  " BEFORE NEW FOLLOWERS !"
    # New followers that are not in my fan list.
    my_followers.each{|f|
      begin
        if uids.to_s.include? f[:id].to_s then
#          puts  "#{f[:id]} exists.."
        else
          new_followers << user_from_list(f[:id],my_followers)
          puts  " NEW USER -> #{user_from_list(f[:id],my_followers)}"
        end
      rescue Exception => e
        puts  "RECOVER FROM follower #{e.message}"
        next
      end
    
    }
  
    puts  " NEW FOLLOWERS !"
  
    # add the people who left me
    current_followers_ids = [];  my_followers.each{|f| current_followers_ids << f[:id]}
    db_followers_ids = [] ;uids.split(",").each{| uid | db_followers_ids << uid unless uid.nil? or uid.empty? }
    for item in db_followers_ids do 
      if current_followers_ids.to_s.include? item
      else
        not_following <<  user_from_list(item,my_followers)
      end
    end
  
    puts  " TWITTER PROFILE"
  
    #
    # Add profile
    #
    sts = []
    user = api.user(profile)
  
    puts  " TWITTER MY POSTS"
    my_posts = []
    Twitter.search("from:#{user.screen_name}").statuses.each{|r| my_posts << short_post(r)}
  
    puts  " TWITTER MENTIONS"
    #
    # mentions
    #
    mentions = []
    api.mentions.each{|m |  mentions << { :created_at => m.created_at , :text => m.text , :user => short_user(m.user)} }

    puts  " TWITTER RETWEETS"
    #
    # Add my retweets
    #
    retweets = []
    api.retweets_of_me.each{|t| retweets << short_post(t)}
  
  
    #
    # Build the YAML tree
    #
  

    channel_info[:profile] = short_user(user)
    channel_info[:mentions] = mentions
    channel_info[:statuses] = my_posts
    #  channel_info[:messages] = messages
    channel_info[:followers] = my_followers
    channel_info[:new_followers] = new_followers
    channel_info[:not_following] = not_following
    channel_info[:retweets] = retweets
    channel_info[:scan] = { 
        :scan_start_time => t00,
        :scan_end_time => Time.now, 
        :channel => channel_id,
        :posts => my_posts.size,
        :new_friends => new_followers.size,
        :total_friends => my_followers.size,
        :no_longer_friends => not_following.size
        }
    
    debug =2
    if debug == 2
  
      #
      # Search for content
      #
  
      puts  " TWITTER CONTENT SEARCH"
      uids = "-1"
      content_users = []
      posts = []
      for search_word in search_1.split(",") do 
        Twitter.search(search_word).statuses.each{|r|
          #        puts " --> #{r.inspect}"
          posts << short_post(r) 
          uids << ",#{ short_post(r)[:from_user_id] }"
        
        }
      end
  
      begin
        search_url =  "http://api.twitter.com/1/users/lookup.json?user_id=#{uids}&include_entities=true"
        uri = URI.parse(search_url)
        response = Net::HTTP.get_response(uri)
        res = Net::HTTP.get(uri)
        json = JSON.parse(res)
        json.each{|p| content_users << short_user(p) } 
      rescue Exception => e
        puts "FAILED TO RUN CONTENT SEARCH CALL #{e.message}.   :::: URI => #{uri} res => #{res}"
      end
  
      channel_info[:recommended_content] = {:search => search_1, :posts_found => posts.size, :users_found => content_users.size,  :users => content_users, :posts => posts }
  
 
      #
      # Search for competitors
      #
      puts  " TWITTER COMPETITOR SEARCH"
      uids = "-1"
      competitors_users = []
      posts = []
      for search_word in search_2.split(",") do 
        Twitter.search(search_word).statuses.each{|r| posts << short_post(r) ; uids << ",#{short_post(r)[:from_user_id]}"}
      end
  
      begin
        search_url =  "http://api.twitter.com/1/users/lookup.json?user_id=#{uids}&include_entities=true"
        uri = URI.parse(search_url)
        response = Net::HTTP.get_response(uri)
        res = Net::HTTP.get(uri)
        json = JSON.parse(res)
        json.each{|p| competitors_users << short_user(p) } 
      rescue Exception => e
        puts "FAILED TO RUN TWITTER COMPETITORS SEARCH CALL #{e.message}"
      end
  
      channel_info[:recommended_competitors] = {:search => search_2, :posts_found => posts.size, :users_found => competitors_users.size,  :users => competitors_users, :posts => posts }
  
 
      #
      # Search for prospects
      #
      puts  " TWITTER PROSPECTS SEARCH"
      uids = "-1"
      prospects = []
      posts = []
      for search_word in search_3.split(",") do 
        Twitter.search(search_word).statuses.each{|r| posts << short_post(r) ; uids << ",#{short_post(r)[:from_user_id]}"}
      end
  
      begin
        search_url =  "http://api.twitter.com/1/users/lookup.json?user_id=#{uids}&include_entities=true"
        uri = URI.parse(search_url)
        response = Net::HTTP.get_response(uri)
        res = Net::HTTP.get(uri)
        json = JSON.parse(res)
        json.each{|p| prospects << short_user(p) } 
      rescue Exception => e
        puts "FAILED TO RUN PROSPECTS SEARCH CALL : #{e.message}"
      end
  
      channel_info[:recommended_prospects] = {:search => search_3, :posts_found => posts.size, :users_found => prospects.size,  :users => prospects, :posts => posts }
  
    end
  
    
    
    #
    # Update the Channel Status
    #
    puts " UPDATING THE SOCIAL MACHINE .."
    params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
    x = Net::HTTP.post_form(URI.parse("#{url}/api/process_scan"), params)
    puts " POSTED YAML TO SOCIALMACHINE"
    
    
    File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
    puts "Finished twitter scan!"
    
    
   
  end
  
  
  
 


  desc "logger [VALUE]", "Logging stuff"
  def logger(s)
    puts " #{Time.now}  STATUS: #{s}"
  end
  
  
end