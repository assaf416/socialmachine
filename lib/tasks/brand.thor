class Scan < Thor
 
  require "uri"
  require "yaml"
  require "net/http"
  
  desc "brand [BRAND_ID] [HOST] [OUT_FOLDER]", "scan a single brand"
  def brand( brand, url, folder)
    
    
    begin
      uri = URI.parse("#{url}/api/brands")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      brands = YAML::load(res)
      
      puts "BRAND: #{brand} : FILE "
      begin
        twitter brand, url, "#{folder}/#{brand}.twitter.yaml"
      rescue Exception => e
        puts " FAILED TO RUN TWITTER SCAN FOR #{brand} #{e.message}"
      end
      begin
        facebook brand, url, "#{folder}/#{brand}.facebook.yaml"
      rescue Exception => e
        puts " FAILED TO RUN FACEBOOK SCAN FOR #{brand} #{e.message}"
      end
      begin
        gmail brand, url, "#{folder}/#{brand}.gmail.yaml"
      rescue Exception => e
        puts " FAILED TO RUN GAMIL SCAN FOR #{brand} #{e.message}"
      end
      begin
        google_reader brand, url, "#{folder}/#{brand}.rss.yaml"
      rescue Exception => e
        puts " FAILED TO RUN GOOGLE READER SCAN FOR #{brand} #{e.message}"
      end
      
      begin
        foursquare brand, url, "#{folder}/#{brand}.foursquare.yaml"
      rescue Exception => e
        puts " FAILED TO RUN FOURSQUARE SCAN FOR #{brand} #{e.message}"
      end
      
      begin
        youtube brand, url, "#{folder}/#{brand}.youtube.yaml"
      rescue Exception => e
        puts " FAILED TO RUN YOUTUBE SCAN FOR #{brand} #{e.message}"
      end
      
      begin
        linkedin brand, url, "#{folder}/#{brand}.linkedin.yaml"
      rescue Exception => e
        puts " FAILED TO RUN LINKEDIN SCAN FOR #{brand} #{e.message}"
      end
      
      
    rescue Exception => e
      puts " error #{e.message}"
      exit
    end
    
  end

 
end