
class Scan < Thor

  
  
  #
  # GMail Scan
  # 
  
  desc "competitors [BRAND] [URL] [OUTPUT-YML-FILE]", "scanning competitors"
  def competitors(brand, url, file)
    
    def clean_string(s)
      return s.gsub(/\s+/, ' ').strip
      return s.strip
    end
    
    def short_twitter_user(user)
      {
        :id => user["id"] ,
        :friends => user["friends_count"],
        :description => clean_string(user["description"]),
        :profile_image_url => user["profile_image_url"],
        :name => user["name"],
        :lang => user["lang"],
        :profile_background_image_url => user["profile_background_image_url"],
        :followers_count => user["followers_count"],
        :screen_name => user["screen_name"],
        :url => user["url"],
        :statuses_count => user["statuses_count"],
        :location => user["location"],
        :time_zone => user["time_zone"],
        :retweet_count => user["retweet_count"]
      }
    end
    
    puts "SCANNING COMPETITORS"
    
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      puts "FAILED THE COMPETITOR SCAN #{e.message}"
      exit
    end
      
    email = plan[0][:gmail_email]
    pwd = plan[0][:gmail_secret]
    
    
    
    channel_info = {}

    #
    # Twitter search for competitors
    # 
    api = Twitter.configure do |config|
      config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
      config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
      config.oauth_token = token
      config.oauth_token_secret = secret
    end
  
    
    twitter_profiles = []
    begin
      user_ids = "-1,"
      api.follower_ids.collection.each{|p | user_ids << ",#{p}"}
      search_url =  "http://api.twitter.com/1/users/lookup.json?user_id=#{user_ids}&include_entities=true"
      uri = URI.parse(search_url)
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      json = JSON.parse(res)
      json.each{|p| twitter_profiles << short_twitter_user(p) } 
    rescue
      puts "FAILED TO RUN  TWITTER COMPETITOR CALL"
    end



    
    channel_info[:scan_time] = Time.now
    
    puts channel_info.to_yaml
    
    File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
    puts "Done loading competitors for #{brand}"
    
  end
end

 


  