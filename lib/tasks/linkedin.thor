class Scan < Thor
  # encoding: UTF-8 
  require 'oauth'
  require 'yaml'
  require 'json'
  require 'xmlsimple'  
  require 'uri'
  require 'date'
  #
  # LINKEDIN  Scan
  # 
  
  desc "linkedin [USER] [PWD] [OUTPUT-YML-FILE]", "scanning linkedin user"
  def linkedin(brand, url, file_name)
    
    t00 = Time.now
    puts "SCANNING LINKEDIN"
    
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      puts "FAILED THE YOUTUBE SCAN #{e.message}"
      exit
    end
      
    user_token   = plan[0][:linkedin_token]
    user_secret  = plan[0][:linkedin_secret]
    
    job_search      = plan[0][:linkedin_search_1]
    competitor_search   = plan[0][:linkedin_search_2]
    groups_search       = plan[0][:linkedin_search_3]
    prospect_search     = plan[0][:linkedin_search_4]
    
   
    # Fill the keys and secrets you retrieved after registering your app
    api_key = 'y1ifejqt5oss'
    api_secret = '0EI8fpBTNyOf1G0U'
 
    # Specify LinkedIn API endpoint
    configuration = { :site => 'https://api.linkedin.com' }
 
    # Use your API key and secret to instantiate consumer object
    consumer = OAuth::Consumer.new(api_key, api_secret, configuration)
 
    # Use your developer token and secret to instantiate access token object
    access_token = OAuth::AccessToken.new(consumer, user_token, user_secret)
 
    # Make call to LinkedIn to retrieve your own profile
    response = access_token.get("http://api.linkedin.com/v1/people/~?format=json")
    profile = JSON.parse   response.body


    
    puts "LINKEDIN IN  JOB SEARCH for #{job_search}"
    
    ar_jobs = []
    for search in job_search.split(",") do 
      encoded_search  = URI.escape(search)
      xurl =    "http://api.linkedin.com/v1/job-search:(jobs:(id,customer-job-code,active,posting-date,posting-timestamp,company:(name),position:(title,location),description-snippet,description,salary,job-poster:(id,first-name,last-name,headline,picture-url),referral-bonus,site-job-url,location-description))?job-title=#{encoded_search}"
      response = access_token.get(xurl)
      jobs =  XmlSimple.xml_in( response.body)
      puts " JOBS => #{jobs.inspect}"
      begin
        for item in jobs["jobs"][0]["job"] do 
          
          

          job_title = item["position"][0]["title"][0]
          job_description = item["description-snippet"][0]
          job_company = item["company"][0]["name"][0]
          job_salary = item["salary"][0] unless item["salary"].nil?
          job_url    = item["site-job-url"][0]
          job_location = item["location-description"][0]
          job_posted_at = Time.at((item["posting-timestamp"][0]).to_i/1000).to_datetime
          
          puts "======================"
          puts "ITEM => #{item.inspect}"
          ar_jobs <<  {
            :query => encoded_search,
            :title => job_title,
            :sallary => job_salary,
            :company => job_company,
            :description => job_description,
            :location => job_location,
            :url => job_url,
            :posted_at => job_posted_at
          }
        end
      rescue Exception => e
        puts "FAILED ON JOB PARSING : #{e.message}"
        puts e.backtrace
        exit
        #        next
      end
      
      #      begin
      #        for item in list["people"][0]["person"] do
      #          ar_competitors << item
      #        end
      #      rescue Exception => e
      #        puts " @@@ FAILED ON SEARCHING #{encoded_prospect}. error: #{e.message}"
      #        next
      #      end
    end
    
    
    
    
    #response = access_token.get("http://api.linkedin.com/v1/people/~/mailbox")
    #puts response.body.to_yaml
    #
    response = access_token.get("http://api.linkedin.com/v1/people/~/connections:(first-name,last-name,id,site-standard-profile-request,headline,location:(name),industry,num-connections,summary,picture-url)?format=json")
    friends =  JSON.parse  response.body
    ar_friends = []
    for item in friends["values"] do
  
      if item["pictureUrl"]
        #   puts " DEBUG:  #{item.inspect}"
        ar_friends <<  { 
          :name => "#{item["firstName"]} #{item["lastName"]}" ,
          :image_url => item["pictureUrl"] ,
          :location => item['location'],
          :industry => item['industry'],
          :uid => item['id'],
          :headline => item['headline'],
          :summary => item['summary'],
          :friends_count => item['numConnections'],
          :industry => item['industry'],
          :private_url => item['site-standard-profile-request']
        }
      end
    end
    puts "FREINDS "


    #search_prospects
    ar_prospects = []
    for prospect in prospect_search.split(",") do 
      encoded_prospect  = URI.escape(prospect)
      url = "http://api.linkedin.com/v1/people-search:(people:(first-name,last-name,id,public-profile-url,site-standard-profile-request,headline,location:(name),industry,num-connections,summary,picture-url))?keywords=#{encoded_prospect}"
      puts " *** SEARCHING FOR #{prospect} URL:#{url}"
      response = access_token.get(url)
      list =   XmlSimple.xml_in( response.body)
      begin
        for item in list["people"][0]["person"] do
          ar_prospects << item
        end
      rescue Exception => e
        puts " @@@ FAILED ON SEARCHING #{encoded_prospect}. error: #{e.message}"
        next
      end
    end
    

    ar_return_prospects = []
    #    for item in competitors["people"][0]["person"] do
    for item in ar_prospects do
      if item["first-name"]

        v ={} 
        v[:name]          = "#{item["first-name"][0]} #{item["last-name"][0]}" 
        v[:image_url]     = item["picture-url"][0]  unless item["picture-url"].nil?
        v[:location]      = item['location'][0]     unless item["location"].nil?
        v[:industry]      = item['industry'][0]     unless item["industry"].nil?
        v[:headline]      = item['headline'][0]     unless item["headline"].nil?
        v[:summary]       = item['summary'][0]      unless item["summary"].nil?
        v[:uid]           = item['id'][0]               unless item["summary"].nil?
        v[:friends_count] = item['num-connections'][0] unless item["num-connections"].nil?
        v[:industry]      = item['industry'][0] unless item["industry"].nil?
        v[:public_url] = item['public-profile-url'] unless item['public-profile-url'].nil?
        v[:private_url] = item['site-standard-profile-request'] unless item['site-standard-profile-request'].nil?
        ar_return_prospects << v
        puts "PROSPECTS : #{item.inspect}"
        puts "------- ------"
      end
    end
    puts "RECOMMENDED PROSPECTS  "
    
    
    
    #search competitors
    ar_competitors = []
    for competitor in competitor_search.split(",") do 
      encoded_competitor  = URI.escape(competitor)
      url = "http://api.linkedin.com/v1/people-search:(people:(first-name,last-name,id,site-standard-profile-request,headline,location:(name),industry,num-connections,summary,picture-url))?keywords=#{encoded_competitor}"
      puts " *** COMPETITOR SEARCHING FOR #{competitor} URL:#{url}"
      response = access_token.get(url)
      list =   XmlSimple.xml_in( response.body)
      begin
        for item in list["people"][0]["person"] do
          ar_competitors << item
        end
      rescue Exception => e
        puts " @@@ FAILED ON SEARCHING #{encoded_prospect}. error: #{e.message}"
        next
      end
    end
    

    ar_return_competitors = []
    #    for item in competitors["people"][0]["person"] do
    for item in ar_competitors do
      if item["first-name"]
        v ={} 
        v[:name]          = "#{item["first-name"][0]} #{item["last-name"][0]}" 
        v[:image_url]     = item["picture-url"][0]  unless item["picture-url"].nil?
        v[:location]      = item['location'][0]     unless item["location"].nil?
        v[:industry]      = item['industry'][0]     unless item["industry"].nil?
        v[:headline]      = item['headline'][0]     unless item["headline"].nil?
        v[:summary]       = item['summary'][0]      unless item["summary"].nil?
        v[:uid]           = item['id'][0]               unless item["summary"].nil?
        v[:friends_count] = item['num-connections'][0] unless item["num-connections"].nil?
        v[:industry]      = item['industry'][0] unless item["industry"].nil?
        v[:private_url]      = item['site-standard-profile-request'][0] unless item["site-standard-profile-request"].nil?
        ar_return_competitors << v
        
        puts "COMPETITOR : #{item.inspect}"
        puts "------- ------"
      end
    end
    puts "RECOMMENDED COMPETITORS  "


    
    
    
    response = access_token.get("http://api.linkedin.com/v1/people/~/group-memberships:(group:(id,name,description,website-url,site-group-url,num-members,large-logo-url))?membership-state=member")
    groups =   XmlSimple.xml_in( response.body)
    puts "GROUPS  "


    # GROUP POST
    #    raise groups.inspect
    
    
    group_posts = []
    ar_group_posts = []
    for item in groups["group-membership"] do 
      qroup_id =  item["group"][0]["id"][0]
      qroup_name =  item["group"][0]["name"][0]
      recent_posts_url = "http://api.linkedin.com/v1/groups/#{qroup_id}/posts:(title,site-group-post-url,summary,creator,creation-timestamp,likes,comments)?order=recency"
      puts "recent_posts_url : #{recent_posts_url}"
      response = access_token.get(recent_posts_url)
      posts =   XmlSimple.xml_in( response.body)
  
  
      for post in posts do 
        begin
          #      puts " ==> #{post[1].inspect}"
          unless post[1][0]["title"].nil?
            puts " POST --> #{post[1][0].inspect}"
            item = post[1][0]
            user = item["creator"][0]
            puts " ++++ USER  #{user.inspect}"
            pp =  { 
              :group_name => qroup_name,
              :title => "#{item["title"][0]}" ,
              :image_url => user["picture-url"][0] ,
              :message => item['summary'][0],
              :from_name =>  "#{user["first-name"][0]} #{user["last-name"][0]}",
              :from_uid  => "#{user["id"][0]}",
              :target_link  => "#{item["site-group-post-url"][0]}",
              :posted_at => item["creation-timestamp"][0]
            }
        
            puts " ++++ POST  #{pp.inspect}"
            ar_group_posts << pp
            puts ".........."
          end
        rescue Exception => e
          puts " ERROR  => #{e.message}. skipping record.."
          next
        end
      end
  
    end



    # By default, the LinkedIn API responses are in XML format. If you prefer JSON, simply specify the format in your call
    # response = @access_token.get("http://api.linkedin.com/v1/people/~?format=json")

    response = access_token.get("http://api.linkedin.com/v1/people/~/suggestions/groups:(name,description,num-members,large-logo-url,counts-by-category)")
    recommended_groups =   XmlSimple.xml_in( response.body)
    puts "RECOMMENDED GROUPS  "





    company_search = "http://api.linkedin.com/v1/company-search:(companies:(id,name,universal-name,website-url,industries,status,logo-url,blog-rss-url,twitter-id,employee-count-range,specialties,locations,description,stock-exchange,founded-year,end-year,num-followers))?keywords=Marketing"

    response = access_token.get(company_search)
    companies =   XmlSimple.xml_in( response.body)
    puts "COMPANY SEARCH COMPETITORS  "



    response = access_token.get("http://api.linkedin.com/v1/people/~/network/updates?scope=self")
    shares =   XmlSimple.xml_in( response.body)
    puts "MY UPDATES   "


    #    response = access_token.get("http://api.linkedin.com/v1/people/~/suggestions/to-follow/companies")
    #    recommended_companies =   XmlSimple.xml_in( response.body)
    #    puts "RECOMMENDED COMPANIES TO FOLLOW  "



    channel_info = { 
      :scan => { :scan_start_time => t00, :scan_end_time => Time.now, :channel => plan[0][:li_cid],
        :posts => shares['totals'], :new_friends => ar_friends.size,
        :total_friends => ar_friends.size,
        :no_longer_friends => ar_friends.size },
      :profile => profile,
      :jobs => ar_jobs,
      :prospects => { :total => ar_return_prospects.count, :list => ar_return_prospects},
      #      :competitors => { :total => ar_return_competitors.count, :list => ar_return_competitors},
      :companies => companies ,
      #      :recommended_companies => recommended_companies, 
      #      :shares => shares, 
      :friends => ar_friends, 
      :groups => groups , :recommended_groups => recommended_groups,
      :group_posts => ar_group_posts
    }  

    
    #
    # Update the Channel Status
    #
    puts " UPDATING THE SOCIAL MACHINE .."
    params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
    begin
      x = Net::HTTP.post_form(URI.parse("#{url}/api/process_scan"), params)
      puts "RETURNED FROM SOCIAL MACHINE : #{x}"
    rescue exception => e
      puts "FAILED ON POSTING TO SOCIAL MACHINE #{e.message}"  
    end
    
    File.open(file_name, 'w') { |file| file.write(channel_info.to_yaml) }
    puts "LINKEDIN FILE (#{file_name}) IS CREATED "
    
    
  end
end

