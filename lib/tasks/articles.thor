class Scan < Thor
 
  require 'uri'
  require 'cgi'
  require 'nokogiri'
  require 'open-uri'
  
  
  
  desc "artciles [BRAND_ID] [HOST] [OUT_YAML]", "scan articles deployment on google"
  def articles( brand, url, file)
    article_ids = ""
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
      article_ids = plan[0][:article_urls]
      #puts article_ids
      res =  google_search_articles(article_ids,"www")
      File.open(file, 'w') { |file| file.write(res.inspect) }
    rescue Exception => e
      puts " error #{e.message}"
      exit
    end
    
  end

 

  
  desc "google_keywords [keywords] [url]", "RETURN YML WITH GOOGLE KEYWORD SEARCH "
  def google_search_articles(keywords,url)
    puts " GOOGLE SEARCHING FOR #{keywords} of SITE:#{url}"
   
    results = []
    
    
      
    puts "WHAT: #{keywords}"
    
    new_search = CGI::escape(keywords)
    begin    
      doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{new_search}&num=100"))

      ar_1 = []
      ar_2 = []
      ar_3 = []

      doc.css('h3.r a').each do |link|
        begin
          ar_1 << link.content.encode
        rescue 
          next
        end
      end

      doc.search("cite").each do |cite|
        begin
          link = cite.inner_text.encode
          ar_2 << link
        rescue
          next
        end
      end

      doc.css("li.g").each do |x|
        begin
          ar_3 << x.inner_text.encode
        rescue
          next
        end
      end

      res_google = []
      i = 0
      for item in ar_1 do
        begin
          description = ar_3[i]
          res_google << {:title => ar_1[i], :description => description, :url => ar_2[i]} 
          i = i + 1
        rescue
          next
        end
      end
      return {:search => keywords, :resutls => res_google}
    rescue Exception => e
      puts "ERROR *** #{e.message}"
      return results
    end
  end

  
  
end