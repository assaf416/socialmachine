
class Scan < Thor
  require 'youtube_it'
  # encoding: UTF-8 
  #
  # Youtube Scan
  # 
  desc "short_video_url [VID] ", "return Video Object for youtube"
  def short_video_url(vid)
    v1 = vid.index("video:")
    return vid[v1+6..100] unless v1.nil?
    return ""
  end
    
  desc "profile_info [VID] ", "return Video Object for youtube"
  def profile_info(profile)
    puts "PROFILE CLASS : #{profile.class}"
    s = {
      :name => profile.first_name[0] + " " + profile.last_name[0], 
      :image_url => profile.avatar,
      :location =>  profile.location,
      :subscribers => profile.subscribers,
      :movies     => profile.upload_count
    } unless profile.nil?
    puts "PROFILE : #{s}"
    return s
  end
  
  desc "video_info [VID] ", "return Video Object for youtube"
  def video_info(video)
    
    s = {:title => video.title,
      :published => video.published_at ,
      :author_name => video.author.name, 
      :author_url => video.author.uri, 
      :player_url => video.player_url, 
      :views => video.view_count,
      :thumbnails => video.thumbnails[0].url,
      :keywords => video.keywords,
      :category => video.categories[0].label,
      :comments_count => video.comment_count
    }
    puts s
    return s
  end
  
  desc "youtube [USER] [PWD] [OUTPUT-YML-FILE]", "scanning youtube channel"
  def youtube(brand, url, file)
    t00 = Time.now
    
    
    puts "SCANNING YOUTUBE"
    
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      puts "FAILED THE YOUTUBE SCAN #{e.message}"
      exit
    end
      
    youtube_channel = plan[0][:youtube_profile_name]
    search_query = plan[0][:youtube_search_1]
    channel_info = {}
    my_videos = []
    comments = []
    competitors = []
    search_videos = []
    channel_id = plan[0][:yt_cid]
    
    
    client = YouTubeIt::Client.new 
    puts " MY YOUTUBE CHANNEL: #{youtube_channel}"
    my_profile = client.profile(youtube_channel)
    
    for video in client.videos_by(:user => youtube_channel).videos do 

      puts " GETTING VIDEOS"
      v_id = short_video_url(video.video_id)
      my_videos << {:video => video_info(video)}
      
      if video.comment_count > 0
        puts " GETTING COMMENTS for #{v_id}"
        #comments
        for comment in client.comments(video.unique_id) do 
          comments <<   comment 
        end
      end
    end
    
    # search for competitors and content
    competitors_names = []
    for video in client.videos_by(:query => search_query).videos do 
      search_videos << { :video => video_info(video)}
      competitors_names << video.author.name 
    end

    
    for profile in client.profiles(competitors_names)
      begin
        competitors << profile_info(profile[1])
      rescue
        next
      end
    end

    
    
    names = []
    ar_subscriptions = []
    for item in client.subscriptions(youtube_channel)  do
      names << item.title.gsub!("Activity of: ","")
    end
    for profile in client.profiles(names)
      new_videos =[]
      for video in client.videos_by(:fields => {:published  => ((Date.today - 14)..(Date.today)), :user => profile[0]["username"]} ).videos do 
        puts " GETTING  SUBSCRIPTION  VIDEOS"
        v_id = short_video_url(video.video_id)
        new_videos << video_info(video)
      end
      
      ar_subscriptions <<  { :name => profile, :new_videos => new_videos}
    end
    
   
    channel_info[:scan] =   { :scan_start_time => t00, :scan_end_time => Time.now, :channel => plan[0][:yt_cid]}
    channel_info[:profile] = my_profile
    channel_info[:my_videos] = my_videos
    channel_info[:search_videos] = search_videos
    channel_info[:competitors] = competitors
    channel_info[:comments] = comments
    channel_info[:subscriptions] = ar_subscriptions
    
    #
    # Update the Channel Status
    #
    puts " UPDATING THE SOCIAL MACHINE .."
    params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
    x = Net::HTTP.post_form(URI.parse("#{url}/api/process_scan"), params)
    puts " POSTED YAML TO SOCIALMACHINE"
    
    #    puts channel_info.to_yaml
    
    File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
    puts "Done loading youtube for #{youtube_channel}"
    
  
  end
end


