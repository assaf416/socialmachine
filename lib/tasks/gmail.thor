
class Scan < Thor
  require 'gmail'
  require 'mail'
  # encoding: UTF-8 
  
  #
  # GMail Scan
  # 
  
  desc "gmail [USER] [PWD] [OUTPUT-YML-FILE]", "scanning website"
  def gmail(brand, url, file)
    
    t00 = Time.now
    puts "SCANNING GMAIL"
    
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      puts "FAILED THE GMAIL SCAN #{e.message}"
      exit
    end
      
    email = plan[0][:gmail_email]
    pwd = plan[0][:gmail_secret]
    channel_id = plan[0][:gm_cid]
    
    channel_info = {}
    the_emails = []
    contacts = []
    begin
      gmail = Gmail.connect(email, pwd) 
      t = Time.now - 86400
      the_date = t.strftime("%Y-%m-%d")
      puts "READING MAIL FROM #{the_date}"
      emails =  gmail.inbox.mails(:after => Date.parse(the_date))
      for item in emails[0..10] do 
        begin
          mail = Mail.read_from_string item.message
        
          m = { :subject => item.subject , 
            :from_email => "#{item.envelope.sender[0].mailbox}@#{item.envelope.sender[0].host}" ,
            :posted_at => item.envelope.date,
            :uid => item.uid}
        
          m[:body] = mail.html_part.decoded if mail.html_part
          if mail.text_part
            m[:body_text] = mail.text_part.decoded 
          else
            m[:body] = mail.html_part
          end
        
          the_emails << m
          
          puts "EMAIL : #{m[:subject]}"
        
          contacts << { :name => item.envelope.sender[0].name ,
            :email =>"#{item.envelope.sender[0].mailbox}@#{item.envelope.sender[0].host}" }
                    
        
        rescue Exception => e2
          puts "FAILED on email_reading #{e2.message}"
          if e2.message.include? "SCALAR, SEQUENCE-START"
            channel_info[:emails] = the_emails
            channel_info[:contacts] = contacts
    
            File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
            puts "Done partial  loading gmail for #{email}"
          end
          next
        end
      end
    
      channel_info[:scan] = { :scan_start_time => t00, :scan_end_time => Time.now, :channel => channel_id}
      channel_info[:emails] = the_emails
      channel_info[:contacts] = contacts
    
      #
      # Update the Channel Status
      #
      puts " UPDATING THE SOCIAL MACHINE .."
      params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
      x = Net::HTTP.post_form(URI.parse("#{url}/api/process_scan"), params)
      puts " POSTED YAML TO SOCIALMACHINE"
      
      #      puts channel_info.to_yaml
    
      File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
      puts "Done loading gmail for #{email}"
    
      
      
      #
      # Update the Channel Status
      #
      puts " UPDATING THE SOCIAL MACHINE .."
      channel_id = plan[0][:gm_cid]
      uri = URI.parse("#{url}/api/channel_scanned?id=#{channel_id}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      
    end
    gmail.logout
  end

 


  desc "logger [VALUE]", "Logging stuff"
  def logger(s)
    puts " #{Time.now}  STATUS: #{s}"
  end
  
  
end