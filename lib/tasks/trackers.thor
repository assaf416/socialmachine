class Scan < Thor
  
  require 'geocoder'
  require 'chronic'
  
  desc "trackers  [OUT_YAML]", "scan trackers.log "
  def trackers( file)
    
    results = []
    
    
    def update_location
      location = Geocoder.search(ip)[0]
      unless location.nil?
        country = location.country
        city = location.city
        address = location.address
        latitude = location.latitude
        longitude = location.longitude
        save
      end
    end
  
  
    def the_os(str)
      return " " if str.nil?
      return "Mac" if str.include? "Mac"
      return "Mac" if str.include? "IPhone"
      return "Android" if str.include? "Android"
      return "Linux" if str.include? "Linux"
      return "PC" 
    end

    def the_article_id(str)
      return nil if str.nil?
      if str.include? "article="
        val = str[str.index("article")..1000].gsub("article=","")
      end
    end

    def the_email_id(str)
      return nil if str.nil?
      if str.include? "email="
        val = str[str.index("email=")..1000].gsub("email=","")
      end
    end



    def the_channel_id(str)
      return nil if str.nil?
      if str.include? "channel="
        val = str[str.index("channel=")..1000].gsub("channel=","")
      end
    end

    def browser(str)
  
    end

    def the_device(str)
      return " " if str.nil?
      return "iPad" if str.include? "iPad"
      return "iPhone" if str.include? "iPhone"
      return "mobile" if str.include? "Mobile"
      return "unknown"
    end

  
    def fix_time(str)
      return "" if str.nil?
      day = str[0..1]
      month = str[3..5]
      year = str[7..10]
      hour = str[12..13]
      minute =str[15..16] 
      second =str[18..19] 
      return "#{month} #{day} #{year} #{hour}:#{minute}:#{second}"
    end

  
  
  
    def parse_line(str)
      
      list = []
      
      ar = str.split(" ")
      pp = ar[6]
      ip = ar[0]
      time =  Chronic::parse(fix_time( ar[3].to_s.gsub!("[","")) )
      token = pp[pp.index("?")..pp.size+1] if pp and pp.include? "?"
      status = ar[8]
      referer = ar[11..100]
      device = the_device(referer)
      os = the_os(referer)
      article_id = the_article_id(token)
      email_id = the_email_id(token)
      channel_id = the_channel_id(token)

      location = Geocoder.search(ip)[0]
      if location
        # puts "LOCATION: #{location.address} , #{location.country} #{ location.city} (#{location.latitude}:#{location.longitude}) , Channel: #{channel_id} | Email ID: #{email_id} : Article: #{article_id} |   IP: #{ip} | Device:#{device} - #{os}| Time: #{time} | Token: #{token} | Parameters: #{pp} } Referer: #{referer}"  
        puts " TIME: #{time}"
      
        list = {
          :logged_at => time,
          :ip_address => ip,
          :latitude => location.latitude,
          :longitude => location.longitude,
          :address => location.address,
          :city => location.city,
          :country => location.country,
          :os => os,
          :email_id => email_id,
          :channel_id => channel_id,
          :article_id => article_id
        }
      end
      return list
    end
  
  
    def load
      begin
        
        list = []
        
        path = "../logs/trackers.log"
        puts  "PATH = #{path}"
        log = File.open(path).read
        for line in log.split("\n") do 
          list << parse_line(line) 
        end
        puts list.to_yaml
      rescue Exception => e
        puts  " FAILD to open trackers.log file #{e.message}" 
      end
      
    end

    load
    
  end

 

  
  
  
end