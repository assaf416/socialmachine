class Scanner < Thor
 
  desc "facebook [PLAN_URL]", "scanning facebook channel"
  method_options :url => :string
  def facebook(url = "*")
    logger " SCANNING FACEBOOK"
    sleep(10)
  end
  

  desc "gmail [NAME]", "scanning gmail channel"
  method_options :force => :boolean
  def twitter(name = "*")
    logger "READING GMAIL EMAILS FROM YESTERDAY"
    logger "MARK COMPETITOR EMAILS"
    logger "MARK BOUNCED EMAILS"
    sleep(1)
  end


  desc "tracker [NAME]", "scanning trackers channel"
  method_options :force => :boolean
  def tracker(name = "*")
    logger "GET TRACKERS FOR GIVEN PAGE | FANPAGE | EMAIL | ARTICLE"
    sleep(1)
  end

  desc "rss [NAME]", "scanning google reader channel"
  def website(name = "*")
    logger "GOOGLE SEARCH FOR WEBSITE KEYWORDS"
    sleep(2)
  end
 


  desc "website [NAME]", "scanning website channel"
  def website(name = "*")
    logger "GOOGLE SEARCH FOR WEBSITE KEYWORDS"
    logger "BING SEARCH FOR WEBSITE KEYWORDS"
    sleep(2)
  end
  

  desc "youtube ", "scanning youtube channel"
  def youtube(name = "*")
    logger "GET VIDEOS"
    logger "GET COMMENTS"
    logger "GET RECOMMENDATIONS"
    sleep(2)
  end
  

  desc "company ", "scanning youtube channel"
  def company(name = "*")
    facebook
    twitter
    youtube
    sleep(2)
  end
 


  desc "logger [VALUE]", "Logging stuff"
  def logger(s)
    puts " #{Time.now}  STATUS: #{s}"
  end
end