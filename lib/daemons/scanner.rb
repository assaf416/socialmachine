#!/usr/bin/env ruby

# You might want to change this
ENV["RAILS_ENV"] ||= "development"
require File.dirname(__FILE__) + "/../../config/application"
Rails.application.require_environment!

$running = true
Signal.trap("TERM") do 
  $running = false
end


def xlog(str)
  puts str
  $stdout.write str
  $stderr.write str
end


 xlog " STARTED DEAMON ..."
while($running) do
  xlog " RUNNING DEAMON ..."
  sleep 10
end


