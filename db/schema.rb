# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131008174601) do

  create_table "articles", :force => true do |t|
    t.string   "name"
    t.string   "kind"
    t.boolean  "draft"
    t.text     "body"
    t.string   "image"
    t.string   "audio"
    t.string   "video"
    t.string   "author"
    t.integer  "brand_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "category"
    t.string   "tags"
    t.string   "remote_movie_url"
    t.string   "short_url"
    t.string   "url"
    t.string   "cite"
    t.text     "abstract"
    t.integer  "views"
    t.integer  "bulk_mail_id"
    t.integer  "order_in_newsletter"
  end

  create_table "autoresponders", :force => true do |t|
    t.integer  "brand_id"
    t.string   "short_message"
    t.string   "name"
    t.string   "target_link"
    t.text     "long_message"
    t.integer  "sent"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "bookmarks", :force => true do |t|
    t.integer  "brand_id"
    t.string   "title"
    t.string   "url"
    t.string   "message"
    t.string   "tags"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "brands", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.text     "description"
    t.string   "facebook_token"
    t.string   "facebook_secret"
    t.string   "facebook_page"
    t.string   "twitter_token"
    t.string   "twitter_secret"
    t.string   "blogger_url"
    t.string   "blogger_rss"
    t.string   "wordpress_url"
    t.string   "wordpress_email"
    t.string   "word_press_rss"
    t.string   "reddit_rss"
    t.string   "stumble_rss"
    t.string   "delicious_rss"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.string   "image"
    t.string   "gmail_email"
    t.string   "gmail_pwd"
    t.string   "mailchimp_token"
    t.string   "mailchimp_secret"
    t.string   "google_token"
    t.string   "google_secret"
    t.string   "linkedin_token"
    t.string   "linkedin_secret"
    t.string   "foursquare_token"
    t.string   "foursquare_secret"
    t.string   "digg_rss"
    t.string   "youtube_url"
    t.string   "youtube_rss"
    t.string   "youtube_email"
    t.string   "tumblr_url"
    t.string   "tumblr_rss"
    t.string   "tumblr_email"
    t.string   "blogger_email"
    t.string   "twitter_search"
    t.string   "twitter_ignore"
    t.string   "rss_search"
    t.string   "rss_ignore"
    t.string   "twitter_user_id"
    t.string   "twitter_last_tweet"
    t.integer  "twitter_comment_count"
    t.integer  "twitter_posts_count"
    t.integer  "twitter_retweets_count"
    t.string   "twitter_image_url"
    t.string   "google_page"
    t.integer  "google_plus_fans_count"
    t.integer  "google_plus_follow_count"
    t.integer  "youtube_subscribers_count"
    t.integer  "youtube_views_count"
    t.string   "youtube_profile_picture"
    t.string   "forsquare_profile_picture"
    t.string   "forsquare_venue"
    t.integer  "forsquare_checkins_count"
    t.integer  "forsquare_users_count"
    t.integer  "forsquare_tip_count"
    t.integer  "forsquare_venue_address"
    t.string   "youtube_occupation"
    t.string   "youtube_image_url"
    t.string   "linkedin_pin"
    t.string   "reddit_user"
    t.string   "reddit_pwd"
    t.string   "phone"
    t.string   "fax"
    t.text     "address"
    t.string   "keywords"
    t.text     "email_signiture"
    t.text     "short_urls"
    t.string   "content_search"
    t.string   "prospect_search"
    t.string   "group_search"
    t.string   "competitor_search"
    t.string   "jobs_search"
    t.text     "email_template"
    t.text     "newsletter_template"
    t.text     "website_template"
    t.string   "job_locations"
    t.string   "competitors_locations"
    t.string   "prospects_locations"
    t.text     "json_prospects",            :limit => 2147483647
    t.string   "linkedin_url"
  end

  create_table "bulk_mails", :force => true do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.text     "header"
    t.text     "body"
    t.text     "footer"
    t.text     "sidebar"
    t.string   "top_image_url"
    t.string   "footer_image_url"
    t.string   "side_image_url"
    t.string   "body_image_url"
    t.integer  "page_id"
    t.boolean  "rtl"
    t.string   "unique_token"
    t.string   "side_type"
    t.string   "text_font_name"
    t.string   "text_font_size"
    t.string   "header_font_name"
    t.string   "header_font_size"
    t.string   "sidebar_bk_color"
    t.string   "body_bk_color"
    t.string   "top_bk_color"
    t.string   "footer_bk_color"
    t.string   "body_hd_color"
    t.string   "side_hd_color"
    t.string   "top_hd_color"
    t.string   "footer_hd_color"
    t.string   "body_fg_color"
    t.string   "side_fg_color"
    t.string   "top_fg_color"
    t.string   "footer_fg_color"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "kind"
    t.integer  "template_id"
    t.text     "fan_list"
  end

  add_index "bulk_mails", ["kind"], :name => "index_bulk_mails_on_kind"

  create_table "calls", :force => true do |t|
    t.text     "request"
    t.text     "params"
    t.string   "session"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "channels", :force => true do |t|
    t.integer  "brand_id"
    t.string   "kind"
    t.string   "name"
    t.string   "token"
    t.string   "secret"
    t.string   "uid"
    t.string   "profile_name"
    t.text     "profile_description"
    t.string   "profile_image_url"
    t.string   "profile_url"
    t.string   "email"
    t.string   "posts_feed"
    t.string   "comments_feed"
    t.boolean  "active"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "fans_count"
    t.integer  "views_count"
    t.integer  "followings_count"
    t.integer  "likes_count"
    t.integer  "dislikes_count"
    t.text     "auto_respond_message"
    t.string   "home_page_short_url"
    t.string   "search_1"
    t.string   "search_2"
    t.string   "search_3"
    t.string   "search_4"
    t.string   "search_5"
    t.text     "ignore_uids"
    t.datetime "scanned_at"
    t.datetime "marked_at"
    t.text     "ignore_ids"
    t.string   "pin"
  end

  create_table "colors", :force => true do |t|
    t.string   "name"
    t.string   "color_1"
    t.string   "color_2"
    t.string   "color_3"
    t.string   "color_4"
    t.string   "color_5"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "color_6"
    t.string   "color_7"
    t.string   "color_8"
    t.string   "color_9"
  end

  create_table "comments", :force => true do |t|
    t.integer  "article_id"
    t.text     "message"
    t.integer  "fan_id"
    t.string   "kind"
    t.boolean  "read"
    t.integer  "rank"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "email"
    t.string   "name"
    t.integer  "post_id"
    t.integer  "brand_id"
    t.string   "image_url"
    t.datetime "posted_at"
    t.integer  "competitor_id"
    t.integer  "channel_id"
    t.string   "uid"
    t.string   "from_uid"
    t.string   "post_uid"
    t.string   "target_link"
  end

  add_index "comments", ["channel_id"], :name => "index_comments_on_channel_id"
  add_index "comments", ["post_id"], :name => "index_comments_on_post_id"

  create_table "competitors", :force => true do |t|
    t.string   "name"
    t.string   "home_page"
    t.string   "facebook_page"
    t.string   "twtitter_name"
    t.string   "google_page"
    t.string   "linkedin_company_page"
    t.string   "wordpress_rss"
    t.string   "blogger_rss"
    t.string   "youtube_rss"
    t.string   "tumblr_rss"
    t.integer  "brand_id"
    t.string   "foursquare_profile_image"
    t.integer  "forsquare_checkins_count"
    t.integer  "forsquare_users_count"
    t.integer  "forsquare_tip_count"
    t.integer  "google_plus_fans_count"
    t.integer  "google_plus_follow_count"
    t.string   "youtube_profile_image"
    t.integer  "youtube_subscribers_count"
    t.integer  "youtube_views_count"
    t.string   "facebook_profile_image"
    t.integer  "facebook_likes_count"
    t.integer  "facebook_checkins_count"
    t.string   "facebook_category"
    t.string   "google_plus_profile_image"
    t.integer  "google_plus_comments_count"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "reddit_user"
    t.string   "image_url"
    t.string   "twitter_uid"
    t.integer  "facebook_shares_count"
    t.integer  "facebook_comments_count"
    t.text     "facebook_description"
    t.string   "facebook_fanpage_image_url"
    t.string   "email"
    t.string   "phone"
    t.string   "website"
    t.string   "location"
    t.integer  "facebook_share_count"
    t.text     "website_keywords"
    t.text     "score"
    t.integer  "twitter_followers"
    t.integer  "twitter_posts"
    t.integer  "twitter_retweets"
    t.string   "twitter_profile_image_url"
    t.integer  "twitter_following_count"
    t.text     "twitter_description"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "deployments", :force => true do |t|
    t.integer  "brand_id"
    t.integer  "article_id"
    t.text     "emails"
    t.string   "title"
    t.text     "body"
    t.text     "url"
    t.text     "comment"
    t.boolean  "sent"
    t.string   "category"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "email_listings", :force => true do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.boolean  "draft"
    t.integer  "template_id"
    t.text     "header"
    t.text     "body"
    t.datetime "sent_at"
    t.string   "image"
    t.string   "movie"
    t.text     "footer"
    t.boolean  "daily"
    t.boolean  "weekly"
    t.boolean  "monthly"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "emails", :force => true do |t|
    t.integer  "brand_id"
    t.integer  "fan_id"
    t.integer  "bulk_mail_id"
    t.string   "template_id"
    t.string   "uid"
    t.string   "kind"
    t.string   "status"
    t.string   "unique_token"
    t.string   "from_name"
    t.string   "from_email"
    t.string   "to_name"
    t.string   "to_email"
    t.text     "text_body"
    t.text     "html_body"
    t.datetime "sent_at"
    t.datetime "opened_at"
    t.datetime "clicked_at"
    t.datetime "unregistered_at"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "subject"
    t.datetime "send_on"
  end

  add_index "emails", ["bulk_mail_id", "kind"], :name => "index_emails_on_bulk_mail_id_and_kind"
  add_index "emails", ["bulk_mail_id", "status"], :name => "index_emails_on_bulk_mail_id_and_status"
  add_index "emails", ["clicked_at"], :name => "index_emails_on_clicked_at"
  add_index "emails", ["opened_at"], :name => "index_emails_on_opened_at"
  add_index "emails", ["sent_at"], :name => "index_emails_on_sent_at"
  add_index "emails", ["uid"], :name => "index_emails_on_uid"

  create_table "events", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "brand_id"
    t.date     "starts_on"
    t.string   "location"
    t.decimal  "price",             :precision => 10, :scale => 0
    t.string   "facebook_id"
    t.string   "eventfull_id"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.string   "image"
    t.integer  "fan_id"
    t.integer  "comming_count"
    t.integer  "not_comming_count"
    t.integer  "sent_count"
    t.boolean  "published"
    t.string   "currency"
  end

  create_table "facebook_pages", :force => true do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.string   "uid"
    t.string   "category"
    t.string   "image_url"
    t.integer  "posts_count"
    t.integer  "likes_count"
    t.integer  "checkins_count"
    t.integer  "statuses_count"
    t.text     "html_fans"
    t.text     "css"
    t.text     "html_public"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "fan_pages", :force => true do |t|
    t.string   "name"
    t.string   "uid"
    t.string   "url"
    t.string   "image_url"
    t.integer  "likes"
    t.integer  "comments"
    t.integer  "shares"
    t.integer  "commenters"
    t.integer  "views"
    t.integer  "posts"
    t.integer  "channel_id"
    t.integer  "brand_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "posted_ids"
  end

  create_table "fans", :force => true do |t|
    t.string   "name"
    t.string   "home_page"
    t.string   "uid"
    t.string   "friends"
    t.string   "followers"
    t.datetime "last_post"
    t.integer  "brand_id"
    t.string   "email"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "image_url"
    t.string   "channel"
    t.integer  "channel_id"
    t.text     "description"
    t.string   "kind"
    t.boolean  "converted"
    t.datetime "converted_at"
    t.datetime "last_scan_time"
    t.integer  "friends_count"
    t.integer  "comments_counter"
    t.string   "location"
    t.text     "bio"
    t.string   "group_tags"
    t.integer  "likes"
    t.integer  "talking_about"
    t.string   "website"
    t.datetime "processed_at"
    t.integer  "following"
    t.integer  "posts_count"
    t.datetime "invited_at"
    t.string   "screen_name"
    t.string   "facebook_uid"
    t.string   "twitter_uid"
    t.string   "google_plus_uid"
    t.string   "linkedin_uid"
    t.string   "foursquare_uid"
    t.string   "youtube_uid"
  end

  add_index "fans", ["brand_id", "channel", "uid"], :name => "index_fans_on_brand_id_and_channel_and_uid"
  add_index "fans", ["brand_id", "channel_id", "kind"], :name => "index_fans_on_brand_id_and_channel_id_and_kind"
  add_index "fans", ["channel_id", "uid"], :name => "index_fans_on_channel_id_and_uid"
  add_index "fans", ["uid"], :name => "index_fans_on_uid"

  create_table "feed_entries", :force => true do |t|
    t.integer  "feed_id"
    t.string   "title"
    t.string   "uid"
    t.text     "body"
    t.string   "url"
    t.datetime "posted_at"
    t.string   "image_url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "feeds", :force => true do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.string   "url"
    t.string   "keywords"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "icon"
    t.text     "clear_script"
    t.text     "scrape_script"
  end

  create_table "forum_posts", :force => true do |t|
    t.integer  "topic_id"
    t.integer  "brand_id"
    t.string   "title"
    t.text     "body"
    t.string   "image_url"
    t.string   "movie_url"
    t.integer  "parent_id"
    t.integer  "fan_id"
    t.string   "singniture"
    t.integer  "rating"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "groups", :force => true do |t|
    t.string   "title"
    t.string   "uid"
    t.text     "description"
    t.string   "owner"
    t.string   "channel"
    t.string   "owner_image_url"
    t.string   "image_url"
    t.integer  "members_count"
    t.integer  "posts_count"
    t.string   "url"
    t.string   "last_update"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "fan_id"
  end

  add_index "groups", ["uid"], :name => "index_groups_on_uid"

  create_table "inquieries", :force => true do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.string   "email"
    t.string   "captcha_token"
    t.text     "message"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "source"
    t.string   "token"
    t.date     "from_date"
    t.date     "to_date"
    t.integer  "product_id"
    t.string   "article_id"
    t.string   "request_url"
  end

  create_table "mentions", :force => true do |t|
    t.integer  "brand_id"
    t.integer  "competitor_id"
    t.string   "channel_name"
    t.text     "body"
    t.string   "author"
    t.string   "url"
    t.string   "user_image_url"
    t.string   "image_url"
    t.datetime "posted_at"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "messages", :force => true do |t|
    t.integer  "brand_id"
    t.string   "channel"
    t.string   "from"
    t.string   "to"
    t.text     "body"
    t.string   "kind"
    t.boolean  "read"
    t.string   "uid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "page_partials", :force => true do |t|
    t.integer  "page_id"
    t.integer  "row"
    t.integer  "col"
    t.string   "title"
    t.text     "body"
    t.string   "image"
    t.string   "img_width"
    t.string   "img_height"
    t.integer  "grid"
    t.boolean  "first"
    t.boolean  "visible"
    t.integer  "product_id"
    t.integer  "event_id"
    t.integer  "article_id"
    t.string   "kind"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "slide_style"
    t.string   "slide_1"
    t.string   "slide_2"
    t.string   "slide_3"
    t.string   "slide_4"
    t.string   "slide_5"
    t.string   "slide_1_header"
    t.text     "slide_1_body"
    t.string   "slide_2_header"
    t.text     "slide_2_body"
    t.string   "slide_3_header"
    t.text     "slide_3_body"
    t.string   "slide_4_header"
    t.text     "slide_4_body"
    t.string   "slide_5_header"
    t.text     "slide_5_body"
    t.boolean  "show_message"
    t.string   "twitter_search"
    t.boolean  "hero"
    t.integer  "link_to_page_id"
    t.boolean  "row_is_row_top"
    t.boolean  "row_is_alter"
    t.boolean  "row_is_footer"
    t.string   "style"
    t.string   "align"
  end

  add_index "page_partials", ["id"], :name => "index_page_partials_on_id"

  create_table "pages", :force => true do |t|
    t.integer  "website_id"
    t.integer  "template_id"
    t.string   "name"
    t.integer  "order_in_toolbar"
    t.boolean  "show_in_toolbar"
    t.string   "kind"
    t.text     "css"
    t.text     "header"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "view_count"
    t.string   "preview"
    t.string   "background_image_url"
    t.string   "background_image_style"
    t.boolean  "float_layout"
    t.integer  "brand_id"
  end

  create_table "posts", :force => true do |t|
    t.integer  "brand_id"
    t.string   "network"
    t.string   "uid"
    t.string   "kind"
    t.integer  "fan_id"
    t.string   "title"
    t.text     "body"
    t.string   "image_url"
    t.datetime "posted_at"
    t.string   "posted_by"
    t.string   "source_name"
    t.string   "from"
    t.string   "to"
    t.integer  "comments_count"
    t.integer  "rank"
    t.string   "target_link"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "competitor_id"
    t.integer  "channel_id"
    t.string   "from_uid"
    t.string   "from_name"
    t.string   "from_profile_image_url"
    t.integer  "from_friends_count"
    t.string   "from_description"
    t.integer  "from_followers_count"
    t.integer  "from_post_count"
    t.datetime "from_last_post_time"
    t.string   "from_website"
    t.string   "from_location"
    t.datetime "opened_at"
    t.string   "short_url"
  end

  add_index "posts", ["brand_id", "channel_id", "kind"], :name => "index_posts_on_brand_id_and_channel_id_and_kind"
  add_index "posts", ["brand_id", "channel_id", "posted_at"], :name => "index_posts_on_brand_id_and_channel_id_and_posted_at"
  add_index "posts", ["brand_id", "network", "uid"], :name => "index_posts_on_brand_id_and_network_and_uid"
  add_index "posts", ["brand_id", "uid"], :name => "index_posts_on_brand_id_and_uid"
  add_index "posts", ["channel_id", "posted_at"], :name => "index_posts_on_channel_id_and_posted_at"
  add_index "posts", ["channel_id", "uid"], :name => "index_posts_on_channel_id_and_uid"
  add_index "posts", ["channel_id"], :name => "index_posts_on_channel_id"
  add_index "posts", ["fan_id"], :name => "index_posts_on_fan_id"
  add_index "posts", ["id"], :name => "index_posts_on_id"
  add_index "posts", ["uid"], :name => "index_posts_on_uid"

  create_table "products", :force => true do |t|
    t.string   "name"
    t.decimal  "price",           :precision => 10, :scale => 0
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.text     "paypal"
    t.text     "google_checkout"
    t.string   "image"
    t.text     "description"
    t.integer  "qty"
    t.string   "category"
    t.integer  "brand_id"
    t.text     "notes"
    t.text     "abstract"
    t.string   "image_1"
    t.string   "image_2"
    t.string   "image_3"
    t.string   "image_4"
    t.string   "image_5"
    t.integer  "views"
    t.string   "currency"
  end

  create_table "scores", :force => true do |t|
    t.integer  "brand_id"
    t.integer  "channel_id"
    t.integer  "competitor_id"
    t.string   "kind"
    t.string   "profile_score"
    t.text     "profile_notes"
    t.integer  "content_score"
    t.integer  "content_posts_count"
    t.integer  "exposure_score"
    t.integer  "exposure_users_count"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "score_date"
    t.string   "uid"
    t.integer  "posts"
    t.integer  "likes"
    t.integer  "views"
    t.integer  "comments"
    t.integer  "commenters"
    t.integer  "fans"
    t.integer  "new_fans"
    t.integer  "no_longer_fans"
    t.integer  "groups"
    t.integer  "posts_in_groups"
    t.integer  "messages"
  end

  add_index "scores", ["brand_id"], :name => "index_scores_on_brand_id_and_score_date"
  add_index "scores", ["channel_id"], :name => "index_scores_on_channel_id_and_score_date"
  add_index "scores", ["competitor_id"], :name => "index_scores_on_competitor_id_and_score_date"

  create_table "slides", :force => true do |t|
    t.integer  "article_id"
    t.string   "name"
    t.integer  "order"
    t.text     "css"
    t.text     "body"
    t.string   "before_transition"
    t.string   "after_transition"
    t.string   "backgroud_image_url"
    t.string   "logo_url"
    t.string   "layout"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "templates", :force => true do |t|
    t.string   "name"
    t.string   "kind"
    t.text     "html"
    t.text     "css"
    t.text     "js"
    t.boolean  "free"
    t.decimal  "price",         :precision => 10, :scale => 0
    t.integer  "user_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.string   "page_1_name"
    t.text     "page_1_html"
    t.boolean  "page_1_show"
    t.string   "page_2_name"
    t.text     "page_2_html"
    t.boolean  "page_2_show"
    t.string   "page_3_name"
    t.text     "page_3_html"
    t.boolean  "page_3_show"
    t.string   "page_4_name"
    t.text     "page_4_html"
    t.boolean  "page_4_show"
    t.string   "page_5_name"
    t.text     "page_5_html"
    t.boolean  "page_5_show"
    t.string   "css_file_name"
    t.string   "color_1"
    t.string   "color_2"
    t.string   "color_3"
  end

  create_table "tickets", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "url"
    t.integer  "user_id"
    t.string   "status"
    t.string   "priority"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "topics", :force => true do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.text     "description"
    t.string   "image_url"
    t.string   "short_url"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "trackers", :force => true do |t|
    t.integer  "brand_id"
    t.integer  "channel_id"
    t.integer  "article_id"
    t.integer  "page_id"
    t.integer  "email_id"
    t.integer  "bulk_mail_id"
    t.string   "ip"
    t.integer  "visits"
    t.string   "country"
    t.string   "city"
    t.string   "address"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "os"
    t.string   "browser"
    t.string   "device"
    t.datetime "logged_at"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.boolean  "engaged"
    t.text     "original_request"
    t.string   "referrer"
    t.integer  "product_id"
    t.integer  "event_id"
  end

  add_index "trackers", ["article_id", "logged_at"], :name => "index_trackers_on_article_id_and_logged_at"
  add_index "trackers", ["channel_id", "logged_at"], :name => "index_trackers_on_channel_id_and_logged_at"
  add_index "trackers", ["email_id", "logged_at"], :name => "index_trackers_on_email_id_and_logged_at"
  add_index "trackers", ["ip", "logged_at"], :name => "index_trackers_on_ip_and_logged_at"

  create_table "tutorials", :force => true do |t|
    t.string   "name"
    t.string   "category"
    t.text     "abstract"
    t.text     "body"
    t.string   "movie_url"
    t.string   "cover_image_url"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "description"
    t.string   "ui"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "image"
    t.string   "phone"
    t.datetime "logged_in_at"
  end

  create_table "websites", :force => true do |t|
    t.integer  "brand_id"
    t.string   "url"
    t.integer  "template_id"
    t.text     "css"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.text     "google_analyitics_snippet"
    t.text     "keywords"
    t.text     "meta_description"
    t.boolean  "rtl"
    t.string   "text_color"
    t.string   "background_color"
    t.string   "heading_color"
    t.string   "background_image"
    t.string   "text_font_name"
    t.string   "header_font_name"
    t.integer  "text_size"
    t.integer  "h1_size"
    t.integer  "h2_size"
    t.integer  "h3_size"
    t.integer  "h4_size"
    t.integer  "h5_size"
    t.string   "toolbar_background_color"
    t.string   "toolbar_logo_image"
    t.string   "toolbar_text_color"
    t.integer  "toolbar_text_size"
    t.string   "hero_background_color"
    t.string   "hero_text_color"
    t.string   "hero_header_color"
    t.string   "hero_image_align"
    t.string   "toolbar_font_name"
    t.string   "logo_font_name"
    t.integer  "logo_font_size"
    t.integer  "temp_page_id"
    t.string   "footer_background_color"
    t.string   "footer_image_url"
    t.string   "heading_background_color"
    t.string   "heading_image_url"
    t.boolean  "fluid"
    t.boolean  "fixed_toolbar"
    t.text     "json_scan",                 :limit => 2147483647
  end

end
