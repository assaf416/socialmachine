class ClearFans < ActiveRecord::Migration
  def change
    remove_column(:fans,:facebook)
    remove_column(:fans,:twitter)
    remove_column(:fans,:linkedin)
    remove_column(:fans,:google)
    remove_column(:fans,:foursquares)
    remove_column(:fans,:movies_hash)
    remove_column(:fans,:posts_hash)
    remove_column(:fans,:pictures_hash)
    remove_column(:fans,:music_hash)
    remove_column(:fans,:events_hash)
    remove_column(:fans,:groups_hash)
  end
end
