class AddTemplatesToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :email_template, :text
    add_column :brands, :newsletter_template, :text
    add_column :brands, :website_template, :text
  end
end
