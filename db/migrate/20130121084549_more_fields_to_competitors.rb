class MoreFieldsToCompetitors < ActiveRecord::Migration
  def change
    
    add_column :competitors ,:facebook_shares_count ,:integer
    add_column :competitors ,:facebook_comments_count ,:integer
    add_column :competitors ,:facebook_description ,:text
    add_column :competitors ,:facebook_fanpage_image_url ,:string
    add_column :competitors ,:email ,:string
    add_column :competitors ,:phone ,:string
    add_column :competitors ,:website ,:string
    add_column :competitors ,:location ,:string
    add_column :competitors ,:facebook_share_count ,:integer
    add_column :competitors ,:website_keywords ,:text
    
    add_column :competitors ,:score ,:text
    add_column :competitors ,:twitter_followers ,:integer
    add_column :competitors ,:twitter_posts ,:integer
    add_column :competitors ,:twitter_retweets ,:integer
    add_column :competitors ,:twitter_description ,:integer
    add_column :competitors ,:twitter_profile_image_url ,:string
  end
end
