class AddJsonProspectsToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :json_prospects,  :text, :limit => 4294967295
  end
end
