class CreateMentions < ActiveRecord::Migration
  def change
    create_table :mentions do |t|
      t.integer :brand_id
      t.integer :competitor_id
      t.string :channel_name
      t.text :body
      t.string :author
      t.string :url
      t.string :user_image_url
      t.string :image_url
      t.datetime :posted_at

      t.timestamps
    end
  end
end
