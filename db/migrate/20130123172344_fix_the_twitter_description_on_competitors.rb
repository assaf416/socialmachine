class FixTheTwitterDescriptionOnCompetitors < ActiveRecord::Migration
  def change
    remove_column :competitors, :twitter_description 
    add_column :competitors, :twitter_description , :text
  end
end
