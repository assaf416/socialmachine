class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.integer :article_id
      t.string :name
      t.integer :order
      t.text :css
      t.text :body
      t.string :before_transition
      t.string :after_transition
      t.string :backgroud_image_url
      t.string :logo_url
      t.string :layout

      t.timestamps
    end
  end
end
