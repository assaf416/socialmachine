class AddFollowingCountToFans < ActiveRecord::Migration
  def change
    add_column :fans, :following, :integer

    add_column :fans, :posts_count, :integer

  end
end
