class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :brand_id
      t.string :channel
      t.string :from
      t.string :to
      t.text :body
      t.string :kind
      t.boolean :read
      t.string :uid

      t.timestamps
    end
  end
end
