class AddAccountUidToScores < ActiveRecord::Migration
  def change
    add_column :scores, :uid, :string
    add_column :scores, :posts, :integer
    add_column :scores, :likes, :integer
    add_column :scores, :views, :integer
    add_column :scores, :comments, :integer
    add_column :scores, :commenters, :integer
    add_column :scores, :fans, :integer
    add_column :scores, :new_fans, :integer
    add_column :scores, :no_longer_fans, :integer
    add_column :scores, :groups, :integer
    add_column :scores, :posts_in_groups, :integer
    remove_column :scores, :engadge_score, :engadge_users_count,:engadge_posts_count
  end
end
