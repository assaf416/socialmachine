class CreatePagePartials < ActiveRecord::Migration
  def change
    create_table :page_partials do |t|
      t.integer :page_id
      t.integer :row
      t.integer :col
      t.string :title
      t.text :body
      t.string :image
      t.string :img_width
      t.string :img_height
      t.integer :grid
      t.boolean :first
      t.boolean :visible
      t.integer :product_id
      t.integer :event_id
      t.integer :article_id
      t.string :kind

      t.timestamps
    end
  end
end
