class AddBrandIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :brand_id, :integer
    add_column :comments, :network, :string
    add_column :comments, :image_url, :string
#    add_column :comments, :uid, :string
    add_column :comments, :posted_at, :datetime

  end
end
