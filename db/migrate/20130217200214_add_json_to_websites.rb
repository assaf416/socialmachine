class AddJsonToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :json_scan, :text
  end
end
