class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :article_id
      t.text :message
      t.integer :fan_id
      t.string :channel
      t.string :kind
      t.boolean :read
      t.integer :rank

      t.timestamps
    end
  end
end
