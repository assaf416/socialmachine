class CreateForumPosts < ActiveRecord::Migration
  def change
    create_table :forum_posts do |t|
      t.integer :topic_id
      t.integer :brand_id
      t.string :title
      t.text :body
      t.string :image_url
      t.string :movie_url
      t.integer :parent_id
      t.integer :fan_id
      t.string :singniture
      t.integer :rating

      t.timestamps
    end
  end
end
