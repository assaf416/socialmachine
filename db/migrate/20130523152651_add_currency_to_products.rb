class AddCurrencyToProducts < ActiveRecord::Migration
  def change
    add_column :products, :currency, :string
    add_column :events, :currency, :string
    add_column :trackers, :product_id, :integer
    add_column :trackers, :event_id, :integer
  end
end
