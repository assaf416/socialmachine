class AddTwitterFieldsToBrands < ActiveRecord::Migration
  def change
     add_column :brands, :twitter_last_tweet, :string
     add_column :brands, :twitter_comment_count, :integer
     add_column :brands, :twitter_posts_count, :integer
     add_column :brands, :twitter_retweets_count, :integer
     add_column :brands, :twitter_image_url, :string
     add_column :brands, :google_page, :string
#     add_column :competitors, :google_page, :string
  end
end
