class AddMoreFieldsToWebsite < ActiveRecord::Migration
  def change
    add_column :websites, :toolbar_font_name, :string

    add_column :websites, :logo_font_name, :string

    add_column :websites, :logo_font_size, :integer

  end
end
