class AddCompetitorLocationsToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :competitors_locations, :string
    add_column :brands, :prospects_locations, :string
  end
end
