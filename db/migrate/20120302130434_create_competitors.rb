class CreateCompetitors < ActiveRecord::Migration
  def change
    create_table :competitors do |t|
      t.string   "name"
      t.string   "home_page"
      t.string   "facebook_page"
      t.string   "twtitter_name"
      t.string   "google_page"
      t.string   "linkedin_company_page"
      t.string   "wordpress_rss"
      t.string   "blogger_rss"
      t.string   "youtube_rss"
      t.string   "tumblr_rss"
      t.integer  "brand_id"
      t.string   "foursquare_profile_image"
      t.integer  "forsquare_checkins_count"
      t.integer  "forsquare_users_count"
      t.integer  "forsquare_tip_count"
      t.integer  "google_plus_fans_count"
      t.integer  "google_plus_follow_count"
      t.string   "youtube_profile_image"
      t.integer  "youtube_subscribers_count"
      t.integer  "youtube_views_count"
      t.string   "facebook_profile_image"
      t.integer  "facebook_likes_count"
      t.integer  "facebook_checkins_count"
      t.string   "facebook_category"
      t.string   "google_plus_profile_image"
      t.integer  "google_plus_comments_count"

      t.timestamps
    end
  end
end
