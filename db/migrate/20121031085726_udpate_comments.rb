class UdpateComments < ActiveRecord::Migration
  def change
    add_column :comments,  :from_uid, :string
    add_column :comments,  :post_uid, :string
    remove_column(:comments, :network)
    remove_column(:comments, :channel)
  end
end
