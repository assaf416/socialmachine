class CreateTrackers < ActiveRecord::Migration
  def change
    create_table :trackers do |t|
      t.integer :brand_id
      t.integer :channel_id
      t.integer :article_id
      t.integer :page_id
      t.integer :email_id
      t.integer :bulk_mail_id
      t.string :ip
      t.integer :visits
      t.string :country
      t.string :city
      t.string :address
      t.string :latitude
      t.string :longitude
      t.string :os
      t.string :browser
      t.string :device
      t.datetime :logged_at

      t.timestamps
    end
  end
end
