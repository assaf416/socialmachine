class CreateFeedEntries < ActiveRecord::Migration
  def change
    create_table :feed_entries do |t|
      t.integer :feed_id
      t.string :title , :uid
      t.text :body
      t.string :url
      t.datetime :posted_at
      t.string :image_url
      t.timestamps
    end
  end
end
