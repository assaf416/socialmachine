class AddRowsInfoToPagePartials < ActiveRecord::Migration
  def change
    add_column :page_partials, :row_is_row_top, :boolean
    add_column :page_partials, :row_is_alter, :boolean
    add_column :page_partials, :row_is_footer, :boolean
  end
end
