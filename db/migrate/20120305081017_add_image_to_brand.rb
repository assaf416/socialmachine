class AddImageToBrand < ActiveRecord::Migration
  def change
    add_column :brands, :image, :string
    add_column :brands, :gmail_email, :string
    add_column :brands, :gmail_pwd, :string
    add_column :brands, :mailchimp_token, :string
    add_column :brands, :mailchimp_secret, :string
  end
end
