class AddFieldToPagePartials < ActiveRecord::Migration
  def change
    add_column :page_partials, :style, :string

    add_column :page_partials, :align, :string

  end
end
