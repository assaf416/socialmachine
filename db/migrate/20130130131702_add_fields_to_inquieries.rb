class AddFieldsToInquieries < ActiveRecord::Migration
  def change
    add_column :inquieries, :from_date, :date

    add_column :inquieries, :to_date, :date

    add_column :inquieries, :product_id, :integer

  end
end
