class AddIndexToPosts < ActiveRecord::Migration
  def change
     add_index "posts", ["brand_id", "channel_id", "posted_at"]
     add_index "posts", [ "channel_id", "posted_at"]
  end
end
