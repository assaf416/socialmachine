class AddDescriptionToFans < ActiveRecord::Migration
  def change
    add_column :fans, :description, :text
    add_column :fans, :kind, :string
    add_column :fans, :converted, :boolean
    add_column :fans, :converted_at, :datetime
  end
end
