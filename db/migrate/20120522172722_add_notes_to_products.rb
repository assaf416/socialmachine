class AddNotesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :notes, :text

    add_column :products, :abstract, :text

  end
end
