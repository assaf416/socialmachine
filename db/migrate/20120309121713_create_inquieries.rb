class CreateInquieries < ActiveRecord::Migration
  def change
    create_table :inquieries do |t|
      t.integer :brand_id
      t.string :name
      t.string :email
      t.string :captcha_token
      t.text :message

      t.timestamps
    end
  end
end
