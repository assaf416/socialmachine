class CreateTutorials < ActiveRecord::Migration
  def change
    create_table :tutorials do |t|
      t.string :name
      t.string :category
      t.text :abstract
      t.text :body
      t.string :movie_url
      t.string :cover_image_url

      t.timestamps
    end
  end
end
