class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.integer :brand_id
      t.string :name
      t.text :description
      t.string :image_url
      t.string :short_url

      t.timestamps
    end
  end
end
