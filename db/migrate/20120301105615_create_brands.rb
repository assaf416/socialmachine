class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name
      t.integer :user_id
      t.text :description
      t.string :facebook_token
      t.string :facebook_secret
      t.string :facebook_page
      t.string :twitter_token
      t.string :twitter_secret
      t.string :blogger_url
      t.string :blogger_posting_email
      t.string :blogger_rss
      t.string :wordpress_url
      t.string :wordpress_email
      t.string :word_press_rss
      t.string :reddit_rss
      t.string :stumble_rss
      t.string :delicious_rss

      t.timestamps
    end
  end
end
