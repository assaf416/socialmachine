class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :name
      t.string :kind
      t.boolean :draft
      t.text :body
      t.string :image
      t.string :audio
      t.string :video
      t.string :author
      t.integer :brand_id

      t.timestamps
    end
  end
end
