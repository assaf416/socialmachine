class MoreToFix < ActiveRecord::Migration
  def change
    add_column :brands ,:youtube_occupation, :string
    add_column :brands ,:youtube_image_url, :string
    add_index :posts, [:brand_id,:network,:uid]  
    add_index :posts, [:brand_id,:uid]  
  end
end
