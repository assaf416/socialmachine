class ChangeScoreDate < ActiveRecord::Migration
  def change
    remove_column(:scores, :score_date)
    add_column(:scores, :score_date,:string)
  end
end
