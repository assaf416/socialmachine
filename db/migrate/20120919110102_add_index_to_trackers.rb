class AddIndexToTrackers < ActiveRecord::Migration
  def change
    add_index :trackers , [:ip ,:logged_at]
    add_index :trackers , [:channel_id ,:logged_at]
    add_index :trackers , [:article_id ,:logged_at]
    add_index :trackers , [:email_id ,:logged_at]
  end
end
