class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :title
      t.text :description
      t.string :url
      t.integer :user_id
      t.string :status
      t.string :priority

      t.timestamps
    end
  end
end
