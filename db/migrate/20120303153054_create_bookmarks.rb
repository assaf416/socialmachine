class CreateBookmarks < ActiveRecord::Migration
  def change
    create_table :bookmarks do |t|
      t.integer :brand_id
      t.string :title
      t.string :url
      t.string :message
      t.string :tags

      t.timestamps
    end
  end
end
