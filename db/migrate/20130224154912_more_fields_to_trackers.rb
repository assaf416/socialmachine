class MoreFieldsToTrackers < ActiveRecord::Migration
  def up
    add_column :trackers, :original_request , :text
    add_column :trackers, :referrer , :string
  end

  def down
  end
end
