class CreateWebsites < ActiveRecord::Migration
  def change
    create_table :websites do |t|
      t.integer :brand_id
      t.string :url
      t.integer :template_id
      t.text :css

      t.timestamps
    end
  end
end
