class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :brand_id
      t.string :network,:uid
      t.string :kind
      t.integer :fan_id
      t.string :title
      t.text :body
      t.string :image_url
      t.datetime :posted_at
      t.string :posted_by
      t.string :source_name
      t.string :from
      t.string :to
      t.integer :comments_count
      t.integer :rank
      t.string :target_link

      t.timestamps
    end
    add_column :comments ,:post_id, :integer
    add_column :comments ,:uid, :integer
  end
end
