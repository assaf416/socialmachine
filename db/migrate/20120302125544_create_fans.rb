class CreateFans < ActiveRecord::Migration
  def change
    create_table :fans do |t|
      t.string :name
      t.string :home_page
      t.string :uid
      t.string :friends
      t.string :followers
      t.datetime :last_post
      t.integer :brand_id
      t.boolean :facebook
      t.boolean :twitter
      t.boolean :linkedin
      t.boolean :google
      t.boolean :foursquares
      t.string :email

      t.timestamps
    end
  end
end
