class AddMessagesToScores < ActiveRecord::Migration
  def change
    add_column :scores, :messages, :integer
  end
end
