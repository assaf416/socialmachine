class CreateAutoresponders < ActiveRecord::Migration
  def change
    create_table :autoresponders do |t|
      t.integer :brand_id
      t.string :short_message,:name, :target_link
      t.text :long_message
      t.integer :sent

      t.timestamps
    end
  end
end
