class CommentsUidShouldBeString < ActiveRecord::Migration
  def change
    remove_column :comments, :uid 
    add_column :comments, :uid , :string
  end
end
