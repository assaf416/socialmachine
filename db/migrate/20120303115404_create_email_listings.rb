class CreateEmailListings < ActiveRecord::Migration
  def change
    create_table :email_listings do |t|
      t.integer :brand_id
      t.string :name
      t.boolean :draft
      t.integer :template_id
      t.text :header
      t.text :body
      t.datetime :sent_at
      t.string :image
      t.string :movie
      t.text :footer
      t.text :header
      t.boolean :daily
      t.boolean :weekly
      t.boolean :monthly
      t.timestamps
    end
  end
end
