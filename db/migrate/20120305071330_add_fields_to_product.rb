class AddFieldsToProduct < ActiveRecord::Migration
  def change
    add_column :products, :description, :text

    add_column :products, :qty, :integer

    add_column :products, :category, :string

  end
end
