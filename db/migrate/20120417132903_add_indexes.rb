class AddIndexes < ActiveRecord::Migration
  def change
    add_index(:page_partials, :id)
    add_index(:posts, :id)
    add_index(:posts, :uid)
    add_index(:posts, [:channel_id, :uid])
    add_index(:fans, [:channel_id, :uid])
  end
end
