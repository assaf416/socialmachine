class CreateColors < ActiveRecord::Migration
  def change
    create_table :colors do |t|
      t.string :name
      t.string :color_1
      t.string :color_2
      t.string :color_3
      t.string :color_4
      t.string :color_5

      t.timestamps
    end
  end
end
