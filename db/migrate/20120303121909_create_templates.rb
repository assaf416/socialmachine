class CreateTemplates < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.string :name
      t.string :kind
      t.text :html
      t.text :css
      t.text :js
      t.boolean :free
      t.decimal :price
      t.integer :user_id

      t.timestamps
    end
  end
end
