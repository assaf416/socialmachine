
class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.integer :brand_id
      t.date :starts_on
      t.string :location
      t.decimal :price
      t.string :facebook_id
      t.string :eventfull_id

      t.timestamps
    end
  end
end
