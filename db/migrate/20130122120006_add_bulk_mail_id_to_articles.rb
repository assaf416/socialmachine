class AddBulkMailIdToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :bulk_mail_id, :integer
    add_column :articles, :order_in_newsletter, :integer
  end
end
