class AddMoreFieldsToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :footer_background_color, :string

    add_column :websites, :footer_image_url, :string

    add_column :websites, :heading_background_color, :string

    add_column :websites, :heading_image_url, :string

    add_column :websites, :fluid, :boolean

    add_column :websites, :fixed_toolbar, :boolean

  end
end
