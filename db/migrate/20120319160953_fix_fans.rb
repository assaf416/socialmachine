class FixFans < ActiveRecord::Migration
  def change
    add_column :fans, :channel ,:string
    add_index :fans, [:brand_id,:channel,:uid]
    
    # youtube
    add_column :brands , :youtube_subscribers_count, :integer
    add_column :brands , :youtube_views_count, :integer
    add_column :brands , :youtube_profile_picture, :string

    # foursquare
    add_column :brands , :forsquare_profile_picture, :string
    add_column :brands , :forsquare_venue, :string
    add_column :brands , :forsquare_checkins_count, :integer
    add_column :brands , :forsquare_users_count, :integer
    add_column :brands , :forsquare_tip_count, :integer
    add_column :brands , :forsquare_venue_address, :integer
  end
end
