class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.integer :brand_id
      t.integer :channel_id
      t.integer :competitor_id
      t.string  :kind
      t.date    :score_date
      t.string  :profile_score
      t.text    :profile_notes
      t.integer :content_score
      t.integer :content_posts_count
      t.integer :exposure_score
      t.integer :exposure_users_count
      t.integer :engadge_score
      t.integer :engadge_users_count
      t.integer :engadge_posts_count
      
      t.timestamps
    end
    add_index :scores ,[:brand_id, :score_date]
    add_index :scores ,[:competitor_id, :score_date]
    add_index :scores ,[:channel_id, :score_date]
  end
end
