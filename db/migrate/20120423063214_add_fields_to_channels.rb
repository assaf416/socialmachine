class AddFieldsToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :auto_respond_message, :text

    add_column :channels, :home_page_short_url, :string

  end
end
