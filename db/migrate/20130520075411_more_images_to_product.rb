class MoreImagesToProduct < ActiveRecord::Migration
  def change
    add_column :products, :image_1, :string
    add_column :products, :image_2, :string
    add_column :products, :image_3, :string
    add_column :products, :image_4, :string
    add_column :products, :image_5, :string
  end
end
