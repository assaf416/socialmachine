class AddColorsToTemplates < ActiveRecord::Migration
  def change
    add_column :templates, :color_1, :string

    add_column :templates, :color_2, :string

    add_column :templates, :color_3, :string

  end
end
