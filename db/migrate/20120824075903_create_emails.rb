class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.integer :brand_id
      t.integer :fan_id
      t.integer :bulk_mail_id
      t.string :template_id
      t.string :uid
      t.string :kind
      t.string :status
      t.string :unique_token
      
      t.string :from_name
      t.string :from_email
      t.string :to_name
      t.string :to_email
      t.text :text_body
      t.text :html_body
      
      t.datetime :sent_at
      t.datetime :opened_at
      t.datetime :clicked_at
      t.datetime :unregistered_at
      

      t.timestamps
    end
    add_column :bulk_mails , :kind , :string
    add_index :bulk_mails , :kind 
    add_index :emails , [:bulk_mail_id,:kind ]
    add_index :emails , :uid 
    add_index :emails , [:bulk_mail_id,:status ]
    add_index :emails , :sent_at 
    add_index :emails , :opened_at 
    add_index :emails , :clicked_at 
  end
end
