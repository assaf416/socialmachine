class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.integer :brand_id
      t.string :name
      t.string :url
      t.string :keywords

      t.timestamps
    end
  end
end
