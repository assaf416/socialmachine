class AddTwitterSearchToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :twitter_search, :string
    add_column :brands, :twitter_ignore, :string
    add_column :brands, :rss_search, :string
    add_column :brands, :rss_ignore, :string
  end
end
