class AddRedditSupport < ActiveRecord::Migration
  def change
    add_column :brands ,:reddit_user, :string
    add_column :brands ,:reddit_pwd, :string
    add_column :competitors ,:reddit_user, :string
  end
end
