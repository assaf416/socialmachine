class CreateCalls < ActiveRecord::Migration
  def change
    create_table :calls do |t|
      t.text :request
      t.text :params
      t.string :session
      t.timestamps
    end
  end
end
