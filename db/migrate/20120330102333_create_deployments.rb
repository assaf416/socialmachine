class CreateDeployments < ActiveRecord::Migration
  def change
    create_table :deployments do |t|
      t.integer :brand_id
      t.integer :article_id
      t.text :emails
      t.string :title
      t.text :body
      t.text :url
      t.text :comment
      t.boolean :sent
      t.string :category

      t.timestamps
    end
  end
end
