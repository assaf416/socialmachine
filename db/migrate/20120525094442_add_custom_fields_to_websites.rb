class AddCustomFieldsToWebsites < ActiveRecord::Migration
  def change
  	# BG Colors and defult Text



    add_column :websites, :text_color, :string
    add_column :websites, :background_color, :string
    add_column :websites, :heading_color, :string
    add_column :websites, :background_image, :string
    add_column :websites, :text_font_name, :string
    add_column :websites, :header_font_name, :string
    add_column :websites, :text_size, :integer

    # Headers
    add_column :websites, :h1_size, :integer
    add_column :websites, :h2_size, :integer
    add_column :websites, :h3_size, :integer
    add_column :websites, :h4_size, :integer
    add_column :websites, :h5_size, :integer

    # Toolbar
    add_column :websites, :toolbar_background_color, :string
    add_column :websites, :toolbar_logo_image, :string
    add_column :websites, :toolbar_text_color, :string
    add_column :websites, :toolbar_text_size, :integer

    # Hero
	add_column :websites, :hero_background_color, :string
	add_column :websites, :hero_text_color, :string
	add_column :websites, :hero_header_color, :string
	add_column :websites, :hero_image_align, :string
  end
end
