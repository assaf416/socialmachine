class AddCategoryToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :category, :string
    add_column :articles, :tags, :string
  end
end
