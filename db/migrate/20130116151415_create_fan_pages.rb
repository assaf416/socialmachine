class CreateFanPages < ActiveRecord::Migration
  def change
    create_table :fan_pages do |t|
      t.string :name
      t.string :uid
      t.string :url
      t.string :image_url
      t.integer :likes
      t.integer :comments
      t.integer :shares
      t.integer :commenters
      t.integer :views
      t.integer :posts
      t.integer :channel_id
      t.integer :brand_id
      t.timestamps
    end
  end
end
