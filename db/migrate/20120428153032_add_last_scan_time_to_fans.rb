class AddLastScanTimeToFans < ActiveRecord::Migration
  def change
    add_column :fans, :last_scan_time, :datetime
    add_column :fans, :friends_count, :integer
    add_column :fans, :movies_hash, :text
    add_column :fans, :posts_hash, :text
    add_column :fans, :pictures_hash, :text
    add_column :fans, :music_hash, :text
    add_column :fans, :events_hash, :text
    add_column :fans, :groups_hash, :text
  end
end
