class AddThsesFieldsToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :from_uid, :string
    add_column :posts, :from_name, :string
    add_column :posts, :from_profile_image_url, :string
    add_column :posts, :from_friends_count, :integer
    add_column :posts, :from_description, :string
    add_column :posts, :from_followers_count, :integer
    add_column :posts, :from_post_count, :integer
    add_column :posts, :from_last_post_time, :datetime
    add_column :posts, :from_website, :string
    add_column :posts, :from_location, :string
  end
end
