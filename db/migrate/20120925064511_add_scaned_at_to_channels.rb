class AddScanedAtToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :scanned_at, :datetime
    add_column :channels, :marked_at, :datetime
  end
end
