class AddMoreFields < ActiveRecord::Migration
  def change
    add_column  :products , :image ,:string
    add_column  :events , :image ,:string
  end
end
