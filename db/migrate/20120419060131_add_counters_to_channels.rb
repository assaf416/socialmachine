class AddCountersToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :fans_count, :integer
    add_column :channels, :views_count, :integer
    add_column :channels, :followings_count, :integer
    add_column :channels, :likes_count, :integer
    add_column :channels, :dislikes_count, :integer
  end
end
