class AddFieldsToFeeds < ActiveRecord::Migration
  def change
    add_column :feeds, :clear_script, :text

    add_column :feeds, :scrape_script, :text

  end
end
