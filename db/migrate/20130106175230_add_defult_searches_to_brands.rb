class AddDefultSearchesToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :content_search, :string

    add_column :brands, :prospect_search, :string

    add_column :brands, :group_search, :string

    add_column :brands, :competitor_search, :string

  end
end
