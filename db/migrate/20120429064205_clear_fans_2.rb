class ClearFans2 < ActiveRecord::Migration
  def change
    add_column :fans, :comments_counter , :integer
    add_column :fans, :location , :string
    add_column :fans, :bio , :text
  end
end
