class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :title, :uid
      t.text :description
      t.string :owner
      t.string :channel
      t.string :owner_image_url
      t.string :image_url
      t.integer :members_count
      t.integer :posts_count
      t.string :url
      t.string :last_update
      t.timestamps
    end
    add_index :groups , :uid
  end
end
