class AddFieldsAgainToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :phone, :string

    add_column :brands, :fax, :string

    add_column :brands, :address, :text

  end
end
