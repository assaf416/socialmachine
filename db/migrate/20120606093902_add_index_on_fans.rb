class AddIndexOnFans < ActiveRecord::Migration
  def change
  	add_index "fans"	,  "uid"
  	add_index "posts"	,  "fan_id"
  	add_index "posts"	,  "channel_id"
	add_index "comments",  "post_id"
	add_index "comments",  "channel_id"
	
  end
end
