class AddBackgroundImageToPages < ActiveRecord::Migration
  def change
    add_column :pages, :background_image_url, :string

  end
end
