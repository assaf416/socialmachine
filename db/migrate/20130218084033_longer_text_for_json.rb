class LongerTextForJson < ActiveRecord::Migration
  def change
    change_column :websites, :json_scan, :text, :limit => 4294967295
  end
end
