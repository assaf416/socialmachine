class MoreSocialNetworksForBrands < ActiveRecord::Migration
  def change
    add_column :brands, :google_token, :string
    add_column :brands, :google_secret, :string
    add_column :brands, :linkedin_token, :string
    add_column :brands, :linkedin_secret, :string
    add_column :brands, :foursquare_token, :string
    add_column :brands, :foursquare_secret, :string
    
    add_column :brands, :digg_rss, :string
    
    add_column :brands ,:youtube_url, :string
    add_column :brands ,:youtube_rss, :string
    add_column :brands ,:youtube_email, :string
    
    add_column :brands ,:tumblr_url, :string
    add_column :brands ,:tumblr_rss, :string
    add_column :brands ,:tumblr_email, :string
    add_column :brands ,:blogger_email, :string
    
    
    remove_column :brands, :blogger_posting_email
  end
end
