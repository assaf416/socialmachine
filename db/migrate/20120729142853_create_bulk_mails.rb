class CreateBulkMails < ActiveRecord::Migration
  def change
    create_table :bulk_mails do |t|
      t.integer :brand_id
      t.string :name
      t.text :header, :body, :footer,:sidebar
      t.string :top_image_url
      t.string :footer_image_url
      t.string :side_image_url
      t.string :body_image_url
      t.integer :page_id
      t.boolean :rtl
      t.string :unique_token
      t.string :side_type # left , right ,none
      t.string :text_font_name
      t.string :text_font_size
      t.string :header_font_name
      t.string :header_font_size
      t.string :sidebar_bk_color
      t.string :body_bk_color
      t.string :top_bk_color
      t.string :footer_bk_color
      t.string :body_hd_color
      t.string :side_hd_color
      t.string :top_hd_color
      t.string :footer_hd_color
      t.string :body_fg_color
      t.string :side_fg_color
      t.string :top_fg_color
      t.string :footer_fg_color

      t.timestamps
    end
  end
end
