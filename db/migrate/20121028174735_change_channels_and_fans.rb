class ChangeChannelsAndFans < ActiveRecord::Migration
  def change
    add_column :fans, :screen_name , :string
    add_column :channels, :ignore_ids , :text
  end
end
