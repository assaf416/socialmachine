class AddFanToEvents < ActiveRecord::Migration
  def change
    add_column :events, :fan_id, :integer
    add_column :events, :comming_count, :integer
    add_column :events, :not_comming_count, :integer
    add_column :events, :sent_count, :integer
    add_column :groups, :fan_id, :integer

  end
end
