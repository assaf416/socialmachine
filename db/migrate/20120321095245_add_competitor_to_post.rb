class AddCompetitorToPost < ActiveRecord::Migration
  def change
    add_column :posts , :competitor_id, :integer
    add_column :comments , :competitor_id, :integer
  end
end

