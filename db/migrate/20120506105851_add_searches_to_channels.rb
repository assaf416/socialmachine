class AddSearchesToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :search_1, :string

    add_column :channels, :search_2, :string

    add_column :channels, :search_3, :string

    add_column :channels, :search_4, :string

    add_column :channels, :search_5, :string

  end
end
