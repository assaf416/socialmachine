class MoreFansAndPostsIndexes < ActiveRecord::Migration
 def change
    add_index "fans", ["brand_id", "channel_id", "kind"]
    add_index "posts", ["brand_id", "channel_id", "kind"]
  end
end
