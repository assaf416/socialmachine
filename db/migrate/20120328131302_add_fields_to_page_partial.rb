class AddFieldsToPagePartial < ActiveRecord::Migration
  def change
    
    
    add_column :page_partials, :slide_style , :string
    add_column :page_partials, :slide_1 , :string
    add_column :page_partials, :slide_2 , :string
    add_column :page_partials, :slide_3 , :string
    add_column :page_partials, :slide_4 , :string
    add_column :page_partials, :slide_5 , :string

    
    add_column :page_partials, :slide_1_header , :string
    add_column :page_partials, :slide_1_body , :text
    add_column :page_partials, :slide_2_header , :string
    add_column :page_partials, :slide_2_body , :text
    add_column :page_partials, :slide_3_header , :string
    add_column :page_partials, :slide_3_body , :text
    add_column :page_partials, :slide_4_header , :string
    add_column :page_partials, :slide_4_body , :text
    add_column :page_partials, :slide_5_header , :string
    add_column :page_partials, :slide_5_body , :text

    add_column :page_partials, :show_message , :boolean
    add_column :page_partials, :twitter_search , :string

    
  end
end
