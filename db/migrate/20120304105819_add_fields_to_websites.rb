class AddFieldsToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :google_analyitics_snippet, :text
    add_column :websites, :keywords, :text
    add_column :websites, :meta_description, :text

    add_column :products, :paypal , :text
    add_column :products, :google_checkout , :text

    add_column :templates, :page_1_name , :string
    add_column :templates, :page_1_html , :text
    add_column :templates, :page_1_show , :boolean
    
    add_column :templates, :page_2_name , :string
    add_column :templates, :page_2_html , :text
    add_column :templates, :page_2_show , :boolean
    
    add_column :templates, :page_3_name , :string
    add_column :templates, :page_3_html , :text
    add_column :templates, :page_3_show , :boolean
    
    add_column :templates, :page_4_name , :string
    add_column :templates, :page_4_html , :text
    add_column :templates, :page_4_show , :boolean
    
    
    add_column :templates, :page_5_name , :string
    add_column :templates, :page_5_html , :text
    add_column :templates, :page_5_show , :boolean

    
  end
end
