class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.integer :brand_id
      t.string :kind
      t.string :name
      t.string :token
      t.string :secret
      t.string :uid
      t.string :profile_name
      t.text :profile_description
      t.string :profile_image_url
      t.string :profile_url
      t.string :email
      t.string :posts_feed
      t.string :comments_feed
      t.boolean :active

      t.timestamps
    end
    add_column :posts, :channel_id, :integer
    add_column :comments, :channel_id, :integer
    add_column :fans, :channel_id, :integer
    
  end
end
