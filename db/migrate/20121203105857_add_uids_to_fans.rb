class AddUidsToFans < ActiveRecord::Migration
  def change
    add_column :fans, :facebook_uid, :string

    add_column :fans, :twitter_uid, :string

    add_column :fans, :google_plus_uid, :string

    add_column :fans, :linkedin_uid, :string

    add_column :fans, :foursquare_uid, :string

    add_column :fans, :youtube_uid, :string

  end
end
