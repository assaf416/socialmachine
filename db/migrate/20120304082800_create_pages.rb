class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.integer :website_id
      t.integer :template_id
      t.string :name
      t.integer :order_in_toolbar
      t.boolean :show_in_toolbar
      t.string :kind
      t.text :css
      t.text :header

      t.timestamps
    end
  end
end
