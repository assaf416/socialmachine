class PagePartial < ActiveRecord::Base
  belongs_to :page
  belongs_to :product
  mount_uploader :image, ImageUploader
#  mount_uploader :slide_1, ImageUploader
#  mount_uploader :slide_2, ImageUploader
#  mount_uploader :slide_3, ImageUploader
#  mount_uploader :slide_4, ImageUploader
#  mount_uploader :slide_5, ImageUploader
#  
  
  def the_image
    return self.image_url if self.image_url
    return "/assets/placeholder.png"
  end

  def the_movie
      s = self.title 
      f1 = s.index("v=").to_i
      f2 = s.index("&").to_i 
      #      raise s.inspect
      return s[f1+2..f2+1]
  end

  def newsletter?
    self.kind.include? "newsletter"
  end
  def text?
    self.kind.include? "text"
  end
  def feature?
    self.kind.include? "feature"
  end
  def article?
    self.kind.include? "article"
  end
  def image?
    self.kind.include? "image"
  end
  def movie?
    self.kind.include? "movie"
  end
  def slider?
    self.kind.include? "slider"
  end
  def product?
    self.kind.include? "product"
  end
  def form?
    self.kind.include? "form"
  end
  def navigation?
    self.kind.include? "navigation"
  end
  def share_buttons?
    self.kind.include? "share"
  end
  def social_links?
    self.kind.include? "social-links"
  end
    
  def page_links?
    self.kind.include? "page_links"
  end
   
  def hero?
    self.kind.include? "hero"
  end
  def map?
    self.kind.include? "map"
  end
    
end
