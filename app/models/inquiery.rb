class Inquiery < ActiveRecord::Base
  belongs_to :brand
  belongs_to :article
  belongs_to :product
  
#   require "gmail_sender"
  def send_email(to, subject , body)
    begin
      Email.new(
        :brand_id => self.brand_id,
        :kind     => "autorespond",
        :subject  => subject,
        :text_body     => body,
        :to_email => to,
        :to_name  => self.name,
        :status   => "pending"
      ).save
      puts "### -- SENDINF EMAIL to  " + to
      g = GmailSender.new(self.brand.gmail_email, self.brand.gmail_pwd)
      g.send( :to => to,   :subject => subject,  :content => body)
    rescue Exception => e
      puts "Email was not sent. " + e.message
    end
  end
  
  
  def facebook?
    self.source and self.source.include? "facebook"
  end
  
  def auto_response
    puts "AUTO_RESPONSE"
    send_email(self.email, "SocialMachine Beta Registration", "Thank you for registering to the SocialMachine. We will Contact you with your account info soon")
  end
  
  def notify
    puts "NOTIFY"
    send_email(self.brand.gmail_email, "Notification from your SocialMachine website!", "You got new messaeg.  from #{self.name} #{self.email}<br/>#{self.message}")
  end
  
end
