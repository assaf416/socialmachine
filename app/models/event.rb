class Event < ActiveRecord::Base
  belongs_to :brand
  belongs_to :fan
  has_many :trackers
  mount_uploader :image, ImageUploader
end
