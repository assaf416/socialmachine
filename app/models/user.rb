class User < ActiveRecord::Base
  has_many :brands
  has_many :tickets
  mount_uploader :image, ImageUploader
  
  #  attr_accessible :email, :password, :password_confirmation, :name , :ui , :description
  
  attr_accessor :password
  before_save :encrypt_password
  
  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_presence_of :email
#  validates_uniqueness_of :email
  
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user.logged_in_at = Time.now
      user.save
      user
    else
      nil
    end
  end
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
  
  
  require "gmail_sender"
  def send_mail(to, subject , body)
    begin
      puts "### -- SENDINF EMAIL to  " + to
      g = GmailSender.new("assaf.goldstein@gmail.com", "clippeR!")
      g.send( :to => to,   :subject => subject,  :content => body)
    rescue Exception => e
      puts "FAILED Email was not sent. " + e.message
    end
  end
  
  
  def init_user_account
    #  create brand
    brand = Brand.new( :name => "My New Brand!", :user_id => self.id)
    brand.save
    brand.create_channels
    
    #  create website
    brand.create_defult_website
    #  send email to user

    body = ""
    body << " Hello #{self.name}, and  Welcome to the Social Machine."
    body << " To login from your Desktop or Mobile Phone go to \r\n\r\n http://23.21.17.104/login?id=#{self.brand.id}    "
    body << "Your password is #{self.password} \n"
    send_mail(self.email , "Welcome to the Social Machine", body)
  end
  
  def admin?
    self.id == 1
  end
  
  def brand
    self.brands.order("ID ASC").first
  end
  
  def website
    self.brand.websites.order("ID ASC").first
  end
  
  
end
