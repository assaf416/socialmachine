class Post < ActiveRecord::Base
  belongs_to :fan
  belongs_to :brand
  belongs_to :competitor
  belongs_to :channel
  has_many :comments

  scope :brand_posts, where("competitor_id = NULL and fan_id = NULL ")
  scope :delayed_twits , where(:kind => "delayed")
  
  include ApplicationHelper
  
  def empty?
    (self.title.nil? or self.title.empty?) and (self.body.nil? or self.body.empty?)
  end
  
  def striped_twitter_tag
    s = ""
    self.body.split(" ").each{|w | s  <<  " " + w unless w[0] == "#" or w[0] == "@" or w == "RT" or w == "via" }
    s
  end
  
  
  def bounced_email
    # everything between 'permanently:' and 'Technical'
    self.body
    l1 = self.body.index("permanently:") + 14
    l2 = self.body.index("Technical") -1
    if l1 > 0 and l2 > 0 then 
      return self.body[l1..l2]
    end
  end
  
  
  def convert_to_article(brand_id)
    article = Article.new 
    article.brand_id = brand_id
    if self.target_link and self.target_link.include? "<object"
      article.kind = "movie-repost"  
      article.remote_image_url = self.img
      article.name = self.body[0..30]
      article.body = self.target_link
      article.body << "<br/><br/> Add your text here "
      article.cite = ""
    else
      article.kind = "article-repost"  
      article.name = self.body[0..30]
      article.body = self.body
    end
    
    begin
      article.remote_image_url = self.fixed_img
    rescue
    end
    article.cite = self.target_link
    article.save
  end
  
  def convert_to_facebook_message
    # body should have  links to articles and attahced urls
    body = self.body
    begin
      article = Article.find(self.from_name)
      if article
        self.title = article.name
        body = strip_html(article.body) if body.empty? or body.nil?
        body << "\n link to full article: #{article.url}" 
      end
    rescue Exception => e
      logger.error( "FAILED TO CREATE MESSAGE TO FACEBOOK PAGE: #{e.message}")
    end
    
    #    if uri?(self.target_link)
    ##      body << "\n link to full article: #{self.target_link}" 
    #    end
    self.body = body
    self.title = self.body if self.title.nil?
    #    self.channel =
    self.save
    post_to_facebook(self.to)
  end
  
  def post_to_twitter
    logger.error "ABOUT TO POST TO TWITTER"
    c = self.brand.get_channel("twitter")
    Twitter.configure do |config|
      config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
      config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
      config.oauth_token = c.token
      config.oauth_token_secret = c.secret
    end
    s_message = c.auto_respond_message # ADDING SIGNITURE
    s_message <<  " "
    if self.short_url.present?
      s_message <<  self.short_url 
      s_message <<  " "
    end
     s_message << " " +  self.brand.website_short_url 
    s_message <<  self.body unless self.body.nil?
    s_message <<  self.title unless self.title.nil?
    s_message <<  " "
    
    Twitter.update(s_message[0..139])
    logger.error "STATUS POSTED TO TWITTER FOR BRAND #{c.brand.name}"
  end
  
  require 'json'
  require 'xmlsimple'  
  require "linkedin"
  def post_to_linkedin(channel)
    logger.error "ABOUT TO POST TO LINKEDIN"
    client = LinkedIn::Client.new("y1ifejqt5oss", "0EI8fpBTNyOf1G0U")
    client.authorize_from_access(channel.token,channel.secret)
    
    
    xml = "<?xml version='1.0' encoding='UTF-8'?> 
            <share>  
              <comment>#{self.title}</comment>  
              <content> 
                 <title>#{self.title}</title>  
                <description>#{self.body}</description> 
                <submitted-url>#{self.target_link}</submitted-url> 
                <submitted-image-url>#{self.image_url}</submitted-image-url> 
              </content> 
              <visibility> 
                <code>anyone</code> 
              </visibility>  
            </share>#"
    #    puts " XML -> #{xml}"
    
    if self.body.present?
      client.add_share(:comment => self.short_body + " " + self.brand.website_short_url) 
    else
      client.add_share(:comment => self.title + " " + self.brand.website_short_url)  
    end
    
  end
    
    

  def post_to_facebook(page)
    logger.error "ABOUT TO POST TO FACEBOOK PAGE :#{page}"
    begin
#      brand = self.brand
#      token = brand.facebook_pages.find_by_uid(page).token
        
      @fb = self.brand.get_channel("facebook")
      if self.short_url
        link_url = self.short_url
      else
        link_url = self.brand.website_short_url
      end
      
      logger.error " =====  POSTING TO FACEBOOK PAGE :#{page}"
      @graph = Koala::Facebook::GraphAPI.new(self.brand.facebook_token)  
      @graph.put_object("#{page}", "feed", {:message => "#{strip_html(self.body)}\n\n#{self.brand.website_short_url}" , 
          :link =>  link_url,
          :caption => self.title ,
          :name => self.title        })
      logger.error "STATUS:: POSTED MESSAGE TO FAN PAGE #{page}"
    rescue Exception => e
      logger.error "FAILED TO POST TO FANPAGE #{page}: #{e.message}"
    end
  end
  
#  def post_text_to_facebook(page, content)
#    begin
#      logger.error " =====  POSTING TO FACEBOOK PAGE :#{page}"
#      @graph = Koala::Facebook::GraphAPI.new(self.brand.facebook_token)  
#      @graph.put_object("#{page}", "feed", {:message => strip_html(self.body) , 
#          :link =>  self.target_link,
#          :caption => self.title ,
#          :name => self.title        })
#      logger.error "STATUS:: SENT MESSAGE TO FAN PAGE #{page}"
#    rescue Exception => e
#      logger.error "FAILED TO POST TO FANPAGE #{page}: #{e.message}"
#    end
#  end
  
  
  def icon
    begin
      return "" if self.channel.nil?
      return "/assets/icons/youtube_64.png" if self.channel.youtube?
      return "/assets/icons/facebook_64.png" if self.channel.facebook?
      return "/assets/icons/twitter_64.png" if self.channel.twitter?
      return "/assets/icons/google_64.png" if self.channel.google_plus?
      return "/assets/icons/foursquare_64.png" if self.channel.foursquare?
      return "/assets/icons/blogger_64.png" if self.channel.blogger?
      return "/assets/icons/wordpress_64.png" if self.channel.wordpress?
      return "/assets/icons/wordpress_64.png"
    rescue
      " "
    end
  end
  
  
  def fixed_img
    s  = self.img
    if s[0..1]= "//"
      return s.gsub!("//","http://")
    end
    s
  end
  
  def img
    begin
      return self.image_url unless self.image_url.nil? or self.image_url.empty?
      return self.extracted_image unless self.extracted_image.nil? or self.extracted_image.empty?
      return self.channel.icon if self.channel.present?
    rescue
      return "/ccc"
    end
  end
  
   
  
  def links
    return extract_url_from_html(self.body) unless self.body.nil?
    return ""
  end
  
  def plain_text
    unless self.body.nil?
      strip_html(self.striped_twitter_tag)
    end
  end
  
  
  
  def short_body
    unless self.body.nil?
      strip_html(self.striped_twitter_tag)[0..200]
    else
      ""
    end
  end
  
  include ActionView::Helpers::DateHelper
  def posted_ago
    if self.posted_at
      time_ago_in_words(self.posted_at)
    else
      time_ago_in_words(self.updated_at)
    end
  end
  
  require 'nokogiri'
  def extracted_image
    doc = Nokogiri::HTML(self.body.to_s)
    img_srcs = doc.css('img').map{ |i| i['src'] }
    for img in img_srcs do 
      return img unless img.empty? or img.include? "php" or img.include? "php.jpeg"
    end
    
    return nil
  end
  
  
  def make_delayed_twit
    self.channel = self.brand.get_channel("twitter")
    self.body = self.channel.search_4 + " " + self.plain_text
    self.kind = "delayed"
    self.save
  end
  
  
  
end
