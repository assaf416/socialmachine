class Fan < ActiveRecord::Base
  belongs_to :brand
  belongs_to :channel
  has_many :comments
  has_many :events
  has_many :groups
  has_many :posts
  has_many :emails
    
  def short_bio
    return '' if self.bio.nil?
    return self.bio[0..200]
  end
  
  def ignore
    self.channel.add_to_ignored(self.uid)
    self.delete
  end
  
  def unfollow
    if self.channel.twitter?
      # call twitter un-follow
    end
    if self.channel.youtube?
      # call youtube un-subscribe
    end
    if self.channel.linkedin?
      # call linkedin un-subscribe
    end
  end
  
  def follow
     if self.channel.twitter?
      # call twitter follow
    end
    if self.channel.youtube?
      # call youtube subscribe
    end
    if self.channel.linkedin?
      # call linkedin connect
    end
  end
  
  def icon
    return '' if self.channel.nil?
    return '/assets/icons/facebook_64.png' if self.channel.facebook?
    return '/assets/icons/twitter_64.png' if self.channel.twitter?
    return '/assets/icons/youtube_64.png' if self.channel.youtube?
    return '/assets/icons/foursquare_64.png' if self.channel.foursquare?
    return '/assets/icons/linkedin_64.png' if self.channel.linkedin?
    return '/assets/profile-image.png'
  end
  
  
  def exposure
    #    begin
    #      return self.friends.gsub('friend_count:','').gsub('---','').gsub('-','').to_i unless self.friends.nil?
    #    rescue
    #      0
    #    end
    return self.friends_count || 0
  end

  
  def invite
    self.invited_at = Time.now
    msg = self.channel.auto_respond_message
    logger.error ' IMPLEMENT INVITE WIDH MESSAGE : #{msg}'  
    self.save
  end
  
#  def ignore
#    channel = self.channel
#    channel.add_to_ignore_uids(self.uid)
#    channel.save
#  end
  
  def profile_url_or_home
    return self.home_page unless self.home_page.nil?
    return "http://www.facebook.com/#{self.uid}"  if self.channel.facebook?
    return "http://www.twitter.com/#{self.screen_name}"  if self.channel.twitter?
  end

  
  def profile_image
    
    begin
      if self.image_url
        self.image_url 
      else
        icon
      end
    rescue
      
    end
  end
  
  
  require "uri"
  def linkedin_in_url
    URI.extract(self.website.gsub("url:",""))[0]
  end
  
  
  def scan
    return false
  end
  
  #
  # 
  #
  def self.search_on_social_networks(name, brand)
    
    # search facebook users
    begin
      ids = []
      fb = brand.get_channel("facebook")
      graph = Koala::Facebook::GraphAPI.new(fb.token)
      rest = Koala::Facebook::RestAPI.new(fb.token)
      search_res = graph.search("#{name}",{:type => "user"})
      search_res.each{ |i|  ids << i["id"]}
      puts "IDS => #{ids.to_s}"
      idss ="0" ; ids.each{|id| idss << ",#{id}"}
      fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
      fb_res = rest.fql_query(fql)
    rescue
      fb_res = []
    end
    
    # search twitter users
    begin
      tw = brand.get_channel("twitter")

      api = Twitter.configure do |config|
        config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
        config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
        config.oauth_token = tw.token
        config.oauth_token_secret = tw.secret
      end
      
      tw_res = api.user_search(name)
    rescue
      tw_res = []
    end
    
    # search linkedin user
    begin
      li = brand.get_channel("linkedin")
      api_key = 'y1ifejqt5oss'
      api_secret = '0EI8fpBTNyOf1G0U'
      configuration = { :site => 'https://api.linkedin.com' }
      consumer = OAuth::Consumer.new(api_key, api_secret, configuration)
      access_token = OAuth::AccessToken.new(consumer, li.token, li.secret)
 
      # Make call to LinkedIn to retrieve your own profile
      response = access_token.get("http://api.linkedin.com/v1/people/~?format=json")
      profile = JSON.parse   response.body

      
      encoded_prospect  = URI.escape(name)
      url = "http://api.linkedin.com/v1/people-search:(people:(first-name,last-name,id,headline,location:(name),industry,num-connections,summary,picture-url))?keywords=#{encoded_prospect}"
      puts " *** SEARCHING FOR #{name} URL:#{url}"
      response = access_token.get(url)
      li_res =   XmlSimple.xml_in( response.body)["people"]
      
    rescue
      @li_res = []
    end
    
    # search youtube user
    begin
      yt = self.brand.get_channel("youtube")
    rescue
      @yt_res = []
    end
    
    puts " DONE WITH CONNECTION TO SOCIAL NETWORKS"
    results = {:facebook => fb_res, :twitter => tw_res, :linkedin => li_res}
  end
  
  
  

  def add_post(uid,kind, title, body , url , image_url, the_date,comments)
    p = self.posts.find_by_uid(uid)
    p = Post.new( :brand_id => self.brand.id, :channel_id => self.channel, :fan_id => self.id) if p.nil?
    p.kind = kind
    p.title = title
    p.body = body
    p.target_link = url
    #    p.image_url = url
    p.posted_at = the_date
    p.comments_count = comments
    p.save
    puts ' HERE IS THE SHIT : #{p.inspect}'
  end
  
  
  
end
