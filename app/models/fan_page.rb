class FanPage < ActiveRecord::Base
  belongs_to :brand
  has_one :bulk_mail
end
