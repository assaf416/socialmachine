class Template < ActiveRecord::Base
  has_many :bulk_mails
  has_many :websites
  has_many :pages
  has_many :emails
  
  
  include ApplicationHelper
  include EmailsHelper
  require 'inline-style'
  
  
  def the_css
    xcss = self.css.to_s
    x1 = xcss.gsub("$BG-COLOR",self.color_1) unless self.color_1.nil?
    x2 = x1.gsub("$FG-COLOR",self.color_2) unless self.color_2.nil?
    x3 = x2.gsub("$HD-COLOR",self.color_3) unless self.color_3.nil?
    x3
    
  end
  
  
  def preview
    brand = Brand.first
    s = self.html 
    x_css = self.css     
    
    #    the_header = self.header
    the_body = "<h3> #{lorem(10)}</h3>"
    the_body << lorem(150)
    the_body << "<h3> #{lorem(10)}</h3>"
    the_body << lorem(70)
    the_body << "<h3> #{lorem(10)}</h3>"
    the_body << lorem(50)
    
    the_token = rand(10000000).to_s
    
    
    s.gsub!("THE_HEADER", "<h3>Company name</h3><br/> your slogan goes here")
    s.gsub!("THE_BODY", the_body)
    
    s.gsub!("THE_TOKEN", the_token)
    s.gsub!("THE_CAMPAIGN", self.id.to_s)
    s.gsub!("BRAND-ADDRESS", brand.address )
    s.gsub!("BRAND-PHONE", brand.phone )
    s.gsub!("BRAND-FAX", brand.fax )
    s.gsub!("SOCIAL-LINKS",social_links_for_email(Brand.first.id))
    s.gsub!("#home", "http://www.socialmachine.biz/site/?token=#{rand(1000000)}&email=EMAIL-ID")
    s << "<style>" +  x_css + "</style>"
    s = InlineStyle.process(s, :stylesheets_paths => "./styles")
    
  end
  
  
  def parse_page(content)
    res = []
    row = 0
    for line in content.lines do 
      row = row + 1
      grids =[]
      data = line.split(",")
      
      
      
      
      layout = data[0]
      
      grids = [0,12,0,0,0] if layout.include? "1"
      grids = [0,6,6,0,0] if layout.include? "2eq"
      grids = [0,4,4,4,0] if layout.include?"3eq"
      grids = [0,3,3,3,3] if layout.include? "4eq"
      grids = [0,3,1,0,0] if layout.include? "31l"
      grids = [0,1,3,0,0] if layout.include? "31r"
      
      
      col1 = data[1]
      col2 = data[2]
      col3 = data[3]
      col4 = data[4]
      
      res << {:kind => data[1] , :col => 1 , :row => row  ,:grid => grids[1]  } unless data[1].nil?
      res << {:kind => data[2] , :col => 2 , :row => row  ,:grid => grids[2] } unless data[2].nil?
      res << {:kind => data[3] , :col => 3 , :row => row  ,:grid => grids[3] } unless data[3].nil?
      res << {:kind => data[4] , :col => 4 , :row => row  ,:grid => grids[4] } unless data[4].nil?
    
    end
    return res  
  end
  
  def add_partial_to_page(partial,page)
    pp = PagePartial.new(:page_id => page.id, :kind => partial[:kind], :title => lorem(3), 
      :row => partial[:row], :col => partial[:col], :grid => partial[:grid],
      :body => lorem(20), :image => "/assets/placeholder.png")
    pp.save
  end
    
  def create_pages(site)
  
    unless self.page_1_html.nil?
      page = Page.new(:website_id => site.id, :name =>  self.page_1_name, :kind => 'web')
      page.save
      for p in parse_page(page_1_html)do 
        add_partial_to_page(p,page)
      end
    end

    
    unless self.page_2_html.nil?
      page = Page.new(:website_id => site.id, :name =>  self.page_2_name, :kind => 'web' )
      page.save
      for p in parse_page(page_2_html)do 
        add_partial_to_page(p,page)
      end
    end
      
    #    unless self.page_3_html.nil?
    #        
    #    end
    #      
    #    unless self.page_4_html.nil?
    #        
    #    end
    #      
    #    unless self.page_5_html.nil?
    #        
    #    end
  end
  
  
end
