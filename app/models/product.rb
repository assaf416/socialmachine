class Product < ActiveRecord::Base
    belongs_to :brand
    has_many :page_partials
    has_many :inquieries
    has_many :trackers
    mount_uploader :image, ImageUploader
    
    mount_uploader :image_1, ImageUploader
    mount_uploader :image_2, ImageUploader
    mount_uploader :image_3, ImageUploader
    mount_uploader :image_4, ImageUploader
    mount_uploader :image_5, ImageUploader
end
