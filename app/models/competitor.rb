class Competitor < ActiveRecord::Base
  belongs_to :brand
  has_many :posts
  has_many :comments
  has_many :scores


  def profile_image
    return self.image_url unless self.image_url.nil?
    return "/assets/profile-image.png"
  end

  def short_description
    if self.facebook_description.present?
      return self.facebook_description
    end
    
    if self.twitter_description.present?
      return self.twitter_description
    end
  end
  

  
  def new_score(day,kind,posts_count, users_count,eng_posts_count, eng_users_count)
    s = Score.new(
      :brand_id => self.brand_id,  :competitor_id => self.id , :kind => kind, :score_date => day,
      :content_posts_count => posts_count, :exposure_users_count => users_count,
      :engadge_users_count => eng_users_count, :engadge_posts_count => eng_posts_count
    )
    s.save
    return s 
  end
  
  def score
    today = Time.now.strftime("%Y-%m-%d")
    
    logger.error "SCORE FACEBOOK"

    
    unless self.facebook_page.nil?
      begin
        unless self.scores.where(:kind => "facebook" , :score_date => today).present?
          logger.error " FETCHING FACEBOOK INFO ABOUT #{self.name}"
          oauth_access_token = self.brand.facebook_token#get_channel("facebook").token
          logger.error " ACCESS TOKEN: " +  oauth_access_token
          @graph = Koala::Facebook::API.new(oauth_access_token) 
          profile = @graph.get_object(self.facebook_page)
          feed = @graph.get_connections(self.facebook_page, "feed")
          return new_score(today, "facebook", feed.size,0,profile["talking_about_count"].to_i,profile["likes"])
        else
          return self.scores.where(:kind => "facebook" , :score_date => today).first
        end
      rescue Exception => e
        puts  e.message + " --> #{self.facebook_page} on FACEBOOK"
      end
    end
    
    logger.error "SCORE TWITTER"
    logger.error "SCORE YOUTUBE"
    logger.error "SCORE EMAIL"
    logger.error "SCORE WEB"
  end
  

  
  

  require 'nokogiri'
  def extract_image(body, network)
    doc = Nokogiri::HTML(body)
    img_srcs = doc.css('img').map{ |i| i['src'] }
    return img_srcs[0] unless img_srcs.empty?
    return "/assets/icons/twitter_64.png" if network.include? "twitter"
    return "/assets/icons/facebook_64.png" if network.include? "facebook"
    return "/assets/icons/rss.png"
  end

  
  
end
