class Tracker < ActiveRecord::Base
  belongs_to :channel
  belongs_to :email
  belongs_to :bulk_mail
  belongs_to :article
  belongs_to :product
  belongs_to :article
  belongs_to :brand
    
  
  def today?
    self.logged_at.strftime("%y-%m-%d") == Time.now.strftime("%y-%m-%d")
  end 
  
  
  require 'geocoder'
  def update_location
    location = Geocoder.search(self.ip)[0]
    unless location.nil?
      self.country = location.country
      self.city = location.city
      self.address = location.address
      self.latitude = location.latitude
      self.longitude = location.longitude
      self.save
    end
  end
  
  
  def self.the_os(str)
    return " " if str.nil?
    return "Mac" if str.include? "Mac"
    return "Mac" if str.include? "IPhone"
    return "Android" if str.include? "Android"
    return "Linux" if str.include? "Linux"
    return "PC" 
  end

  def self.the_article_id(str)
    return nil if str.nil?
    if str.include? "article="
      val = str[str.index("article")..1000].gsub("article=","")
    end
  end

  def self.the_email_id(str)
    return nil if str.nil?
    if str.include? "email="
      val = str[str.index("email=")..1000].gsub("email=","")
    end
  end



  def self.the_channel_id(str)
    return nil if str.nil?
    if str.include? "channel="
      val = str[str.index("channel=")..1000].gsub("channel=","")
    end
  end

  def self.browser(str)
  
  end

  def self.the_device(str)
    return " " if str.nil?
    return "iPad" if str.include? "iPad"
    return "iPhone" if str.include? "iPhone"
    return "mobile" if str.include? "Mobile"
    return "unknown"
  end

  
  def self.fix_time(str)
    return "" if str.nil?
    day = str[0..1]
    month = str[3..5]
    year = str[7..10]
    hour = str[12..13]
    minute =str[15..16] 
    second =str[18..19] 
    return "#{month} #{day} #{year} #{hour}:#{minute}:#{second}"
  end

  
  
  
  def self.parse_line(str)
    ar = str.split(" ")
    pp = ar[6]
    ip = ar[0]
    time =  Chronic::parse(fix_time( ar[3].to_s.gsub!("[","")) )
    token = pp[pp.index("?")..pp.size+1] if pp and pp.include? "?"
    status = ar[8]
    referer = ar[11..100]
    device = the_device(referer)
    os = the_os(referer)
    article_id = the_article_id(token)
    email_id = the_email_id(token)
    channel_id = the_channel_id(token)

    location = Geocoder.search(ip)[0]
    if location
      # puts "LOCATION: #{location.address} , #{location.country} #{ location.city} (#{location.latitude}:#{location.longitude}) , Channel: #{channel_id} | Email ID: #{email_id} : Article: #{article_id} |   IP: #{ip} | Device:#{device} - #{os}| Time: #{time} | Token: #{token} | Parameters: #{pp} } Referer: #{referer}"  
      puts " TIME: #{time}"
      
      
      if Tracker.find_by_ip_and_logged_at(ip , time).nil?
        tracker = Tracker.find_by_ip(ip)
        tracker = Tracker.new if tracker.nil?
      
        tracker.ip = ip
        tracker.latitude = location.latitude
        tracker.longitude = location.longitude
        tracker.address = location.address
        tracker.city = location.city
        tracker.country = location.country
        tracker.visits = tracker.visits.to_i + 1
        tracker.os = os
        tracker.email_id = email_id
        tracker.logged_at = time
        tracker.channel_id = channel_id    
        tracker.article_id = article_id
        tracker.article_id = article_id
        tracker.engaged = false
        tracker.save
      else
        puts "sorry ignoring this trakcer..."
      end
      
      
    end
  end
  
  
  def self.load
    begin
      path = "#{Rails.root}/../logs/trackers.log"
      logger.error "PATH = #{path}"
      log = File.open(path).read
      for line in log.split("\n") do 
        parse_line(line) 
      end
    rescue Exception => e
      logger.error " FAILD to open trackers.log file #{e.message}" 
    end
      
  end
  
end
