class Deployment < ActiveRecord::Base
  belongs_to :article
  belongs_to :brand
  
  require "gmail_sender"
  
  def send_email
  
    for addr in self.emails.split(",") do 
      begin
          puts "Sending Emails to: #{addr} " 
        g = GmailSender.new(self.brand.gmail_email, self.brand.gmail_pwd)
        g.send( :to => addr,   :subject => self.title,  :content => self.body)
      rescue Exception => e
        puts "Email was not sent.  to #{addr}, " + e.message
      end
    end
    
    puts "Using Following Gmail Account : #{self.brand.gmail_email} | #{self.brand.gmail_pwd}"
    
  end
end
