class BulkMail < ActiveRecord::Base
  belongs_to :brand
  belongs_to :template
  belongs_to :page
  has_many :emails
  has_many :articles
  has_many :trackers

  include BulkMailsHelper
  
  def short_url
    begin
    self.page.short_url
    rescue
      ""
    end
  end
  
  def started?
    self.emails.count > 0
  end
  
  def newsletter?
    self.kind.include? "newsletter"
  end
  
  def promotion?
    self.kind.include? "promotion"
  end
  
  def product?
    self.kind.include? "product"
  end

  def duplicate
    new_record = self.dup
    new_record.name = "copy of #{self.name}"
    new_record.save
  end

  def opened_trackers
    ar = []
    for email in self.emails do 
      for tracker in email.trackers do
        ar << tracker if tracker.engaged.nil? or tracker.engaged == false
      end
    end
    ar
  end
  
  def engaged_trackers
    ar = []
    for email in self.emails do 
      for tracker in email.trackers do
        ar << tracker if tracker.engaged == true
      end
    end
    ar
  end
  
  
  def emails_by_status_chart_data
    keys = self.emails.group(:status).count.keys
    values = self.emails.group(:status).count.values
    return keys, values
  end
  
  def total_views
    opened_trackers.count
  end
  
  def total_clicked
    engaged_trackers.count
  end
  
 
  
  def self.send_10_emails
    logger.error " >>> SENDING 10 EMAILS"
  end
  
  
  include ApplicationHelper
  def generate_emails(list_of_fan_ids)
    return 
    for item in list_of_fan_ids do 
      fan = Fan.find(item)
      e = Email.new( :brand_id => self.brand_id, :bulk_mail_id => self.id, 
        :subject => self.name , :to_email => fan.email , :to_name => fan.name , 
        :kind => self.kind , :fan_id => fan.id,
        :html_body => self.preview,
        :text_body => strip_html(self.preview))
      e.save
    end
  end
  
  # encoding: UTF-8
  def create_emails
    #    raise self.fan_list.inspect
    for item in self.fan_list.split(",") do 
     
      begin
        fan = Fan.find(item)
        #        content = self.preview
        e = Email.new( :brand_id => self.brand_id, :bulk_mail_id => self.id, 
          :subject => self.name , :to_email => fan.email , :to_name => fan.name , 
          :kind => self.kind , :fan_id => fan.id,
          :status => "pending",
          :unique_token => "blk#{self.id}-rnd#{rand(100000000)}",
          :html_body => self.preview,
          :text_body => strip_html(self.preview_text) )
        e.save
        
        content = e.html_body
        
        tracking_url = "http://www.socialmachine.biz/assets/this-is-the-email-image.png?token=#{e.unique_token}&email=#{e.id}"
        content.gsub!("UNSUBSCRIBE-URL","http://www.socialmachine/public/unsubscribe_from_email?token=#{e.unique_token}&email=#{e.id}")
        content.gsub!("TRACKING-IMAGE", tracking_url)
        content.gsub!("#user", e.to_name)
        content.gsub!("user#", e.to_name)
        e.html_body = content
        e.save
        puts " =============== SAVED: #{e.to_name}"
        #        raise content.inspect
        #        e.text_body= strip_html(content)
        #        e.save
      rescue Exception => e
        logger.error "FAILED TO GENERATE EMAIL #{e.message}"
        next
      end
    end
  end
  
  
  
  
  
  def icon
    return "/assets/icons/news.png"
  end
  
  
  
  def add_articles(ids)
    order = 0
    for id in ids do
      order = order + 1
      ar = Article.find(id)
      ar.bulk_mail_id = self.id
      ar.order_in_newsletter = order
      ar.save
    end
  end
  
  include EmailsHelper
  require 'inline-style'
 
  def preview_text
    html = "\n"
    html << "===================================================\n"
    html << " --  #{self.brand.name} : #{self.name}\n"
    html << "===================================================\n"
    if self.promotion?
      html << self.body 
      html << "\n"
    end
    if self.newsletter?
      for item in self.articles do 
        html  << item.to_newsletter_text + "\n"
      end
    end
    html << "\n"
    html << "===================================================\n"
    html << " -- to unsubscribe : #{self.name}\n"
    html << "===================================================\n"
  end
  
  
  def articles_to_html
    _body = ""
    _body << "<table class='articles'>"
    for item in self.articles do 
      _body << item.to_newsletter_html
    end
    _body << "</table>"
    _body << "<tr><td colspan=2>#{self.footer}</td></tr>"
      
  end
  
  def preview
    # plan:
    # grab the template
    # replace TITLE with title text
    # replace BODY with title text
    # replace FOOTER with title text
    
    # ADD Links to social pages
    # replace THE_TOKEN and THE_CAMPAIGN
    
    
    if self.newsletter?
      _body = ""
      _body << "<table class='articles'>"
      for item in self.articles do 
        _body << item.to_newsletter
      end
      _body << "</table>"
      _body << "<tr><td colspan=2>#{self.footer}</td></tr>"
      
      x_css = ""
      if self.template_id.nil?
        s = Template.where(:kind => "newsletter").first.html
        x_css = Template.where(:kind => "newsletter").first.css
      else
        s = self.template.html
        x_css = self.template.css
      end
        
      the_token = rand(10000000).to_s
      s.gsub!("THE_HEADER", self.header) unless self.header.nil?
      s.gsub!("THE_BODY", _body ) unless _body.nil?
      s.gsub!("COMPANY-LOGO-AND-SLOGAN", logo_and_slogan(self.brand) ) 
      s.gsub!("THE_TOKEN", the_token)
      s.gsub!("COMPANY-INFO", company_info_for_emails(self.brand))
      s.gsub!("THE_CAMPAIGN", self.id.to_s)
      s.gsub!("SOCIAL-LINKS",social_links_for_email(self.brand_id))
      s.gsub!("#home", "http://www.socialmachine.biz/site/#{self.brand.id}?token=#{rand(1000000)}&email=EMAIL-ID")
      s << "<style>" +  x_css + "</style>"
      s = InlineStyle.process(s, :stylesheets_paths => "./styles")
      return s
    end
    

    if self.promotion?
      x_css = ""
      if self.template_id.nil?
        s = Template.where(:kind => "email").first.html
        x_css = Template.where(:kind => "email").first.css
      else
        s = self.template.html
        x_css = self.template.css
      end
        
      the_token = rand(10000000).to_s
      s.gsub!("THE_HEADER", self.header) unless self.header.nil?
      s.gsub!("THE_BODY", self.body ) unless self.body.nil?
      s.gsub!("COMPANY-LOGO-AND-SLOGAN", logo_and_slogan(self.brand) ) 
      s.gsub!("THE_TOKEN", the_token)
      s.gsub!("BRAND-WEBSITE", self.brand.website_short_url)
      s.gsub!("COMPANY-INFO", company_info_for_emails(self.brand))
      s.gsub!("THE_CAMPAIGN", self.id.to_s)
      s.gsub!("SOCIAL-LINKS",social_links_for_email(self.brand_id))
      s.gsub!("#home", "http://www.socialmachine.biz/site/#{self.brand.id}?token=#{rand(1000000)}&email=EMAIL-ID")
      
      s.gsub!("BRAND-ADDRESS", self.brand.address )
      s.gsub!("BRAND-PHONE", self.brand.phone )
      s.gsub!("BRAND-FAX", self.brand.fax )
      
      s << "<style>" +  x_css + "</style>"
      s = InlineStyle.process(s, :stylesheets_paths => "./styles")
      return s
    end
    
  end
end
