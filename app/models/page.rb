class Page < ActiveRecord::Base
  belongs_to :website
  belongs_to :brand
  belongs_to :template
  has_many :page_partials
  mount_uploader :preview, ImageUploader
  
  
  def to_yaml
    partials = []
    self.to_mapx.each{|p| 
      
      if p[1][0][:kind].include? "slider" 
        partials << {  :row => p[0],
          :kind => p[1][0].kind,
          :grid => p[1][0].grid ,
          :title => p[1][0].title,
          :slide_1 => p[1][0].slide_1_url,
          :slide_1_header => p[1][0].slide_1_header,
          :slide_1_body => p[1][0].slide_1_body,
          :slide_2 => p[1][0].slide_2_url,
          :slide_2_header => p[1][0].slide_2_header,
          :slide_2_body => p[1][0].slide_2_body,
          :slide_3 => p[1][0].slide_3_url,
          :slide_3_header => p[1][0].slide_3_header,
          :slide_3_body => p[1][0].slide_3_body,
          :slide_4 => p[1][0].slide_4_url,
          :slide_4_header => p[1][0].slide_4_header,
          :slide_4_body => p[1][0].slide_4_body,
          :slide_5 => p[1][0].slide_5_url,
          :slide_5_header  => p[1][0].slide_5_header,
          :slide_5_body => p[1][0].slide_5_body,
        }
      else
        partials << {  :row => p[0], :grid => p[1][0].grid ,  :kind => p[1][0].kind,:title => p[1][0].title,:image_url => p[1][0].image_url ,:body => p[1][0].body}
      end
        
      
      
      
    }
    #    self.to_mapx.each{|p| partials << {:id => p[1].inspect}}
    return {  :id => self.id, :name => self.name , :partials => partials }
  end
  
  def toolbar_class
    if self.website.fixed_toolbar == false
      ""
    else
      "navbar-fixed-top"
    end
  end
  
  def row_class
    if self.website.fluid == true
      "row-fluid"
    else
      "row"
    end
  end
  
  
  def type_icon
    return "/assets/icons/facebook_64.png"
  end
  
  
  include ApplicationHelper
  
  
  def add_line(layout)
    puts " -- adding line with layout #{layout}"
    xrow = self.next_row
    
    puts "Adding New line : #{xrow}"
    
    if layout == "1"
      add_item( xrow, 1, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-12)
    end
     
    if layout == "2eq"
      add_item( xrow, 1, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-6)
      add_item( xrow, 2, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,6)
    end
     
    if layout == "3eq"
      add_item( xrow, 1, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-4)
      add_item( xrow, 2, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,4)
      add_item( xrow, 3, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,4)
    end
    if layout == "4eq"
      add_item( xrow, 1, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-3)
      add_item( xrow, 2, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,3)
      add_item( xrow, 3, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,3)
      add_item( xrow, 4, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,3)
    end
    if layout == "31l"
      add_item( xrow, 1, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,-3)
      add_item( xrow, 2, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,9)
    end
    if layout == "31r"
      add_item( xrow, 1, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-9)
      add_item( xrow, 2, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,3)
    end
    
    # more layouts
    if layout == "1hero"
      add_item( xrow, 1, "hero",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-12)
    end
    
    if layout == "1slider"
      add_item( xrow, 1, "slider",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-12)
    end
    
    if layout == "1movie"
      add_item( xrow, 1, "movie","http://www.youtube.com/watch?v=hvQfk4cV5GA&",lorem(30),"/assets/placeholder_icon.png", true,-12)
    end
    
    if layout == "1image"
      add_item( xrow, 1, "image",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-12)
    end
    
    
    if layout == "2logo_and_toolbar"
      add_item( xrow, 1, "logo",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-5)
      add_item( xrow, 2, "page_links",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,7)
    end
    
    if layout == "2logo_and_info"
      add_item( xrow, 1, "logo",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-6)
      add_item( xrow, 2, "contact",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,6)
    end
    
    if layout == "fixed_toolbar"
      add_item( xrow, 1, "fixed_toolbar",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-6)
    end
    
    
    if layout == "434"
      add_item( xrow, 1, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", true,-4)
      add_item( xrow, 2, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,3)
      add_item( xrow, 3, "text",lorem(4),lorem(30),"/assets/placeholder_icon.png", false,4)
    end
    
  end
  
  
  def to_mapx
    partials = self.page_partials
    rows = partials.where("row_is_row_top is NULL").order("row").group_by { |a| a.row }
  end
  
  def to_mapx_top
    partials = self.page_partials
    rows = partials.where(:row_is_row_top => true).order("row").group_by { |a| a.row }
  end
  
  include PagesHelper
  def to_html
    html = convert_to_html(self.to_mapx, self) # GENERATING THE HTML 
  end
  
  def to_html_top
    html = convert_to_html(self.to_mapx_top, self) # GENERATING THE HTML 
  end
  
  def last_row
    r = 0
    self.page_partials.each { |e| r= e.row if e.row and e.row > r  }
    r
  end
  
  def next_row
    begin
      self.last_row + 1 
    rescue
      puts "** Next row is 1 !!"
      1
    end
  end
  
 
  
  def add_item(row, col, kind, title, body, image_url,first, grid)
    pp = PagePartial.new(:page_id => self.id, :kind => kind, :title => title, 
      :body => body,  :image => image_url, :row => row, :col => col, :first => first, :grid => grid)
    pp.save
    self.reload
  end
  
  
  def create_default_content
    # clear old data
    #    for item in self.page_partials do 
    #      item.delete
    #    end
    #    self.reload
    #    
    #    add_line("1")
    #    add_line("2eq")
    #    add_line("1")
    #    add_line("3eq")
  end
  

  def deep_xml(builder=nil)
    to_xml(:builder => builder) do |xml|
      page_partials.each{|child| child.to_xml}
    end
  end
  
  
  def duplicate
    #1 create the new page
    p = Page.new( :name => "copy of #{self.name}", :website_id => 1)
    p.css = self.website.template.css
    p.save
    
    #2 duplicate the page_partials
    for partial in self.page_partials do 
      pp = PagePartial.new(partial.attributes)
      pp.page_id = p.id
      pp.save
    end
  end
  
  
  def duplicate_to_template
    #1 create the new page
    p = Page.new( :name => "#{self.name}", :website_id => -1)
    #    p.css = self.website.template.css
    p.save
    
    #2 duplicate the page_partials
    for partial in self.page_partials do 
      pp = PagePartial.new(partial.attributes)
      pp.page_id = p.id
      pp.save
    end
    
    
    puts " === PAGE CREATED: ID = #{p.id}"
  end
  


  def  increase_views
    if self.view_count.nil?
      self.view_count = 1 
    else
      self.view_count = self.view_count + 1
    end
    self.save
  end
  
  
  
  def create_from_template(template)
    self.reload
    if template == 1
      add_line("fixed_toolbar")
      add_line("1")
      add_line("3eq")
    end
    if template == 2
      add_line("fixed_toolbar")
      add_line("hero")
      add_line("3eq")
    end
      
    if template == 3
      add_line("fixed_toolbar")
      add_line("1slider")
      add_line("3eq")
    end
    
    if template == 4
      add_line("2logo_and_toolbar")
      add_line("1movie")
      add_line("3eq")
    end
    if template == 5
      add_line("2logo_and_toolbar")
      add_line("1slider")
      add_line("3eq")
    end
    if template == 6
      add_line("2logo_and_toolbar")
      add_line("1slider")
      add_line("3eq")
    end
    if template == 7
      add_line("2logo_and_toolbar")
      add_line("1movie")
      add_line("3eq")
    end
    if template == 8
      add_line("fixed_toolbar")
      add_line("1hero")
      add_line("1")
      add_line("1")
      add_line("1")
    end
    
    if template == 9
      add_line("fixed_toolbar")
      add_line("1hero")
      add_line("2eq")
    end
    if template == 10
      add_line("2logo_and_toolbar")
      add_line("1hero")
      add_line("2eq")
    end
    if template == 11
      add_line("2logo_and_toolbar")
      add_line("31l")
      add_line("31l")
      add_line("31l")
    end
    if template == 12
      add_line("2logo_and_toolbar")
      add_line("31l")
      add_line("31r")
      add_line("31l")
    end
    if template == 13
      add_line("fixed_toolbar")
      add_line("1hero")
      add_line("3eq")
    end
    if template == 14
      add_line("2logo_and_toolbar")
      add_line("1hero")
      add_line("3eq")
    end
    if template == 15
      add_line("2logo_and_toolbar")
      add_line("1hero")
      add_line("3eq")
    end
    if template == 16
      add_line("2logo_and_toolbar")
      add_line("1hero")
      add_line("3eq")
    end
  end
  
end
