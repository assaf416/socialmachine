class Channel < ActiveRecord::Base
  belongs_to :brand
  has_many :fans
  has_many :comments
  has_many :posts
  has_many :scores
  has_many :trackers
  
  def blog?
    self.wordpress? or self.blogger? or self.youtube?
  end
  
  
  require "google_reader_api"
  def add_rss_url(url)
    begin
      user = GoogleReaderApi::User.new(:email => self.email, :password => self.secret)
      user.subscriptions.add url
      return "URL ADDED"
    rescue Exception => e
      return e.message
    end
  end
  
  def total_fans
    self.fans.where(:kind => "fan").count
  end
  
  def add_to_ignored(id)
    logger.error "STATUS:: ADDING #{id} to #{self.ignore_ids}"
    
    sss = self.ignore_ids
    if sss.nil?
      sss = id
    else
      sss= sss + ",#{id}"
    end
    self.ignore_ids = sss
    #    if self.ignore_ids.nil?
    #      self.ignore_ids = id.to_s
    #    else
    #      self.ignore_ids << ",#{id.to_s}"
    #    end
    self.save
  end
  
  def ignored?(id)
    self.ignore_ids.include? id
  end
  
  def views_track_image
    "/assets/this-is-the-email-image.png?token=#{rand(100000)}&channel=#{self.id}"
  end
  
  
  
  def total_post_sent
    if self.facebook?
      self.posts.where(:kind => "fan_page").count
    end
    
    if self.twitter?
      self.posts.where(:kind => "mention")
    end
  end
  
  
  def profile_mark
    if self.facebook?
      self.followings_count
    end
    return rand(100)
  end
  
  def content_mark
    if self.facebook?
      self.posts.where(:kind => "fan_page").count
    end
    return rand(100)
  end
  
  def exposure_mark
    if self.facebook?
      ex = 0
      self.fans.where(:kind => 'fan').each{|fan| ex = ex + fan.exposure.to_i }
      return ex.to_i
    end
    if self.twitter?
      ex = 0
      self.fans.where(:kind => 'fan').each{|fan| ex = ex + fan.exposure.to_i }
      return ex.to_i
    end
    
    if self.linkedin?
      ex = 0
      self.fans.where(:kind => 'fan').each{|fan| ex = ex + fan.exposure.to_i }
      return ex.to_i
    end
    
    return rand(100)
  end
  
  def engaged_users
    uids=[]
    
    if self.facebook?
      for comment in self.comments do 
        uids << comment.from_uid
      end
      for message in self.posts.where(:kind => "inbox") do 
        uids << message.from_uid
      end
    end
    if self.twitter?
      for message in self.posts.where(:kind => "mention") do 
        uids << message.from_uid
      end
    end
    if self.linkedin?
      
    end
    if self.youtube?
      
    end
    uids
  end
  
  def engaged_users_exposure
    if self.facebook?
      
    end
    if self.twitter?
      
    end
    if self.linkedin?
      
    end
    if self.youtube?
      
    end
  end
  
  
  def engagement_mark
    #return self.fans.where(:kind => "engadge").count
    return 14
  end
  
  def commenters
  end
  
  def fans_by_industry
    ar_lbl = [];ar_values = []
    for fan in self.fans.where(:kind => "fan") do 
      if pos = ar_lbl.index(fan.screen_name)
        ar_values[pos] = ar_values[pos].to_i + 1
        ar_lbl[pos] = fan.screen_name
      else
        ar_lbl << fan.screen_name
        ar_values << 1
      end
    end
    return ar_lbl, ar_values
  end
  
  
  def fan_pages # To REPLACE WITH FAN PAGES.. from table
    begin
      ar = []
      token = self.brand.facebook_token
      @graph = Koala::Facebook::GraphAPI.new(token) 
      items = @graph.get_connections("me", "accounts")
      for item in items do
        unless item['category'].include? "Application"
          ar << item
        end
      end
      #      fb_searches
      ar
    rescue
      []
    end
  end
  
  def most_viewed # youtube
    self.posts[0..3] unless self.posts.nil?
  end
  
  def total_fans_count
    if self.facebook?
      self.fans.where(:kind => "fan").count
    else
      self.fans.count
    end
  end
  

  require 'linkedin'

  def chart_line_1(count)
    ar = []
    count.times do 
      ar << rand(80)
    end
    ar
  end
  
  def chart_line_2(count)
    ar = []
    count.times do 
      ar << rand(20)
    end
    ar
  end
  
  def in_use?
    if self.facebook?
      return self.token.present? 
    end
    
    if self.twitter?
      return self.token.present? 
    end
    
    if self.linkedin?
      return self.token.present? 
    end
    
    if self.foursquare?
      return self.token.present? 
    end
    if self.youtube?
      return self.secret.present?
    end
    
    if self.gmail?
      return self.secret.present? 
    end
    if self.wordpress?
      return (self.email.present?)
    end
    if self.blogger?
      return (self.email.present?)
    end
    
    if self.rss?
      return self.secret.present?
    end
    
    if self.dropbox?
      return self.token.present? 
    end
    
    return false
    
  end

  
  def follow(uid)
    logger.error "STATUS FOLLOWING USER #{uid}"
    if self.twitter?
      api = Twitter.configure do |config|
        config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
        config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
        config.oauth_token = self.token
        config.oauth_token_secret = self.secret
      end
      begin
        Twitter.follow!(uid)
      rescue exception => e
        logger.error "FAILED ON FOLLOWING USER : #{uid}. #{e.message}"
      end
    end
    
    if self.facebook?
      
    end
  end
  
  
  def unfollow(uid)
    logger.error "STATUS :: UNFOLLOWING USER #{uid}"
    if self.twitter?
      api = Twitter.configure do |config|
        config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
        config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
        config.oauth_token = self.token
        config.oauth_token_secret = self.secret
      end
      begin
        Twitter.follow!(uid)
      rescue exception => e
        logger.error "FAILED ON UNFOLLOWING USER : #{uid}. #{e.message}"
      end
    end
    
    if self.facebook?
      
    end
  end
  
  

  def disconnect
    self.token = nil
    self.profile_name = nil
    self.secret = nil
    self.pin = nil
    self.save
  end
  
  
  def home_url
    return "http://www.facebook.com/#{self.brand.facebook_page}" if self.facebook? 
    return "http://www.twitter.com/#{self.brand.twitter_user_id}" if self.twitter? 
    return "http://www.youtube.com/user/#{self.brand.youtube_url}" if self.youtube? 
    return self.profile_url
  end

  def dropbox?
    self.name.include? "dropbox"
  end
  def rss?
    self.name.include? "rss"
  end
  def facebook?
    self.name.include? "facebook"
  end
  def twitter?
    self.name.include? "twitter"
  end
  def youtube?
    self.name.include? "youtube"
  end
  def google_plus?
    self.name.include? "google"
  end

  def wordpress?
    self.name.include? "wordpress"
  end

  def blogger?
    self.name.include? "blogger"
  end
  def foursquare?
    self.name.include? "foursq"
  end

  def gmail?
    self.name.include? "gmail"
  end
  def reddit?
    self.name.include? "reddit"
  end
  def linkedin?
    self.name.include? "linkedin"
  end


  def icon
    if self.youtube?
      if self.in_use?
        return "/assets/flat/youtube-3-64.png" 
      else
        return "/assets/flat/youtube-off.png" 
      end
    end
    

    if self.facebook?
      if self.in_use?
        return "/assets/flat/facebook-3-64.png" 
      else
        return "/assets/flat/facebook-off.png" 
      end
    end    
    
    
    
    if self.twitter?
      if self.in_use?
        return "/assets/flat/twitter-3-64.png" 
      else
        return "/assets/flat/twitter-off.png" 
      end
    end    
    
    if self.linkedin?
      if self.in_use?
        return "/assets/flat/linkedin-3-64.png" 
      else
        return "/assets/flat/linkedin-off.png" 
      end
    end    
    
    if self.google_plus?
      if self.in_use?
        return "/assets/flat/google-plus-3-64.png" 
      else
        return "/assets/flat/google-plus-off.png" 
      end
    end  
    
    if self.wordpress?
      if self.in_use?
        return "/assets/flat/wordpress-3-64.png" 
      else
        return "/assets/flat/wordpress-off.png" 
      end
    end  
    
    if self.blogger?
      if self.in_use?
        return "/assets/flat/blogger-3-64.png" 
      else
        return "/assets/flat/blogger-off.png" 
      end
    end  
    
    if self.foursquare?
      if self.in_use?
        return "/assets/flat/posterous-spaces-64.png" 
      else
        return "/assets/flat/foursquare-off.png" 
      end
    end  
    
     if self.dropbox?
      if self.in_use?
        return "/assets/flat/dropbox_64.png" 
      else
        return "/assets/flat/dropbox_off.png" 
      end
    end  
    
    return "/assets/flat/facebook-3-64.png" if self.facebook?
    return "/assets/flat/twitter-3-64 .png" if self.twitter?
    return "/assets/flat/google-plus-3-64.png" if self.google_plus?
    return "/assets/flat/posterous-spaces-64.png" if self.foursquare?
    return "/assets/flat/blogger-3-64.png" if self.blogger?
    return "/assets/flat/wordpress-3-64.png" if self.wordpress?
    return "/assets/flat/email-3-64.png" if self.gmail?
    return "/assets/flat/reddit.png" if self.reddit?
    return "/assets/flat/linkedin-3-64.png" if self.linkedin?
    return "/assets/flat/rss-3-64.png" if self.rss?
    return "/assets/flat/rss-3-64.png"
  end



  def scan
    logger.error "STATUS:: SCAN IS NOT APPLICABLE ANY MORE!"
    return 
    
  end


  
end
