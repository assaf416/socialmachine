class Comment < ActiveRecord::Base
  belongs_to :fan
  belongs_to :article
  belongs_to :post
  belongs_to :brand
  belongs_to :competitor
  belongs_to :channel
end
