class Website < ActiveRecord::Base
  belongs_to :brand
  belongs_to :template
  has_many :pages
  
  
  
  def home_page_id
    self.pages.where(:kind => nil).order("order_in_toolbar ASC").first.id
  end
  
  def to_yaml
    pages = []
    for page in self.pages do 
      pages << page.to_yaml
    end
    return {  :seo => self.keywords, :meta => self.meta_description, :pages => pages }
  end
  
  
  # include AnalysisHelper

  def valid_competitor_url(url)
    return false if url.nil?
    return false if url.include? ".org"
    return false if url.include? "wikipedia"
    return true
  end
  
  def competitors_on_google
#    return if self.keywords.nil? or self.keywords.empty?
#    res = []
#    for item in self.keywords.split(",") do 
#       google_search(item).each{|o | res << o if valid_competitor_url(o[:url])}
#    end
#    res
  end
  
  
  
  
  
  def move_page(page,direction)
    # if no order is in page create order
    if page.order_in_toolbar.nil?
      logger.error " ====  RESET PAGE ORDER"
      for p in self.pages do 
        p.order_in_toolbar = p.id
        p.save
      end
    else
      logger.error " ====  MOVING PAGE UP OR DOWN"
      pgs = []
      for p in self.pages.order("order_in_toolbar ASC") do 
        pgs << p
        original_page = p if p.id == page.id
      end

      i = pgs.index(original_page)

      if direction.include? "up" then 
        old_page = pgs[i-1]
        original_page.order_in_toolbar = old_page.order_in_toolbar - 1 unless old_page.nil?
        original_page.save
      end

      if direction.include? "down" then 
        old_page = pgs[i+1]
        original_page.order_in_toolbar = old_page.order_in_toolbar + 1 unless old_page.nil?
        original_page.save
      end

    end
  end
  
  def deep_xml(builder=nil)
    to_xml(:builder => builder) do |xml|
      pages.each{|child| child.deep_xml(xml)}
    end
  end
  
  def reset_custom_css
    self.toolbar_background_color = nil
    self.toolbar_text_color = nil
    self.hero_background_color = nil
    self.hero_text_color = nil 
    self.hero_header_color = nil
    self.background_color = nil
    self.text_color = nil
    self.text_font_name = nil
  end


  def create_color_patch_from_site_colors
    c = Color.new( :name => "Color Patch #{rand(1000)}")

    c.color_1 = self.background_color 
    c.color_2 = self.text_color 
    c.color_3 = self.heading_color 
    c.color_4 = self.hero_background_color 
    c.color_5 = self.hero_text_color 
    c.color_6 = self.hero_header_color
    c.save

  end
  
  def apply_color_patch(color)
    self.toolbar_background_color = nil
    self.toolbar_text_color = nil
    self.background_color = color.color_1
    self.text_color =  color.color_2
    self.heading_color = color.color_3
    self.hero_background_color = color.color_4
    self.hero_text_color =  color.color_5
    self.hero_header_color = color.color_6
  end

  
  def remove
    for page in self.pages do 
      for par in page.page_partials do 
        par.delete
      end
      page.delete
    end
    self.delete
  end
  
  def load_page
  end

  def seo_backlinks
  end

  def seo_meta_description
  end

  def seo_meta_keywords
  end

  def seo_visitors_by_reference
  end

  def seo_visitors_by_date
  end

  def seo_text_relevance
  end


  def computed_css

    css = ""
    css << "body{ color:#{self.text_color}} "  unless self.text_color.nil?
    css << "body{ background-color:#{self.background_color}} "  unless self.background_color.nil?
    css << "body{ font-family:#{self.text_font_name}} "  unless self.text_font_name.nil?
    css << "p{ font-family:#{self.text_font_name}} "  unless self.text_font_name.nil?
    css << "p{ font-size:#{self.text_size}px} "  unless self.text_size.nil?

    css << "h1, h2, h3, h4, h5, h6 { color:#{self.heading_color}; font-family:#{self.header_font_name}}" unless self.heading_color.nil?
    css << "h1, h2, h3, h4, h5, h6 {  font-family:#{self.header_font_name}}" unless self.header_font_name.nil?

    css << ".hero-unit { color:#{self.hero_text_color}} "  unless self.hero_text_color.nil?
    css << ".hero-unit { background-color:#{self.hero_background_color}} "  unless self.hero_background_color.nil?
    css << ".hero-unit h1, .hero-unit h2, .hero-unit h3 { color:#{self.hero_header_color}} "  unless self.hero_header_color.nil?

    css << ".well h1, .well h2, .well h3 { color:#{self.hero_header_color}} "  unless self.hero_header_color.nil?
    css << " .well{ background-color:#{self.hero_background_color}} " 
    css << " .well p , .well label { color:#{self.hero_text_color}} " 
    css << " .well h3 { color:#{self.hero_header_color}} " 

    css <<  " .navbar-inner {
   min-height: 50px;
   padding-left: 20px;
   padding-right: 20px;
   background-color: #{self.toolbar_background_color};
   background-image: -moz-linear-gradient(top, #{self.toolbar_background_color}, #{self.toolbar_background_color});
   background-image: -ms-linear-gradient(top, #{self.toolbar_background_color}, #{self.toolbar_background_color});
   background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#{self.toolbar_background_color}), to(#{self.toolbar_background_color}));
   background-image: -webkit-linear-gradient(top, #{self.toolbar_background_color}, #{self.toolbar_background_color});
   background-image: -o-linear-gradient(top, #{self.toolbar_background_color}, #{self.toolbar_background_color});
   background-image: linear-gradient(top, #{self.toolbar_background_color}, #{self.toolbar_background_color});
   background-repeat: repeat-x;
   filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#{self.toolbar_background_color}', endColorstr='#{self.toolbar_background_color}', GradientType=0);
   -webkit-border-radius: 4px;
   -moz-border-radius: 4px;
   border-radius: 4px;
   -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.25), inset 0 -1px 0 rgba(0,0,0,.1);
   -moz-box-shadow: 0 1px 3px rgba(0,0,0,.25), inset 0 -1px 0 rgba(0,0,0,.1);
   box-shadow: 0 1px 3px rgba(0,0,0,.25), inset 0 -1px 0 rgba(0,0,0,.1);
   color:#{self.toolbar_text_color};}"
    
    css << ".navbar li  a, .navbar li  a:visited {font-family:#{self.toolbar_font_name}; font-size:#{self.toolbar_text_size}px; color:#{self.toolbar_text_color};}"
    
    
    css
  end

  def total_views
    ar = 0
    for page in self.pages do 
      ar = ar + page.view_count.to_i 
    end
    ar
  end
end
