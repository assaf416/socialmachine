class Article < ActiveRecord::Base
  belongs_to :brand
  belongs_to :bulk_mail
  has_many  :comments
  has_many  :deployments
  has_many  :slides
  has_many :trackers
  has_many :inquieries
  
  
  def views_track_image
    "/assets/this-is-the-email-image.png?token=#{rand(100000)}&article=#{self.id}"
  end
  
  def medium_body
    if self.body.present?
      strip_html(self.body)[0..500]
    else
      ""
    end
  end
  
  
  def short_body
    if self.body.present?
      strip_html(self.body)[0..140]
    else
      ""
    end
  end
  
  require 'nokogiri'
  def icon
    doc = Nokogiri::HTML(self.body.to_s)
    img_srcs = doc.css('img').map{ |i| i['src'] }
    return img_srcs[0] unless img_srcs.empty?
    return image_url if image_url.present?
    return "/assets/flat/document-file-2-64.png"
  end

  def increase_view_count
    if self.views.nil?
      self.views = 1
    else
      self.views = self.views + 1
    end
    self.save
  end
  
  
  def self.create_from_post(post_id,brand_id)
    
    brand = Brand.find(brand_id)
    item = brand.post.find(post_id)
    logger.error " $$$$ CREATEING ARTICLE FROM POST ID #{item}"
      
    Article.create!(
      :brand_id => brand.id, 
      :name => item.title,
      :body => item.plain_text,
      :image_url => item.img,
      :kind => "long-twitter-post",
      :cite => item.target_url
    )
  end
    
  
  def self.create_from_rss_items(list,brand_id)
    for item in list do 
      fi = FeedEntry.find(item)
      logger.error " $$$$ CREATEING ARTICLE FROM POST ID #{item}"
      
      Article.create!(
        :brand_id => brand_id, 
        :name => fi.title,
        :body => fi.plain_text,
        :image_url => fi.icon,
        :kind => "article-repost",
        :cite => fi.url
      )
    end
    
  end
  
  def article?
    self.kind.nil? or self.kind.include? "article" or self.kind.include? "text" 
  end
  
  def podcast?
    self.kind.include? "podcast"
  end
  
  def movie_repost?
    self.kind and self.kind.include? "movie-repost"
  end
  
  
  
  def clip?
    return false if self.kind.nil?
    self.kind.include? "clip" or self.kind.include? "movie"
  end
  def presentation?
    return false
    self.kind.include? "presentation"
  end
  
  
  
  def short_video_url
    v1 = remote_movie_url.index("video:")
    return remote_movie_url[v1+6..100] unless v1.nil?
    return ""
  end
  
  mount_uploader :image, ImageUploader
  mount_uploader :audio, AudioUploader
  mount_uploader :video, VideoUploader
  
  
#  require "gmail_sender"
  def send_mail(to, subject , body, file_url)
    begin
    Email.new(
        :brand_id => self.brand_id,
        :kind     => "article_deployment",
        :subject  => subject,
        :text_body     => body,
        :to_email => to,
        :to_name  => to,
        :status   => "pending"
      ).save
    rescue Exception => e
      logger.error "FAILED to send email id=#{self.id}. error:#{e.message}"
    end
  end
  
  include FacebookHelper
  include TwitterHelper
  
  def deploy
    send_mail(self.brand.wordpress_email,self.name,self.body,"") if self.brand.wordpress_email
    send_mail(self.brand.blogger_email,self.name,self.body,"") if self.brand.blogger_email
  end
  
  def to_newsletter_html
    html = '<tr class="article" style="vertical-align:top">'
    html << "   <td width='128px'><img src='#{self.icon}' width='128px' /></td>"
    html << "   <td> <a href='/public/article?id=#{self.id}'><h5>#{self.name}</h5></a>
                     Posted at #{self.created_at.strftime("%m/%d")} &nbsp; by &nbsp; #{self.author}
                     <p>#{self.medium_body}</p><hr/>
                      </td>"
    html << "</tr>"
  end
  
  def to_newsletter
    html = '<tr class="article">'
    html << "   <td width='48px'><img src='#{self.icon}' /></td>"
    html << "   <td> <a href='#{self.short_url}'>#{self.name}</a><br/><p>#{self.medium_body}</p></td>"
    html << "</tr>"
  end
  
  def to_newsletter_text
    html = "\n\n #{self.name}\n"
    html << "-----------------------------------------------------\n"
    html << "#{self.medium_body}\n READ MORE : #{self.short_url}\n"
  end
  
  
  def post_to_twitter
    raise "YOU ARE NOT SUPPOSE TO BE HERE!"
    c = self.brand.get_channel("twitter")
    Twitter.configure do |config|
      config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
      config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
      config.oauth_token = c.token
      config.oauth_token_secret = c.secret
    end
    s_message = c.search_4
    s_message <<  " "
    s_message <<  self.name
    s_message <<  " "
    s_message <<  self.short_url
    Twitter.update(s_message)
  end
  
  def create_default_slides
    Slide.create!(:article_id => self.id , :layout => "header", :name => "first page", :body => "Hello World")
    Slide.create!(:article_id => self.id , :layout => "list", :name => "page 2", :body => "item1, item 2")
    Slide.create!(:article_id => self.id , :layout => "list", :name => "page 3", :body => "item1, item 2")
  end
end
