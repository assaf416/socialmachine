class Brand < ActiveRecord::Base
  has_many :articles
  has_many :products
  has_many :competitors
  has_many :fans
  has_many :events
  has_many :emails
  has_many :bookmarks
  has_many :feeds
  has_many :bulk_mails
  has_many :fan_pages
  has_many :websites
  has_many :inquieries
  belongs_to :user
  has_many :posts
  has_many :comments
  has_many :messages
  has_many :deployments
  has_many :channels
  has_many :trackers
  has_many :scores
  has_many :autoresponders
  has_many :pages
  mount_uploader :image, ImageUploader

  @found_users = []
  
  def prospects
    @found_users || []
  end
 
  
  def short_user(user,network)
    if network == "facebook" then
      return  {
        :uid => user["id"],
        :name => user["name"],
        :picture => user["picture"]["data"]["url"] ,
        :short_description => user["bio"],
        :long_description => user["bio"],
        :followers => "",
        :location => user["location"],
        :home_page => "user.url",
        :posts => "",
        :channel_kind => "facebook",
        :channel_icon => "/assets/flat/facebook.png",
        #      :likes => user.friends_count, # reccomendations
        :email => "",
        :phone => "",
        :website => "",
        :last_update => -1 , :network => network }
      
    end
    
    if network == "twitter" then
    end
    
    if network == "linkedin" then
      if user["picture-url"]
        picture = user["picture-url"][0] 
      else
        picture  = "/assets/flat/user-8-64.png"
      end
    
      if user["public-profile-url"]
        home_page = user["public-profile-url"]
      else
        home_page = ""
      end
    
      if user["summary"]
        summary = user["summary"][0]
      else
        summary = ""
      end
    
      return {
        :uid => user["id"][0],
        :name => "#{user['first-name'][0]} #{user['last-name'][0]}",
        :picture => picture,
        :short_description => user["headline"][0],
        :long_description => summary,
        :followers => user["num-connections"][0],
        :location => user["location"][0]["name"][0],
        :home_page => home_page,
        :channel_kind => "linkedin",
        :channel_icon => "/assets/flat/linkedin.png",
        :likes => -1, # reccomendations
        :email => "",
        :phone => "",
        :website => "",
        :last_update => -1 , :network => network
      }
    end
  end
  
  
  require "koala"
  require "twitter"
  require "linkedin"
  
  def find_people(q)
    
    user_list = []
    
    # Linkedin FIND 
    
    # Fill the keys and secrets you retrieved after registering your app
    api_key = 'u2ax8n7ilttl'
    api_secret = 'h9Wvwl6FPfCDL9Qi'
 
    # Specify LinkedIn API endpoint
    configuration = { :site => 'https://api.linkedin.com' }
    # Use your API key and secret to instantiate consumer object
    consumer = OAuth::Consumer.new(api_key, api_secret, configuration)
    # Use your developer token and secret to instantiate access token object
    access_token = OAuth::AccessToken.new(consumer, self.get_channel("linkedin").token, self.get_channel("linkedin").secret)
 
    
    encoded_prospect  = URI.escape(q)
    url = "http://api.linkedin.com/v1/people-search:(people:(first-name,last-name,id,public-profile-url,site-standard-profile-request,headline,location:(name),industry,num-connections,summary,picture-url))?keywords=#{encoded_prospect}"
    #    puts " *** SEARCHING FOR #{query} URL:#{url}"
    response = access_token.get(url)
    list =   XmlSimple.xml_in( response.body)
    begin
      for item in list["people"][0]["person"] do
        user_list << short_user(item, "linkedin")
      end
    rescue Exception => e
      puts " @@@ FAILED ON SEARCHING #{encoded_prospect}. error: #{e.message}"
      puts e.backtrace
    end
    
    # FACEBOOK FIND
    
    @graph = Koala::Facebook::API.new(self.get_channel("facebook").token)
    @rest = Koala::Facebook::API.new(self.get_channel("facebook").token)
    fb_res = @graph.search(q, :fields => "id,link,name,gender,bio,location,devices,education,hometown,picture,website,work", :type => "user")
    for item in fb_res do 
      user_list <<  short_user(item,"facebook")
    end
    
    return  user_list
  end
  
  
  
  
  
  
  
  def website_short_url
    self.site.url
  end
  
  def running_campaigns
    self.bulk_mails.where(:kind => "campaign")
  end
  
  
  def short_links
    res = []
    res << {:name => "Home Page", :url => self.website_short_url}
    
    for item in self.fan_pages do 
      res << { :name => item.name, :url =>  item.url}
    end
    
    for item in self.newsletters do 
      res << { :name => item.name, :url =>  item.short_url}
    end
    return res
  end
  
  def newsletters
    self.bulk_mails.where(:kind => "newsletter")
  end
  
  
  def campaigns_views
    count = 0
    for item in self.site.pages.where(:kind => "promotion") do 
      count = count + item.view_count.to_i
    end
    count
  end
  
  def newsletter_views
    count = 0
    for item in self.site.pages.where(:kind => "newsletter") do 
      count = count + item.view_count.to_i
    end
    count
  end
  
  def campaign_pages
    self.site.pages.where(:kind => "promotion")
  end
  
  def newsletter_pages
    self.site.pages.where(:kind => "newsletter")
  end
  
  include CompetitorsHelper  
  # Charts data functions
  def chart_social_exposure
    names = []
    values = []
    
    for ch in self.channels do 
      if ch.fans.where(:kind => "fan").count > 0
        names << ch.name
        values << ch.fans.where(:kind => "fan").count
      end
    end
    return names, values
  end


  def find_user(name)
    li = self.get_channel("linkedin")
    fs = self.get_channel("foursquare")
    fb = self.get_channel("facebook")
    
    find_user_on_social_networds(name, fb.token, li.token, li.secret, fs.token)
  end
  

  def chart_emails_by_status
    names = []
    values = []
    
    sent_emails_count =self.emails.where(:status => 'sent').count
    pending_emails_count = self.emails.where(:status => 'pending').count
    opened_emails_count = self.trackers.where("bulk_mail_id is NOT null").count
    unsubscribed_count = self.emails.where(:status => "unsubscribed").count
    
    
    
    return ["Sent","Pending","Opened","Unsubscribed"], [sent_emails_count,pending_emails_count,opened_emails_count,unsubscribed_count]
  end 
  
  
  # Charts data functions
  def chart_trackers_by_source
    
    email_count = 0
    facebook_count = 0
    articles_count = 0
    youtube_count = self.get_channel("youtube").views_count
    website_views = self.site.total_views
    
    for t in self.trackers do 
      email_count += t.visits.to_i if t.bulk_mail_id
      facebook_count += t.visits.to_i if t.channel_id
      articles_count += t.visits.to_i if t.article_id
    end
    
    names = ["Emails", "Facebook", "Articles","Youtube", "Website"]
    values = [email_count,facebook_count,articles_count,youtube_count,website_views]
    
    
    return names, values
  end
  
  
  
  def email_templates
    return Template.where(:kind => "email")
  end
  
  def emails_sent_today
    count=0
    for b in self.bulk_mails do 
      b.emails.each{|e| count = count + 1 if e.status.include? "sent" }
    end
    count
  end
  
  def emails_opened_today
    ar = []
    for b in self.bulk_mails do 
      b.engaged_trackers.each{|t| ar << t}
    end
    ar.count
  end
  
  
  def total_emails_sent
    count=0
    for b in self.bulk_mails do 
      b.emails.each{|e| count = count + 1 if e.status.include? "sent" }
    end
    count
  end
  
  
  def engaged_users
    self.fans.where(:kind => "engaged").count
  end
  
  
   
  def total_emails_opened
    ar = []
    for b in self.bulk_mails do 
      b.engaged_trackers.each{|t| ar << t}
    end
    ar.count
  end
  
  def potential_exposure
    # all my friends friends...
    c = 0
    for f in self.fans.where(:kind => "fan")
      if f.exposure
        c = c + f.exposure
      end
    end
    c
  end
    
  def estimated_exposure
    c = 0
    for f in self.fans.where(:kind => "engadge")
      if f.exposure
        c = c + f.exposure
      end
    end
    c
  end

  def tracked_views
    c = 0
    for t in self.get_channel("facebook").trackers
      c = c+ t.visits
    end
    c
  end

    
    
  def opened_trackers
    ar = []
    for b in self.bulk_mails do 
      b.opened_trackers.each{|t| ar << t}
    end
    ar
  end
   
  def engaged_trackers
    ar = []
    for b in self.bulk_mails do 
      b.engaged_trackers.each{|t| ar << t}
    end
    ar
  end
  


  def twitter_search_and_mentions
    twitter_followers_friends(self)
    twitter_multiple_search(self, self.twitter_search , "search_result")
    twitter_multiple_search(self, self.twitter_search  , "mention")
  end
  
  def update_channels
    create_channels
    reload
    
    fb = get_channel("facebook")
    fb.token = facebook_token
    fb.uid = "me"
    fb.save
    #    fb.scan
    
    tw = get_channel("twitter")
    tw.token = twitter_token
    tw.secret = twitter_secret
    tw.profile_name = twitter_user_id
    tw.save
    tw.scan
    
    
    yt = get_channel("youtube")
    yt.profile_name = youtube_url
    yt.profile_url = youtube_url
    yt.email = youtube_email
    yt.save
    yt.scan
    
    bl = get_channel("blogger")
    bl.profile_name = blogger_url
    bl.profile_url = blogger_url
    bl.email = blogger_email
    bl.save
    bl.scan
    
    wp = get_channel("wordpress")
    wp.profile_name = wordpress_url
    wp.profile_url = wordpress_url
    wp.email = wordpress_email
    wp.save
    wp.scan
    
    gm = get_channel("gmail")
    gm.email = gmail_email
    gm.secret = gmail_pwd
    gm.save
    gm.scan
    
    gp = get_channel("gmail")
    gp.email = gmail_email
    gp.secret = gmail_pwd
    gp.save
    gp.scan
    
    rd = get_channel("reddit")
    rd.email = reddit_user
    rd.secret = reddit_pwd
    rd.save
    rd.scan
    
    li = get_channel("linkedin")
    li.token = linkedin_token
    li.secret= linkedin_secret
    li.save
    li.scan


    rs = get_channel("rss")
    rs.email = gmail_email
    rs.secret= gmail_pwd
    rs.save
    rs.scan
    
    #    wp = get_channel("wordpress")
    #    bl = get_channel("blogger")
  end
  
  def get_channel(name)# facebook,twitter
    self.channels.where(:name => name).first
  end
  
  def create_channels
    #    self.channels.delete_all
    Channel.new(:brand_id => self.id , :name => 'facebook').save if get_channel("facebook").nil?
    Channel.new(:brand_id => self.id , :name => 'twitter').save if get_channel("twitter").nil?
    Channel.new(:brand_id => self.id , :name => 'youtube').save if get_channel("youtube").nil?
    Channel.new(:brand_id => self.id , :name => 'foursquare').save if get_channel("foursquare").nil?
    Channel.new(:brand_id => self.id , :name => 'wordpress').save if get_channel("wordpress").nil?
    Channel.new(:brand_id => self.id , :name => 'blogger').save if get_channel("blogger").nil?
    Channel.new(:brand_id => self.id , :name => 'gmail').save if get_channel("gmail").nil?
    Channel.new(:brand_id => self.id , :name => 'google').save if get_channel("google").nil?
    Channel.new(:brand_id => self.id , :name => 'reddit').save if get_channel("reddit").nil?
    Channel.new(:brand_id => self.id , :name => 'linkedin').save if get_channel("linkedin").nil?
    Channel.new(:brand_id => self.id , :name => 'rss').save if get_channel("rss").nil?
  end

  
  require "facebook_helper"
  include FacebookHelper
  
  def scan_groups
    scan_for_groups(self)
  end
  
  def facebook_posts
    facebook_posts_and_comments(self)
  end
  
  def facebook_fans_count
    self.fans.where( :facebook => true).count
  end
  
  def facebook_page_views_count
    rand(100)
  end
  
  def facebook_comments_count
    rand(100)
  end
  
  
  
  def twitter_fans_count
    self.fans.where( :twitter => true).count
  end
  
  def twitter_page_views_count
    rand(100)
  end
  
  def twitter_comments_count
    rand(100)
  end
  
  
  
  def google_fans_count
    self.fans.where(:channel => 'google').count
  end
  
  def google_posts_count
    self.posts.where(:network => 'google').count
  end
  
  def google_comment_count
    self.posts.where(:network => 'google').count
  end
  
  
  def scan
  end
  
  
  def group_scan
    scan_for_groups(self)
  end
  
  
  def site 
    self.websites.first
  end 


    
  
  def create_defult_website
    return if self.websites.count > 0
    site = Website.new(:brand_id => self.id, :template_id => Template.first.id)
    site.save
    Page.new( :website_id => site.id, :name => "Home").save
    Page.new( :website_id => site.id, :name => "Products").save
    Page.new( :website_id => site.id, :name => "About").save
    Page.new( :website_id => site.id, :name => "Contact Us").save
    Page.new( :website_id => site.id, :name => "public" , :kind => "facebook_page").save
    Page.new( :website_id => site.id, :name => "private", :kind => "facebook_page").save
    for p in site.pages do 
      p.create_default_content
    end
    
  end
    
  
end

