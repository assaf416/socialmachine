class Email < ActiveRecord::Base
  belongs_to :brand
  belongs_to :fan
  belongs_to :bulk_mail
  belongs_to :template
  has_many :trackers
  
  
  
  def pending?
    self.status == "pending"
  end
  
  def total_viewed
    self.trackers.where(:engaged => false).count
  end
  
  def total_clicked
    self.trackers.where(:engaged => false).count
  end
  
  
  include EmailsHelper
  
  def views_track_image
    "http://23.21.17.104/assets/this-is-the-email-image.png?token=#{rand(100000)}&email=#{self.id}"
  end
  
  
  
  
  
  
  require 'gmail_sender'
  def send_email(full_url)
    begin
      ch = self.brand.get_channel("gmail")
      g = GmailSender.new("assaf.goldstein",ch.secret)
      g.send(
#        :from_email => "<#{self.from_email}>",
        :to => "#{self.to_name} <#{self.to_email}>",
        :subject => self.subject,
        :content =>  self.preview,
        :content_type => "text/html; charset='utf-8'"
      )
      puts "sent"
      self.status = "sent"
      self.save
    rescue Exception => e
      self.status = e.message
      self.save
      puts g.inspect
      puts "FAILED TO SEND EMAIL to #{self.to_email}:  #{e.message}"
    end
  end
    

  
  def email_sender(user,pwd,from_email,to_email,subject, email_file)
    begin
      cmd = " smtp-cli --verbose --host smtp.gmail.com --user='#{user}' --auth-plain --from='#{from_email}' --to='#{to_email}' --pass='#{pwd}' --subject='#{subject}' --body-html='#{email_file}' >> ~/logs/gmail_sender.log"
      exec(cmd)
      logger.error "STATUS:: CALLING #{cmd}"
      #      sts = `#{cmd} `
      logger.error "RUNNING STATUS : #{sts}"
    rescue Exception => e
      logger.error "FAILED TO RUN THE smtp-cli. #{e.message} #{cmd}"
    end
  end
  
  
  def _send_email(full_url)
    logger.error " STATUS SENDINGMAIL"
    url = "";n = 0
    full_url.chars.each{ |c |
      
      n = n + 1 if c == "/"
      url << c
      
      break if n == 3
    }
    url << "api/email_status?email_id=#{self.id}&status="
    ch = Brand.find(self.brand_id).get_channel("gmail")
    
    the_email = { 
      :user => ch.email , :pwd => ch.secret,
      :email_id => self.id, :url =>  url,
      :to_email => self.to_email, :to_name => self.to_name,
      :from_name => self.from_name, :from_email => self.from_email,
      :subject => self.subject,
      :html => self.preview , :text => self.text_body
    }
    begin
      email_file = "/home/ubuntu/scanner/#{self.brand_id}.email.#{self.id}.yml"
      email_data = "/home/ubuntu/scanner/#{self.brand_id}.email.#{self.id}.dta"
      email_log_file = "/home/ubuntu/logs/email_sender.log"
      File.open(email_file, 'w') { |file| file.write(the_email.to_yaml) }
      File.open(email_data, 'w') { |file| file.write(self.preview) }
      email_sender(ch.email.gsub("@gmail.com",""),ch.secret,ch.email,self.to_email,self.subject, email_data)
    
    
    rescue Exception => e
      logger.error "FAILED ON CALLING THE gamil_sender on Email.rb  Message:  #{e.message}"
    end
  end
  
  
  
  def preview
    vv = self.html_body
    vv.gsub!("#user",self.to_name)
    vv.gsub!("user#",self.to_name)
    vv.gsub!("UNSUBSCRIBE-URL","http://www.socialmachine.biz/public/unsubscribe_from_email?token=#{self.unique_token}&email=#{self.id}")
    vv.gsub!("TRACKING-IMAGE","http://23.21.17.104/assets/this-is-the-email-image.png?token=#{self.unique_token}&email=#{self.id}")
    vv
  end
end
