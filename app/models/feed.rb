class Feed < ActiveRecord::Base
  belongs_to :brand
  has_many :feed_entries
  
  def load
  	logger.error "LOADING RSS FEEDS for #{self.url}"
    begin
      FeedEntry.update_from_feed(self.url , self)
    rescue Exception => e
      logger.error "FAILED LOADING RSS Feed #{self.url} error: #{e.message} " 
    end
  end
end
