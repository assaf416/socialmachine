class Score < ActiveRecord::Base
  belongs_to :channel
  belongs_to :brand
  belongs_to :competitor
end
