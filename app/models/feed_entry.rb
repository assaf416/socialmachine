class FeedEntry < ActiveRecord::Base
  
  include ApplicationHelper
  belongs_to :feed
  def self.update_from_feed(feed_url, feedx)
    feed = Feedzirra::Feed.fetch_and_parse(feed_url)
    add_entries(feed.entries,feedx)
  end
  
  
  
  
  def post_to_twitter(c)
    Twitter.configure do |config|
      config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
      config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
      config.oauth_token = c.token
      config.oauth_token_secret = c.secret
    end
    Twitter.update(self.body[0..138])
  end
  
  
  
  def plain_text
    begin
      text = strip_html(self.body)
      unless self.feed.clear_script.nil? or self.feed.clear_script.empty? then
        p = text.index(self.feed.clear_script)
        text[0..p-1]
      else
        text
      end
    rescue
      text = strip_html(self.body)
    end
  end
 
  require 'nokogiri'
  def icon
    doc = Nokogiri::HTML(self.body.to_s)
    img_srcs = doc.css('img').map{ |i| i['src'] }
    return img_srcs[0] unless img_srcs.empty?
    return self.feed.icon
  end
  
  def next_item  
    return FeedEntry.where("id > #{self.id}").first.id || self.id
  end

  def prev_item
    return FeedEntry.where("id < #{self.id}").last.id || self.id
  end
  

  private
  def self.add_entries(entries,feed)
    entries.each do |entry|
      begin
        unless exists? :uid => entry.id
          entry.sanitize!
          unless entry.title.nil?
            create!(
              :title     => entry.title,
              :body      => entry.summary,
              :url       => entry.url,
              :image_url => entry.url,
              :posted_at => entry.published,
              :uid       => entry.id,
              :feed_id  => feed.id
            ) unless  entry.summary.nil? 
          end
        end
      rescue Exception => e
        logger.error  "FAILED TO CREATE FEED ENTRY FROM #{feed.inspect} #{e.message}"
        next
      end
    end
  end

  
end
