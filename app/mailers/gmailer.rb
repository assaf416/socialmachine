class Gmailer < ActionMailer::Base
  #default from: "from@example.com"
  
  def send_gmail_email(user,pwd, email)
    logger.error "STATUS ABOUT TO SEND EMAIL"
    
#    ActionMailer::Base.smtp_settings = {
#      :tls => true,
#      :address => "smtp.gmail.com",
#      :port => "587",
#      :domain => 'gmail.com',
#      :authentication => :plain,
#      :user_name => user.gsub("@gmail.com",""),
#      :password => pwd
#    }
    
    ActionMailer::Base.smtp_settings = {
      address:              'smtp.gmail.com',
      port:                 465,
      domain:               'gmail.com',
      user_name:            user.gsub("@gmail.com",""),
      password:             pwd,
      authentication:       :plain,
      enable_starttls_auto: false,
      ssl:                  true
    }
    
    logger.error  " HERE IS THE ACTION MAILER SETTING: #{ActionMailer::Base.smtp_settings.inspect}"
    
    x_from = "<#{user}>"
    x_to   = "#{email.to_name}<#{email.to_email}>"
    
    
    @html_conent = email.preview
    @text_content = email.text_body
    @user = user
    @url  = "http://example.com/login"
    begin
      mail(:from => x_from, :to => x_to, :subject =>email.subject)
      logger.error "AFTER SEND EMAIL #{mail.inspect}"
    rescue Exception => e
      logger.error "FAILED TO SEND EMAIL : ERROR #{e.message}"
    end
    
  end
end
