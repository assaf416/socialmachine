json.id @fan.id
json.name @fan.name
json.picture @fan.profile_image
json.kind @fan.kind
json.network @fan.channel.name
json.home_page @fan.profile_url_or_home
json.email @fan.email
json.bio @fan.short_bio
json.fans_count number_with_delimiter @fan.friends_count
json.posts_count @fan.posts_count.to_i
json.last_post  "3 days"
json.likes  number_with_delimiter @fan.likes
json.talking_about  number_with_delimiter @fan.talking_about