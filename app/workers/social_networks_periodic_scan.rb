class SocialNetworksPeriodicScan
  
  
  @queue = :periodic_social_scan
  
  def self.perform(brand_id)
    @brand = Brand.find(brand_id)
    puts "SCAN SOCiAL AND INTERNET FOR BRAND MENTIONING for #{@brand.name}"
    @brand.social_scan
  end
  
end
  
