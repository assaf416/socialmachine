/*
 * jQuery FacebookFeed Plugin
 * Requires jQuery 1.4.2
 * Requires jQuery Templates Plugin 1.0.0pre
 * Author Vladimir Shugaev <vladimir.shugaev@junvo.com>
 * Copyright Vladimir Shugaev <vladimir.shugaev@junvo.com>
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 */
(function($){
    $.fn.facebookfeed=function(options){
        var settings={
            group:'wewew',
            id: '145975662131224', //id of the facebook entity
            template: '<tr><td><img src="https://graph.facebook.com/${from.id}/picture" /></td><td><h4>${from.name}</h4><p>${message}</p><p>Read more:&nbsp;<a href="${link}">${name}</a> :: ${type} :: </p><br/>(( ${options.group}</td></tr>', //template for formatting each feed entry
            query: {},
            access_token: ''
        };
        if (options)
            $.extend(settings, options);
        var container=this;
        var requestURL='https://graph.facebook.com/'+settings.id+'/feed?access_token='+settings.access_token+'&'+$.param(settings.query)+'&callback=?'; //calback=? is required to get JSONP behaviour
        var template=$.template(null, settings.template);

        $.getJSON(requestURL, function(json){
            var messages=$.tmpl(template, json.data).appendTo(container);
        });
        return this;
    };
    
    
    
    $.fn.facebookprofile=function(options){
         
        var settings={
            id: '145975662131224', //id of the facebook entity
            template: '<img src="https://graph.facebook.com/${id}/picture" /><h4>${id}</h4><p>${category}</p><p><img src="${picture}"/></p><p>likes:${likes}:&nbsp;<a href="${link}">${name}</a></p>', //template for formatting each feed entry
            query: {},
            access_token: ''
        };
        if (options)
            $.extend(settings, options);
        var container=this;
        var requestURL='https://graph.facebook.com/'+settings.id; //calback=? is required to get JSONP behaviour
        var template=$.template(null, settings.template);

        $.getJSON(requestURL, function(json){
            var messages=$.tmpl(template, json).appendTo(container);
        });
        return this;
    };
    
    $.fn.facebookevents=function(options){
         
        var settings={
            id: '145975662131224', //id of the facebook entity
            template: '<td><img src="https://graph.facebook.com/${id}/picture" /></td><td><h4>${id}</h4><p>${name}<br/>${description}</p><p> time: ${start_time}:${end_time} ==> ${location}</p><p>likes:${likes}:&nbsp;<a href="${link}">${name}</a></p></td></tr>', //template for formatting each feed entry
            query: {},
            access_token: ''
        };
        if (options)
            $.extend(settings, options);
        var container=this;
        var requestURL='https://graph.facebook.com/'+settings.id+'/events?access_token='+settings.access_token+'&'+$.param(settings.query)+'&callback=?'; //calback=? is required to get JSONP behaviour
        var template=$.template(null, settings.template);

        $.getJSON(requestURL, function(json){
            var messages=$.tmpl(template, json.data).appendTo(container);
        });
        return this;
    };
    
    $.fn.facebookgroups=function(options){
         
        var settings={
            id: '145975662131224', //id of the facebook entity
            template: '<td><img src="https://graph.facebook.com/${id}/picture" /></td><td><h4>${id}</h4><p>${name}<br/>${description}</p><p> time: ${start_time}:${end_time} ==> ${location}</p><p>likes:${likes}:&nbsp;<a href="${link}">${name}</a></p></td></tr>', //template for formatting each feed entry
            query: {},
            access_token: ''
        };
        if (options)
            $.extend(settings, options);
        
        var container=this;
        var requestURL='https://graph.facebook.com/'+settings.id+'/groups?access_token='+settings.access_token+'&'+$.param(settings.query)+'&callback=?'; //calback=? is required to get JSONP behaviour
        var template=$.template(null, settings.template);

        $.getJSON(requestURL, function(json){
            var messages=$.tmpl(template, json.data).appendTo(container);
        });
        return this;
    };
    
    
})(jQuery);

