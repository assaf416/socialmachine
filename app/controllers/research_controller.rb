class ResearchController < ApplicationController
  layout 'social'
  require 'nokogiri'
  include FacebookHelper
  include GooglePlusHelper
  include ResearchHelper
  
  def index
    @url= params[:q]
    #    { :links => ar, :meta => meta_description, :keywords => keywords}
    @scan = scan_url(@url)
    @links = @scan[:links] || []
    @meta = @scan[:meta]
    @keywords = @scan[:keywords]
    @images = @scan[:images]
    @google_results = google_search( @url)
    
  end

  def rss
  end

  def twitter
    @v = ""
    if params[:post]
      
      doc = Nokogiri::HTML(open(params[:post]))
      doc.css('.entry-content').each do |article|
        @v << article.content.inspect
      end
      doc.css('.body-copy').each do |article|
        @v << article.content.inspect
      end
    end
  end

  def groups
  end
  
  def people
    if params[:post].nil?
      search = current_user.name.gsub(" ","+") 
    else
      search = params[:post].gsub(" ","+") 
    end
    
    @tw_search = [] # in google.com with site -> twitter.com and grab twitter details
    @fb_users = []
    @fb_groups = []
    @fb_pages = []
    @google_search = []
    @youtube_search = []
    @res_google = []
    
    
    puts " --- >  #{Time.now}  before google search" 
    @res_google = google_search("'" + search+ "'")
    puts " --- >  #{Time.now}  before google plus search" 
    @google_search = [] #google_search_user(search)
    puts " --- >  #{Time.now}  before FB User search" 
    @fb_users  = fb_user_search(search)
    #    @fb_groups  = fb_group_search(search)
    puts " --- >  #{Time.now}  before FB Pages search" 
    @fb_pages  = fb_page_search(search)
    puts " --- >  #{Time.now}  after FB Pages search" 
  end
    
  
end
