class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user
  before_filter :prepare_for_mobile 
  before_filter :authorize 
  before_filter :scan_channels

  
  
  def scan_channels # runs a scan 
  end
  

  def mobile_device?
    
    return false if   request.user_agent =~ /iPad/
    
    if session[:mobile_param]
      session[:mobile_param] == "1"
    else
      request.user_agent =~ /Mobile|webOS|iPhone/
    end
  end
  helper_method :mobile_device?

  def prepare_for_mobile
    session[:mobile_param] = params[:mobile] if params[:mobile]
    request.format = :mobile if mobile_device?
  end
  
  private

  
  def mongo_connector
    return @db_connection if @db_connection
    @db_connection = Mongo::Connection.new("dharma.mongohq.com","10074").db("socialmachine")
    @db_connection.authenticate("assaf", "clipper") #unless (db.user.nil? || db.user.nil?)
    @db_connection
  end
  
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  def authorize
    redirect_to login_path , alert: "Not authorized" if current_user.nil?
  end
end
