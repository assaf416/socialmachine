class PostsController < ApplicationController
  
  
  def post_to
    network = params[:channel]
    page_id = params[:page]
    post  = Post.find(params[:id])
    redirect = params[:redirect]
    
    post.post_to_facebook(page_id) if network == "facebook"
    post.post_to_twitter if network == "twitter"
    post.post_to_linkedin(current_user.brand.get_channel("linkedin")) if network == "linkedin"
    
    redirect_to params[:redirect] , :notice => "Posted to #{network}"
  end

  def email_action
    @post = Post.find(params[:id])
    the_notice = "Email updated"
    if params[:command].include? "autorespon"
      Autoresponder.create!( :name => @post.title, :short_message => @post.short_body , :long_message => @post.body, :brand_id => current_user.brand.id)
      the_notice = "Email content was copied"
    else
      @post.network = params[:command]
      @post.save
    end
    redirect_to params[:redirect_to] , :notice => the_notice
  end
  
  
  def repost
    @post = Post.find(params[:id])
    @brand= current_user.brand
    if params[:channel] == "facebook" and params[:page_id]
      @post.post_to_facebook(params[:page_id])
    end
    
    @post.post_to_twitter if params[:channel] == "twitter" 
    @post.post_to_linkedin if params[:channel] == "linkedin" 
   
    unless @post.target_link.nil?
      Google::UrlShortener::Base.api_key = "AIzaSyDQU97mjsTO_qfzAHNcfguvhcd2CNvv6Yw"
      url = Google::UrlShortener::Url.new(:long_url => @post.target_link)
      if @brand.short_urls.nil?
        @brand.short_urls = "#{url},"
      else
        @brand.short_urls << "#{url},"
      end
      @brand.save
      @post.short_url =  url.shorten! 
    else
      @post.short_url = "" 
    end
    redirect_to params[:redirect] ,  notice: "Posted to #{params[:channel]}."
  end
  
  def deploy_to_facebook
    raise params.inspect
    @post = Post.find(params[:id])
    if params[:content]
      @post.post_to_facebook(@post.brand.get_channel("facebook").fan_pages.first["id"])
    else
      @post.post_text_to_facebook(@post.brand.get_channel("facebook").fan_pages.first["id"], params[:content])
    end
    redirect_to params[:redirect]
  end
  
  def retweet
    @post = Post.find(params[:id])
  end
  
  def add_to_delayed
    @post = Post.find(params[:id])
    if params[:now] then 
      @post.post_to_twitter
    else
      @post.make_delayed_twit
    end
    
    if params[:redirect] then
      redirect_to "#{params[:redirect]}" , notice: 'Delayed Twit Added.'
    else
      redirect_to "/brands/#{@post.brand.id}" , notice: 'Delayed Twit Added.'
    end
  end
  
  
  
  def preview
    @post =current_user.brand.posts.find(params[:id])
    @post.opened_at = Time.now
    @post.save
    render :layout => nil
  end
  
  # GET /posts
  # GET /posts.json
  def index
    if params[:kind] then 
      if params[:channel]
        @posts = current_user.brand.posts.where(:kind => params[:kind], :channel_id => params[:channel])
      else
        @posts = current_user.brand.posts.where(:kind => params[:kind])
      end
    else
      @posts = current_user.brand.posts.where(:kind => "my-post").order("posted_at DESC")
      @comments = current_user.brand.comments.where("channel_id is not null")
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @posts }
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @post = Post.find(params[:id])

    #    if @post.kind == "inbox"
    #      @post.rank = -1   
    #      @post.save
    #    end
    
    if @post.kind == "inbox-unread"
      @post.kind = "inbox"
      @post.save
    end
    
  end

  # GET /posts/new
  # GET /posts/new.json
  def new
    @post = Post.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/1/edit
  def edit
    @post = Post.find(params[:id])
  end

  # POST /posts
  # POST /posts.json
  def create
    @brand = current_user.brand
    li = @brand.get_channel("linkedin")
    @post = Post.new(params[:post])
    @post.convert_to_facebook_message if @post.kind.include? "facebook-post"
    
    if @post.kind.include? "linkedin"
      @post.post_to_linkedin(li)
    end
    
    if params[:deploy]
      
      unless @post.target_link.nil?
        Google::UrlShortener::Base.api_key = "AIzaSyDQU97mjsTO_qfzAHNcfguvhcd2CNvv6Yw"
        url = Google::UrlShortener::Url.new(:long_url => @post.target_link)
        if @brand.short_urls.nil?
          @brand.short_urls = "#{url},"
        else
          @brand.short_urls << "#{url},"
        end
        @brand.save
        @post.short_url =  url.shorten! 
      else
        @post.short_url = ""
      end
      
      
      if  params[:deploy]["linkedin"] == "yes"
        @post.post_to_linkedin(@brand.get_channel("linkedin"))
      end
      
      @post.convert_to_article(current_user.brand.id) if params[:deploy][:reblog] == "yes"
      
      params[:deploy].each{| k,v| 
        if k.include? "fan-page-" and v == "yes" then         
          @post.post_to_facebook(k.gsub("fan-page-",""))
        end
      }
      
    end
    
    if params[:redirect]
      redirect_to params[:redirect], notice: 'Post was successfully posted.'
    else
      redirect_to "/channels/#{li.id}", notice: 'Post was successfully posted.'
    end
  end

  # PUT /posts/1
  # PUT /posts/1.json
  def update
    @post = Post.find(params[:id])

    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post = Post.find(params[:id])
    @post.comments.delete_all
    @post.destroy
    respond_to do |format|
      format.html {
        if params[:redirect_to]
          redirect_to params[:redirect_to]
        else
          redirect_to "/posts/?kind=#{params[:kind]}" 
        end
      }
      format.json { head :no_content }
    end
  end
end
