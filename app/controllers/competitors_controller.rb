class CompetitorsController < ApplicationController
  include CompetitorsHelper
  def index
    @brand = current_user.brand
    @competitors = current_user.brand.competitors

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @competitors }
    end
  end

  def show
    @brand = current_user.brand
    @competitor = Competitor.find(params[:id])
     @name_search_results = Fan.search_on_social_networks(@competitor.name, current_user.brand)
    @google_results =  [] #google_search(@competitor.name)
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @competitor }
    end
  end

  # GET /competitors/new
  # GET /competitors/new.json
  def new
    @competitor = Competitor.new(params[:f])
    @competitor.brand_id = current_user.brand.id
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @competitor }
    end
  end

  # GET /competitors/1/edit
  def edit
    @competitor = Competitor.find(params[:id])
  end

  # POST /competitors
  # POST /competitors.json
  def create
    @competitor = Competitor.new(params[:competitor])

    respond_to do |format|
      if @competitor.save
        @competitor.delay.scan
        format.html { redirect_to @competitor, notice: 'Competitor was successfully created.' }
        format.json { render json: @competitor, status: :created, location: @competitor }
      else
        format.html { render action: "new" }
        format.json { render json: @competitor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /competitors/1
  # PUT /competitors/1.json
  def update
    @competitor = Competitor.find(params[:id])

    respond_to do |format|
      if @competitor.update_attributes(params[:competitor])
        format.html { redirect_to @competitor, notice: 'Competitor was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @competitor.errors, status: :unprocessable_entity }
      end
    end
  end

  
  
  def scan
    @competitor = Competitor.find(params[:id])
    @competitor.scan
    redirect_to @competitor
  end
  
  # DELETE /competitors/1
  # DELETE /competitors/1.json
  def destroy
    @competitor = Competitor.find(params[:id])
    @competitor.posts.delete_all
    @competitor.scores.delete_all
    @competitor.destroy

    respond_to do |format|
      format.html { redirect_to "/competitors" }
      format.json { head :no_content }
    end
  end
end
