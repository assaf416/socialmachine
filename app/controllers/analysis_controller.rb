class AnalysisController < ApplicationController
  include AnalysisHelper
  #  require 'linkedin'
  def index
    # get your api keys at https://www.linkedin.com/secure/developer
    client = LinkedIn::Client.new("y1ifejqt5oss", "0EI8fpBTNyOf1G0U")
    request_token = client.request_token(:oauth_callback => "http://#{request.host_with_port}/auth/callback")
    session[:rtoken] = request_token.token
    session[:rsecret] = request_token.secret
    redirect_to client.request_token.authorize_url
  end

  
  def callback
    client = LinkedIn::Client.new("your_api_key", "your_secret")
    if session[:atoken].nil?
      pin = params[:oauth_verifier]
      atoken, asecret = client.authorize_from_request(session[:rtoken], session[:rsecret], pin)
      session[:atoken] = atoken
      session[:asecret] = asecret
    else
      client.authorize_from_access(session[:atoken], session[:asecret])
    end
    @profile = client.profile
    @connections = client.connections
  end
  
  
  def fans
  end

  def competitors
  end

  def social_networks
  end

  def mentions
  end

  def website
    if params[:site]
      @page = get_page(params[:site])
      unless @page.nil?
        @backlinks_count = "" #backlinks_count(params[:site])
        @backlinks = backlinks_yahoo(params[:site])
        @meta_data = meta_description(@page)
        @keywords = meta_keywords(@page)
        @words = words(@page)
        @links = page_links(@page)
      end
    else
      @keywords = []
      @backlinks = []
      @links = []
    end
  end

  def mailing_list
  end
end
