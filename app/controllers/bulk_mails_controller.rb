class BulkMailsController < ApplicationController
  # GET /bulk_mails
  # GET /bulk_mails.json
  
  def move_article
    @article = params[:article]
    @bulk_mail = params[:id]
    notice = ""
    if params[:action] == "up" then 
      notice = "Article #{@article.name} moved up"
    end
    if params[:action] == "down" then 
      notice = "Article #{@article.name} moved down"
    end
    if params[:action] == "remove" then 
      @article.bulk_mail_id = nil
      @article.save
      notice = "Article #{@article.name} removed"
    end
    
    redirect_to @bulk_mail, :notice => notice
  end
  
  def check
    Tracker.delay.load
    redirect_to "/bulk_mails"
  end
  
  def duplicate
    b = BulkMail.find(params[:id])
    b.duplicate
    redirect_to "/bulk_mails"
    
  end
  
  #  require 'gdata'
  #  require 'contacts'
  def import

  end

  
  def stop
    @b = BulkMail.find(params[:id])
    @b.emails.delete_all
    redirect_to @b
  end

  def start
    @b = BulkMail.find(params[:id])
    @b.create_emails
    redirect_to @b
  end
  
  def index
    @show_campaigns = params[:campaign]
    @brand = current_user.brand
    #    @trackers = @brand.opened_trackers
    @trackers = Tracker.where("ADDRESS IS NOT NULL").order("logged_at DESC").limit(15)
    @bulk_mails = BulkMail.all
    @autoresponders = @brand.autoresponders
    @newsletters = BulkMail.where(:kind => "newsletter", :brand_id => current_user.brand.id)
    @promotions = BulkMail.where(:kind => "promotion", :brand_id => current_user.brand.id)
    @unsubscribed = @brand.emails.where(:status => "unsubscribed")
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bulk_mails }
    end
  end
  
  
  
  def configure
    @bulk_mail = BulkMail.first
  end
  def preview
    @bulk_mail = BulkMail.find(params[:id])
    if @bulk_mail.newsletter?
      @newsletter_templates = Template.where(:kind => "newsletter")
    else
      @newsletter_templates = Template.where(:kind => "email")
    end
    if params[:version].present? and params[:version] == "text"
      @version = "text"
    else
      @version = "html"
    end
      
  end


  # GET /bulk_mails/1
  # GET /bulk_mails/1.json
  def show
    @bulk_mail = BulkMail.find(params[:id])
    @brand = current_user.brand
    @articles = @bulk_mail.articles
    @articles_to_add = @brand.articles.where("bulk_mail_id is NULL")
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bulk_mail }
    end
  end

  # GET /bulk_mails/new
  # GET /bulk_mails/new.json
  def new
    @bulk_mail = BulkMail.new
    @bulk_mail.kind = params[:kind]
    @brand = current_user.brand
    @pages = @brand.site.pages
    @articles = @bulk_mail.articles
    @articles_to_add = @brand.articles.where("bulk_mail_id is NULL")
    @templates = Template.where(:kind => "email")
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bulk_mail }
    end
  end

  # GET /bulk_mails/1/edit
  def edit
    @brand = current_user.brand
    @pages = @brand.site.pages
    @articles_to_add = @brand.articles.where("bulk_mail_id is NULL")
    @templates = Template.where(:kind => "email")
    @bulk_mail = BulkMail.find(params[:id])
    @articles = @bulk_mail.articles
  end

  # POST /bulk_mails
  # POST /bulk_mails.json
  def create
    
    ar_fans = []
    
    for p in params do 
      if p[0].include? "fan-"
        ar_fans << p[0].gsub("fan-","")
      end
    end
    @bulk_mail = BulkMail.new(params[:bulk_mail])
    @bulk_mail.brand_id = current_user.brand.id
    respond_to do |format|
      
      if @bulk_mail.save
        format.html { redirect_to @bulk_mail, notice: 'Bulk mail was successfully created.' }
        format.json { render json: @bulk_mail, status: :created, location: @bulk_mail }
      else
        format.html { render action: "new" }
        format.json { render json: @bulk_mail.errors, status: :unprocessable_entity }
      end
    end
  end

  
  def add_articles
    articles_ids = []
    params.each{ |key,value| articles_ids << key if value == "on"}
    @bulk_mail = BulkMail.find(params[:bulk_mail_id])
    @bulk_mail.add_articles(articles_ids)
    redirect_to "/bulk_mails/#{@bulk_mail.id}" , :notice => "Article(s) added "
  end
  
  # PUT /bulk_mails/1
  # PUT /bulk_mails/1.json
  def update
    @bulk_mail = BulkMail.find(params[:id])
    @bulk_mail.brand_id = current_user.brand.id
    ar_fans = []
    
    for p in params do 
      if p[0].include? "fan-"
        ar_fans << p[0].gsub("fan-","")
      end
    end
    
    list = ""
    ar_fans.each { |f| list << "#{f},"  }
    list << "-1"
    @bulk_mail.fan_list  = list
    
    
    respond_to do |format|
      if @bulk_mail.update_attributes(params[:bulk_mail])
        format.html {
          if params[:bulk_mail].present? and params[:bulk_mail][:template_id].present?
            redirect_to @bulk_mail, notice: 'Bulk mail was successfully updated.' 
#            redirect_to "/bulk_mails/preview?id=#{@bulk_mail.id}", notice: 'Bulk mail was successfully updated.' 
          else
            redirect_to @bulk_mail, notice: 'Bulk mail was successfully updated.' 
          end
        }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bulk_mail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bulk_mails/1
  # DELETE /bulk_mails/1.json
  def destroy
    @bulk_mail = BulkMail.find(params[:id])
    @bulk_mail.emails.delete_all
    @bulk_mail.emails.delete_all
    
    @bulk_mail.destroy
    

    respond_to do |format|
      format.html { redirect_to bulk_mails_url }
      format.json { head :no_content }
    end
  end
end
