class ApiController < ApplicationController
  
  require "yaml"
  require "json"
  include ChannelsHelper
  
  def pending_emails 
    brands =[]
    for brand in Brand.all do 
      info = {}
      info[:brand_id] = brand.id
      info[:brand_name] = brand.name
      info[:sender_email] = brand.get_channel("gmail").email
      info[:sender_secret] = brand.get_channel("gmail").secret
      
      emails = []
      for item in brand.emails.where(:status => "pending").limit(2) do 
        emails << {
          :id => item.id,
          :to_email => item.to_email, :to_name => item.to_name, :send_on => item.send_on,
          :token => item.unique_token, :subject => item.subject, :body_html => item.html_body, :body_text => item.text_body
        }
      end
      info[:emails] = emails
      brands << info
    end
    
    render :text => brands.to_yaml
  end
  
  def process_emails
    
  end
  
  
  def channel_scanned
    channel = Channel.find(params[:id])
    channel.scanned_at = Time.now
    channel.save
    render :text => "THANKS"
  end
  
  def email_status
    email = Email.find(params[:email_id])
    email.status = params[:status]
    email.save
    render :text => "THANKS"
  end
  
  def brands
    list = []
    for brand in Brand.all do 
      list << brand.id
    end
    render  :text => list.to_yaml
  end
  
  def scan_plan
    @plan = []
    brand = Brand.find(params[:id]) 
    
    fb = brand.get_channel("facebook")
    tw = brand.get_channel("twitter")
    yt = brand.get_channel("youtube")
    fs = brand.get_channel("foursquare")
    gm = brand.get_channel("gmail")
    li = brand.get_channel("linkedin")
    rs = brand.get_channel("rss")
    wp = brand.get_channel("wordpress")
    bl = brand.get_channel("blogger")
      
    fb_fan_uids = ""; fb.fans.where(:kind => "fan").each{|f| fb_fan_uids << "#{f.facebook_uid},"}
    tw_fan_uids = "";  tw.fans.where(:kind => "fan").each{|f| tw_fan_uids << "#{f.twitter_uid},"} 
    li_fan_uids = "";  li.fans.where(:kind => "fan").each{|f| li_fan_uids << "#{f.linkedin_uid},"} 
    
       
    fb_urls = "" ;fb.posts.where('short_url is not null').each{|f| fb_urls << "#{f.short_url},"}
    tw_urls = "" ;tw.posts.where('short_url is not null').each{|f| tw_urls << "#{f.short_url},"}
    li_urls = "" ;li.posts.where('short_url is not null').each{|f| li_urls << "#{f.short_url},"}
    fs_urls = "" ;fs.posts.where('short_url is not null').each{|f| fs_urls << "#{f.short_url},"}
    
    articles_urls = []; brand.articles.each{|a| articles_urls << {:id => a.id, :url => a.short_url}  unless a.short_url.nil?}
    
    competitors = []
    for c in brand.competitors do 
      competitors << {  :id => c.id,:name => c.name, :facebook_fan_page => c.facebook_page , :twitter_name => c.twtitter_name,
        :linkedin => c.linkedin_company_page, :youtube_channel => c.youtube_rss  }
    end
    
    rss_feeds =[]
    rs.fans.each{|feed| rss_feeds << {:id => feed.id, :url => feed.website}}
    
    @plan << {
      :brand => brand.id, :brand_name => brand.name, :website_id => brand.site.id,:short_urls => brand.short_urls.to_s,
      :google_token => brand.google_token,:google_secret => brand.google_secret,
      :content_search => brand.content_search, 
      :competitor_search => brand.competitor_search, 
      :prospect_search => brand.prospect_search,
      :group_search => brand.group_search, 
      :job_search => brand.jobs_search,
      :website_url => "/site/#{brand.site.id}",
        
      :facebook_token => fb.token , :facebook_fan_uids => fb_fan_uids, :facebook_urls => fb_urls, :facebook_ingore_ids => fb.ignore_ids,
          
      :twitter_token => tw.token ,:twitter_secret => brand.twitter_secret , :twitter_profile_name => tw.profile_name ,  :twitter_fan_uids => tw_fan_uids,
      :twitter_ingore_ids => tw.ignore_ids, :twitter_uid => tw.uid,
      #      :twitter_search_1 => tw.search_1 , :twitter_search_2 => tw.search_2 , :twitter_search_3 => tw.search_3 , :twitter_search_4 => tw.search_4, 
      :twitter_urls => tw_urls,
      :google_plus_token => brand.google_token, :google_plus_profile_id => brand.google_page,
      :youtube_profile_name => yt.home_page_short_url,:youtube_search_1 => yt.search_1, :youtube_user => yt.uid, :youtube_pwd => yt.secret,
      
      
      :linkedin_token => brand.linkedin_token , :linkedin_fan_uids => li_fan_uids,:linkedin_urls => li_urls,
      :linkedin_ingore_ids => li.ignore_ids,
      :linkedin_secret => brand.linkedin_secret , 
      :linkedin_pin =>brand.linkedin_pin, 
      
      :wordpress_url => wp.posts_feed,  :blogger_url => bl.posts_feed,
      
      :foursquare_token => fs.token, :foursquare_urls => fs_urls,
      :gmail_email => gm.email,:gmail_secret => gm.secret,
      :article_urls => articles_urls,
      :website_url => brand.site.url,
      :competitors => competitors,
      :rss_feeds => rss_feeds,
      :fb_cid =>fb.id,
      :tw_cid => tw.id,
      :yt_cid => yt.id,
      :fs_cid => fs.id,
      :li_cid => li.id,
      :gm_cid => gm.id,
      :rs_cid => rs.id,
      :wp_cid => wp.id,
      :bl_cid => bl.id,
    }
    render  :text => @plan.to_yaml
  end

  
  def process_website
    if request.post?
      begin
      site = JSON.parse(params[:json])
      @website = Website.find(site["scan"]["website_id"])
      @website.json_scan = params[:json]
      @website.save
      
        render :text => "OK"
      rescue Exception => e
        logger.error "FAILED ON LOADING WEBSITE JSON.  #{e.message}"
        logger.error e.backtrace
      end
    end
  end
  
  def process_scan
    begin
      
      
      # CLEAR RECORDS
      Post.where(:channel_id => nil).delete_all
      Comment.where(:channel_id => nil).delete_all
      
      if request.post?
        yaml = YAML::load(params[:yaml])
        channel = Channel.find(yaml[:scan][:channel])
        unless channel.nil?
          t00 = Time.now
          foursquare_import(channel,yaml) if  channel.foursquare?
          gmail_import(channel,yaml) if  channel.gmail?
          twitter_import(channel,yaml) if  channel.twitter?
          facebook_import(channel,yaml) if  channel.facebook?
          rss_import(channel,yaml) if  channel.rss?
          linkedin_import(channel,yaml) if channel.linkedin?
          youtube_import(channel,yaml) if channel.youtube?
          wordpress_import(channel,yaml) if channel.wordpress?
        
         
        
          ## create Score daily score
          score = yaml[:scan]
          if channel.facebook?
            for page in score[:fan_pages_stats][:pages_stats] do 
              s = Score.new( 
                :score_date => Time.now.strftime("%Y-%m-%d"),
                :brand_id => channel.brand_id,
                :channel_id => channel.id,
                :uid => page[:uid],
                :posts => page[:posts],
                :comments => page[:comments],
                :commenters => channel.engaged_users.count,
                :fans => score[:total_friends],
                :no_longer_fans => score[:no_longer_friends],
                :new_fans => score[:new_friends],
                :messages => score[:messages],
                :likes =>  score[:fan_pages_stats][:likes]
              )
              s.save
              logger.error "ADDING FACEBOOK SCORE"
            end
         
          
          end
        
          if channel.twitter?
            s = Score.new( 
              :score_date => Time.now.strftime("%Y-%m-%d"),
              :brand_id => channel.brand_id,
              :channel_id => channel.id,
              :uid => score[:account_name],
              :posts => score[:posts],
              :comments => score[:comments],
              :commenters => channel.engaged_users.count,
              :fans => score[:total_friends],
              :messages => score[:messages],
              :no_longer_fans => score[:no_longer_friends],
              :new_fans => score[:new_friends]
            )
            s.save
            logger.error "ADDING TWITTER SCORE"
          end
        
          if channel.linkedin?
            s = Score.new( 
              :score_date =>  Time.now.strftime("%Y-%m-%d"),
              :brand_id => channel.brand_id,
              :channel_id => channel.id,
              :uid => score[:uid],
              :posts => score[:posts],
              #            :comments => score[:comments],
              :fans => score[:total_friends],
              :messages => score[:messages],
              :no_longer_fans => score[:no_longer_friends],
              :new_fans => score[:new_friends]
            )
            s.save
            logger.error "ADDING LInkedin SCORE"
          end
        
          
          if channel.youtube?
            s = Score.new( 
              :score_date => Time.now.strftime("%Y-%m-%d"),
              :brand_id => channel.brand_id,
              :channel_id => channel.id,
              :uid => score[:uid],
              :posts => score[:posts],
              :comments => score[:comments],
              :commenters => channel.engaged_users.count,
              :fans => score[:total_friends],
              :no_longer_fans => score[:no_longer_friends],
              :new_fans => score[:total_friends]
            )
            s.save
            logger.error "ADDING Youtube SCORE"
          end
        
          
          if channel.wordpress?
            s = Score.new( 
              :score_date => Time.now.strftime("%Y-%m-%d"),
              :brand_id => channel.brand_id,
              :channel_id => channel.id,
              :uid => score[:uid],
              :views => 0,
              :posts => channel.posts.count,
              :comments => channel.comments.count )
            s.save
            logger.error "ADDING WORDPRESS SCORE"
          end
        
           if channel.blogger?
            s = Score.new( 
              :score_date => Time.now.strftime("%Y-%m-%d"),
              :brand_id => channel.brand_id,
              :channel_id => channel.id,
              :uid => score[:uid],
              :views => 0,
              :posts => channel.posts.count,
              :comments => channel.comments.count )
            s.save
            logger.error "ADDING BLogger SCORE"
          end
        
          
          logger.error " STATUS :: PROCESSED CHANNEL : #{channel.name} channel : #{channel.id}  in #{Time.now - t00} seconds"
          render :text => "GOT IT!"
          channel.scanned_at = Time.now
          channel.save
        end
      end
    rescue Exception => e
      logger.error "FAILED LOADING SCAN DATA: #{e.message}"
      logger.error e.backtrace
    end
  end
  
  
  
  def export_brand
    @brand = Brand.find(params[:id])
    @website = @brand.site.to_yaml
  end
  
  
  
  def monitor
    if params[:command] == "brands"
      brands = []
      for brand in Brand.all do 
        brands << { :id => brand.id , :name => brand.name ,:fans => brand.fans.count, :posts => brand.posts.count}
      end
      render :text => brands.to_yaml
    end
  end
  
  def logs
    if params[:kind] == "application"
      res = `sudo tail -n 1500  #{Rails.root}/log/development.log `
    end
    
    if params[:kind] == "trackers"
      cmd = "sudo cat /opt/nginx/logs/access.log | grep this-is > ~/logs/trackers.log "
      res = `cmd`
    end
    
    if params[:kind] == "system"
      res = `sudo ps -fe `
    end
    
    if params[:kind] == "system:disk"
      res = `free `
    end
    
    render :text => res
  end
  
  
  require 'nokogiri'
  require 'yaml'
  def parse_google_search
    begin    
      doc = Nokogiri::HTML(params[:html])
      @res_google = []
      # Do funky things with it using Nokogiri::XML::Node methods...

      ar_1 = [] 
      ar_2 = []
      ar_3 = []

      ####
      # Search for nodes by css
      doc.css('h3.r a').each do |link|
        ar_1 << link.content
      end

      doc.search("cite").each do |cite|
        link = cite.inner_text
        ar_2 << link
      end

      doc.css("li.g").each do |x|
        ar_3 << x.inner_text
      end

     
      i = 0
      for item in ar_1 do
        description = ar_3[i]
        @res_google << {:title => ar_1[i], :description => description, :url => ar_2[i]} 
        i = i + 1
      end
    rescue
    end
    render :text => @res_google.to_yaml
  end
  
  
  def scan
    if request.post?
      yml = YAML::load( params[:yml] )
      #raise yml[:profile].inspect
    end
  end
  
  def authorize

  end
end
