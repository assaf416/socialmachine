class AutorespondersController < ApplicationController
  # GET /autoresponders
  # GET /autoresponders.json
  def index
    @brand = current_user.brand
    @autoresponders =  @brand.autoresponders
  end

  # GET /autoresponders/1
  # GET /autoresponders/1.json
  def show
    @autoresponder = Autoresponder.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @autoresponder }
    end
  end

  # GET /autoresponders/new
  # GET /autoresponders/new.json
  def new
    @autoresponder = Autoresponder.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @autoresponder }
    end
  end

  # GET /autoresponders/1/edit
  def edit
    @brand = current_user.brand
    @autoresponder = Autoresponder.find(params[:id])
  end

  # POST /autoresponders
  # POST /autoresponders.json
  def create
    @autoresponder = Autoresponder.new(params[:autoresponder])

    respond_to do |format|
      if @autoresponder.save
        format.html { redirect_to @autoresponder, notice: 'Autoresponder was successfully created.' }
        format.json { render json: @autoresponder, status: :created, location: @autoresponder }
      else
        format.html { render action: "new" }
        format.json { render json: @autoresponder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /autoresponders/1
  # PUT /autoresponders/1.json
  def update
    @autoresponder = Autoresponder.find(params[:id])

    respond_to do |format|
      if @autoresponder.update_attributes(params[:autoresponder])
        format.html { redirect_to @autoresponder, notice: 'Autoresponder was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @autoresponder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /autoresponders/1
  # DELETE /autoresponders/1.json
  def destroy
    @autoresponder = Autoresponder.find(params[:id])
    @autoresponder.destroy

    respond_to do |format|
      format.html { redirect_to autoresponders_url }
      format.json { head :no_content }
    end
  end
end
