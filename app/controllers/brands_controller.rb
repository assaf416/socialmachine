class BrandsController < ApplicationController
  include BrandsHelper
  include TwitterHelper
  include GooglePlusHelper
  include ERB::Util 
  include ScoresHelper
  require 'linkedin'
  
  
  
  # GET /brands
  # GET /brands.json
  def index
    redirect_to  "/brands/#{current_user.brand.id}" #unless current_user.admin?
    #    @brands = Brand.all
  end

  def recommendations
    #    raise params.inspect
  end

  def search
    q = params[:search]
    @prospects =  current_user.brand.find_people(q)
  end
  
  
  def templates
    @emails = Template.where(:kind => "email")
    @newsletters = Template.where(:kind => "email")
    @websites = Template.where(:kind => "website")
  end
  
 
  def connections
    @brand = current_user.brand
    @fb = @brand.get_channel("facebook")
    @tw = @brand.get_channel("twitter")
    @yt = @brand.get_channel("youtube")
    @gm = @brand.get_channel("gmail")
    @rss = @brand.get_channel("rss")
    @gp = @brand.get_channel("google")
    @fs = @brand.get_channel("foursquare")
    @li = @brand.get_channel("linkedin")
    @wp = @brand.get_channel("wordpress")
    @bl = @brand.get_channel("blogger")
  end
  

 
  def searches
    @brand = current_user.brand
  end
  
  def disconnect
    @brand = current_user.brand
    @channel = Channel.find(params[:channel])
    @channel.disconnect
    redirect_to "/brands/connections"
  end
  
  # GET /brands/1
  # GET /brands/1.json
  
  #  require 'twitter'
  def show
    @brand = current_user.brand
    
    for channel in @brand.channels do
      @fb = channel if channel.facebook?
      @tw = channel if channel.twitter?
      @yt = channel if channel.youtube?
      @gm = channel if channel.gmail?
      @rss = channel if channel.rss?
      @rs = channel if channel.rss?
      @gp = channel if channel.google_plus?
      @fs = channel if channel.foursquare?
      @li = channel if channel.linkedin?
      @wp = channel if channel.wordpress?
      @bl = channel if channel.blogger?
    end
    @inquiries = @brand.inquieries
    @comments = @brand.comments.where("channel_id is NOT NULL") 
    @mentions = @brand.comments.where( "kind = 'mention' and channel_id is NOT NULL")
    @competitors_posts = @brand.posts.where(:kind => "competitor-post").order("posted_at DESC").limit(50)
    @jobs = @brand.posts.where(:kind => "job").order("rank desc")
    @messages = @brand.posts.where(:kind => "inbox").order("posted_at DESC").limit(10)
    @content  = []
    @rss.posts.where(:kind => "recommended").each{|p| @content << {:id => p.id, :title => p.title, :body => p.short_body, :image_url => p.img , :target_link => p.target_link} } 
    @li.posts.where(:kind => "job").each{|p| @content << {:id => p.id, :title => p.title, :body => p.short_body, :image_url => p.img , :target_link => p.target_link} } 
   
  


  end
  
  
  def map
    @brand = current_user.brand
    @people = @brand.fans.where("location is not null")
  end
  
  
  
  def exposure
    @brand = current_user.brand
    @fb = @brand.get_channel("facebook")
    @tw_channel = @brand.get_channel("twitter")
    @google_results = google_search( @brand.name)
    @site_url_results = google_search( "http://goo.gl/g8sAs")
    @posted_articles = @brand.posts.where(:kind => "repost")
  end
  
  
  # GET /brands/new
  # GET /brands/new.json
  def new
    redirect_to "/brands"  unless current_user.admin?
    @brand = Brand.new(:user_id => params[:user_id])
  end

  # GET /brands/1/edit
  def edit
    @brand = Brand.find(params[:id])
  end

  # POST /brands
  # POST /brands.json
  def create
    @brand = Brand.new(params[:brand])
    @brand.create_defult_website  
    respond_to do |format|
      if @brand.save
        format.html { redirect_to @brand, notice: 'Brand was successfully created.' }
        format.json { render json: @brand, status: :created, location: @brand }
      else
        format.html { render action: "new" }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /brands/1
  # PUT /brands/1.json
  def update
    #    raise params.inspect
    @brand = Brand.find(params[:id])
    respond_to do |format|
      if @brand.update_attributes(params[:brand])
        @brand.save
         

        unless params[:brand][:twitter_search] or params[:brand][:twitter_ignore]
          #          @brand.delay.update_channels
          #          @brand.delay.scan
        end

        format.html { redirect_to @brand, notice: 'Brand was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
#      ProspectsSearchWorker.perform_async(@brand)  
    end
  end

  # DELETE /brands/1
  # DELETE /brands/1.json
  def destroy
    @brand = Brand.find(params[:id])
    @brand.destroy

    respond_to do |format|
      format.html { redirect_to brands_url }
      format.json { head :no_content }
    end
  end
  
  
  def social_login_callback
    note = ""
    #    raise params.inspect
    @brand = current_user.brand
    
    if params[:provider].include? "twitter"
      token = request.env["omniauth.auth"][:credentials][:token]
      secret = request.env["omniauth.auth"][:credentials][:secret]
      
      #      raise "TOKEN:#{token} |    SECERT:#{secret}"
      username = request.env["omniauth.auth"][:info][:nickname]
      @brand.twitter_token = token
      @brand.twitter_secret = secret
      @brand.twitter_user_id = username
      @brand.save
      
      ch = @brand.get_channel("twitter")
      ch.token = token
      ch.secret = secret
      ch.uid = username
      ch.save
      note =  "You are now connected to Twitter!"
      
    end

    if params[:provider].include? "facebook"
      token = request.env["omniauth.auth"][:credentials][:token]
      @brand.facebook_token = token
      @brand.save
      
      ch = @brand.get_channel("facebook")
      ch.token = token
      #      ch.secret = secret
      ch.save
      note =  "You are now connected to Facebook!"
    end

    
    
    if params[:provider].include? "foursquare"
      token = request.env["omniauth.auth"][:credentials][:token]
      secret = request.env["omniauth.auth"][:credentials][:secret]
      ch = @brand.get_channel("foursquare")
      ch.token = token
      ch.save
      note =  "You are now connected to Foursquare!"
    end
    
    if params[:provider].include? "google"
      #      raise request.env["omniauth.auth"].inspect
      token = request.env["omniauth.auth"][:credentials][:token]
      secret = request.env["omniauth.auth"][:credentials][:secret]
      @brand.google_token = token
      @brand.google_secret = secret
      @brand.save
      note =  "You are now connected to Google!"
    end
    
    
    if params[:provider].include? "drop"
#      raise request.env["omniauth.auth"].inspect
      token = request.env["omniauth.auth"][:credentials][:token]
      secret = request.env["omniauth.auth"][:credentials][:secret]
      ch = @brand.get_channel("dropbox")
      ch.token = token
      ch.secret = secret
      ch.save
      note =  "You are now connected to DropBox"
    end

    if params[:provider].include? "linkedin"
      token = request.env["omniauth.auth"][:credentials][:token]
      secret = request.env["omniauth.auth"][:credentials][:secret]
      
      @brand.linkedin_token = token
      @brand.linkedin_secret = secret
      @brand.linkedin_pin= params[:oauth_verifier]
      @brand.save
      
      
      ch = @brand.get_channel("linkedin")
      ch.token = token
      ch.secret = secret
      ch.pin =params[:oauth_verifier]
      ch.save

      note =  "You are now connected to Linkedin!"
    end
    redirect_to "/brands", :notice => note
  end
  
  def social_login_failure
    raise params.inspect
  end
  
  def social_dashboard
    @brand = current_user.brand
    @tw_results =   []
    @tw_mentions = []  #Twitter.search(@brand.name,  :rpp => 100)
    @tw_timeline =  [] #Twitter.user_timeline("assaf604")
    #    raise @tw_timeline.inspect
  end
   
  
  def twitter_search
    @brand = current_user.brand
    @tw_channel = @brand.get_channel("twitter")
    render :layout => "embedded"
  end
  
  
  def scan_brand
    system "sudo thor scan brand #{params[:id]} http://23.21.17.104 #{Rails.root}/../scanner &"
    redirect_to "/brands"
  end


  def repost_rss_items
    ar = []
    params.keys.each{|key| ar << key[6..100] if key.include? "check"}
    Article.create_from_rss_items(ar, current_user.brand.id)
    redirect_to "/articles"
  end
  
  
end
