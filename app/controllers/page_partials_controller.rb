class PagePartialsController < ApplicationController
  layout 'wider_sidebar'
  # GET /page_partials
  # GET /page_partials.json
  
  def images
    format.json { render json: "[]" }
  end
  
  def index
    @page_partials = PagePartial.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @page_partials }
    end
  end

  # GET /page_partials/1
  # GET /page_partials/1.json
  def show
    @page_partial = PagePartial.find(params[:id])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @page_partial.attributes.merge( :remote_image_url => @page_partial.image_url.to_s)  }
    end
  end

  # GET /page_partials/new
  # GET /page_partials/new.json
  def new
    @page_partial = PagePartial.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @page_partial }
    end
  end

  # GET /page_partials/1/edit
  def edit
    @page_partial = PagePartial.find(params[:id])
  end

  # POST /page_partials
  # POST /page_partials.json
  def create
    @page_partial = PagePartial.new(params[:page_partial])

    respond_to do |format|
      if @page_partial.save
        format.html { redirect_to @page_partial, notice: 'Page partial was successfully created.' }
        format.json { render json: @page_partial, status: :created, location: @page_partial }
      else
        format.html { render action: "new" }
        format.json { render json: @page_partial.errors, status: :unprocessable_entity }
      end
    end
  end

  def row_settings
    kind= params[:kind]
    pp = PagePartial.find(params[:id])
    
    if kind.include? "heading" then 
      pp.row_is_row_top = true
      website = pp.page.website
      website.fixed_toolbar = false
      website.save
    end
    if kind.include? "footer" then 
      pp.row_is_footer = true
    end
    pp.save
    redirect_to "/pages/preview?id=#{pp.page_id}"
  end
  
  def quick_update
    pp = PagePartial.find(params["p-id"])
    pp.body = params["p-body"]
    pp.title = params["p-title"]
   
    pp.remote_image_url = params["p-image-url"]
    
    
    
    if pp.form?
      pp.body = params["p-form-body"] unless params["p-form-body"].nil?
      pp.title = params["p-form-title"] unless params["p-form-title"].nil?
      pp.style = params["p-form-style"]
      pp.align = params["p-form-align"]
    end
    
    
    if pp.map?
      pp.title = params["p-map-address"]
    end
    
    if pp.hero?
      pp.style = params["p-hero-bg-color"]
      pp.body = params["p-hero-body"]
    end
    
    if pp.social_links?
      pp.style = params["social_links_style"]
      pp.align = params["social_links_align"]
    end
    
    
    if pp.newsletter?
      pp.title = params["p-newsletter-title"]
      pp.body = params["p-newsletter-body"]
      pp.article_id = params["selected_newsletter"]
      pp.align = params["p-newsletter-image-location"]
      pp.img_height = params["p-newsletter-image-height"]
    end
    
    if pp.page_links?
      pp.align = params["page_links_align"]
    end
    
    if pp.image?
      pp.img_width = params["p-image-width"] 
      pp.img_height = params["p-image-height"] 
      pp.style = params["p-image-style"] 
    end
    
    if pp.feature?
      pp.remote_image_url = params["p-feature-icon"]
      pp.title = params["p-feature-title"]
      pp.body = params["p-feature-body"]
      pp.style = params["p-feature-style"]
    end
    
    if pp.movie?
      pp.img_width = params["p-movie-width"] 
      pp.img_height = params["p-movie-height"] 
      pp.title = params["p-movie-url"] if pp.movie?
    end
    
    if pp.article?
      pp.article_id = params[:selected_article]
    end
    
    if pp.slider?
      pp.slide_1 = params["p-slide-1"] unless params["p-slide-1"].empty?
      pp.slide_2 = params["p-slide-2"] unless params["p-slide-2"].empty?
      pp.slide_3 = params["p-slide-3"] unless params["p-slide-3"].empty?
      pp.slide_4 = params["p-slide-4"] unless params["p-slide-4"].empty?
      pp.slide_5 = params["p-slide-5"] unless params["p-slide-5"].empty?
      pp.img_height = params["p-slide-height"]
#      raise pp.img_height.inspect
    end
    
    pp.save
#    raise pp.img_height.inspect
    redirect_to "/pages/preview?id=#{pp.page_id}"
  end
  
  # PUT /page_partials/1
  # PUT /page_partials/1.json
  def update
    #raise params.inspect
    @page_partial = PagePartial.find(params[:id])
    respond_to do |format|
      if @page_partial.update_attributes(params[:page_partial])
        format.html { redirect_to "/pages/preview?id=#{@page_partial.page_id}", notice: 'Page partial was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @page_partial.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /page_partials/1
  # DELETE /page_partials/1.json
  def destroy
    @page_partial = PagePartial.find(params[:id])
    @page_partial.destroy

    respond_to do |format|
      format.html { redirect_to page_partials_url }
      format.json { head :no_content }
    end
  end
  
  def replace_with
    @page_partial = PagePartial.find(params[:id])
    @page_partial.kind = params[:kind]
    
    
    #    if params[:kind].include? "page_links"
    #      pages = @page_partial.page.website.pages
    #      @page_partial.body = "<ul class='nav nav-pills'>"
    #      for p in pages do 
    #         @page_partial.body << "<li><a href='/pages/preview?id=#{p.id} '> #{p.name} </a></li>"
    #      end
    #      @page_partial.body << "</ul>"
    #    end
    
    
    
    @page_partial.save
    #    redirect_to :action => 'edit' , :id => @page_partial.id
    redirect_to  "/pages/preview/#{@page_partial.page_id}"
  end
end



