class SlidesController < ApplicationController
  def create
  end

  def edit
  end

  def destroy
  end

  def index
  end

  
  def update
    @slide = Slide.find(params[:id])
    if @slide.update_attributes(params[:slide])
      
    end
    redirect_to "/articles/"+ @slide.article.id.to_s + "/edit", notice: 'Deployment was successfully updated.'
  end
  
  def new
  end
end
