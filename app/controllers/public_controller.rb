class PublicController < ApplicationController
  layout "public"
  
  def newsletter
    @brand = params[:brand] || Brand.first
    @newsletters = @brand.newsletters
    
    if @brand.site.template.css_file_name.present?
      file = File.open("#{Rails.root}/public/templates/css/#{@brand.site.template.css_file_name}", "rb")
      @css = file.read
      begin
        @fonts = File.open("#{Rails.root}/public/templates/css/#{@brand.site.template.css_file_name.gsub(".css",".fonts")}", "rb").read
      rescue Exception => e
        @fonts = ""
        logger.error "FAILED ON PARSING FONTS FOR TEMPALTE #{e.message}"
      end
    end
    @socialmachine_css = File.open("#{Rails.root}/public/templates/css/socialmachine.css", "rb").read
    
    
    
    if params[:issue]
      @newsletter = @brand.newsletters.find(params[:issue])
    else
      @newsletter = @brand.newsletters.first
    end
  end
  
  def campaign
    @campaign = BulkMail.find(params[:id])
    @brand = @campaign.brand
    @website = @brand.site
    render :layout => "site"
  end
  
  def email_verification
    logger.error "EMAIL VERIFICATION LINK!!! #{params.inspect}"
    render :text =>  '<img src="/assets/this-is-the-email-image.png" border=0>'
  end
  
  def authorize
    
  end
  
  
  def form_post
    @inquiery = Inquiery.new(params[:inquiery])
    if @inquiery.save
#      @inquiery.auto_response
      @inquiery.notify
      redirect_to @inquiery.request_url, notice: 'Inquiery was successfully created.'
    end
  end
  
  def unsubscribe_from_email
    if params[:email]
      e = Email.find(params[:email])
      e.status = "unsubscribed"
      e.save
    end
  end
  
  
  def index_heb
    
    @brand = Brand.find(1)
    t = Tracker.new 
    t.brand_id = @brand.id
    t.ip = request.remote_ip
    t.browser = request.env["HTTP_USER_AGENT"]
    t.email_id = params[:email]
    t.engaged = true
    t.logged_at = Time.now
    t.page_id = -301 #  
    t.referrer = request.referer
    t.save
    t.update_location
  end
  
  def index
    @brand = Brand.find(1)
    
    t = Tracker.new 
    t.brand_id = @brand.id
    t.ip = request.remote_ip
    t.browser = request.env["HTTP_USER_AGENT"]
    t.email_id = params[:email]
    t.engaged = true
    t.logged_at = Time.now
    t.page_id = -300 # 
    t.referrer = request.referer
    t.save
    t.update_location
    
  end

  def tour
    @brand = Brand.find(1)
    t = Tracker.new 
    t.brand_id = @brand.id
    t.browser = request.env["HTTP_USER_AGENT"]
    t.ip = request.remote_ip
    t.email_id = params[:email]
    t.engaged = true
    t.logged_at = Time.now
    t.page_id = -302
    t.referrer = request.referer
    t.save
    t.update_location
  end

  def blog
    
  end

  def prices
  end
  

  def beta
    @brand = Brand.find(1)
    t = Tracker.new 
    t.brand_id = @brand.id
    t.ip = request.remote_ip
    t.browser = request.env["HTTP_USER_AGENT"]
    t.engaged = true
    t.logged_at = Time.now
    t.page_id = -304
    t.referrer = request.referer
    t.save
    t.update_location
  end

  def product
    @product = Product.find(params[:id])
    @brand = @product.brand
    @website = @brand.site
    
    if params[:preview].nil?
      
      
      t = Tracker.new 
      t.brand_id = @brand.id
      t.ip = request.remote_ip
      t.browser = request.env["HTTP_USER_AGENT"]
      t.product_id = @product.id
      t.engaged = true
      t.logged_at = Time.now
      t.page_id = -401 #  
      t.referrer = request.referer
      t.save
      t.update_location
      
      
      @product.views = @product.views.to_i + 1
      @product.save
    end
    
    render :layout => "site"
  end

  def event
    @event = Event.find(params[:id])
    @brand = @event.brand
    @website = @brand.site
    
    if params[:preview].nil?
      t = Tracker.new 
      t.brand_id = @brand.id
      t.ip = request.remote_ip
      t.browser = request.env["HTTP_USER_AGENT"]
      t.event_id = @event.id
      t.engaged = true
      t.logged_at = Time.now
      t.page_id = -401 #  
      t.referrer = request.referer
      t.save
      t.update_location
    end
    
    render :layout => "site"
  end
  
  def facebook_page_public
    render :layout => 'facebook'
  end
  
  def facebook_page_private
    render :layout => 'facebook'
  end
  
  
  def brand
    @brand = Brand.find(params[:id])
    t = Tracker.new 
    t.brand_id = @brand.id
    t.ip = request.remote_ip
    t.email_id = params[:email]
    t.engaged = true
    t.logged_at = Time.now
    t.page_id = params[:page]
    t.referrer = request.referer
    t.browser = request.env["HTTP_USER_AGENT"]
    t.save
    t.update_location
    
    
    if params[:page]
      @page = Page.find(params[:page])
    else
      @page = @brand.websites.order("ID ASC").first.pages.first
    end
    @page.increase_views
    @website = @page.website
    #    @website_css = ""
    #    if @website.template.css_file_name.present?
    #      file = File.open("#{Rails.root}/public/templates/css/#{@website.template.css_file_name}", "rb")
    #      @website_css = file.read
    #    end
    
    
    render :layout => "site"
  end
  
  def article
    @article = Article.find(params[:id])
    @article.increase_view_count
    @articles = Article.all
    
    t = Tracker.new 
    t.brand_id = @article.brand_id
    t.ip = request.remote_ip
    t.email_id = params[:email]
    t.engaged = true
    t.logged_at = Time.now
    t.article_id = @article.id
    t.referrer = request.referer
    t.browser = request.env["HTTP_USER_AGENT"]
    t.save
    t.update_location
  end

  def register
  end

  def login
  end
  
  
  
end
