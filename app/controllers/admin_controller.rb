class AdminController < ApplicationController
  
  #encoding: UTF-8
  require 'mongo'
  require 'json'
  
  def website_templates
    @pages = Page.where(:website_id => -1)
    @templates = Template.where(:kind => "website")
  end

  def page_preview
    @page = Page.find(params[:id])
    if params[:template]
      @template = Template.find(params[:template])
    else
    end
    #    render :layout => nil
  end
  
  def email_templatese
    
  end
  
  def index
    redirect_to "/brands"  unless current_user.admin?
    @jobs = []#DelayedJob.all
    @users = User.all
    @brands = Brand.all
    @websites = Website.all
    @templates = Template.all
    @messages = Inquiery.all
    @processes = ""
    @emails = ""
    
    begin
      #      @scanner_log =  `tail -n 1500  #{Rails.root}/../logs/thor.log `
      #      @scanner_log =  `tail -n 1500  #{Rails.root}/../logs/thor.log | grep STATUS `
      #      @scanner_log <<  `tail -n 1500  #{Rails.root}/../logs/thor.log | grep FAILED `
      @log_failures =  `tail -n 1500  #{Rails.root}/log/development.log | grep FAILED`
      @log_statuses =  `tail -n 1500  #{Rails.root}/log/development.log | grep STATUS`
      @processes = `tail-n 50 /home/ubuntu/logs/processes.log`
      @db_status = `cat /home/ubuntu/logs/db.log`
      @emails = `cat #{Rails.root}/../logs//trackers.log`
    rescue Exception => e
      logger.error "FAILED TO LOAD LOG FILES IN ADMIN : #{e.message}"
      @log = ""
    end
      
      
    
  end
  
  def refresh_rss
    for feed in Feed.all do 
      feed.load
    end
    redirect_to "/feeds"
  end


  def licenses
  end

  def background_jobs
    @jobs = DelayedJob.all
  end
  
  def delete_all_jobs
    DelayedJob.delete_all
    redirect_to :action => "background_jobs"
  end
  def delete_jobs
    DelayedJob.find(params[:id]).delete
    redirect_to :action => "background_jobs"
  end

  def users
  end

  def posts
  end

  def comments
  end

  def websites
  end

  
  def bgc
    render :layout => nil
  end
  
  
  
  def find_people
    coll = mongo_connector.collection("brands")
    @brand =  coll.find(:name => "dbm").to_a[0]
    
    q = params[:find_query]
    current_user.brand.find_people(q)
    redirect_to :action => "test" 
  end
  
  
  def test
    #     brand = current_user.brand
    @bb = Brand.find(current_user.brand.id, :include => [:articles, :posts, :competitors, :fans, :pages, :channels, :inquieries, :autoresponders])
    @page_views = 0
    @bb.pages.each{ |p| @page_views = @page_views + p.view_count.to_i }
    
    @articles = @bb.articles
    @competitors = @bb.competitors
    @engaged_users = @bb.fans.where(:kind => "engaged")
    @channels = @bb.channels
    @inquiries = @bb.inquieries.reverse
    @autoresponders = @bb.autoresponders
    coll = mongo_connector.collection("brands")
    @brand =  coll.find(:name => "dbm").to_a[0]
    
    @prospects = []
#    JSON.parse(@bb.json_prospects).each{|p| @prospects << p} unless @bb.json_prospects.nil?
    
    
    @fan_pages = [] ;@posts =[] ; @comments = []
    JSON.parse(@brand["facebook_fan_pages"]).each{ |i| @fan_pages << i; i["posts"].each{|p| @posts << p}}
    
    @rss_unread = [] 
    JSON.parse(@brand["rss_unread"]).each{ |i| @rss_unread << i}
    
    
    @messages = []
    JSON.parse(@brand["facebook_inbox"]).each{| i | @messages << i}
    
 
    
    
    render :layout => "all_in_one"
  end
  
  
end
