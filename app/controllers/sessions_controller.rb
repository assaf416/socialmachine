class SessionsController < ApplicationController
  layout "public"

  def authorize

  end

  def new
    @email = ""
    begin
      if params[:id] and not User.find(params[:id]).nil?
        @email = User.find(params[:id]).email
      end
    rescue
    end
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to "/brands"
    else
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end
end
