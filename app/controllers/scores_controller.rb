class ScoresController < ApplicationController
  # GET /scores
  # GET /scores.json
  
  include ScoresHelper
  def index
    @brand = current_user.brand
    
    @scores = current_user.brand.scores.group(:score_date)
    
    
    gon.web_chart_1_data = website_chart_1(@scores)
    
    gon.web_chart_2_data =[
      
    { :period =>  "2012-1-1", :google =>  rand(100), :bing => rand(100)},
    { :period =>  "2012-2-1", :google =>  rand(100), :bing => rand(100)},
    { :period =>  "2012-3-1", :google =>  rand(100), :bing => rand(100)},
    { :period =>  "2012-4-1", :google =>  rand(100), :bing => rand(100)},
    { :period =>  "2012-5-1", :google =>  rand(100), :bing => rand(100)}
    
  ]
  
    gon.chart_fans_by_channel_data = pie_chart_fans_by_channel_data(@brand)
    gon.chart_engaged_users_data = pie_chart_engaged_users_data(@brand)
    
    
    
    gon.bar_chart_engaged_users_data,
      gon.bar_chart_engaged_users_events ,
      gon.bar_chart_engaged_users_xkey ,
      gon.bar_chart_engaged_users_ykeys ,
      gon.bar_chart_engaged_users_lables = bar_chart_engaged_users_data(@scores)
    
    
    gon.bar_chart_fans_data,
      gon.bar_chart_fans_events ,
      gon.bar_chart_fans_xkey ,
      gon.bar_chart_fans_ykeys ,
      gon.bar_chart_fans_lables = bar_chart_fans_data(@scores)
   
    
     gon.bar_chart_facebook_fans_data,
      gon.bar_chart_facebook_fans_events ,
      gon.bar_chart_facebook_fans_xkey ,
      gon.bar_chart_facebook_fans_ykeys ,
      gon.bar_chart_facebook_fans_lables = bar_chart_facebook_fans_data(@scores)
   
     gon.bar_chart_facebook_engaged_data,
      gon.bar_chart_facebook_engaged_events ,
      gon.bar_chart_facebook_engaged_xkey ,
      gon.bar_chart_facebook_engaged_ykeys ,
      gon.bar_chart_facebook_engaged_lables = bar_chart_facebook_engaged_data(@scores)
   
    
     gon.bar_chart_facebook_posts_and_comments_data,
      gon.bar_chart_facebook_posts_and_comments_events ,
      gon.bar_chart_facebook_posts_and_comments_xkey ,
      gon.bar_chart_facebook_posts_and_comments_ykeys ,
      gon.bar_chart_facebook_posts_and_comments_lables = bar_chart_facebook_posts_and_comments_data(@scores)
   
     
     @scores = current_user.brand.scores.where("competitor_id is not null") if params[:competitors].present?
   
   
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @scores }
    end
  end

  # GET /scores/1
  # GET /scores/1.json
  def show
    @score = Score.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @score }
    end
  end

  # GET /scores/new
  # GET /scores/new.json
  def new
    @score = Score.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @score }
    end
  end

  # GET /scores/1/edit
  def edit
    @score = Score.find(params[:id])
  end

  # POST /scores
  # POST /scores.json
  def create
    @score = Score.new(params[:score])

    respond_to do |format|
      if @score.save
        format.html { redirect_to @score, notice: 'Score was successfully created.' }
        format.json { render json: @score, status: :created, location: @score }
      else
        format.html { render action: "new" }
        format.json { render json: @score.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /scores/1
  # PUT /scores/1.json
  def update
    @score = Score.find(params[:id])

    respond_to do |format|
      if @score.update_attributes(params[:score])
        format.html { redirect_to @score, notice: 'Score was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @score.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /scores/1
  # DELETE /scores/1.json
  def destroy
    @score = Score.find(params[:id])
    @score.destroy

    respond_to do |format|
      format.html { redirect_to scores_url }
      format.json { head :no_content }
    end
  end
end
