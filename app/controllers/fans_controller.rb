class FansController < ApplicationController
  require 'csv'    
  include ApplicationHelper
  def import 
    @brand = current_user.brand
    @gmail = @brand.get_channel("gmail")
    if request.post? 
      
      @brand.fans.where(:kind => 'imported-email').delete_all
      
      @list =[]
      for row in params[:data].split("\n") do 
        name = row.split(",")[0]
        email = row.split(",")[1]
        
        unless @brand.fans.find_by_email(email)
          Fan.create!( :brand_id => @brand.id , :kind => 'imported-email' , :name => name , :email => email , :channel_id => @gmail.id )
        end
      end
    else
      @list = []
    end
  end


  def search
    @brand = current_user.brand
    #    if params[:uid]
    #      @fan = @brand.fans.find_by_uid(params[:uid])
    #    else
    #      @fan = Fan.find(params[:id])
    #    end
    @name = params[:name]
    @res = Fan.search_on_social_networks(params[:name], @brand)
  end
  
  # GET /fans
  # GET /fans.json
  def index
    @brand = current_user.brand
    if params[:kind]
      if params[:channel]
        @fans = current_user.brand.fans.where(:kind => params[:kind], :channel_id => params[:channel] ).order("created_at DESC")
      else
        @fans = current_user.brand.fans.where(:kind => params[:kind] ).where("channel_id is not NULL").order("created_at DESC")
      end
    else
      @fans = @brand.fans.where("channel_id is not NULL and kind = 'fan'").order("friends_count DESC")
    end
#    @prospects = current_user.brand.fans.where("kind = 'prospect' and channel_id is NOT NULL" )
  end

  def ignore
    @fan = Fan.find(params[:id])
    name= @fan.name
    @fan.ignore
    redirect_to params[:redirect] , :notice => "#{name} will be ignored from now on.."
  end
  
  
  
  def competitor
    @fan = current_user.brand.fans.find(params[:id])
    c = Competitor.new(:brand_id => current_user.brand.id,  :name => @fan.name)
    c.facebook_page = @fan.uid if @fan.channel.facebook?
    c.twitter_uid = @fan.uid if @fan.channel.twitter?
    c.twtitter_name = @fan.screen_name if @fan.channel.twitter?
    c.home_page = @fan.home_page
    c.image_url = @fan.profile_image
    c.save
    redirect_to params[:redirect], :notice => "#{@fan.name} is now a competitor"
  end
  
  
  # GET /fans/1
  # GET /fans/1.json

  
  def show
    
    @fan = current_user.brand.fans.find(params[:id])
  
    respond_to do |format|
      format.html {
          @name_search_results = Fan.search_on_social_networks(@fan.name, current_user.brand)
      }
      format.mobile {
        @google_results =  []#google_search(@fan.name)
      }
      format.json { 
      }
    end
  end

  
  
  def follow
    @brand = current_user.brand
    @fan = @brand.fans(params[:id])
    @fan.follow
    redirect_to params[:redirect]
  end
  
  def unfollow
    @brand = current_user.brand
    @fan = @brand.fans(params[:id])
    @fan.unfollow
    redirect_to params[:redirect]
  end
  
   
  def invite
    fan = current_user.brand.fans.find(params[:id])
    fan.invite
    redirect_to params[:redirect] , :notice => "Invitation sent to #{fan.name}"
  end
  
  
  # GET /fans/new
  # GET /fans/new.json
  def new
    @fan = Fan.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @fan }
    end
  end

  # GET /fans/1/edit
  def edit
    @fan = Fan.find(params[:id])
  end

  # POST /fans
  # POST /fans.json
  def create
    @fan = Fan.new(params[:fan])

    respond_to do |format|
      if @fan.save
        format.html { redirect_to @fan, notice: 'Fan was successfully created.' }
        format.json { render json: @fan, status: :created, location: @fan }
      else
        format.html { render action: "new" }
        format.json { render json: @fan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /fans/1
  # PUT /fans/1.json
  def update
    @fan = Fan.find(params[:id])

    respond_to do |format|
      if @fan.update_attributes(params[:fan])
        format.html { redirect_to @fan, notice: 'Fan was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @fan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fans/1
  # DELETE /fans/1.json
  def destroy
    @fan = Fan.find(params[:id])
    @fan.destroy

    respond_to do |format|
      format.html { redirect_to fans_url }
      format.json { head :no_content }
    end
  end
  
  def scan
    @fan = Fan.find(params[:id])
    @fan.scan
    redirect_to :action => 'show', :id => params['id']
  end
end
