class FeedEntriesController < ApplicationController
  include FacebookHelper

  # encoding: utf-8
  def show
    @entry = FeedEntry.find(params[:id])
  end
  
  def deploy
    @entry = FeedEntry.find(params[:id])
    
    if params[:delayed] # add to Delayed twits
      @channel = current_user.brand.get_channel("twitter")
      p =  Post.create!( :brand_id => current_user.brand, :channel_id => @channel.id, :kind => "delayed" , :body => @channel.search_4 + " " +  @entry.title + " " + @entry.url )      
      p.post_to_twitter if params[:now] 
      
    end
    
    if params["page"] # Facebook Page
      @entry = FeedEntry.find(params[:id])
      fb_deploy_feed_to_page(@entry,params[:page])
    end

    if params[:redirect]
      redirect_to params[:redirect] ,  notice: 'Article added to delayed twits'
    else
      redirect_to "/feed_entries?id=#{@entry.id}" ,  notice: 'Article added to delayed twits'
    end    
    
  end
  
  
  def index
    @brand = current_user.brand
    @items = Feed.first.feed_entries.order("posted_at DESC")
    @item = @items.last
    
    if params[:id]
      @item = FeedEntry.find(params[:id])
      @items = Feed.find(@item.feed_id).feed_entries.order("posted_at DESC")
    end
    if params[:feed]
      @items = Feed.find(params[:feed]).feed_entries.order("posted_at DESC")
      @item = @items.first
    end
    
  end
end

  