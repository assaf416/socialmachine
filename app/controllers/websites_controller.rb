class WebsitesController < ApplicationController
  # GET /websites
  
  require 'json'
  include AnalysisHelper
  include WebsitesHelper
  
  # layout "wider_sidebar"
  def index
    @websites = current_user.brand.websites.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @websites }
    end
  end

  
  def create_page
    if params[:id]
      @website = Website.find(params[:id])
    else
      @website = current_user.brand.site
    end
    @p = Page.new(:website_id => @website.id , :kind => params[:kind] , :name =>  "New Page")
    @p.save
    # Create from Temaplte
    if params[:template]
      @p.create_from_template(params[:template].to_i)
    end
    @p.save
    
    redirect_to @website
  end


  def move_page
    if params[:direction] 
      dir = params[:direction]
      page = Page.find(params[:id])
      @site = Website.find(page.website.id)
      @site.move_page(page,params[:direction])
    end
    redirect_to "/websites/#{current_user.brand.site.id}"
  end
  
  def apply_color_patch
    @color = Color.find(params[:color])
    @website = Website.find(params[:id])
    @website.apply_color_patch(@color)
    @website.save
    redirect_to "/pages/preview?id=#{@website.pages.first.id}"
  end
  
  

  def reset_custom_colors
    @website = Website.find(params[:id])
    @website.reset_custom_css
    @website.save
    redirect_to "/pages/preview?id=#{@website.pages.first.id}"
  end

  
  # GET /websites/1
  # GET /websites/1.json
  def show
    @brand = current_user.brand
    @website = @brand.websites.first
    @templates = Template.all
    @articles = @brand.articles.where("short_url is not null").order("views Desc").limit(5)
      
    if @website.json_scan.present?
      site = JSON.parse(@website.json_scan)
      @meta =  site["meta"]
      @searches =  site["results"] 
      
      @competitors_keywords = []
      for item in site["competitors_keywords"] do 
        @competitors_keywords << {:key => item[0] ,:value => item[1] } if item[1].to_i > 2
      end
      @competitors_keywords.sort_by {|_key, value| :value}
      
      @site_stats = site["website_stats"]
    end
     
    
    # raise "Sorry.. no #{@website.url}"
    @keywords = []
    @backlinks = []
    @links = []
    
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @website }
    end
  end

  def css
    @website = Website.find(params[:id])
    @templates = Template.all#
  end
  
  
  # GET /websites/new
  # GET /websites/new.json
  def new
    @website = Website.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @website }
    end
  end

  def edit
    if current_user.admin?
      @website = Website.find(params[:id])
    else
      @website = current_user.brand.websites.first
    end
    @templates = Template.where(:kind => 'css')
  end

  def create
    @website = Website.new(params[:website])
    @website.brand_id = current_user.brand.id
    respond_to do |format|
      if @website.save
        format.html { redirect_to @website, notice: 'Website was successfully created.' }
        format.json { render json: @website, status: :created, location: @website }
      else
        format.html { render action: "new" }
        format.json { render json: @website.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    #        raise params.inspect
    @website = Website.find(params[:id])
    @website.brand_id = current_user.brand.id


    if params[:website][:background_image] then 
      @page = @website.pages.find(params[:website][:temp_page_id])
      @page.background_image_url = params[:website][:background_image]
      @page.background_image_style = params[:image_style] unless params[:image_style].include? "Select"
      @page.save
      params[:website][:background_image] = nil
    end

    
    if @website.url.nil?
      Google::UrlShortener::Base.api_key = "AIzaSyDQU97mjsTO_qfzAHNcfguvhcd2CNvv6Yw"
      url = Google::UrlShortener::Url.new(:long_url =>  "http://www.socialmachine.biz/site/#{@website.brand.id}")
      @website.url =  url.shorten! 
    end
    
    if @website.update_attributes(params[:website])
      if params[:website][:temp_page_id]  then
        redirect_to "/pages/preview?id=#{params[:website][:temp_page_id]}", notice: 'Website was successfully updated.' 
      else
        redirect_to "/pages/preview?id=#{@website.pages.first.id}", notice: 'Website was successfully updated.' 
      end
      #         redirect_to "/brands/#{@website.brand.id}", notice: 'Website was successfully updated.' 
    else
      render action: "edit" 
    end
  end

  
  def stats
    @website = Website.find(params[:id])
    @brand = current_user.brand
    
    @scores = current_user.brand.scores.group(:score_date)
    
    
    gon.web_chart_1_data = website_chart_1(@scores)
    
    gon.web_chart_2_data =[
      
      { :period =>  "2012-1-1", :google =>  rand(100), :bing => rand(100)},
      { :period =>  "2012-2-1", :google =>  rand(100), :bing => rand(100)},
      { :period =>  "2012-3-1", :google =>  rand(100), :bing => rand(100)},
      { :period =>  "2012-4-1", :google =>  rand(100), :bing => rand(100)},
      { :period =>  "2012-5-1", :google =>  rand(100), :bing => rand(100)}
    
    ]
  end
  
  
  def destroy
    @website = Website.find(params[:id])
    @website.destroy

    respond_to do |format|
      format.html { redirect_to websites_url }
      format.json { head :no_content }
    end
  end
end
