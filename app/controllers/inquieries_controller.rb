class InquieriesController < ApplicationController
  def authorize
    true
  end
  
  def reply
    #    raise params.inspect
    @inquiery = current_user.brand.inquieries.find(params[:id])
    @template = BulkMail.find(params[:mail])
    @email = Email.new
    @email.to_email = @inquiery.email
    @email.to_name = @inquiery.name
    @email.text_body = (@template.body.gsub("#user", @inquiery.name))
    @email.html_body = (@template.body.gsub("#user", @inquiery.name)).to_s
  end
  
  def index
    @inquieries = current_user.brand.inquieries.order("created_at DESC")
  end

  def show
    @inquiery = current_user.brand.inquieries.find(params[:id])
#    @google_results = google_search(@inquiery.name)
    @emails = BulkMail.where(:kind => "autoresponder" , :brand_id => current_user.brand.id)
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @inquiery }
    end
  end

  def new
    @inquiery = Inquiery.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @inquiery }
    end
  end

  def edit
    @inquiery = Inquiery.find(params[:id])
  end

  def create
    @inquiery = Inquiery.new(params[:inquiery])
    
    if @inquiery.save
      @inquiery.auto_response
      @inquiery.notify
      #      if @inquiery.facebook?
      #        redirect_to "/public/facebook_page_public", notice: 'Inquiery was successfully created.'
      #      else
      #      raise request.original_url.inspect
      redirect_to @inquiery.request_url, notice: 'Inquiery was successfully created.'
      #      end
    end
  end

  def update
    @inquiery = Inquiery.find(params[:id])

    respond_to do |format|
      if @inquiery.update_attributes(params[:inquiery])
        format.html { redirect_to @inquiery, notice: 'Inquiery was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @inquiery.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @inquiery = Inquiery.find(params[:id])
    @inquiery.destroy

    respond_to do |format|
      format.html { redirect_to inquieries_url }
      format.json { head :no_content }
    end
  end
end
