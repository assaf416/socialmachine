class ChannelsController < ApplicationController
  #  layout 'social'
  include ChannelsHelper
  
  
  def create_short_link
    @brand = current_user.brand
    @channel = Channel.find(params[:id])
    website_url = "http://23.21.17.104/public/brand?id=#{@brand.id}&page=#{@brand.site.home_page_id}"
    Google::UrlShortener::Base.api_key = "AIzaSyDQU97mjsTO_qfzAHNcfguvhcd2CNvv6Yw"
    url = Google::UrlShortener::Url.new(:long_url => website_url)
    @channel.home_page_short_url =  url.shorten! 
    @channel.save
    redirect_to "/channels/#{@channel.id}/edit" , :note => "Short Link Created!"
  end
  
  def index
    #    raise params.inspect
    @brand = current_user.brand
    @channels = @brand.channels
  end
 
  
  def register
    if request.post?
      @channel = current_user.brand.channels.find(params[:channel])
      if @channel.youtube?
        @channel.uid = params[:user]
        @channel.secret = params[:pwd]
        @channel.save
        redirect_to @channel
      end
      if @channel.rss?
        @channel.email = params[:email]
        @channel.secret = params[:pwd]
        @channel.save
        redirect_to @channel
      end
      if @channel.gmail?
        @channel.email = params[:email]
        @channel.secret = params[:pwd]
        @channel.save
        redirect_to @channel
      end
      
      if @channel.wordpress?
        @channel.posts_feed = params[:url]
        @channel.email = params[:email]
        @channel.save
        redirect_to @channel
      end
      
      if @channel.blogger?
        @channel.posts_feed = params[:url]
        @channel.email = params[:email]
        @channel.save
        redirect_to @channel
      end
      
      if @channel.reddit?
        @channel.uid = params[:user]
        @channel.secret = params[:pwd]
        @channel.save
        redirect_to @channel
      end
      
      if @channel.google_plus?
        @channel.email = params[:email]
        @channel.secret = params[:pwd]
        @channel.save
        redirect_to @channel
      end
      
      if @channel.dropbox?
        @channel.token = params[:token]
        @channel.secret = params[:pwd]
        @channel.save
        redirect_to @channel
      end
    else
      @channel = current_user.brand.channels.find(params[:id])
    end
    @my_posts = []
    @comments = []
  end
  
  def edit
    @channel = current_user.brand.channels.find(params[:id])
  end
  
  
  def ignore
    @channel = Channel.find(params[:id])
    id = params[:uid]
    @channel.add_to_ignored(id)
    @channel.save
    if params[:redirect]
      redirect_to params[:redirect]
    else
      redirect_to @channel , :notice => "Ignoring user"
    end
  end
  
  def update
    #    raise params.inspect
    @channel = Channel.find(params[:channel][:id])
    if @channel.update_attributes(params[:channel])
    end
    redirect_to @channel, notice: "Channel #{@channel.name} was successfully updated."
  end
  
  def show
    @brand = current_user.brand
    @channel = current_user.brand.channels.find(params[:id])
    
#    redirect_to :action => "register" , :id => @channel.id if @channel.in_use? == false
    
    @jobs = @channel.posts.where(:kind => "job")
    @content = @channel.posts.where(:kind => "recommended")
    @notifications = @channel.posts.where(:kind => "notification").order("posted_at DESC")
    @my_posts = @channel.posts.where(:kind => "my-post").order("posted_at DESC")
    @posts = @channel.posts.order("posted_at DESC")
    @mentions = @channel.posts.where(:kind => "mention").order("posted_at DESC")
    @competitors = @channel.fans.where(:kind => "competitor").order("friends_count DESC").limit(14)
    @fans = @channel.fans.where(:kind => "fan").order("friends_count DESC")
    @new_fans = @channel.fans.where(:kind => "fan").order("friends_count DESC").limit(3)
    @prospects = @channel.fans.where(:kind => "prospect").order("friends_count DESC").limit(14)
    @groups = @channel.fans.where(:kind => "group").order("friends_count DESC").limit(14)
    @messages = @channel.posts.where(:kind => "inbox").order("posted_at DESC")
    @comments = @channel.comments
    
    # for the top bar
    begin
      @comment_ratio =  1.0 * @comments.count/ @my_posts.count   * 100
    rescue
      @comment_ratio = 0
    end
  end
  
  def scan
    redirect_to "/channels/#{@channel.id}"
  end


  
  def add_feed
    @channel = current_user.brand.get_channel("rss")
    feed = Fan.new(:brand_id => current_user.brand.id, :channel_id => @channel.id, :kind => "feed", :name => params[:url], :website => params[:url], :likes => 0, :posts_count => 0)
    feed.save
    logger.error "ADDING NEW RSS FEED"
    redirect_to @channel , :notice => "ADDING NEW RSS FEED"
  end
  
  def remove_feed
    @channel = current_user.brand.get_channel("rss")
    if params[:id]== "all"
      @channel.fans.delete_all
    else
      fan = @channel.fans.find(params[:id])
      fan.delete
    end
    redirect_to @channel , :notice => "RSS FEED REMOVED"
  end
end
