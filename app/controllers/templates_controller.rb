class TemplatesController < ApplicationController
  # GET /templates
  # GET /templates.json
  def index
    @emails = Template.where(:kind => "email")
    @websites = Template.where(:kind => "website")
    @newsletters = Template.where(:kind => "newsletter")
    @css = Template.where(:kind => "css")
    @templates = Template.all
    @pages = Page.where(:website_id => -1)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @templates }
    end
  end

  # GET /templates/1
  # GET /templates/1.json
  def show
    @template = Template.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @template }
    end
  end

  # GET /templates/new
  # GET /templates/new.json
  def new
    @template = Template.new
    @template.kind = params[:kind]
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @template }
    end
  end

  # GET /templates/1/edit
  def edit
    @template = Template.find(params[:id])
  end

  # POST /templates
  # POST /templates.json
  def create
    @template = Template.new(params[:template])

    respond_to do |format|
      if @template.save
        format.html { redirect_to @template, notice: 'Template was successfully created.' }
        format.json { render json: @template, status: :created, location: @template }
      else
        format.html { render action: "new" }
        format.json { render json: @template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /templates/1
  # PUT /templates/1.json
  def update
    @template = Template.find(params[:id])

    respond_to do |format|
      if @template.update_attributes(params[:template])
        format.html { redirect_to "/templates/#{@template.id}/edit", notice: 'Template was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /templates/1
  # DELETE /templates/1.json
  def destroy
    @template = Template.find(params[:id])
    @template.destroy

    respond_to do |format|
      format.html { redirect_to templates_url }
      format.json { head :no_content }
    end
  end
  
  def preview
    @template = Template.find(params[:id])
    @site = current_user.website
#    @site.pages.delete_all
    @template.create_pages(@site)
    redirect_to "/pages/preview?id=#{@site.pages.first.id}"
  end
  
  
  def duplicate
    @template = Template.find(params[:id])
    @new_template = Template.new(
      :name => "copy of #{@template.name} " , 
      :html => @template.html,
      :css => @template.css,
      :kind => @template.kind
    )
    @new_template.save
    redirect_to @new_template
  end
end
