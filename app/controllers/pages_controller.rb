class PagesController < ApplicationController
  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pages }
    end
  end
  
  
  def copy_template
    @page = Page.find(params[:id])
    code = Template.find(params[:template]).html
    raise code.inspect
    @page.save
    redirect_to  "/pages/preview?preview=true&id="+@page.id.to_s
  end
  
  def preview_inner
    
    
    @css_templates = Template.where(:kind => "css")
    @templates = Template.where(:kind => "website")
    @page = Page.find(params[:id])
    @website = @page.website
    @fonts =""
    @page.create_default_content if @page.page_partials.count == 0
    @website_css = ""
#    if @website.template.css_file_name.present?
#      file = File.open("#{Rails.root}/public/templates/css/#{@website.template.css_file_name}", "rb")
#      @website_css = file.read
#      begin
#        @fonts = File.open("#{Rails.root}/public/templates/css/#{@website.template.css_file_name.gsub(".css",".fonts")}", "rb").read
#      rescue Exception => e
#        @fonts = ""
#        logger.error "FAILED ON PARSING FONTS FOR TEMPALTE #{e.message}"
#      end
#    end
    @socialmachine_css = File.open("#{Rails.root}/public/templates/css/socialmachine.css", "rb").read
    
    render :layout => 'site'
  end
  
  
  def make_template
    @page = Page.find(params[:id])
    @page.duplicate_to_template
    redirect_to :action => 'preview' , :id => params[:id]
  end
  
  def save_colors
    page = Page.find(params[:id])
    site = page.website
    site.create_color_patch_from_site_colors
    redirect_to :action => 'preview' , :id => params[:id]
  end


  # create new page from given 
  def create_page
    @page = Page.find(params[:id])
    @page.duplicate
    redirect_to "/pages/preview?id=#{@page.id}"
  end

  
  def layout
    @page = Page.find(params[:id])
    @yaml = @page.to_mapx
  end
  
  
  # GET /pages/1
  # GET /pages/1.json
  def show
    @page = Page.find(params[:id])
    #    @page.create_default_content if @page.page_partials
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @page }
    end
  end
  
  def preview
    @css_templates = Template.where(:kind => "css")
    @templates = Template.where(:kind => "website")
    @page = Page.find(params[:id])
    @website = @page.website
    
#    @website_css = ""
#    if @website.template.css_file_name.present?
#      file = File.open("#{Rails.root}/public/templates/css/#{@website.template.css_file_name}", "rb")
#      @website_css = file.read
#    end
#    
    @page.create_default_content if @page.page_partials.count == 0
    
    if @page.float_layout == true
      render :layout => "preview_float"
    else
      render :layout => "preview"
    end
    
    
    
  end

  def css
    @page = Page.find(params[:id])
    render :layout => "preview"
  end

  # GET /pages/new
  # GET /pages/new.json
  def new
    @page = Page.new
    if params[:kind]
      if params[:kind] == "facebook"
        
      end
      if params[:kind] == "promotion"
        @page.name = "New Promotion Page"
        @page.website_id = current_user.brand.site.id
        @page.kind = params[:kind]
        @page.save
      end
      if params[:kind] == "newsletter"
        @page.name = "My Newsletter"
        @page.website_id = current_user.brand.site.id
        @page.kind = params[:kind]
        @page.save
        
      end
    end
    redirect_to current_user.brand.site
  end

  # GET /pages/1/edit
  def edit
    @page = Page.find(params[:id])
    @templates = Template.all
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(params[:page])
    if @page.save
      redirect_to "/websites/#{@page.website.id}", notice: 'Page was successfully created.'
    else
      render action: "new" 
    end
  end

  # PUT /pages/1
  # PUT /pages/1.json
  def update
   
    @page = Page.find(params[:id])

    respond_to do |format|
      if @page.update_attributes(params[:page])
        format.html { redirect_to :action => "edit", :id => @page, notice: 'Page was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page = Page.find(params[:id])
    @site = @page.website
    # remove all partials 
    for p in @page.page_partials do 
      p.destroy
    end
    @page.destroy
    redirect_to "/websites/#{@site.id}"
  end
  
  
  def move_row_top
    @page = Page.find(params[:id])
    pp = PagePartial.find(params[:pp])
    if pp
      if pp.row  > 0
        old_row = pp.row
        first_row = @page.page_partials.order("row ASC").first.row       
        new_row = first_row - 1
        old_row_pps =[]
        @page.page_partials.where(:row => old_row).each{|pp| old_row_pps << pp.id }
        new_row_pps =[]
        @page.page_partials.where(:row => new_row).each{|pp| new_row_pps << pp.id }
        
        old_row_pps.each{|id |item = PagePartial.find(id); item.row = new_row; item.save; item.reload}
        new_row_pps.each{|id |item = PagePartial.find(id); item.row = old_row; item.save; item.reload}
      end
    end
    redirect_to "/pages/preview?id="+@page.id.to_s 
  end
 
  def move_row_up
    @page = Page.find(params[:id])
    pp = PagePartial.find(params[:pp])
    if pp
      if pp.row  > 0
        old_row = pp.row
        new_row = old_row - 1
        old_row_pps =[]
        @page.page_partials.where(:row => old_row).each{|pp| old_row_pps << pp.id }
        new_row_pps =[]
        @page.page_partials.where(:row => new_row).each{|pp| new_row_pps << pp.id }
        
        old_row_pps.each{|id |item = PagePartial.find(id); item.row = new_row; item.save; item.reload}
        new_row_pps.each{|id |item = PagePartial.find(id); item.row = old_row; item.save; item.reload}
      end
    end
    redirect_to "/pages/preview?id="+@page.id.to_s 
  end
  
  def move_row_down
    @page = Page.find(params[:id])
    pp = PagePartial.find(params[:pp])
    if pp
      if pp.row  <  @page.last_row
        old_row = pp.row
        new_row = old_row + 1
        old_row_pps =[]
        @page.page_partials.where(:row => old_row).each{|pp| old_row_pps << pp.id }
        new_row_pps =[]
        @page.page_partials.where(:row => new_row).each{|pp| new_row_pps << pp.id }
        
        old_row_pps.each{|id |item = PagePartial.find(id); item.row = new_row; item.save; item.reload}
        new_row_pps.each{|id |item = PagePartial.find(id); item.row = old_row; item.save; item.reload}
      end
    end
    
    redirect_to "/pages/preview?id="+@page.id.to_s 
  end
  
  def remove_row
    @page = Page.find(params[:id])
    pp = PagePartial.find(params[:pp])
    if pp
      @page.page_partials.where(:row => pp.row).delete_all
    end
    redirect_to "/pages/preview?id="+@page.id.to_s 
  end
 
  def add_line
    @page = Page.find(params[:id])
    @page.add_line(params[:layout])
    redirect_to "/pages/preview?preview=true&id="+@page.id.to_s
  end  
  

  def export
    @page = Page.find(params[:id])
    @site = @page.website
    render :text => @site.deep_xml
  end

  def set_css_template
    @page = Page.find(params[:id])
    @site = @page.website
    @site.template_id = params[:template]
    @site.save
    #    @page.css = Template.find(params[:template]).css
    #    @page.save
  
    redirect_to  "/pages/preview?preview=true&id="+@page.id.to_s
  end
  
end
