class EmailsController < ApplicationController
  # GET /emails
  # GET /emails.json

  
  def delete_all
    Email.delete_all
    redirect_to :action => "index"
  end
  
  def send_email
#    @email = Email.find(params[:id])
#     ch = current_user.brand.get_channel("gmail")
#     Gmailer.send_gmail_email(ch.email, ch.secret, @email).deliver
##    @email.send_email(request.original_url)
    redirect_to @email
  end
  
  def index
    @emails = Email.all
#    @emails = current_user.brand.emails.reverse

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @emails }
    end
  end

  # GET /emails/1
  # GET /emails/1.json
  def show
    @email = Email.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @email }
    end
  end

  # GET /emails/new
  # GET /emails/new.json
  def new
    @email = Email.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @email }
    end
  end

  # GET /emails/1/edit
  def edit
    @email = Email.find(params[:id])
  end

  # POST /emails
  # POST /emails.json
  def create
#    raise params.inspect
    @email = Email.new(params[:email])

    if @email.kind.include? "manual"
      @email.brand_id  = current_user.brand.id
      @email.status = "created"
      @email.from_email = current_user.brand.get_channel("gmail").email
      @email.from_name = current_user.name
      @email.template_id = Template.where(:kind => "email").first.id if params[:email][:template_id].nil?
    end
    
    
    if @email.save
      if @email.kind.include? "manual"
        @email.send_email
        redirect_to current_user.brand.get_channel("gmail"), notice: 'Email was successfully created.' 
      else
        redirect_to @email, notice: 'Email was successfully created.' 
      end
    end
  end

  # PUT /emails/1
  # PUT /emails/1.json
  def update
    @email = Email.find(params[:id])

    respond_to do |format|
      if @email.update_attributes(params[:email])
        format.html { redirect_to @email, notice: 'Email was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @email.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emails/1
  # DELETE /emails/1.json
  def destroy
    @email = Email.find(params[:id])
    @email.destroy

    respond_to do |format|
      format.html { 
        if params[:rediret_to]
          redirect_to params[:redirect_to]
        else
          redirect_to emails_url 
        end
      }
      format.json { head :no_content }
    end
  end
end
