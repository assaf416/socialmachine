class ArticlesController < ApplicationController
  include ApplicationHelper
  include FacebookHelper
  
  def deploy_to_facebook_page
    @article = Article.find(params[:id])
    begin
      fb_deploy_to_page(@article, params[:page], params[:token])
    rescue Exception => e
      logger.error "FAILDED DEPLOYING ARTICLE TO FACEBOOK #{e.message}"
    end
    redirect_to @article , notice: 'Article sent to Facebook Page'
  end

  def deploy_to_twitter
    @article = Article.find(params[:id])
    @article.post_to_twitter
    redirect_to @article , notice: 'Article sent to Twitter'
  end

  def deploy_to_blog_via_email
    @article = Article.find(params[:id])
    @channel = current_user.brand.get_channel(params[:channel])
    begin
      logger.error "SENDING EMAIL TO BLOG #{params[:channel]}"
      @article.send_mail(@channel.email, @article.name , @article.body, "")
    rescue Exception => e
      logger.error "FAILDED DEPLOYING ARTICLE TO BLOGS #{e.message}"
    end
    redirect_to @article , notice: "Article Sent to #{@channel.name} "
  end

  
  def publish
    @article = Article.find(params[:id])
    @article.draft = false
    @article.url = "www.socialmachine.biz/public/article/#{params[:id]}"
    Google::UrlShortener::Base.api_key = "AIzaSyDQU97mjsTO_qfzAHNcfguvhcd2CNvv6Yw"
    url = Google::UrlShortener::Url.new(:long_url => @article.url)
    @article.short_url =  url.shorten! 
    @article.save
    redirect_to @article
  end
  
  def index
    @brand = current_user.brand
    @articles = current_user.brand.articles.reverse

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @articles }
    end
  end

  def show
    @brand = current_user.brand
    @article = Article.find(params[:id])
    @wp = current_user.brand.get_channel("wordpress")
    @bl = current_user.brand.get_channel("blogger")
  end

  
  include ActionView::Helpers::TextHelper
  def new
    @article = Article.new
    
    if params[:youtube_url]
      @article.kind = "movie"
      @article.brand_id = current_user.brand.id
      @article.remote_movie_url = params[:youtube_url]
      @article.name = params[:name]
      @article.save
    else
      @article.kind = params[:kind]
    end
    
    
    @article.draft = true
    @article.url = "www.socialmachine.biz/public/article/#{params[:id]}"
    Google::UrlShortener::Base.api_key = "AIzaSyDQU97mjsTO_qfzAHNcfguvhcd2CNvv6Yw"
    url = Google::UrlShortener::Url.new(:long_url => @article.url)
    @article.short_url =  url.shorten! 
    
    
    
    if params[:from_kind] and params[:from_id]
      if params[:from_kind].include? "post"
        post = current_user.brand.posts.find(params[:from_id])
        if post
          @article.name = simple_format(post.title)
          @article.body = post.body
          begin
            if post.extracted_image
              @article.remote_image_url = post.extracted_image
            else 
              @article.remote_image_url = post.image_url 
            end
          rescue Exception => e
            @article.remote_image_url = ""
            logger.error " FAILED TO REPOST A POST #{post.id}  reason: #{e.message} "  
          end
          @article.kind = "article"
          @article.cite = post.from_name
        end
      end
      
      if params[:from_kind].include? "rss_feed"
        begin
          @yaml = YAML::load( File.open( "#{Rails.root}/../scanner/#{current_user.brand.id}.rss.yml" ) )
          for item in @yaml[:unread][:articles] do 
            if item[:link] == params[:from_id] then
              @article.name = item[:title]
              @article.body = item[:message]
              @article.kind = "article"
              @article.cite = item[:link]
            end
          end
        rescue Exception => e
          raise e.inspect
        end

      end
      if params[:from_kind].include? "movie"
        post = current_user.brand.posts.find(params[:from_id])
        if post
          @article.brand_id = current_user.brand.id
          @article.name = post.title
          @article.body = post.body
          @article.remote_movie_url = post.uid
          @article.kind = "clip"
          @article.save
        end
      end
      
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def create
    @article = Article.new(params[:article])
    @article.author = current_user.name
    @article.brand_id = current_user.brand.id
    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render json: @article, status: :created, location: @article }
      else
        format.html { render action: "new" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @article = Article.find(params[:id])
    @article.author = current_user.name
    #    Resque.enqueue(ArticlePoster, @article.id)
    respond_to do |format|
      if @article.update_attributes(params[:article])
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    respond_to do |format|
      format.html { redirect_to articles_url }
      format.json { head :no_content }
    end
  end
  
  
  
  def deploy
    #    @article = params[:id]
    raise params.inspect
  end
end
