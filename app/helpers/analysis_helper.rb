module AnalysisHelper
  
  require 'nokogiri'
  require 'open-uri'
  
  
  def get_page(url)
    begin
      return Nokogiri::HTML( open(URI.encode(url)))
    rescue Exception => e
      logger.error  "FAILED TO LOAD PAGE FOR ANALYSIS: #{url} , #{e.message}"
    end
  end
  
  def backlinks(url)
    res = []
    page = open "http://www.google.com/search?num=100&q=#{url}"
    html = Nokogiri::HTML page
    html.search("cite").each do |cite|
      res << cite.inner_text
    end
    res
  end
  
  
  def backlinks_yahoo(url)
    res = []
    item = {}
    my_domain = url.gsub("http://www.","")
    backlinks_url = "http://search.yahoo.com/search?p=inbody: #{my_domain}"
    doc = Nokogiri::HTML( open(URI.encode(backlinks_url)))
    doc.css('.res').each do |link|
      if link.css(".res")
        item[:title] = link.css("h3").inner_text
        item[:description] = link.css(".abstr").inner_text
        item[:url] = link.css(".url").inner_text
        res << item
      end
    end
    res
  end
  
  def backlinks_bing(url)
    res = []
    item = {}
    my_domain = url.gsub("http://","")
    backlinks_url = "http://www.bing.com/search?q=link%3A+#{my_domain}"
    doc = Nokogiri::HTML( open(URI.encode(backlinks_url)))
    doc.css('.sa_cc').each do |link|
      #      item[:all] = link
      item[:title] = link.css(".sb_tlst").text
      item[:description] = link.css("p").text
      item[:url] = link.css(".sb_mets").inner_text
      res << item
    end
    res
  end
  
  
  
  
  def backlinks_count(url)
    res = ""
    my_domain = url.gsub("http://","")
    backlinks_url = "http://search.yahoo.com/search;_ylt=A0PDoS13GAtPrCEAtpKJzbkF?p=linkdomain: #{my_domain}"
    doc = Nokogiri::HTML( open(URI.encode(backlinks_url)))
    doc.css('resultCount').each do |link|
      res =  link.content
    end
    res.to_s
  end
  

  
   
  def meta_keywords(doc)
    begin
      res = []
      doc.css('meta[name="keywords"]').each do |meta_tag|
        res << meta_tag['content'] 
      end
      res
    rescue
      ""
    end
  end
  
  def page_links(doc)
    begin
      res = []
      doc.css('a').each do |link|
        res << link 
      end
      res
    rescue
      ""
    end
  end
  
  
  def meta_description(doc)
    begin
      res = ""
      doc.css('meta[name="description"]').each do |meta_tag|
        res << meta_tag['content'] 
      end
      res
    rescue Exception => e
      puts "!! -- EXEPTION IN META_DESCRIPTION"
      puts e.message
      " "
    end
  end

  def words(doc)
    begin
      res = ""
      text  = doc.at('body').inner_text
      words = text.scan(/\w+/)
    rescue Exception => e
      puts "!! -- EXEPTION IN WORDS"
      puts e.message
      []
    end
  end
  
  def meta_name(doc)
    return ""
    begin
      res = ""
      doc.css('title').each do |meta_tag|
        res << meta_tag.text # => 35.667459;139.706256
      end
      res
    rescue Exception => e
      puts "!! -- EXEPTION IN META_NAME"
      puts e.message
      " "
    end
  end
  
  
  
  
  def keywords(page_content)
    
  end
  
  def links_in_page(page_content)
    
  end
  
  def images_in_page(page_content)
    
  end
  
  def keywords_in_search_engines(page_content)
    
  end
  
  def text_and_graphic_ratio(page_content)
    
  end
  
  def page_size_and_load_time(page_content)
    
  end
  
  
end
