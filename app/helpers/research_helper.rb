module ResearchHelper
  
  require 'koala'
  require 'twitter'
  require 'youtube_it'
  require 'open-uri'
  require 'nokogiri'

    
  
  def scan_url(url)
    begin
      ar = []; images =[]
      keywords = ""
      meta_description = ""
      
      begin
        doc = Nokogiri::HTML( open(URI.encode(url)))
        begin
          doc.css('a').each do |link|
          
            ar << link['href'] if link['href'].include? "www.facebook.com"
            ar << link['href'] if link['href'].include? "www.twitter.com"
            ar << link['href'] if link['href'].include? "www.youtube.com"
            ar << link['href'] if link['href'].include? "www.linkedin.com"
            ar << link['href'] if link['href'].include? "www.reddit.com"
            ar << link['href'] if link['href'].include? "www.digg.com"
            ar << link['href'] if link['href'].include? "www.google.com"
            
          end
        end
      rescue Exception => e3
        puts "!! -- EXEPTION LINKS"
        puts e3.message
        " "      
      end
      
      
      begin
        
        doc.css('img').each do |link|
          if link['src'].include? "http:"
            images << link['src']
          else
            images << url + link['src']
          end
        end
      rescue Exception => e3
        puts "!! -- EXEPTION LINKS"
        puts e3.message
        " "      
      end
      
      
      begin
        doc.css('meta[name="keywords"]').each do |meta_tag|
          keywords = meta_tag['content'] 
        end
      rescue Exception => e2
        puts "!! -- EXEPTION IN META_KEYWORDS"
        puts e2.message
        " "      
      end
  
  
      begin
      
        doc.css('meta[name="description"]').each do |meta_tag|
          meta_description << meta_tag['content'] 
        end
      rescue Exception => e
        puts "!! -- EXEPTION IN META_DESCRIPTION"
        puts e.message
        " "
      end
      return { :links => ar, :meta => meta_description, :keywords => keywords, :images => images}
    rescue Exception => e
      logger.error " FAILED GETTING SOCIAL LINKS FROM URL #{url}, Message :#{e.message}"
    end
  end
  

#  def facebook_mark(name)
#    begin
#      oauth_access_token = "AAAEyg2j03cMBAEVH0Ya0yo8GdMthcmlY4u6dr94qiTVFt843Ew62Qvu1wJqiDiYiN4RREJONwKuDsgW1aPzPZBnvoWeAw0hZA6ZB5xQOAZDZD"
#      @graph = Koala::Facebook::API.new(oauth_access_token) 
#      profile = @graph.get_object(name)
#      feed = @graph.get_connections(name, "feed")
#      return "FACEBOOK::  Likes: #{profile["likes"]} | Talk About #{profile["talking_about_count"]} |  Posts #{feed.size}"
#    rescue Exception => e
#      puts  e.message + " --> #{name} on FACEBOOK"
#    end
#  end


  def xyoutube_mark(name)
    begin
      client = YouTubeIt::Client.new  
      profile = client.profile(name) 
      i = 0
      return  "YOUTUBE:: Subscribers #{profile.subscribers} |  VIEWS: #{profile.view_count} "
    rescue Exception => e
      puts  e.message + " --> #{name} on YOUTUBE"
    end
  end


  def xtwitter_mark(name)
    begin
      profile = Twitter.user(name)
      return "TWITTER:: Posts: #{profile['statuses_count']} | Friends #{profile['friends_count']} | Followers: #{profile['followers_count']} "
    rescue Exception => e
      puts  e.message + " --> #{name} on TWITTER"
    end
  end

  
  
  
end
