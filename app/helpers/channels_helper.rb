module ChannelsHelper
  
  require 'youtube_it'
  
  
  def wordpress_import(channel,yaml)
    posts =[]
    for p in yaml[:articles] do
      post = Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "my-post",
        :posted_at => p[:published_at],
        :title => p[:name],
        :body => p[:summary],
        :from_name => p[:feed],
        :target_link  => p[:url]
        #        :image_url => p[:from_user_profile_image_url]
      )
      posts << post
    end
    logger.error "STATUS :  BEFORE WORDPRESS DELETETING ALL POSTS"
    channel.posts.delete_all
    t0 = Time.now
    Post.import posts
    logger.error "STATUS : AFTER WORDPRESSIMPORT"
    
  end
  
  def foursquare_import(channel,yaml)
    logger.error "STARTING LOADING FOURSQUARE "
    
    #
    # Friends
    #    
    followers = yaml[:friends][:list]
    fans = []
    for f in followers do 
      begin 
        ff = Fan.new( :brand_id => channel.brand.id ,
          :channel_id => channel.id , 
          :kind          => "fan",
          :name          => f["firstName"] + " " + f["lastName"],
          :email          => f["contact"]["email"],
          :bio           => f["bio"] ,
          :website       => f["url"],
          :friends_count => f["friend_count"],
          :location      => "#{f["hometown_location"]} ",
          :posts_count   => f["tips"]["count"],
          :image_url     => f["photo"],
          :foursquare_uid           => f["id"]
        )
        fans << ff;
      rescue Exception => e
        logger.error "FAILED ON ADDING FOURSQUARE FAN #{e.message}"
        next
      end
    end
    
    #
    # competitor (venues )
    #    
    followers = yaml[:venues][:list]
    for f in followers do 
      begin
        ff = Fan.new( :brand_id => channel.brand.id ,
          :channel_id => channel.id , 
          :kind          => "competitor",
          :name          => f[:name] ,
          #          :email          => f["contact"]["email"],
          #          :bio           => f["bio"] ,
          :website       => f["url"],
          :friends_count => f[:stats]["checkinsCount"],
          :location      => "#{f[:location]["address"]} #{f[:location]["city"]}",
          :posts_count   => f[:stats]["tipCount"],
          #        :image_url     => f["photo"],
          :foursquare_uid    => f[:id]
        )
        fans << ff;
      rescue Exception => e
        logger.error "FAILED ON ADDING FOURSQUARE COMPETITOR #{e.message}"
      end
    end
    
    
    logger.error "STATUS : BEFORE DELETETING FOURSQUARE ALL FANS"
    channel.fans.delete_all
    Fan.import fans
    Fan.where(:channel_id => nil).delete_all
    logger.error "STATUS : AFTER FOURSUQARE FAN IMPORT"
    
  end
  
  
  def youtube_import(channel,yaml)
    
    logger.error " YOUTUBE IMPORT "
    
    profile = yaml[:profile]
    
    channel.fans_count = profile.subscribers
    channel.profile_image_url = profile.avatar
    channel.profile_name = profile.username_display
    channel.views_count = profile.upload_views
    channel.uid = profile.user_id
    channel.save
    
    posts = []
    for movie in yaml[:my_videos] do 
      v = movie[:video]
      post = Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "my-post",
        :title => "#{v[:title]}",
        :image_url => v[:thumbnails],
        :body => v[:description],
        :target_link => v[:player_url],
        :posted_at => v[:published],
        :from_name => "#{v[:author_name]}",
        :from_uid => v[:author_url]
      )
      posts << post
    end
    
    
    my_subscriptions = yaml[:subscriptions_videos]
    for movie in my_subscriptions do 
      v = movie
      post = Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "subscribed",
        :title => "#{v[:title]}",
        :image_url => v[:thumbnails],
        :body => v[:description],
        :target_link => v[:player_url],
        :posted_at => v[:published],
        :from_name => "#{v[:author_name]}",
        :from_uid => v[:author_url]
      )
      posts << post
    end
    
    
    search_videos = yaml[:search_videos]
    for movie in search_videos do 
      v = movie[:video]
      post = Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "prospect",
        :title => "#{v[:title]}",
        :image_url => v[:thumbnails],
        :body => v[:description],
        :target_link => v[:player_url],
        :posted_at => v[:published],
        :from_name => "#{v[:author_name]}",
        :from_uid => v[:author_url]
      )
      posts << post
    end
    
    
    
    logger.error "STATUS : BEFORE DELETETING ALL VIDEOS"
    channel.posts.delete_all
    t0 = Time.now
    Post.import posts
    logger.error "STATUS : AFTER  VIDEOS IMPORT"
    
    
    
    users = []
    for cc in yaml[:competitors] do 
      begin
        users << Fan.new( 
          :channel_id => channel.id, :brand_id => channel.brand_id,
          :kind => "competitor", 
          :location => cc[:location],
          :name => cc[:name],
          :friends_count => cc[:subscribers],
          :posts_count => cc[:movies],
          #          :likes => cc[1].view_count,
          :image_url => cc[:image_url]
        )
      rescue Exception => e
        logger.error "FAILED ON GETTING COMPETITOR INFO. #{e.message}"
        next
      end
    end
      
    
    comments = []
    for c in yaml[:comments] do 
      begin
        comment = Comment.new(
          :brand_id   => channel.brand_id ,
          :channel_id => channel.id, 
          :message => c[:title] + c[:message],
          :kind    => "youtube_comment",
          :name    => c[:author],
          :read    => false,
          :posted_at    => c[:posted_at],
          :uid  => c[:url],
          :from_uid => c[:author_url],
          :post_uid => c[:url],
          :target_link => c[:reply_to]
        )
        comments << comment
      rescue Exception => e
        logger.error "FAILED ON GETTING COMMENTS INFO. #{e.message}"
        logger.error e.backtrace
        next
      end
    end
    
    logger.error "STATUS : BEFORE DELETETING ALL COMMENTS"
    channel.comments.delete_all
    t0 = Time.now
    Comment.import comments
    logger.error "STATUS : AFTER  COMMMENTS IMPORT"
    
   
    
     
    #    for cc in yaml[:subscriptions] do 
    #      begin
    #        #        logger.error  " USER NAME:  #{cc[1].username_display}"
    #        users << Fan.new( 
    #          :channel_id => channel.id, :brand_id => channel.brand_id,
    #          :kind => "subscription", 
    #          :name => cc[1].username_display,
    #          :friends_count => cc[1].subscribers,
    #          :posts_count => cc[1].upload_count,
    #          :likes => cc[1].view_count,
    #          :image_url => cc[1].avatar
    #        )
    #      rescue Exception => e
    #        logger.error "FAILED ON GETTING MY SUBSCRIPTIONS INFO. #{e.message}"
    #        next
    #      end
    #    end
    
    logger.error "STATUS : BEFORE DELETETING ALL YOUTUBE FANS"
    channel.fans.delete_all
    t0 = Time.now
    Fan.import  users
    logger.error "STATUS : AFTER YOUTUBE FANS IMPORT"
    
  end
  
  def linkedin_import(channel,yaml)
    friends = []
    posts = []
    competitors = []
    groups = []
    
    
    #    firstName: Itamar
    #  headline: Chief Financial Officer at Enox Biopharma, Inc.
    #  industry: Venture Capital & Private Equity
    #  lastName: David
    #  pictureUrl: http://m3.licdn.com/mpr/mprx/0_2weeFyEIkHBEwdyB22HUF0gkkDrbWEjBmEycF0VJUHXBnaoc8SZkWxmcE_KZoSpUDeEnHZkbf3U8
    #  siteStandardProfileRequest:
    #    url: http://www.linkedin.com/profile?viewProfile=&key=36502355&authToken=dJeU&authType=name&trk=api*a166823*s175075*
    #    
    channel.profile_name = "#{yaml[:profile]["firstName"]} #{yaml[:profile]["lastName"]} "
    channel.profile_image_url = yaml[:profile]["pictureUrl"]
    channel.profile_description = yaml[:profile]["headline"]
    channel.profile_description << yaml[:profile]["summary"]
    channel.profile_url = yaml[:profile]["siteStandardProfileRequest"]["url"]
    channel.save
    
    
    logger.error " LINKED IN IMPORT"
    logger.error " GROUP POSTS:"
  
    for px in yaml[:group_posts]
      begin
        post = Post.new(
          :channel_id => channel.id, 
          :brand_id => channel.brand_id,
          :kind =>  "group",
          :title => "#{px[:title]}  - #{px[:group_name]}",
          :image_url => px[:image_url],
          :body => px[:message],
          :target_link => px[:target_link],
          :posted_at => px[:posted_at],
          :from_name => "#{px[:from_name]}",
          :from_uid => px[:from_uid]
        )
        posts << post
        
      rescue Exception => e
        puts "FAILED error: #{e.message} on #{px.inspect}"
        next
      end
    end
      
      
    
    logger.error " LINKED IN JOBS"
  
    for px in yaml[:jobs]
      begin
        post = Post.new(
          :channel_id => channel.id, 
          :brand_id => channel.brand_id,
          :kind =>  "job",
          :title => "#{px[:title]} #{px[:sallary]}",
          :body => px[:description],
          :target_link => px[:url],
          :posted_at => px[:posted_at],
          :from_name => "#{px[:company]}  (#{px[:location]})"
        )
        post.rank = location_match(p, channel.brand.job_locations)
        posts << post
        
      rescue Exception => e
        puts "FAILED error: #{e.message} on #{px.inspect}"
        next
      end
      #        puts " POST: #{p.inspect}"
    end
      
    
    
    
    logger.error "STATUS : BEFORE DELETETING ALL POSTS"
    channel.posts.delete_all
    t0 = Time.now
    Post.import posts
    logger.error "STATUS : AFTER GROUP POSTS IMPORT"
    
    for fx in yaml[:friends]do 
      
      friends << Fan.new( :brand_id => channel.brand.id ,
        :channel_id => channel.id , 
        :kind          => "fan",
        :name          => fx[:name],
        :screen_name   => fx[:industry],
        :location      => "#{fx[:location]["name"]}",
        :bio           => "#{fx[:headline]}  #{fx[:industry]} ",
        :image_url     => fx[:image_url],
        :friends_count => fx[:friends_count],
        :website  =>      fx[:private_url],
        :linkedin_uid => fx[:uid]
      )
    end
      
#    logger.error "STATUS  BEFORE COMPETITOR LIST"
#    for fx in yaml[:competitors][:list] do 
#      unless fx[:uid].nil?
#        friends << Fan.new( :brand_id => channel.brand.id ,
#          :channel_id => channel.id , 
#          :kind          => "competitor",
#          :website  => fx[:private_url],
#          :linkedin_uid => fx[:uid],
#          :location      => "#{fx[:location]["name"]}",
#          :name          => fx[:name],
#          :bio           => "Industry:  #{fx[:industry]}\n#{fx[:sumary]} ",
#          :image_url     => fx[:image_url],
#          :friends_count => fx[:friends_count]
#        )
#      end
#    end
#    logger.error "STATUS  BEFORE PROSPECTS LIST"
#    for item in yaml[:prospects][:list] do 
#      friends << Fan.new( :brand_id => channel.brand.id ,
#        :channel_id    => channel.id , 
#        :kind          => "prospect",
#        :linkedin_uid  => item[:uid],
#        #        :location      => "#{item[:location]["name"]}",
#        :name          => item[:name],
#        :bio           => item[:summary] ,
#        :image_url     => item[:image_url],
#        :friends_count => item[:friends_count],
#        :website  => item[:private_url])
#    end
#   
    begin
      logger.error "STATUS  BEFORE RECOMMENDED GROUPS"
      groups = yaml[:recommended_groups]["group"]
      for item in groups do 
      
        if item["large-logo-url"]
          img = item["large-logo-url"][0]
        else
          img = ""
        end
        friends << Fan.new( :brand_id => channel.brand.id ,
          :channel_id    => channel.id , 
          #        :linkedin_uid  => item[:uid],
          :kind          => "recommened-group",
          :name          => item["name"][0],
          :bio           => item["description"][0] ,
          :image_url     => img,
          :friends_count => item["num-members"][0]
        )
      end
    
    rescue Exception => e
      logger.error "FAILED ON RECOMMENDED GROUP #{e.message}"
      logger.error e.backtrace
    end
    logger.error "STATUS : BEFORE DELETETING ALL FANS"
    channel.fans.delete_all
    t0 = Time.now
    Fan.import friends
    logger.error "STATUS : AFTER FAN IMPORT"

  end
  
  def rss_import(channel,yaml)

    feeds =[]
    posts =[]
    
    
    
    for p in yaml[:recommended][:articles] do
      post = Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "recommended",
        :posted_at => p[:published_at],
        :title => p[:name],
        :body => p[:summary],
        :from_name => p[:feed],
        :target_link  => p[:url]
        #        :image_url => p[:from_user_profile_image_url]
      )
      posts << post
    end
    
    
    for p in yaml[:recommended][:jobs] do
      post = Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "job",
        :posted_at => p[:published_at],
        :title => p[:name],
        :body => p[:summary],
        :from_name => p[:feed],
        :target_link  => p[:url]
        #        :image_url => p[:from_user_profile_image_url]
      )
      post.rank = location_match(p, channel.brand.job_locations)
      posts << post
    end
    
    
    for p in yaml[:unread]
      post = Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "unread",
        :posted_at => p[:published_at],
        :title => p[:name],
        :body => p[:summary],
        :from_name => p[:feed],
        :target_link  => p[:url]
        #        :image_url => p[:from_user_profile_image_url]
      )
      posts << post
    end
        
    
    feeds = yaml[:feeds]
    fans = []
    for f in feeds do 
      begin
        ff = Fan.new( :brand_id => channel.brand.id ,
          :channel_id => channel.id , 
          :kind          => "rss_feed",
          :name          => f[:name] ,
          :website       => f[:url],
          :posts_count   => f[:found],
          :likes         => f[:jobs]
        )
        
        fans << ff;
      rescue Exception => e
        logger.error "FAILED ON ADDING RSS FEEDS  #{e.message}"
        logger.error e.backtrace
        next
      end
    end
    
    
    logger.error "STATUS :  BEFORE RSS DELETETING ALL POSTS"
    channel.posts.delete_all
    t0 = Time.now
    Post.import posts
    logger.error "STATUS : AFTER RSS POSTS IMPORT"
    
    logger.error "STATUS : BEFORE DELETETING RSS FEEDS "
    channel.fans.delete_all
    Fan.import fans
    Fan.where(:channel_id => nil).delete_all
    logger.error "STATUS : AFTER RSS FEED  IMPORT"
    
  end
  
  def gmail_import(channel,yaml)
    channel.posts.delete_all
    for email in yaml[:emails]
      unless channel.posts.find_by_uid(email[:uid]) 
        p = Post.new( 
          :brand_id   => channel.brand_id, 
          :channel_id => channel.id,
          :from_name  => email[:from_email],
          :title      => email[:subject],
          :body       => email[:body_text], 
          :uid       => email[:uid],
          :posted_at  => email[:posted_at],
          :kind      => "inbox"
        )
        p.save
      end
    end
  end
  
  
  def twitter_import(channel,yaml)
    # update the profile
    profile = yaml[:profile]
    channel.uid = profile[:id]
    channel.profile_name = profile[:name]
    channel.profile_description = profile[:description]
    channel.profile_url = profile[:url]
    channel.fans_count = profile[:followers_count]
    channel.followings_count = profile[:following]
    channel.likes_count = profile[:retweet_count]
    channel.save
      
    
    fans = []
    posts = []
    comments = []
        
    #
    # Followers
    #    
    followers = yaml[:followers]
  
    for f in followers do 
      fans << Fan.new( :brand_id => channel.brand.id ,
        :channel_id => channel.id , 
        :kind          => "fan",
        :name          => f[:name],
        :screen_name   => f[:screen_name],
        :bio           => f[:description],
        :website       => f[:url],
        :friends_count => f[:followers_count],
        :following     => f[:following],
        :location      => "#{f[:location]} #{f[:time_zone]} ",
        :posts_count   => f[:statuses_count],
        :image_url     => f[:profile_image_url],
        :uid           => f[:id],
        :twitter_uid   => f[:id]
      )
    end
    
    
    
    #
    # Competitors
    #    
#    competitors = yaml[:recommended_competitors][:users]
#    for f in competitors do 
#      fans << Fan.new( :brand_id => channel.brand.id ,
#        :channel_id => channel.id , 
#        :kind          => "competitor",
#        :name          => f[:name],
#        :screen_name   => f[:screen_name],
#        :bio           => f[:description],
#        :website       => f[:url],
#        :friends_count => f[:followers_count],
#        :following     => f[:following],
#        :location      => "#{f[:location]} #{f[:time_zone]} ",
#        :posts_count   => f[:statuses_count],
#        :image_url     => f[:profile_image_url],
#        :uid           => f[:id],
#        :twitter_uid   => f[:id]
#      )
#    end
    
    
    #
    # Prospects
    #    
#    prospects = yaml[:recommended_prospects][:users]
#    for f in prospects do 
#      fans << Fan.new( :brand_id => channel.brand.id ,
#        :channel_id => channel.id , 
#        :kind          => "prospect",
#        :name          => f[:name],
#        :screen_name   => f[:screen_name],
#        :bio           => f[:description],
#        :website       => f[:url],
#        :friends_count => f[:followers_count],
#        :following     => f[:following],
#        :location      => "#{f[:location]} #{f[:time_zone]} ",
#        :posts_count   => f[:statuses_count],
#        :image_url     => f[:profile_image_url],
#        :uid           => f[:id],
#        :twitter_uid           => f[:id]
#      )
#    end
    
    #
    # Commenters
    #    
    prospects = yaml[:commenters]
    for f in prospects do 
      fans << Fan.new( :brand_id => channel.brand.id ,
        :channel_id => channel.id , 
        :kind          => "engaged",
        :name          => f[:name],
        :screen_name   => f[:screen_name],
        :bio           => f[:description],
        :website       => f[:url],
        :friends_count => f[:followers_count],
        :following     => f[:following],
        :location      => "#{f[:location]} #{f[:time_zone]} ",
        :posts_count   => f[:statuses_count],
        :image_url     => f[:profile_image_url],
        :uid           => f[:id],
        :twitter_uid           => f[:id]
      )
    end
    
    
    
    
    logger.error "STATUS : BEFORE DELETETING ALL FANS"
    channel.fans.delete_all
    t0 = Time.now
    Fan.import fans, :on_duplicate_key_update => [:friends_count,:following,:posts_count]
    logger.error "STATUS : AFTER FAN IMPORT"
    
    
    #
    # recommended content
    # 
    
#    for p in yaml[:recommended_content][:posts] do 
#      post = Post.new(
#        :channel_id => channel.id, 
#        :brand_id => channel.brand_id,
#        :kind =>  "recommended",
#        :body => p[:message],
#        :from_uid =>  p[:from_user_id],
#        :from_name => p[:from_user_name],
#        :uid  => p[:id],
#        :image_url => p[:from_user_profile_image_url],
#        :posted_at => p[:created_at],
#        :target_link => p[:source]
#      )
#      posts <<  post
#    end
    
    
    #
    # My Posts
    #
    for p in yaml[:statuses] do 
      post = Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "my-post",
        :body => p[:message],
        :from_uid =>  p[:from_user_id],
        :from_name => p[:from_user_name],
        :uid  => p[:id],
        :image_url => p[:from_user_profile_image_url],
        :posted_at => p[:created_at],
        :target_link => p[:source]
      )
      posts << post
    end
    
    
    #
    # Retweets and mentions
    #
    logger.error "STATUS : RETWEETS"
    for p in yaml[:retweets] do 
      post = Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "mention",
        :body => p[:message],
        :from_uid =>  p[:from_user_id],
        :from_name => p[:from_user_name],
        :uid  => p[:id],
        :image_url => p[:from_user_profile_image_url],
        :posted_at => p[:created_at],
        :from_followers_count => p[:from_user_followers],
        :from_post_count => p[:statuses_count],
        :from_website => p[:from_user_url],
        :target_link => p[:source]
      )
      posts << post
    end
    
    
    
    begin
      logger.error "STATUS : BEFORE COMPETITOR UPDATE AND POSTS"
    
      #
      # TWITTER COMPETITORS
      #
      competitors = yaml[:monitored_competitors]
      for c in competitors do 
        _i = c[:info]
        _p = c[:posts]
        
        comp =  Competitor.find(c[:id])
        comp.name = _i[:name]
        comp.location = _i[:time_zone]
        comp.twitter_uid = _i[:id]
        comp.twitter_followers = _i[:followers_count]
        comp.twitter_posts = _i[:statuses_count]
        comp.twitter_retweets = 0
        comp.twitter_description = _i[:description]
        comp.twitter_profile_image_url = _i[:profile_image_url]
        comp.twitter_following_count = _i[:following]
        comp.website = _i[:url]  if comp.website.nil?
        comp.save
        puts "Updated Competitor #{comp.name}"
      
        for p in c[:posts] do 
          #          puts "COMPETITOR POST : #{p.inspect}"
          posts << Post.new(
            :channel_id => channel.id, 
            :competitor_id => comp.id, 
            :brand_id => channel.brand_id,
            :kind =>  "competitor-post",
            :body => p[:message],
            :from_name => p[:from_user_name],
            :from_uid => p[:from_user_id],
            :uid  => p[:id],
            :image_url => p[:from_user_profile_image_url],
            :posted_at => p[:created_at],
            :target_link => p[:urls]
          )
        end
      end
    
    rescue Exception => e
      logger.error "FAILED ON COMPETITOR LOADING #{e.message}"
      puts e.backtrace
    end
    
    
    
    
    logger.error "STATUS : TWITTER BEFORE DELETETING ALL POSTS"
    channel.posts.delete_all
    t0 = Time.now
    Post.import posts
    logger.error "STATUS : AFTER TWITTER POSTS IMPORT"
    
    
    logger.error "STATUS : TWITTER BEFORE DELETETING ALL COMMENTS"
    channel.comments.delete_all
    t0 = Time.now
    Comment.import comments
    logger.error "STATUS : AFTER TWITTER COMMENTS IMPORT"
    
  end
  
  
  def facebook_import(channel,yaml)
    
    fans = []
    posts = []
    comments = []
        
    #
    # PROFILE UPDATE
    #
    #
    profile = yaml[:profile]
    #    channel.uid = profile[:id]
    channel.profile_name = profile[:name]
    channel.profile_description = profile[:bio]
    channel.profile_url = profile[:website]
    channel.profile_image_url = profile[:image_url]
    #    channel.fans_count = profile[:followers_count]
    #    channel.followings_count = profile[:following]
    #    channel.likes_count = profile[:retweet_count]
    channel.save
     
    #
    # article comments
    #
    logger.error "STATUS : LOADING ARTICLES COMMENTS"
    articles_comments = yaml[:articles_comments]
    for item in articles_comments do 
      begin
        for c in item[:comments] do 
          begin
            logger.error "#{c.inspect}"
            comment = Comment.new(
              :brand_id   => channel.brand_id ,
              :channel_id => channel.id, 
              :article_id => item[:article_id],
              :message => c["message"],
              :kind    => "artical_comment",
              :name    => c["from"]["name"],
              :read    => false,
              :posted_at    => c["created_time"],
              :uid  => c["id"],
              :from_uid => c["from"]["id"],
              :post_uid => p["id"]
              #              :target_link => p["actions"][0]["link"]
            )
            comments << comment
          rescue Exception => e
            logger.error e.message
            next
          end
        end
      rescue 
        next
      end
    end
    
    
    #
    # Fan Pages
    #
    brand = channel.brand
    brand.fan_pages.delete_all
    for page in yaml[:scan][:fan_pages_stats][:pages_stats] do 
      FanPage.create!( 
        :brand_id => channel.brand_id, 
        :channel_id => channel.id, 
        :name => page[:name] , 
        :uid => page[:uid] , 
        :image_url => page[:cover_url], 
        :posts => page[:posts], 
        :likes => page[:likes],
        :comments => page[:comments]
      )
    end
    
    
    #
    # Followers
    #    
    followers = yaml[:friends]
  
    for f in followers do 
      
      bio = "Gender : #{f["sex"]}" 
      bio << "<br/> Affiliations: #{f["affiliations"]}"
      ff = Fan.new( :brand_id => channel.brand.id ,
        :channel_id => channel.id , 
        :kind          => "fan",
        :name          => f["name"],
        :email          => f["email"],
        :bio           => bio ,
        :website       => f["url"],
        :friends_count => f["friend_count"],
        :location      => "#{f["hometown_location"]} ",
        #        :posts_count   => f["statuses_count"],
        :image_url     => f["pic_small"],
        :uid           => f["uid"],
        :facebook_uid           => f["uid"]
      )
      fans << ff;
    end
    
    #
    # Prospects
    #    
#    prospects = yaml[:prospects][:users]
#    
#    for f in prospects do 
#      fans << Fan.new( :brand_id => channel.brand.id ,
#        :channel_id => channel.id , 
#        :kind          => "prospect",
#        :name          => f["name"],
#        :home_page     => f["url"],
#        :bio           => f["reason"],
#        :website       => f["website"],
#        :friends_count => f["friend_count"],
#        :posts_count   => f["wall_count"],
#        :uid           => f["uid"],
#        :facebook_uid           => f["uid"],
#        :image_url     => f["pic_small"]
#      )
#    end
    
    #
    # Competitors
    #    
#    competitors = yaml[:recommended_competitors][:users]
#    
#    for f in competitors do 
#      fans << Fan.new( :brand_id => channel.brand.id ,
#        :channel_id => channel.id , 
#        :kind          => "competitor",
#        :name          => f["name"],
#        :home_page     => f["url"],
#        :bio           => f["description"],
#        :website       => f["website"],
#        :friends_count => f["fan_count"],
#        #        :posts_count   => f["statuses_count"],
#        :uid           => f["page_id"],
#        :facebook_uid  => f["page_id"],
#        :image_url     => "https://graph.facebook.com/#{f['page_id']}/picture"
#      )
#    end
    
    commenters = yaml[:commenters]
    for f in commenters do 
      bio = "Gender : #{f["sex"]}" 
      ff = Fan.new( :brand_id => channel.brand.id ,
        :channel_id => channel.id , 
        :kind          => "engaged",
        :name          => f["name"],
        :email          => f["email"],
        :bio           => bio ,
        :website       => f["url"],
        :friends_count => f["friend_count"],
        :location      => "#{f["hometown_location"]} ",
        :image_url     => f["pic_small"],
        :uid           => f["uid"],
        :facebook_uid           => f["uid"]
      )
      fans << ff;
    end
    
    
    #
    # GROUPS
    #    
    groups = yaml[:recommended_groups][:users]
    for f in groups do 
      fans << Fan.new( :brand_id => channel.brand.id ,
        :channel_id => channel.id , 
        :kind          => "group",
        :name          => f["name"],
        :home_page     => f["url"],
        :bio           => f["description"],
        :website       => f["website"],
        :friends_count => f["fan_count"],
        #        :posts_count   => f["statuses_count"],
        :uid           => f["page_id"],
        :image_url     => "https://graph.facebook.com/#{f['page_id']}/picture"
      )
    end
    
    logger.error "STATUS : BEFORE DELETETING ALL FANS"
    channel.fans.delete_all
    Fan.import fans
    Fan.where(:channel_id => nil).delete_all
    logger.error "STATUS : AFTER FAN IMPORT"
     
    
    
    #Groups Posts
    #
    posts = []
    group_posts = yaml[:groups]
    #    puts "-------------------"
    #    puts "GROUP POST -> #{group_posts.inspect}"
    #    puts "-------<< >>------------"
    for group in group_posts do 
      #      puts "GROUP -> #{group.inspect}"
      name = group[:group]["name"]
      
      list = group[:posts]
      for p in list do 
        #        puts "GROUP POST:  #{p.inspect}"
        #        the_tagret_link = "https://www.facebook.com/groups/#{p['id'].to_s.gsub('_','/permalink/')}"
        the_tagret_link = p["actions"][0]["link"]
        
        posts << Post.new(
          :channel_id => channel.id, 
          :brand_id => channel.brand_id,
          :kind =>  "group",
          :title => "#{p["caption"]} - #{name} ",
          :body => p["message"],
          :from_name => p["from"]["name"],
          :uid  => p["id"],
          :image_url => "https://graph.facebook.com/#{p["from"]["id"]}/picture",
          :posted_at => p["created_time"],
          :target_link =>  the_tagret_link
        )
      end
    end
    
    
    
    #
    # fan_page_posts
    # 
    likes_count =0
    for pg in yaml[:fan_pages] do 
      p_name = pg[:page][:name]
      likes_count += pg[:page]["likes"].to_i
      for p in pg[:posts] do 
        title = p["type"]
        body = " "
        body << p["story"] if p["story"]
        body << p["message"] if p["message"]
        
        post = Post.new(
          :channel_id => channel.id, 
          :brand_id => channel.brand_id,
          :kind =>  "my-post",
          :title => title,
          :body => body,
          :from_uid =>  p["from"]["id"],
          :from_name => p["from"]["name"],
          :uid  => p["id"],
          :image_url => p["icon_url"],
          :posted_at => p["created_time"],
          :target_link => p["href"]
        )
        
        if p["comments"]["data"]
          for c in p["comments"]["data"] do 
            comment = Comment.new(
              :brand_id   => channel.brand_id ,
              :channel_id => channel.id, 
              :message => c["message"],
              :kind    => "facebook_comment",
              :name    => c["from"]["name"],
              :read    => false,
              :posted_at    => c["created_time"],
              :uid  => c["id"],
              :from_uid => c["from"]["id"],
              :post_uid => p["id"],
              :target_link => p["actions"][0]["link"]
            )
            comments << comment
          end
        end
        
        posts << post
      end
    end
    
    
    
    
    messages = yaml[:messages]
    
    for p in messages do 
      posts << Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "inbox",
        :title => p["name"],
        :body => p[:message],
        :from_uid =>  p[:from]["data"][0]["id"],
        :from_name => p[:from]["data"][0]["name"],
        #        :from_name => p[:from][:data][:name],
        :uid  => p[:uid],
        :image_url => "https://graph.facebook.com/#{p[:from]["data"][0]["id"]}/picture",
        :posted_at => p[:posted_at],
        :target_link => ""
      )
    end
    
    logger.error "STATUS -- RECOMMENDED POSTS"
    for p in yaml[:recommended_content][:posts] do 
      posts << Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "recommended",
        :uid  => p["id"],
        :title => p["message"],
        :body => p["description"],
        :from_name => p["from"]["name"],
        :from_uid => p["from"]["id"],
        :image_url => p["picture"],
        :posted_at => Time.now,
        :target_link => p["link"]
      )
    end
    
    
    notifications = yaml[:notifications]
    for p in notifications do 
      posts << Post.new(
        :channel_id => channel.id, 
        :brand_id => channel.brand_id,
        :kind =>  "notification",
        :title => p["title_text"],
        :body => p["body_text"],
        :from_name => p["sender_id"],
        :from_uid => p["sender_id"],
        :uid  => p["notification_id"],
        :image_url => p["icon_url"],
        #        :posted_at => p["created_time"],
        :posted_at => Time.now,
        :target_link => p["href"]
      )
    end
        

    
    
    begin
      logger.error "STATUS : BEFORE COMPETITOR UPDATE AND POSTS"
    
      #
      # FACEBOOK COMPETITORS
      #
      competitors = yaml[:monitored_competitors]
      for c in competitors do 
        comp =  Competitor.find(c[:id])
        comp.name = c[:name]
        comp.facebook_likes_count =c[:likes] 
        comp.facebook_comments_count = c[:comments_count] 
        comp.facebook_shares_count =c[:shares] 
        comp.facebook_description = c[:description] 
        comp.facebook_fanpage_image_url = c[:image_url] 
        comp.email = c[:email] 
        comp.phone = c[:phone] 
        comp.website = c[:website]  if comp.website.nil?
        comp.save
        puts "Updated Competitor #{comp.name}"
      
        for p in c[:posts] do 
          #          puts "COMPETITOR POST : #{p.inspect}"
          posts << Post.new(
            :channel_id => channel.id, 
            :competitor_id => comp.id, 
            :brand_id => channel.brand_id,
            :kind =>  "competitor-post",
            :body => p[:text],
            :from_name => p[:from_name],
            :from_uid => p[:from_name],
            :uid  => p[:id],
            :image_url => p[:picture],
            :posted_at => p[:posted_at],
            :target_link => p[:target_link]
          )
        end
      end
    
    rescue Exception => e
      logger.error "FAILED ON COMPETITOR LOADING #{e.message}"
      puts e.backtrace
    end
    
    
    
    logger.error "STATUS : BEFORE DELETETING ALL POSTS"
    channel.posts.delete_all
    t0 = Time.now
    Post.import posts
    logger.error "STATUS : AFTER Post import"
     
    
    logger.error "STATUS : BEFORE DELETETING ALL COMMENTS"
    channel.comments.delete_all
    t0 = Time.now
    Comment.import comments
    logger.error "STATUS : AFTER Comments import"
    
    
    
    
  end
  


  
  #
  # check for job location
  #
  def location_match(post,search_locations)
    return 0
    logger.error " JOBS LOCATION SEARCH :#{post[:title]} in #{search_locations}"
    search_locations.split(","){| w | 
      return 1 if post[:title].include? w
      return 1 if post[:body].include? w
    }
    return 0
  end


  
  
  #
  # Score Calculations
  # 
  def facebook_score(channel,yaml)
    
    comments_count = 0
    likes_count   = 0
    views_today = 0
    
    channel.trackers.each{|t| views_today += 1 if t.today? }
    
    
    for p in yaml[:fan_pages] do 
      likes_count += p[:page]["likes"].to_i
    end
    
    
    
    
    
    return {
      :users => yaml[:friends].count, :daily_views => views_today,
      :new_users => yaml[:new_friends].count , :total_likes => likes_count,
      :date => Time.now.strftime("%Y-%m-%d") , :user_comments => channel.comments.count 
    }
    
  end
end
