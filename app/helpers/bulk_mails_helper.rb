module BulkMailsHelper
  
  def social_links_for_email(id)
    brand = Brand.find(id)
    base_folder = "/templates/social-icons/default"
    html = ""
    if brand.facebook_page.present?
      html <<  "<a href='#{brand.facebook_page}'><img src='#{base_folder}/facebook.png'  width='32px' height='32px' target='_blank'> </a>"
      html << " "
    end
    if brand.twitter_user_id.present?
      html <<  "<a href='#{brand.twitter_user_id}'><img src='#{base_folder}/twitter.png'  width='32px' height='32px' target='_blank'> </a>"
      html << " "
    end
    if brand.linkedin_url.present?
      html <<  "<a href='#{brand.linkedin_url}'><img src='#{base_folder}/linkedin.png'  width='32px' height='32px' target='_blank'> </a>"
      html << " "
    end
    if brand.youtube_url.present?
      html <<  "<a href='#{brand.youtube_url}'><img src='#{base_folder}/youtube.png'  width='32px' height='32px' target='_blank'> </a>"
      html << " "
    end
    html 
    
  end
  
  
  def company_info_for_emails(brand)
    html = "<div class='company-info'>
                Our Address: #{brand.address}
              <br/> Phone: #{brand.phone}
            </div>"
    html
  end
  
  
  def logo_and_slogan(brand)
    html = "
            <div class='company-logo'> 
              <a href='BRAND-WEBSITE'>
                <img src='#{brand.image_url}' width='50px'/> 
                <h3>#{brand.name}<h3/>#{brand.description}
              </a>
            </div>"
  end
end
