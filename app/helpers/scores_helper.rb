module ScoresHelper

  def website_chart_1(scores)
    [
      { :year => '2008', :value => 20 },
      { :year => '2009', :value => 22 },
      { :year => '2010', :value => 25 },
      { :year => '2011', :value => 26 },
      { :year => '2012', :value => 56 }
    ]
  end



  def pie_chart_fans_by_channel_data(brand)
    channels = []
    for c in brand.channels do 
      fans = c.fans.where(:kind => "fan").count
      if fans > 0 then 
        channels << { :label => c.name, :value =>  fans }
      end
    end
    channels
  end

   def pie_chart_engaged_users_data(brand)
   channels = []
    for c in brand.channels do 
      fans = c.fans.where(:kind => "engaged").count
      if fans > 0 then 
        channels << { :label => c.name, :value =>  fans }
      end
    end
    channels
  end

   
   def bar_chart_engaged_users_data(scores)
     
     xkey = "period"
     ykeys = [ 'facebook',  "twitter" , "youtube", ]
     labels = [ 'Post Sent', "User Comments"]
     
     data = []
     for item in scores do 
       if item.channel.facebook? 
         data << {:period =>  item.score_date ,  :facebook => item.comments }
       end
       if item.channel.twitter? 
         data << {:period =>  item.score_date , :twitter => item.comments }
       end
       if item.channel.youtube? 
         data << {:period =>  item.score_date , :youtube => item.comments }
       end
     end
     
     events =  [ data.first[:period],data.last[:period] ]
    
     return data, events, xkey,ykeys,labels
   end
   
   def bar_chart_fans_data(scores)
     
     xkey = "period"
     ykeys = [ 'facebook',  "twitter" ,  "linkedin", "foursquare"]
     labels = [ ]
     
     data = []
     for item in scores do 
       if item.channel.facebook? 
         data << {:period =>  item.score_date ,  :facebook => item.fans }
       end
       if item.channel.twitter? 
         data << {:period =>  item.score_date , :twitter => item.fans }
       end
       if item.channel.linkedin? 
         data << {:period =>  item.score_date , :linkedin => item.fans }
       end
       if item.channel.foursquare? 
         data << {:period =>  item.score_date , :foursquare => item.fans }
       end
#       if item.channel.youtube? 
#         data << {:period =>  item.score_date , :youtube => item.fans }
#       end
     end
     
     events =  [ data.first[:period],data.last[:period] ]
    
     return data, events, xkey,ykeys,labels
   end
   
   
   
   
   def bar_chart_facebook_fans_data(scores)
     
     xkey = "period"
     ykeys = [ 'facebook']
     labels = [ ]
     
     data = []
     for item in scores do 
       if item.channel.facebook? 
         data << {:period =>  item.score_date ,  :facebook => item.fans }
       end
     end
     
     events =  [ data.first[:period],data.last[:period] ]
    
     return data, events, xkey,ykeys,labels
   end
   
   
   def bar_chart_facebook_engaged_data(scores)
     
     xkey = "period"
     ykeys = [ 'facebook']
     labels = [ ]
     
     data = []
     for item in scores do 
       if item.channel.facebook? 
         data << {:period =>  item.score_date ,  :facebook => item.commenters }
       end
     end
     
     events =  [ data.first[:period],data.last[:period] ]
    
     return data, events, xkey,ykeys,labels
   end
   
   
   def bar_chart_facebook_posts_and_comments_data(scores)
     
     xkey = "period"
     ykeys = [ 'posts', 'comments']
     labels = [ ]
     
     data = []
     for item in scores do 
       if item.channel.facebook? 
         data << {:period =>  item.score_date ,  :posts => item.posts , :comments => item.comments }
       end
     end
     
     events =  [ data.first[:period],data.last[:period] ]
    
     return data, events, xkey,ykeys,labels
   end
   
   
   
end
