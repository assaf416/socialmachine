module CompetitorsHelper
  
  require 'open-uri'
  require 'nokogiri'
  require 'oauth'
  require 'yaml'
  require 'json'
  require 'xmlsimple'  
  require 'foursquare2'
  
  
  def find_user_on_social_networds(name, fb_token, li_token,li_secret, fs_token)
      
    # find on Facebook
    oauth_access_token = fb_token
    graph = Koala::Facebook::API.new(oauth_access_token)
    found_fb_users = graph.search(name,{:type => "user"})
    found_fb_pages = graph.search(name,{:type => "page"})
        
    # find on Twitter
    
    
    
    # find on Youtube
    
    # find on Linkedin 
    # Fill the keys and secrets you retrieved after registering your app
    api_key = 'y1ifejqt5oss'
    api_secret = '0EI8fpBTNyOf1G0U'
 
    configuration = { :site => 'https://api.linkedin.com' }
    consumer = OAuth::Consumer.new(api_key, api_secret, configuration)
    access_token = OAuth::AccessToken.new(consumer, li_token, li_secret)
    
    response = access_token.get("http://api.linkedin.com/v1/people-search:(people:(first-name,last-name,id,headline,location:(name),industry,num-connections,summary,picture-url))?keywords=#{name} ")
    li_users =   XmlSimple.xml_in( response.body)

    ar_li_users = []
    for item in li_users["people"][0]["person"] do
  
      if item["picture-url"]
        ar_li_users <<  { 
          :name => "#{item["first-name"][0]} #{item["last-name"][0]}" ,
          :image_url => item["picture-url"][0] ,
          :location => item['location'][0],
          :industry => item['industry'][0],
          :headline => item['headline'][0],
          #          :summary => item['summary'][0],
          :uid => item['id'],
          :friends_count => item['num-connections'][0],
          :industry => item['industry'][0],
        }
      end
    end
    
    
    #
    # Find on Fousquare 
    #
    
    
    fs_client = Foursquare2::Client.new(:oauth_token => fs_token)
    friends = fs_client.search_users(:name => name)
    fs_users = []
    friends.each {|f|
      if  f[1].class == Array
        for i in f[1] do 
          fs_users << i
          #      puts " ITEM : #{i} "
        end
      end
    }  

    
    return { :facebook_users => found_fb_users , :facebook_pages => "found_fb_pages" , :linkedin_users => ar_li_users , :foursquare_users => fs_users}
  end
  
  
  
  def get_social_links_from_website(url)
    doc = Nokogiri::HTML( open(URI.encode(backlinks_url)))
    doc.css('a').each do |link|
      puts link.inspect
    end
  end
  
  
  require 'koala'
  require 'twitter'
  require 'youtube_it'
  require 'open-uri'
  require 'nokogiri'

    
  
  def scan_url(url)
    begin
      ar = []; images =[]
      keywords = ""
      meta_description = ""
      generator = ""
      
      begin
        doc = Nokogiri::HTML( open(URI.encode(url)))
        begin
          doc.css('a').each do |link|
          
            ar << link['href'] if link['href'].include? "www.facebook.com"
            ar << link['href'] if link['href'].include? "www.twitter.com"
            ar << link['href'] if link['href'].include? "www.youtube.com"
            ar << link['href'] if link['href'].include? "www.linkedin.com"
            ar << link['href'] if link['href'].include? "www.reddit.com"
            ar << link['href'] if link['href'].include? "www.digg.com"
            ar << link['href'] if link['href'].include? "www.google.com"
            
          end
        end
      rescue Exception => e3
        puts "FAILED on scan_url !! -- EXEPTION LINKS #{e3.message}"
        " "      
      end
      
      
      begin
        doc.css('img').each do |link|
          if link['src'].include? "http:"
            images << link['src']
          else
            images << url + link['src']
          end
        end
      rescue Exception => e3
        puts "!! -- EXEPTION LINKS"
        puts e3.message
        " "      
      end
      
      
      begin
        doc.css('meta[name="keywords"]').each do |meta_tag|
          keywords = meta_tag['content'] 
        end
      rescue Exception => e2
        puts "!! -- EXEPTION IN META_KEYWORDS"
        puts e2.message
        " "      
      end
  
      begin
        doc.css('meta[name="generator"]').each do |meta_tag|
          generator = meta_tag['content'] 
        end
      rescue Exception => e4
        puts "!! -- EXEPTION IN META_GENERATOR"
        puts e4.message
        " "      
      end

      
      begin
        doc.css('meta[name="description"]').each do |meta_tag|
          meta_description << meta_tag['content'] 
        end
      rescue Exception => e
        puts "!! -- EXEPTION IN META_DESCRIPTION"
        puts e.message
        " "
      end
      return { :links => ar, :meta => meta_description, :keywords => keywords, :generator => generator, :images => images}
    rescue Exception => e
      logger.error " FAILED GETTING SOCIAL LINKS FROM URL #{url}, Message :#{e.message}"
    end
  end
  

  def facebook_mark(name)
    return "<i class='icon-thumbs-up'></i>  Likes: #{rand(1000)} <br/> <i class='icon-globe'></i> Talk About: #{number_with_delimiter rand(100)} <br/> <i class='icon-user'></i>  Posts: #{rand(100)}"
    begin
      oauth_access_token = current_user.brand.facebook_token
      @graph = Koala::Facebook::API.new(oauth_access_token) 
      profile = @graph.get_object(name)
      feed = @graph.get_connections(name, "feed")
      return "<i class='icon-thumbs-up'></i>  Likes: #{number_with_delimiter profile["likes"]} <br/> <i class='icon-globe'></i> Talk About: #{number_with_delimiter profile["talking_about_count"].to_i} <br/> <i class='icon-user'></i>  Posts: #{number_with_delimiter feed.size}"
    rescue Exception => e
      puts  e.message + " --> #{name} on FACEBOOK"
    end
  end


  def youtube_mark(name)
    return "<i class='icon-thumbs-up'></i>  Likes: #{rand(1000)} <br/> <i class='icon-globe'></i> Talk About: #{number_with_delimiter rand(100)} <br/> <i class='icon-user'></i>  Posts: #{rand(100)}"
    begin
      client = YouTubeIt::Client.new  
      profile = client.profile(name) 
      i = 0
      return  "<i class='icon-user'></i> Subscribers: #{number_with_delimiter  profile.subscribers} <br/> <i class='icon-eye-open'></i>  Views: #{number_with_delimiter  profile.view_count} "
    rescue Exception => e
      puts  e.message + " --> #{name} on YOUTUBE"
    end
  end


  def twitter_mark(name)
    return "<i class='icon-thumbs-up'></i>  Likes: #{rand(1000)} <br/> <i class='icon-globe'></i> Talk About: #{number_with_delimiter rand(100)} <br/> <i class='icon-user'></i>  Posts: #{rand(100)}"
    begin
      profile = Twitter.user(name)
      return "<i class='icon-comment'></i> Posts: #{number_with_delimiter  profile['statuses_count']} <br/> <i class='icon-user'></i> Friends #{number_with_delimiter  profile['friends_count']} <br/> <i class='icon-user'></i>  Followers: #{number_with_delimiter  profile['followers_count']} "
    rescue Exception => e
      puts  e.message + " --> #{name} on TWITTER"
    end
  end

  
  
  
  
  
end
