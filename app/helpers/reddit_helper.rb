# To change this template, choose Tools | Templates
# and open the template in the editor.

module RedditHelper
    
  require "snooby"
  
  def setup(user,pwd)
    reddit = Snooby::Client.new.authorize!(user,pwd)
  end
  
  def user_info( reddit, user_name)
    puts "SHOW USER INFO for #{user_name}"
    u =reddit.u(user_name)
    puts u.inspect
  end

  def reddit_search(reddit,what)
    res = []
    reddit.r(what).posts.each do |p|
      res << {:title => p.title, :body => p.selftext, :link => p.permalink} unless p.selftext.empty?
      puts p.title
      puts p.selftext
      puts p.permalink
      puts " ---------------------------- "
      #    if com.body =~ /(vile|rotten|words)/
      #      com.reply("#{$&.capitalize} is a terrible word, #{com.author}!")
      #    end
    end
    res
  end


  def my_profile(reddit)
    puts "MY INFO"
    puts reddit.me.inspect
  end


  def user_posts(reddit,user_name)
    u =reddit.u(user_name)
  
    for item in u.posts do 
      puts " POST TITLE #{item.title}"
      puts " created #{item.created}"
      puts " clicked #{item.clicked}"
      puts " tumbnail #{item.thumbnail}"
      puts " subreddit #{item.subreddit}"
      puts " Ups #{item.ups}"
      puts " Downs #{item.downs}"
      puts item.inspect
    end
  end

  def user_comments(reddit,user_name)
    puts " USER COMMMENTS"
    u =reddit.u(user_name)
    for item in u.comments do 
      #    puts " POST TITLE #{item.title}"
      puts " Link Title: #{item.link_title}"
      puts " User NAme: #{item.name}"
      puts " body: #{item.body}"
      puts " author: #{item.author}"
      puts " subreddit: #{item.subreddit}"
      puts " Ups: #{item.ups}"
      puts " Downs:#{item.downs}"
      puts item.inspect
    end
  end

  def post_link(reddit , category,link, description)
    reddit.submit(category,link,description)
  end

  
  
end
