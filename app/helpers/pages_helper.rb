module PagesHelper
  
  include  ApplicationHelper
  include  ActionView::Helpers
  
  
  def hero(item)
    # row[1].each{ |i | return " hero-unit " if  PagePartial.find(i.id).hero?  }
    # return " hero-unit " if item.hero?
    #    return " well " if item.hero?
    return ""
  end
  
  
  def convert_to_html(rows, page)
    
    brand = Brand.find(Website.find(page.website_id).brand_id)
    
    html = "<section class='container'>"
    row_no = 0
    for item in rows do 
      
      if page and page.float_layout == true
        therow_class ="row"
      else
        therow_class ="row "
      end
      
      row_no = row_no + 1
      item_count =  item[1].size 
      
      if item_count == 1 then 
        html << "<section id='sec_#{row_no}'> <div class=' #{therow_class}  row_#{row_no}  '>"
        html << artifact_tag(brand,item[1][0]  , item[1][0][:grid] || 12)
        html << "</div></section>"
      end
      
      if item_count == 2 then 
        
        html << "<section id='sec_#{row_no}'> <div class=' #{therow_class}  row_#{row_no} '>"
        html << artifact_tag(brand,item[1][0]  , item[1][0][:grid] || -6)
        html << artifact_tag(brand,item[1][1]  , item[1][1][:grid] || 6)
        html << "</div></section>"
      end
      
      if item_count == 3 then 
        
        html << "<section id='sec_#{row_no}'> <div class=' #{therow_class} row_#{row_no}  '>"
        html << artifact_tag(brand,item[1][0]  , item[1][0][:grid] || -4)
        html << artifact_tag(brand,item[1][1]  , item[1][1][:grid] || 4)
        html << artifact_tag(brand,item[1][2]  , item[1][2][:grid] || 4)
        html << "</div></section>"
      end
      
      if item_count == 4 then 
        html << "<section id='sec_#{row_no}'> <div class=' #{therow_class}  row_#{row_no}  '>"
        html << artifact_tag(brand,item[1][0]  , item[1][0][:grid] || -3)
        html << artifact_tag(brand,item[1][1]  , item[1][1][:grid] || 3)
        html << artifact_tag(brand,item[1][2]  , item[1][2][:grid] || 3)
        html << artifact_tag(brand,item[1][3]  , item[1][3][:grid] || 3)
        html << "</div></section>"
      end
      
    end
    html << "</section>"
    html
  end
  
  
  def contact_tag(brand)
    html =  "<div class='contact-us'>"
    html <<    "<h5>Contact Us</h5>"
    html <<    "<ul style='list-style:none;'>"
    html <<    "  <li><i class='icon-envelope'></i> #{brand.user.email} </li>"
    html <<    "  <li><i class='icon-globe'></i>  #{brand.address} </li>"
    html <<    "  <li><i class='icon-phone'></i> #{brand.phone} </li>"
    html <<    "</ul>"
    html << "</div>"
  end  
 
  def pages_toolbar_tag(item,site,current_page_id)
    pages = site.pages.where(:kind => nil).order("order_in_toolbar ASC")
   
    html = "<div class='subnav' style='float:#{item["align"]};'><ul class='nav nav-pills'>"
    for page in pages do 
      if page.id.to_i == current_page_id.to_i then
        the_class="active"
      else
        the_class=""
      end
      html << "<li class='#{the_class}'>"
      html <<  "<a href='/public/brand/#{site.brand_id}?page=#{page.id}' >"
      html << page.name
      html << "</a></li>"
    end
    html << "</ul></div>"
    
  end
  
  def logo_tag(brand)
    html = ""
    html << "<div class='logo'>#{brand.name}<br/><span class='slogen'>#{brand.description}</span></div>"
  end  
  
  
  
  def artifact_tag(brand,item,grid_size)
    u_id = rand(1000000000)
    
    if grid_size < 0 
      div_grid = "span" + (grid_size * -1).to_s + " _first"
    else
      div_grid = "span" + grid_size.to_s 
    end
    
    html = "<div class= '#{div_grid}  kind-#{item[:kind]} #{item[:align]} preview  #{hero(item)}  '  data-pid=#{item['id']}>"

    
    # PAGE LINKS
    if item[:kind].include? "page_links" 
      html << raw(pages_toolbar_tag(item,brand.site,item[:page_id]))
    end
    
    # LOGO
    if item[:kind].include? "logo" 
      html << logo_tag(brand)
    end
    
    # CONTACT US
    if item[:kind].include? "contact" 
      html << contact_tag(brand)
    end
    
    
    # FIXED TOOLBAR
    if item[:kind].include? "fixed_toolbar" 
      html << fixed_toolbar(brand)
    end
    
    # NEWLETTER ARTICLES
    if item[:kind].include? "newsletter" 
      html << newsletter_tag(item,brand)
    end
    
    
    
    # TEXT
    if item[:kind].include? "text" 
      html <<  "<h5> #{raw (item[:title])}</h5>"  unless item[:title].empty?
      html <<  "<p> #{raw (item[:body])}</p>"
    end
    
    # FORM
    begin
      brand_id = Website.find(PagePartial.find(item[:id]).page.website_id).brand_id
      html <<  _form_tag(brand_id,item) if item[:kind].include? "form" 
    rescue
      html <<  _form_tag(1,item) if item[:kind].include? "form" 
    end
      
    
    # FILLER
    html <<   "<div><br/><br/></div>"  if item[:kind].include? "filler"
    
    # IMAGE
    if item[:style].present?
      the_image_class= "border-#{item[:style].downcase}"
    else
      the_image_class= ""
    end
    the_image = PagePartial.find(item[:id]).the_image
    #    html <<   "<image class='image' src='#{the_image}'  width='#{item[:img_width]}' height='#{item[:img_height]}' />" if item[:kind].include? "image"
    html <<   "<image class='image #{the_image_class}' src='#{the_image}'    width='#{item[:img_width]}'  height='#{item[:img_height]}' />" if item[:kind].include? "image"
    


    # Movie
    #    html <<   "<image class='image' src='#{the_image}'  width='#{item[:img_width]}' height='#{item[:img_height]}' />" if item[:kind].include? "image"
    
    if item[:kind].include? "movie"
      if item[:title].include? " "
        the_movie =  "hvQfk4cV5GA"
      else
        the_movie = PagePartial.find(item[:id]).the_movie
      end
      
      if item[:img_height].to_i > 0
        the_height = item[:img_height]
      else
        the_height = "500px"
      end
      the_width = "100%"
      
      html << "<object class='movie-frame' width='#{the_width}' height='#{the_height}' type='application/x-shockwave-flash' data='http://www.youtube.com/v/#{the_movie}=fr&fs=1&rel=0&color1=0x006699&color2=0x54abd6&border=0'>"
      html << "  <param name='movie' value='http://www.youtube.com/v/#{the_movie}=fr&fs=1&rel=0&color1=0x006699&color2=0x54abd6&border=0' />"
      html << "</object>"
      html << "<a href="" class=preview>Edit</a>"
    end      
    
    
    
    # SOCIAL LINKS
    if item[:kind].include? "social-links"
      html << "<div class='social-links #{item["align"]}'> #{raw social_links_tag(brand,item)} </div>"
    end
    
    # Map 
    if item[:kind].include? "map"
      html << "<div class='kind-map'> #{raw _map_tag(item[:title])} </div>"
    end
    
    # Sahre Links 
    if item[:kind].include? "share"
      html << "<div class='kind-share'> #{raw _share_tag('http://www.socialmachine.biz/public/brand/#{brand_form_item(item).id}')} </div>"
    end
    
    
    # HERO
    the_image = PagePartial.find(item[:id]).image
    html << "<div class='feature hero-unit' style='background-color:#{item[:style];}'><image id='hero-image-id-#{u_id}'  class='hero-image' src='#{the_image}'  width='#{item[:img_width]}' height='#{item[:img_height]}' /><div id='feature-body-#{u_id}' class='feature-body'  >#{item[:body]}</div></div>"  if item[:kind].include? "hero"
    
    
    # FEATURE
    the_image = PagePartial.find(item[:id]).image
    html << "<div class='feature'><image id='feature-image-id-#{u_id}'  class='feature-image' src='#{the_image}'  width='#{item[:img_width]}' height='#{item[:img_height]}' /><div class='feature-title' id='feature-title-#{u_id}'> <h5>#{item[:title]}</h5></div><div id='feature-body-#{u_id}' class='feature-body'  ><p>#{item[:body]}</p></div></div>"  if item[:kind].include? "feature"

    html << _slider_tag(item) if item[:kind].include? "slider"
    
    
    # PRDOCUT
    if item[:kind].include? "product"

      item[:product_id] = 1 if item[:product_id].nil?
      
      product = Product.find(item[:product_id])
      the_image = product.image_url 
      the_price = number_to_currency(product.price)
      html << "<div class='product'>"
      #      html << "  <div class='product-title' id='product-title-#{u_id}'><h5> <a  class='product-link' data-id='#{product.id}'  data-toggle='modal' href='#productDialog' > #{Product.find(item[:product_id]).name}</a></h5></div>"
      html << "  <div class='product-title' id='product-title-#{u_id}'><h5> <a href='/public/product?id=#{item[:product_id]}' > #{Product.find(item[:product_id]).name}</a></h5></div>"
      html << "   <a href='/public/product?id=#{item[:product_id]}' >   <image id='product-image-id-#{u_id}'  class='product-image' src='#{the_image}'  width='100%' xheight='#{item[:img_height]}' /> </a>"
      html << "   <div id='product-body-#{u_id}' class='product-body'>#{product.description}</div>"
      html << "   <div class='product-price'>#{the_price}</div>"
      html << "</div>"  
      html 
    end

    
    
    # ARTICLE
    if item[:kind].include? "article"
      
      item[:article_id] = 1 if item[:article_id].nil?
      
      begin
        article = Article.find(item[:article_id]) 
      rescue
        article =  Article.first
      end
      the_image = article.image_url 
      html << "<div class='article'>"

      if article.clip?
        html << '  <iframe style="z-index:-100 " width="100%" xheight="500px" src="http://www.youtube.com/embed/' + article.short_video_url   + '" frameborder="0" allowfullscreen></iframe> '
      else
        html << "  <div class='article-title' id='article-title-#{u_id}'> <a  href='/public/article?id= #{article.id}'> <h5>#{article.name}</h5></a></div>"
        html << "     <a  href='/public/article?id= #{article.id}'><image id='article-image-id-#{u_id}'  class='article-image' src='#{the_image}'  width='100%' /></a>"
        html << "   <div id='article-body-#{u_id}' class='article-body'>#{strip_html(article.body)[0..200]}</div><br/>"
      end
      html << "</div>"  
      html 
    end


    
    # Navigation
    if item[:kind].include? "navigation"
      site = Website.find(Page.find(item[:page_id]).website_id)
      html << "<ul class='navigation'>"
      for page in site.pages.order("order_in_toolbar ASC") do 
        if item[:page_id] == page.id
          the_class = "active" 
        else
          the_class = "not-active" 
        end
          
        html << "<li class='#{the_class}'>#{link_to page.name, "/pages/preview?id=" + page.id.to_s }</li>"
      end
      html << "</ul>"
    end
    
    
    #    html << inline_edit(item[:id], item[:page_id])
    html << "</div>"
    
  end
  
  
  def newsletter_tag(item,brand)
    html = ""
    if item.title.present?
      html << "<h5>#{item.title}</h5>"
    end
    html  << "<ul class='newsletter'>"
    begin
      newsletter = BulkMail.find(item.article_id)
      articles = newsletter.articles
    rescue
      articles = []
      html << " Click to select a newsletter"
    end
    for article in articles do 
      html << "<li> <a href=''> <img src='#{article.icon}' class='newsletter-img' /></a>  #{article.name}<br/>#{link_to article.short_body, "/public/article?id=#{article.id}"}</li>"
    end
    html << "</li>"
    if item.body.present?
      html << "<p>#{item.body}</p>"
    end
    #    html = " NEWSLETTER ARTICLES GOES HERE  picture heigh : #{item.img_height} | picture align: #{item.align} | Newsletter : #{item.article_id}"
  end
  
  
  
  def fixed_toolbar(brand)
    html = "<div class='navbar navbar-fixed-top'>
      <div class='navbar-inner'>
        <div class='container'>
            <a class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'> </a>
            <a class='brand' href=''>#{brand.name}</a>
        <div class='nav-collapse'>
          <ul class='nav'>"
    for page in brand.site.pages.where(:kind => nil).order("order_in_toolbar ASC") do 
      html << "<li> <a href='/public/brand/#{brand.id}?page=#{page.id}'>#{page.name}</a> </li>"
    end

    html << "
        </ul>
      </div>
    </div>
  </div>
</div>"

  end
  
  def _slider_tag(item)
    ddd = PagePartial.find(item)
    html = "<div id='myCarousel' class='carousel slide'> <div class='carousel-inner'>"
   
    html << "  <div class='item active'> <img src='#{item.slide_1}' alt='slide1' width='100%'  height='#{item[:img_height]}'>"
    #    html << " <div class='carousel-caption'>  <h5>#{ddd.slide_1_header}</h5>  </div>" unless ddd.slide_1_header.empty?
    html << "</div>" 

    html << "<div class='item '> <img src='#{item.slide_2}' alt='slide2'  width='100%'  height='#{item[:img_height]}'>"
    #    html << " <div class='carousel-caption'>  <h5>#{ddd.slide_2_header}</h5>  </div>" unless ddd.slide_2_header.empty?
    html << "</div>" 

    html << "<div class='item '> <img src='#{item.slide_3}' alt='slide3'  width='100%' height='#{item[:img_height]}'>"
    #    html << " <div class='carousel-caption'>  <h5>#{ddd.slide_3_header}</h5>  </div>" unless ddd.slide_3_header.empty?
    html << "</div>" 

    html << " #{item[:id]}</div>
  <a class='left carousel-control' href='#myCarousel' data-slide='prev'></a>
  <a class='right carousel-control' href='#myCarousel' data-slide='next'></a>
</div>"
  end
  
  
  def _map_tag(address)
    "<img src='http://maps.googleapis.com/maps/api/staticmap?center=#{address}&zoom=14&size=600x300&sensor=false' width='100%' />"
  end
  
  
  def _share_tag(url)
    twitter_share = "
    <a href='https://twitter.com/share' class='twitter-share-button' data-url='#{url}' data-via='AssafGoldstein' data-size='small'>Tweet</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='//platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','twitter-wjs');</script>     
    "
    
    linkedin_share = "
    <script src='//platform.linkedin.com/in.js' type='text/javascript'></script>
    <script type='IN/Share' data-url='#{url}' data-counter='right'></script>    
    "
    
    facebook_share = "
      <div id='fb-root'></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = '//connect.facebook.net/en_US/all.js#xfbml=1&appId=320750867990208';
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
      <div class='fb-like' data-href='#{url}' data-send='true' data-layout='button_count' data-width='450' data-show-faces='false' data-font='arial' data-action='recommend'></div>
    "
    google_share = "
    
    
<div class='g-plus' data-action='share' data-href='#{url}'></div>

<script type='text/javascript'>
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
    
    "

    return "<table width=100% ><tr><td>"  +   raw(twitter_share) + "</td><td>"+ raw(linkedin_share)+"</td><td>" + raw(facebook_share) + "</td><td>"+raw(google_share) +  "</td></tr></table>"
    
  end
  
  def _form_tag(brand, item )
    html = "<form accept-charset='UTF-8' action='/inquieries' class='  form-verticle' id='new_inquiery' method='post'>"
    html <<      "<input class='hidden_field' id='brand_id' name='inquiery[brand_id]' type='hidden' value='#{brand}'>"
    html <<      "<input class='text_field' id='request_url' name='inquiery[request_url]' type='hidden' value='/public/brand/#{brand}?page=#{item.page.id}'>"
    html << "<fieldset>"
    html << "<xlegend><h5>#{item.title}</h5></xlegend>"

    html << "<p>#{item.body}</p>"

    html << "<div class='control-group'>"
    html <<    "<div class='controls'>"
    html <<      "<input class='text_field' id='inquiery_name' name='inquiery[name]' size='30' type='text' value='Full Name'>"
    html << "    </div>"
    html << " </div>"

    html << "<div class='control-group'>"
    html <<    "<div class='controls'>"
    html <<      "<input class='text_field' id='inquiery_email' name='inquiery[email]' size='30' type='text' value='Email'>"
    html << "    </div>"
    html << " </div>"
    
    if item["style"].present? and item["style"].include? "Message"
      html << "<div class='control-group'>"
      html <<    "<div class='controls'>"
      html <<      "<div class='form-message-div'><textarea id='inquiery_message' name='inquiery[message]'>Message...</textarea></div>"
      html << "    </div>"
    end
    
    if item["style"].present? and item["style"].include? "Event"
      html << "<div class='control-group'>"
      html <<    "<div class='controls'>"
      html <<      "<input class='text_field' id='inquiery_from_date' name='inquiery[from_date]' size='30' type='text' value='From Date'>"
      html << "    </div>"
    
      html << "<div class='control-group'>"
      html <<    "<div class='controls'>"
      html <<      "<input class='text_field' id='inquiery_to_date' name='inquiery[to_date]' size='30' type='text' value='To Date'>"
      html << "    </div>"
      html << " </div>"
    end
    
    html << "      <br/><input class='btn btn-primary' name='commit' type='submit' value='Send'>"
    html << " </fieldset></form>"
  end

  
  def social_links_tag(brand,item)
    
    base_folder = "/templates/social-icons/#{item["style"]}"
    
    #    raise item.inspect
    html = "<div class='xsocial-links'>"
    if brand.facebook_page.present?
      html <<  "<a href='#{brand.facebook_page}'><img src='#{base_folder}/facebook.png'  width='32px' height='32px' target='_blank'> </a>"
    end
    if brand.twitter_user_id.present?
      html <<  "<a href='#{brand.twitter_user_id}'><img src='#{base_folder}/twitter.png'  width='32px' height='32px' target='_blank'> </a>"
    end
    if brand.linkedin_url.present?
      html <<  "<a href='#{brand.linkedin_url}'><img src='#{base_folder}/linkedin.png'  width='32px' height='32px' target='_blank'> </a>"
    end
    if brand.youtube_url.present?
      html <<  "<a href='#{brand.youtube_url}'><img src='#{base_folder}/youtube.png'  width='32px' height='32px' target='_blank'> </a>"
    end
    html << "</div>"
    html 
  end
  
  
  
end