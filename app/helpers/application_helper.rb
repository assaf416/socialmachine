module ApplicationHelper
  def safe_image_tag(source, options = {})
    source 
    image_tag(source, options)
  end
end  




module ApplicationHelper

  
  
  
  
  
  def is_a_valid_email?(email)
    email_regex = %r{
      ^ # Start of string
      [0-9a-z] # First character
      [0-9a-z.+]+ # Middle characters
      [0-9a-z] # Last character
      @ # Separating @ character
      [0-9a-z] # Domain name begin
      [0-9a-z.-]+ # Domain name middle
      [0-9a-z] # Domain name end
      $ # End of string
    }xi # Case insensitive

    (email =~ email_regex)
  end
  
  
  
  require 'nokogiri'
  def extract_image(str)
    doc = Nokogiri::HTML(str)
    img_srcs = doc.css('img').map{ |i| i['src'] }
    return img_srcs[0] unless img_srcs.empty?
    return nil
  end
  
  
  
  require "base64"
  
  def from_qp(str, remove_underscore = true)
    return '' if str.nil? 
    result = str.gsub(/=\r\n/, "")
    result = result.gsub(/_/, " ") if remove_underscore
    result.gsub!(/\r\n/m, $/)
    result.gsub!(/=([\da-fA-F]{2})/) { $1.hex.chr }
    result
  end
  
  
  def hebrew_decode(str1)
    unless str1.nil?
      str = str1.force_encoding("utf-8")
      if str.to_s.include? "=?UTF-8?B?"
        s =  str1.gsub("=?UTF-8?B?","").gsub("?=","") 
        xx = Base64.decode64(s)
        return xx.force_encoding("utf-8")
      else
        if str.include? "=?UTF-8?Q?"
          s =  str1.gsub("=?UTF-8?Q?","").gsub("?=","") 
          str = from_qp(s)
          return str.force_encoding("utf-8")
        else
          return str.force_encoding("utf-8")
        end
      end
    else
      return " "
    end
  end

  # encoding: UTF-8
  def is_hebrew_text?(text)
    return true
    #    heb =0
    #    abc = 'אבגדהוזחטיכלמנסעפצקרשת'
    #    text.chars.each{|c| heb = heb + 1 if abc.include? c }
    #    ret heb > 10
  end
 
  
  def  channel_icon_for_url(url)
    return  "/assets/icons/google_search_64.png" if url.nil?
    return  "/assets/icons/facebook_64.png" if url.include? "facebook"
    return  "/assets/icons/twitter_64.png" if url.include? "twitter"
    return  "/assets/icons/google_64.png" if url.include? "plus.google"
    return  "/assets/icons/linkedin_64.png" if url.include? "linkedin"
    return  "/assets/icons/youtube_64.png" if url.include? "youtube"
    return  "/assets/icons/reddit_64.png" if url.include? "reddit"
    return  "/assets/icons/google_search_64.png"
  end
  
  def uri?(string)
    uri = URI.parse(string)
    %w( http https ).include?(uri.scheme)
  rescue URI::BadURIError
    false
  rescue URI::InvalidURIError
    false
  end
  
  def link_to_social_page(channel)
    html = "<span style='text-align:center;margin:0.1em;padding:0.1em;width:50px;height:70px;float:left' class='alert alert-success'>"
    html << link_to(image_tag(channel.icon, :size => "48x48") , channel.home_url, :target => "_blank" , :class => 'social_button')
    html << "<br/><i class='icon-user'></i> <span style='font-size:11px;'> #{link_to( channel.total_fans_count,'/channels/'+channel.id.to_s+'/edit')}</span> </span>"
    html << "</span>"
  end
  
  def link_to_login_page(channel)
    html = "<span style='text-align:center;margin:0.1em;padding:0.1em;width:50px;height:70px;float:left' class='alert alert-danger'>"
    html << link_to(image_tag(channel.icon, :size => "48x48") , channel.home_url, :target => "_blank" , :class => 'social_button')
    html << "<br/> #{ raw login_link(channel)} </span>"
    html << "</span>"
    
  end
  
  def login_link(channel)
    return link_to("login","/auth/facebook" , :class => "btn btn-danger") if channel.facebook?
    return link_to("login","/auth/twitter" , :class => "btn btn-danger") if channel.twitter?
    return link_to("login","/auth/linkedin" , :class => "btn btn-danger") if channel.linkedin?
    return link_to("login","/auth/foursquare" , :class => "btn btn-danger") if channel.foursquare?
    return link_to("login","/channels/#{channel.id}/edit" , :class => "btn btn-danger")
  end
  

  def you_are_not_logged_in_dialog( network_name , create_account_url, login_url )
    
    html = ""
    html << "<div class='well' style='margin:3em;' >"
    
    html << "   <h3> You are Not Connected to #{network_name} </h3>"
    html << "   #{lorem(30)} <br/>"
    
    html << "   <div  >"
    html <<     link_to( "Log in to #{network_name}" , login_url , :class => "btn")
    html << "         &nbsp;"
    html <<     link_to( "Create new  #{network_name} account" , create_account_url , :class => "btn")
    html << "   </div>"
    html << "</div>"
    
  end
  
  
  def color_patch(color)
    ss = "<span style='background-color:#{color.color_1}'>&nbsp&nbsp&nbsp</span>"
    ss << "<span style='background-color:#{color.color_2}'>&nbsp&nbsp&nbsp</span>"
    ss << "<span style='background-color:#{color.color_3}'>&nbsp&nbsp&nbsp</span>"
    ss << "<span style='background-color:#{color.color_4}'>&nbsp&nbsp&nbsp</span>"
    ss << "<span style='background-color:#{color.color_5}'>&nbsp&nbsp&nbsp</span>"
    ss << "<span style='background-color:#{color.color_6}'>&nbsp&nbsp&nbsp</span>"
    ss << "<span style='background-color:#{color.color_7}'>&nbsp&nbsp&nbsp</span>"
    ss << "<span style='background-color:#{color.color_8}'>&nbsp&nbsp&nbsp</span>"
    
    return link_to raw(ss), "/websites/apply_color_patch?id=#{current_user.brand.site.id}&amp;color=#{color.id}" , :class => "btn"
  end
  
  def font_selector( field_name, font_name)


    fonts = [
      'Arial',
      'Arial Black',
      'Comic Sans',
      'Courier New',
      'Georgia',
      'Impact',
      'Lucida Console',
      'Lucida Sans Unicode',
      'Tahoma',
      'Verdana'     
    ]

    html =  "<select id='website_#{field_name}' name='website[#{field_name}]'> "
    for item in fonts do 
      html << "<option value='#{item}'>#{item}</option>"
    end
    html << "</select>"
  end

  def color_picker(id,color)
    "<div class='input-append color span3' data-color='#{color}' data-color-format='hex' id='#{id}'  class='colorpicker'>
        <input type='text' class='span2' name='website[#{id}]' value='#{color}' rceadonly=''>
        <span class='add-on'><i style='background-color: #{color}; '></i></span>
      </div>"
  end
  
  
  def extract_url_from_html(str)
    return "" if str.nil?
    ar = URI.extract(str)
    for item in ar do 
      return item if item.include? "http"
    end
  end
  
  def social_network_gadget(kind)
    
    channel = @brand.get_channel(kind)
    
    html = ""
    html << "<div class=''>"
    html << " <span ><img src='#{channel.icon}' width='48px'/></span><br/>"
    html << " <p >"
    html << "    <a><i class='icon-user'></i> #{channel.fans.count}</a>"
    html << "    <br/>"
    html << "    <a> <i class='icon-edit'></i> #{channel.posts.count}</a>"
    html << "    <br/>"
    html << "    <a> <i class='icon-comment'></i> #{channel.comments.count}</a>"
    html << " </p>"
    html << "</div>"
    html 
  end
  
  
  def fake_location
    min = [31.89982, 34.8253 ]
    max = [32.22236, 35.0436]

    rx = (max[0] - min[0])/10
    vx = (min[0] + rand(10) * rx).round(4)
    ry = ((max[1] - min[1])/10)
    vy = (min[1] + rand(10) * ry).round(4)
    return vx.round(4).to_s,vy.round(4).to_s
  end


  def strip_html(str)
    return "" if str.nil?
    str.gsub(/<\/?[^>]*>/, "").gsub("<br>","")
  end
  
  def   extract_email(str)
    email = /\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b/.match(str) 
    email.to_s
  end



  def extract_phone(str)
    phone =   /((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}/.match(str)  
    if phone
      phone.to_s 
    else
      ""
    end
  end
  
  
  def current_url(overwrite={})
    the_url = url_for :only_path => true, :params => params.merge(overwrite)
    #    the_url.gsub!("/office/office","/office")
  end
  
  #  def the_current_user
  #    User.find(2)
  #  end
  #  
  def fake_amount(amount)
    number_to_currency(rand(amount), :unit => "$")
  end
  
  def fake_value(ar)
    ar[rand(ar.size)]
  end
  
  def fake_icon(*arx)
    
    ar = []
    for item in arx do 
      ar << item
    end
    image_tag("/assets/" + ar[rand(ar.size)] + ".png", :size => "16x16")
  end

  
  def lorem(word_count = 20)

    #===== words
    words =<<EOS
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer in
mi a mauris ornare sagittis. Suspendisse potenti. Suspendisse dapibus
dignissim dolor. Nam sapien tellus, tempus et, tempus ac, tincidunt
in, arcu. Duis dictum. Proin magna nulla, pellentesque non, commodo
et, iaculis sit amet, mi. Mauris condimentum massa ut metus. Donec
viverra, sapien mattis rutrum tristique, lacus eros semper tellus, et
molestie nisi sapien eu massa. Vestibulum ante ipsum primis in
faucibus orci luctus et ultrices posuere cubilia Curae; Fusce erat
tortor, mollis ut, accumsan ut, lacinia gravida, libero. Curabitur
massa felis, accumsan feugiat, convallis sit amet, porta vel, neque.
Duis et ligula non elit ultricies rutrum. Suspendisse tempor.
Quisque posuere malesuada velit. Sed pellentesque mi a purus. Integer
imperdiet, orci a eleifend mollis, velit nulla iaculis arcu, eu rutrum
magna quam sed elit. Nullam egestas. Integer interdum purus nec
mauris. Vestibulum ac mi in nunc suscipit dapibus. Duis consectetuer,
ipsum et pharetra sollicitudin, metus turpis facilisis magna, vitae
dictum ligula nulla nec mi. Nunc ante urna, gravida sit amet, congue
et, accumsan vitae, magna. Praesent luctus. Nullam in velit. Praesent
est. Curabitur turpis.
Class aptent taciti sociosqu ad litora torquent per conubia nostra,
per inceptos hymenaeos. Cras consectetuer, nibh in lacinia ornare,
turpis sem tempor massa, sagittis feugiat mauris nibh non tellus.
Phasellus mi. Fusce enim. Mauris ultrices, turpis eu adipiscing
viverra, justo libero ullamcorper massa, id ultrices velit est quis
tortor. Quisque condimentum, lacus volutpat nonummy accumsan, est nunc
imperdiet magna, vulputate aliquet nisi risus at est. Aliquam
imperdiet gravida tortor. Praesent interdum accumsan ante. Vivamus est
ligula, consequat sed, pulvinar eu, consequat vitae, eros. Nulla elit
nunc, congue eget, scelerisque a, tempor ac, nisi. Morbi facilisis.
Pellentesque habitant morbi tristique senectus et netus et malesuada
fames ac turpis egestas. In hac habitasse platea dictumst. Suspendisse
vel lorem ut ligula tempor consequat. Quisque consectetuer nisl eget
elit.
Proin quis mauris ac orci accumsan suscipit. Sed ipsum. Sed vel libero
nec elit feugiat blandit. Vestibulum purus nulla, accumsan et,
volutpat at, pellentesque vel, urna. Suspendisse nonummy. Aliquam
pulvinar libero. Donec vulputate, orci ornare bibendum condimentum,
lorem elit dignissim sapien, ut aliquam nibh augue in turpis.
Phasellus ac eros. Praesent luctus, lorem a mollis lacinia, leo turpis
commodo sem, in lacinia mi quam et quam. Curabitur a libero vel tellus
mattis imperdiet. In congue, neque ut scelerisque bibendum, libero
lacus ullamcorper sapien, quis aliquet massa velit vel orci. Fusce in
nulla quis est cursus gravida. In nibh. Lorem ipsum dolor sit amet,
consectetuer adipiscing elit. Integer fermentum pretium massa. Morbi
feugiat iaculis nunc.
Aenean aliquam pretium orci. Cum sociis natoque penatibus et magnis
dis parturient montes, nascetur ridiculus mus. Vivamus quis tellus vel
quam varius bibendum. Fusce est metus, feugiat at, porttitor et,
cursus quis, pede. Nam ut augue. Nulla posuere. Phasellus at dolor a
enim cursus vestibulum. Duis id nisi. Duis semper tellus ac nulla.
Vestibulum scelerisque lobortis dolor. Aenean a felis. Aliquam erat
volutpat. Donec a magna vitae pede sagittis lacinia. Cras vestibulum
diam ut arcu. Mauris a nunc. Duis sollicitudin erat sit amet turpis.
Proin at libero eu diam lobortis fermentum. Nunc lorem turpis,
imperdiet id, gravida eget, aliquet sed, purus. Ut vehicula laoreet
ante.
Mauris eu nunc. Sed sit amet elit nec ipsum aliquam egestas. Donec non
nibh. Cras sodales pretium massa. Praesent hendrerit est et risus.
Vivamus eget pede. Curabitur tristique scelerisque dui. Nullam
ullamcorper. Vivamus venenatis velit eget enim. Nunc eu nunc eget
felis malesuada fermentum. Quisque magna. Mauris ligula felis, luctus
a, aliquet nec, vulputate eget, magna. Quisque placerat diam sed arcu.
Praesent sollicitudin. Aliquam non sapien. Quisque id augue. Class
aptent taciti sociosqu ad litora torquent per conubia nostra, per
inceptos hymenaeos. Etiam lacus lectus, mollis quis, mattis nec,
commodo facilisis, nibh. Sed sodales sapien ac ante. Duis eget lectus
in nibh lacinia auctor.
Fusce interdum lectus non dui. Integer accumsan. Quisque quam.
Curabitur scelerisque imperdiet nisl. Suspendisse potenti. Nam massa
leo, iaculis sed, accumsan id, ultrices nec, velit. Suspendisse
potenti. Mauris bibendum, turpis ac viverra sollicitudin, metus massa
interdum orci, non imperdiet orci ante at ipsum. Etiam eget magna.
Mauris at tortor eu lectus tempor tincidunt. Phasellus justo purus,
pharetra ut, ultricies nec, consequat vel, nisi. Fusce vitae velit at
libero sollicitudin sodales. Aenean mi libero, ultrices id, suscipit
vitae, dapibus eu, metus. Aenean vestibulum nibh ac massa. Vivamus
vestibulum libero vitae purus. In hac habitasse platea dictumst.
Curabitur blandit nunc non arcu.
Ut nec nibh. Morbi quis leo vel magna commodo rhoncus. Donec congue
leo eu lacus. Pellentesque at erat id mi consequat congue. Praesent a
nisl ut diam interdum molestie. Fusce suscipit rhoncus sem. Donec
pretium. Aliquam molestie. Vivamus et justo at augue aliquet dapibus.
Pellentesque felis.
Morbi semper. In venenatis imperdiet neque. Donec auctor molestie
augue. Nulla id arcu sit amet dui lacinia convallis. Proin tincidunt.
Proin a ante. Nunc imperdiet augue. Nullam sit amet arcu. Quisque
laoreet viverra felis. Lorem ipsum dolor sit amet, consectetuer
adipiscing elit. In hac habitasse platea dictumst. Pellentesque
habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas. Class aptent taciti sociosqu ad litora torquent per
conubia nostra, per inceptos hymenaeos. Nullam nibh sapien, volutpat
ut, placerat quis, ornare at, lorem. Class aptent taciti sociosqu ad
litora torquent per conubia nostra, per inceptos hymenaeos.
Morbi dictum massa id libero. Ut neque. Phasellus tincidunt, nibh ut
tincidunt lacinia, lacus nulla aliquam mi, a interdum dui augue non
pede. Duis nunc magna, vulputate a, porta at, tincidunt a, nulla.
Praesent facilisis. Suspendisse sodales feugiat purus. Cras et justo a
mauris mollis imperdiet. Morbi erat mi, ultrices eget, aliquam
elementum, iaculis id, velit. In scelerisque enim sit amet turpis. Sed
aliquam, odio nonummy ullamcorper mollis, lacus nibh tempor dolor, sit
amet varius sem neque ac dui. Nunc et est eu massa eleifend mollis.
Mauris aliquet orci quis tellus. Ut mattis.
Praesent mollis consectetuer quam. Nulla nulla. Nunc accumsan, nunc
sit amet scelerisque porttitor, nibh pede lacinia justo, tristique
mattis purus eros non velit. Aenean sagittis commodo erat. Aliquam id
lacus. Morbi vulputate vestibulum elit.
EOS
    words.gsub!(/\n/,' ')
    words.gsub!(/\./,' ')
    words.gsub!(/\,/,' ')
    words.gsub!(/  */,' ')
    words.strip!
    words.downcase!
    words = words.split(/ /)

    #===== lorem
    lorem = ""
    #0.upto(word_count) {|n| lorem << words[rand(words.length)]}

    #===== total
    twn = 0
    twc = word_count
    while twn < twc

      #===== paragraph
      pwn = 0
      pwc = rand(100)+200
      while pwn < pwc and twn < twc do

        #===== sentence
        swn = 0
        swc = rand(10)+3
        while swn < swc and pwn < pwc and twn < twc do

          word = words[rand(words.length)]
          if swn == 0
            lorem << "#{word.capitalize} "
          else
            lorem << "#{word} "
          end

          twn +=1
          pwn +=1
          swn +=1

        end
        lorem << ". "

      end
      lorem << "\n\n"

    end

    lorem = lorem.gsub!(/ \./,'.')

  end

  
  
  
  def clippy(text, bgcolor='#FFFFFF')
    html = <<-EOF
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
            width="110"
            height="14"
            id="clippy" >
    <param name="movie" value="clippy.swf"/>
    <param name="allowScriptAccess" value="always" />
    <param name="quality" value="high" />
    <param name="scale" value="noscale" />
    <param NAME="FlashVars" value="text=#{text}">
    <param name="bgcolor" value="#{bgcolor}">
    <embed src="/clippy.swf"
           width="110"
           height="14"
           name="clippy"
           quality="high"
           allowScriptAccess="always"
           type="application/x-shockwave-flash"
           pluginspage="http://www.macromedia.com/go/getflashplayer"
           FlashVars="text=#{text}"
           bgcolor="#{bgcolor}"
    />
    </object>
    EOF
  end
  
  
  
end
