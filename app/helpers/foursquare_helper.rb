module FoursquareHelper
  require "foursquare2"
      
  def foursquare_venue_profile(brand)
    
    client = Foursquare2::Client.new(:client_id => 'PK51GIGZAJ4R20M0CUAWGVMIEBYLG2JUY25SZBZ2TUAK1AKV', 
      :client_secret => 'JX1R0Y511Q2YYRDXW554KXITFIOB0L3FQEFEWHNAX4T1OM2S')
    
    puts "Venue Profile"
    puts "--------------"
    venue = client.venue(brand.forsquare_venue)
    puts "Name  #{venue.name}"
    puts "contact  #{venue.contact.formattedPhone}"
    puts "Location  #{venue.location.address} #{venue.location.city},  #{venue.location.country},"
    puts "Stats : #{venue.stats.inspect}"
    puts "TIP Count :#{venue.stats.tipCount}"
    puts "usersCount :#{venue.stats.usersCount}"
    puts "checkinsCount :#{venue.stats.checkinsCount}"
  
    
    brand.forsquare_venue_address = "#{venue.location.address} #{venue.location.city},  #{venue.location.country}"
    brand.forsquare_checkins_count = venue.stats.checkinsCount
    brand.forsquare_users_count = venue.stats.usersCount
    brand.forsquare_tip_count = venue.stats.tipCount
    brand.save
  end
  
  
  def foursquare_venue_tips(brand)
      client = Foursquare2::Client.new(:client_id => 'PK51GIGZAJ4R20M0CUAWGVMIEBYLG2JUY25SZBZ2TUAK1AKV', 
      :client_secret => 'JX1R0Y511Q2YYRDXW554KXITFIOB0L3FQEFEWHNAX4T1OM2S')
    
    puts "Venue Tips"
    puts "--------------"
    client.venue_tips(brand.forsquare_venue).items.each { |tip|
      puts " -- Text : #{tip.text}"
      puts " -- User ID : #{tip.user.id}"
      puts " -- User Name : #{tip.user.firstName}  #{tip.user.lastName}"
      puts " -- User Photo : #{tip.user.photo} "
      puts " "
      
      
      if brand.fans.find_by_uid(tip.user.id).nil?
        Fan.new(:brand_id => brand.id, :name => "#{tip.user.firstName}  #{tip.user.lastName}"  ,:uid => tip.user.id, :image_url => tip.user.photo , :channel =>'foursquare').save
      end
      
      post = brand.posts.find_by_uid(tip.id)
      post = Post.new if post.nil?
        
      post.brand_id = brand.id
      post.uid = tip.id
      post.from = "#{tip.user.firstName}  #{tip.user.lastName}"
      post.kind = "tip"
      post.title = " New Tip from #{tip.user.firstName}  #{tip.user.lastName}"
      post.body = tip.text
      post.image_url = tip.user.photo
      post.network = "foursquare"
      post.save
    }
  end
end
