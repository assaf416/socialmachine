module YoutubeHelper
    

  def youtube_find_videos(search)
    res = []
    client = YouTubeIt::Client.new 
    for video in client.videos_by(:query => search).videos do 
      res << video
      # raise video.inspect
    end
    res
  end
  
  
  
   def youtube_find_videos_for_user(search)
    res = []
    client = YouTubeIt::Client.new 
    for video in client.videos_by(:user => search).videos do 
      res << video
      # raise video.inspect
    end
    res
  end
  
  
  def youtube_profile_update(channel)
    begin
      client = YouTubeIt::Client.new 
      puts "User Profile"
      puts "------------------"
      profile =  client.profile(channel.profile_url) # default: current user
      puts "Name : #{profile.username}"
      puts "image : #{profile.avatar}"
      puts "Location : #{profile.location}"
      puts "Last Login : #{profile.last_login}"
      puts "Occupation : #{profile.occupation}"
      puts "Subscribers : #{profile.subscribers}"
      puts "view_count : #{profile.view_count}"
    
      #    brand.youtube_profile = profile.username
      channel.profile_image_url = profile.avatar
      channel.fans_count = profile.subscribers
      channel.views_count = profile.view_count
      channel.save
    rescue Exception => e
      logger.error " FAILED ON YOUTUBE_PROFILE_UPDATE #{e.message}"
      logger.error e.message
    end
  end
  
  def video_id(video)
    v = video.reverse
    vv = ""
    for c in v.chars do 
      vv << c unless c == '/'
      return vv.reverse! if c == '/'
    end
  end

  
  def youtube_posts_and_comments(channel)
    channel.posts.delete_all
    channel.comments.delete_all
    begin
      client = YouTubeIt::Client.new 
      for video in client.videos_by(:user => channel.profile_name).videos do 
        puts " -- VIDEO --"
        puts "  Video ID : #{video_id(video.video_id)}"
        puts "  Published : #{video.published_at}"
        puts "  Categories : #{video.categories.inspect}"
        #  puts "  Label : #{video.label.inspect}"
        puts "  Keywords : #{video.keywords.inspect}"
        puts "  Title : #{video.title.inspect}"
        puts "  Description : #{video.title.inspect}"
        puts "  Viewes : #{video.view_count.inspect}"
        puts "  Favorites : #{video.favorite_count.inspect}"
  
        post = channel.posts.find_by_uid(video_id(video.video_id))
        post = Post.new if post.nil?
        
        post.brand_id = channel.brand.id
        post.channel_id = channel.id
        post.uid = video_id(video.video_id)
        post.title = video.title
        post.kind = "videos"
        post.rank = video.favorite_count
        post.body = video.title
        post.image_url = video.thumbnails[0].url
        post.posted_at = video.published_at
        post.network = "youtube"
        post.save
  
        #  puts video.inspect
        puts "    -- Comments"
        post.comments.delete_all
        for item in client.comments(video_id(video.video_id)) do 
          avatar = ""
          begin
            avatar = client.profile(item.author.name).avatar
          rescue Exception => r1
            logger.error "FAILED ON LOADING AVATAR , #{r1.message}"
            # next
          end
        
          comment = Comment.new( 
            :brand_id => channel.brand.id, :post_id => post.id , :network => "youtube", :channe_id => channel.id,
            :message => item.content, :image_url => avatar, :name => item.author.name , :uid => item.url)
          comment.save
        
          if channel.fans.find_by_uid(item.author.to_s).nil?
            Fan.new(:brand_id => channel.brand.id, :channel_id => channel.id, :name => item.author.name , :image_url => avatar, :uid => item.author.to_s).save
          end
        
          logger.error  "    --  FROM : #{item.author.name}"
          logger.error  "    --  TITLE #{item.title}"
          logger.error  "    --  CONTENT: #{item.content}"
        end
      end
    rescue Exception => e
      logger.error "FAILED ON youtube_posts_and_comments #{e.message}"
    end
    
  end
  
  
  
  
  def youtube_competitor_posts(competitor)
    begin
      return if competitor.youtube_rss.empty?
    
      client = YouTubeIt::Client.new 
      
      profile =  client.profile(competitor.youtube_rss) # default: current user
      puts "Name : #{profile.username}"
      puts "image : #{profile.avatar}"
      puts "Location : #{profile.location}"
      puts "Last Login : #{profile.last_login}"
      puts "Occupation : #{profile.occupation}"
      puts "Subscribers : #{}"
      puts "view_count : #{profile.view_count}"
      
      competitor.youtube_profile_image = profile.avatar
      competitor.youtube_subscribers_count = profile.subscribers
      competitor.youtube_views_count = profile.view_count
      competitor.save
      
      for video in client.videos_by(:query => competitor.youtube_rss).videos do 
        puts " -- VIDEO --"
        puts "  Video ID : #{video_id(video.video_id)}"
        puts "  Published : #{video.published_at}"
        puts "  Categories : #{video.categories.inspect}"
        #  puts "  Label : #{video.label.inspect}"
        puts "  Keywords : #{video.keywords.inspect}"
        puts "  Title : #{video.title.inspect}"
        puts "  Description : #{video.title.inspect}"
        puts "  Viewes : #{video.view_count.inspect}"
        puts "  Favorites : #{video.favorite_count.inspect}"
  
        post = competitor.posts.find_by_uid(video_id(video.video_id))
        post = Post.new if post.nil?
        
        post.brand_id = competitor.brand.id
        post.uid = video_id(video.video_id)
        post.title = video.title
        post.kind = "videos"
        post.competitor_id = competitor.id
        post.rank = video.favorite_count
        post.body = video.title
        post.image_url = video.thumbnails[0].url
        post.posted_at = video.published_at
        post.network = "youtube"
        post.comments_count = client.comments(video_id(video.video_id)).count
        post.save
      end
    end
  rescue Exception => e
    logger.error "FAILED TO LOAD COMPETITOR YOUTUBE POSTS #{e.message}"
  end
    
end
