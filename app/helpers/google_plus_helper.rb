module GooglePlusHelper

  require "nokogiri"
  require 'open-uri'
  # require 'yml'
  require "google_plus"
  
  
  GooglePlus.api_key = 'AIzaSyC0M_ZcSFq1IQB-kIfeSxvgqmeI1JP5y7A'
  
  
  
  def google_news(search)
    logger.error " -- GOOGLE NEWS SEARCH FOR #{search}"
    new_search = ""
    for c in search.chars do 
      if c == '"' or c==" " then
        new_search << "+"
      else
        new_search << c
      end
    end
   
    begin
      res = []
      
      url = "http://news.google.com/news?q=#{new_search}&num=100&output=rss"
      logger.error "URl => " + url
      feed = Feedzirra::Feed.fetch_and_parse(url)
      for item in feed.entries do 
        logger.error " -- GOOGLE NEWS RESULT :: #{item.inspect}"
        res << item
      end
      res
    rescue Exception => e
      logger.error "EXcpetion ::: #{e.message}"
    end
  end
  
  
  
  def ignore?(link)
    return true if link.include? "facebook.com"
    return true if link.include? "linkedin.com"
    return true if link.include? "twitter.com"
    return true if link.include? "youtube.com"
    return true if link.include? "plus.google.com"
    return false
  end
  
  
  def google_search_doc(html)  

    begin    
      doc = Nokogiri::HTML(html)

      # Do funky things with it using Nokogiri::XML::Node methods...

      ar_1 = []
      ar_2 = []
      ar_3 = []

      ####
      # Search for nodes by css
      doc.css('h3.r a').each do |link|
        ar_1 << link.content
      end

      doc.search("cite").each do |cite|
        link = cite.inner_text
        ar_2 << link
      end

      doc.css("li.g").each do |x|
        ar_3 << x.inner_text
      end

      @res_google = []
      i = 0
      for item in ar_1 do
        description = ar_3[i]
        @res_google << {:title => ar_1[i], :description => description, :url => ar_2[i]} 
        i = i + 1
      end
      @res_google
    rescue
      []
    end
  end
  
  
  def google_search(what)  
    #   return []
    new_search = CGI::escape(what)

    begin    
      doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{new_search}&num=100"))

      # Do funky things with it using Nokogiri::XML::Node methods...

      ar_1 = []
      ar_2 = []
      ar_3 = []

      ####
      # Search for nodes by css
      doc.css('h3.r a').each do |link|
        ar_1 << link.content
      end

      doc.search("cite").each do |cite|
        link = cite.inner_text
        ar_2 << link
      end

      doc.css("li.g").each do |x|
        ar_3 << x.inner_text
      end

      @res_google = []
      i = 0
      for item in ar_1 do
        description = ar_3[i]
        @res_google << {:title => ar_1[i], :description => description, :url => ar_2[i]} 
        i = i + 1
      end
    

      @res_google
    rescue
      []
    end
  end
  
  require 'uri'
   
  def find_competitor_in_google(what)
    return []
    new_search = CGI::escape(what)

    doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{new_search}&num=100"))


    ar_1 = [] # title
    ar_3 = [] # Description
    ar_2 = [] # url
    ar_4 = [] # url-domain

    ####
    # Search for nodes by css
    doc.css('h3.r a').each do |link|
      ar_1 << link.content
    end

    doc.search("cite").each do |cite|
      link = cite.inner_text
      ar_2 << link
      begin
        ar_4 << URI.parse(link.to_s).host
      rescue Exception => e
        logger.error "---->>> #{e.message}"
        ar_4 << "-ERROR-" + link.to_s
      end
    end

    doc.css("li.g").each do |x|
      ar_3 << x.inner_text
      
    end

    @res_google = []
    i = 0
    for item in ar_1 do
      begin
        description = ar_3[i]
        @res_google << {:title => ar_1[i], :description => description, :url => ar_2[i] , :domain => ar_4[i] } unless ignore_item?(ar_2[i],ar_4)
        i = i + 1
      rescue 
      end
    end
    

    @res_google
  end
    
  
  
  def find_competitor_in_facebook(what)
    return []
    new_search = CGI::escape(what)

    doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{new_search}&num=100&site://facebook.com"))


    ar_1 = [] # title
    ar_3 = [] # Description
    ar_2 = [] # url

    ####
    # Search for nodes by css
    doc.css('h3.r a').each do |link|
      ar_1 << link.content
    end

    doc.search("cite").each do |cite|
      link = cite.inner_text
      ar_2 << link
    end

    doc.css("li.g").each do |x|
      ar_3 << x.inner_text
      
    end

    @res_google = []
    i = 0
    for item in ar_1 do
      begin
        description = ar_3[i]
        @res_google << {:title => ar_1[i], :description => description, :url => ar_2[i] } if ar_2[i].include? "www.facebook.com"
        i = i + 1
      rescue
      end
    end
    

    @res_google
  end
  
  
   
  def find_competitor_in_twitter(what)
    return []
    new_search = CGI::escape(what)

    doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{new_search}&num=100&site://twitter.com"))


    ar_1 = [] # title
    ar_3 = [] # Description
    ar_2 = [] # url

    ####
    # Search for nodes by css
    doc.css('h3.r a').each do |link|
      ar_1 << link.content
    end

    doc.search("cite").each do |cite|
      link = cite.inner_text
      ar_2 << link
    end

    doc.css("li.g").each do |x|
      ar_3 << x.inner_text
      
    end

    @res_google = []
    i = 0
    for item in ar_1 do
      begin
        description = ar_3[i]
        @res_google << {:title => ar_1[i], :description => description, :url => ar_2[i] } if ar_2[i].include? "www.twitter.com"
      rescue
      end
      i = i + 1
    end
    

    @res_google
  end
  
  
  def ignore_item?(url, list)
    return true if url.include? ".org/"
    return true if url.include? ".gov/"
    
    
    return false
  end
  
  def google_search_user(name)
    res = []
    GooglePlus.api_key = 'AIzaSyC0M_ZcSFq1IQB-kIfeSxvgqmeI1JP5y7A'
    search = GooglePlus::Person.search("#{name}")
    search.each do |p|
      begin
        desc = p.tagline unless p.tagline.nil?
        bio = p.aboutMe unless p.aboutMe.nil?
      rescue
        desc = ""
        bio = ""
      end
      res << { :name =>  p.display_name , :img => p.image.url , :url => p.url , :about => bio , :description => desc }
    end
    res
  end
    
   
  #
  # Google Plus
  #
  def google_plus_profile_update(brand)
    return if brand.google_page.empty?
    begin
      puts "set Google Fans and Followings"
      GooglePlus.api_key = 'AIzaSyC0M_ZcSFq1IQB-kIfeSxvgqmeI1JP5y7A'

      person = GooglePlus::Person.get(brand.google_page)
      doc = Nokogiri::HTML(open(person.url))
      res = []
      doc.css('h4').each do |link|
        ii =  /\d+/.match(link.content)
        puts  ii
        res << ii.to_s
      end
    
      puts res.to_s
      if res.length == 2
        brand.google_plus_follow_count = res[0]
        brand.google_plus_fans_count = res[1]
        brand.save
      end
    rescue
      return nil
    end
  end
  
  
  
  def google_posts_and_comments(brand)
    return if brand.google_page.empty?
    
    channel_id = brand.get_channel("google").id
    puts "Google Plus Posts and Comments"
    GooglePlus.api_key = 'AIzaSyC0M_ZcSFq1IQB-kIfeSxvgqmeI1JP5y7A'

    person = GooglePlus::Person.get(brand.google_page)
   

    puts "PERSON DETAILS"
    puts "----------------------------------"
    puts "INSPECT:   #{person.inspect}"
    puts "Name:   #{person.display_name}"
    puts "ID:   #{person.id}"
    puts "profile image   #{person.image.url}"


    puts "PERSON Activities"
    puts "============================="
    cursor = person.list_activities
    cursor.each do |item|
  
      puts "  Title:  #{item.title}"
      puts "  Image : #{item.actor.image.url}"
      puts "  When:   #{item.published}"
      puts "  Who     #{item.actor.display_name}"
      puts "  Type    #{item.verb}"
      puts "  Content : #{item.object.content}"
      puts "  Replies   #{item.object.replies.total_items}"
      
      
      
      post = brand.posts.find_by_uid(item.id)
      post = Post.new if post.nil?
        
      post.brand_id = brand.id
      post.uid = item.id
      post.title = item.title
      post.kind = "activity"
      post.channel_id = channel_id
      post.body = item.object.content
      post.image_url = item.actor.image.url
      post.posted_by = item.actor.display_name
      post.posted_at = item.published.to_s
      post.network = "google"
      post.save
      
      
      
      puts " Comments:"
      post.comments.delete_all
      cursor_comments = item.list_comments
      cursor_comments.each do |c|
        
        unless brand.fans.find_by_uid(c.actor.id)
          Fan.new( :brand_id => brand.id , :channel_id => channel_id , :uid =>  c.actor.id , :image_url => c.actor.image.url , :name => c.actor.display_name).save
        end
        
        
        comment = Comment.new( 
          :brand_id => brand.id, :post_id => post.id , :channel_id => channel_id, :uid => c.id,
          :message => c.object.content, :image_url => c.actor.image.url, :name => c.actor.display_name )
        comment.save
        
        puts " -- New Comment -- "  
        puts "  -- Type : #{c.kind}"
        puts "  -- When : #{c.published}"
        puts "  -- Message : #{c.object.content}"
        puts "  -- Image : #{c.actor.image.url}"
        puts "  -- Name : #{c.actor.display_name}"
      end
    end
  end
  
    
  
  def google_competitor_posts(competitor)
    return if competitor.google_page.empty?
    
    puts "Google Plus Posts and Comments"
    GooglePlus.api_key = 'AIzaSyC0M_ZcSFq1IQB-kIfeSxvgqmeI1JP5y7A'

    person = GooglePlus::Person.get(competitor.google_page)
   
    competitor.google_plus_profile_image = person.image.url
    #    competitor.google_plus_comments_count = person.image.url
    competitor.save
    

    puts "Competitor DETAILS"
    puts "----------------------------------"
    puts "INSPECT:   #{person.inspect}"
    puts "Name:   #{person.display_name}"
    puts "ID:   #{person.id}"
    puts "profile image   #{person.image.url}"


    puts "Competitor Activities"
    puts "============================="
    cursor = person.list_activities
    cursor.each do |item|
  
      puts "  Title:  #{item.title}"
      puts "  Image : #{item.actor.image.url}"
      puts "  When:   #{item.published}"
      puts "  Who     #{item.actor.display_name}"
      puts "  Type    #{item.verb}"
      puts "  Content : #{item.object.content}"
      puts "  Replies   #{item.object.replies.total_items}"
      
      
      
      post = competitor.posts.find_by_uid(item.id)
      post = Post.new if post.nil?
        
      post.brand_id = competitor.brand.id
      post.uid = item.id
      post.title = item.title
      post.kind = "activity"
      post.network = "google_plus"
      post.competitor_id = competitor.id
      post.body = item.object.content
      post.image_url = item.actor.image.url
      post.posted_by = item.actor.display_name
      post.posted_at = item.published.to_s
      post.network = "google"
      #      post.comments_count = 
      post.save
    end
  end
  
end
