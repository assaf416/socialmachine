module MentionsHelper


  include YoutubeHelper
  include TwitterHelper
  include GooglePlusHelper
  include FacebookHelper
  include RedditHelper
  include RssHelper


  # @videos = youtube_find_videos(search)
  # puts " --- >  #{Time.now}  before google search"


  def mentions_scan(brand, competitor)

    if competitor
      search =  competitor.name
    else
      # search = params[:search].gsub(" ","+") unless params[:search].nil?
      search = brand.name.gsub(" ","+")
    end

    logger.error " MENTIONS SCAN FOR #{search}"

    logger.error " TWITTER MENTIONS SCAN FOR #{search}"
    ## TWITTER Search
    begin
      @tweets = twitter_find(search,"xxx")
      for item in @tweets do
        logger.error " ---> #{item.text}"
        m = Mention.new
        m.brand_id = brand.id
        m.competitor_id =  competitor_id unless competitor.nil?
        m.channel_name = "twitter"
        m.body = strip_html(item.text).gsub!(/(?:f|ht)tps?:\/[^\s]+/, '')
        m.author =  item.from_user_name
        m.url =  extract_url_from_html(item.text)
        m.user_image_url = item.profile_image_url
        m.image_url = item.profile_image_url
        m.posted_at = Time.now
        m.save unless m.body.empty? or m.body.nil?
      end
    rescue Exception => e
      logger.error "FAILED ON MENTION TWITTER SCAN #{search}"
    end


    logger.error " STARTING RSS SEARCH "
    channel = brand.get_channel("rss")
    for item in channel.posts do 
      if  item.body and item.body.include? search 
        m = Mention.new
        m.brand_id = brand.id
        m.competitor_id =  competitor_id unless competitor.nil?
        m.channel_name = "RSS"
        m.body = strip_html(item.body).gsub!(/(?:f|ht)tps?:\/[^\s]+/, '')
        m.author =  item.from
        m.url =  extract_url_from_html(item.body)
        m.posted_at = item.posted_at
        m.save unless m.body.nil? 
      end
    end

    logger.error "STARTING FACEBOOK MENTIONS SCAN FOR #{search}"
    ## FACEBOOK Search
    begin
      @fb_users  = fb_user_search(search)
      @fb_groups  = fb_group_search(search)
    rescue Exception => e
      logger.error "FAILED ON MENTION TWITTER SCAN #{search}"
    end


    logger.error " Youtube MENTIONS SCAN FOR #{search}"
    ## Youtube Search
    begin
    rescue Exception => e
      logger.error "FAILED ON MENTION TWITTER SCAN #{search}"
    end


    logger.error " GOOGLE SEARCH  SCAN FOR #{search}"
    ## Google Search
    begin
      logger.error "YO YOYOY @"
      res = []
      url = "http://news.google.com/news?q=#{search}&output=rss"
      logger.error " ==> #{url} "
      feed = Feedzirra::Feed.fetch_and_parse(url)
      for item in feed.entries do
        res << item
      end
      res
      for item in res do
        logger.error " ---> #{item.title }"
        m = Mention.new
        m.brand_id = brand.id
        m.competitor_id =  competitor_id unless competitor.nil?
        m.channel_name = "google"
        m.body =  item.title
        m.url =  item.url
        # m.image_url = extract_image_url(item.summary)
        m.posted_at = item.published
        m.save
      end
    rescue Exception => e
      logger.error "FAILED ON MENTION Google  SCAN #{search} reason : #{e.message}"
    end

    logger.error " GOOGLE SEARCH (Linked IN ) SCAN FOR #{search}"
    ## Google Search ( Linkedin Only)
    begin
    rescue Exception => e
      logger.error "FAILED ON MENTION TWITTER SCAN #{search}"
    end

    logger.error " Google Reader SEARCH  SCAN FOR #{search}"
    ## Search in Users Google Reader
    begin
    rescue Exception => e
      logger.error "FAILED ON MENTION TWITTER SCAN #{search}"
    end
  end


  require 'nokogiri' # gem install nokogiri
  def extract_image_url(html)
    doc = Nokogiri::HTML( html )
    img_srcs = doc.css('img').map{ |i| i['src'] } # Array of strings
    img_srcs.first or "/assets/icons/rss.png"
  end


end
