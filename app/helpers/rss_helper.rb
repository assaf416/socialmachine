# To change this template, choose Tools | Templates
# and open the template in the editor.



module RssHelper
  require "google_reader_api"
  
  
  def rss_search(what)
    return ""
    res = []
    begin
      user = GoogleReaderApi::User.new({ :email => current_user.brand.gmail_email, :password => current_user.brand.gmail_pwd })
      for item in user.subscriptions
        feed = Feedzirra::Feed.fetch_and_parse(item.url)        
        feed.entries.each do |entry|
          begin
            entry.sanitize!
          
            res << {
              :title     => entry.title,
              :body      => entry.summary,
              :url       => entry.url,
              :image_url => entry.url,
              :posted_at => entry.published,
              #            :uid       => entry.id,
              #            :feed_id  => feed.id
            } if entry.summary.include? what or entry.title.include? what
          rescue
            next
          end
        end
      end
      return res
    rescue Exception => e
      logger.error "FAILD SEARCHING RSS :#{e.message}"
    end
  end
  
  
  
 
  def rss_facebook_page_posts(pageid)
    
  end
  
  def rss_twitter_user_posts(userid)
    
  end
  
  def rss_linkedin_user_activities(userid)
    
  end
  
  
  
end
  
