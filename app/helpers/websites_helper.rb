module WebsitesHelper
  
   def website_chart_1(scores)
    [
      { :year => '2008', :value => 20 },
      { :year => '2009', :value => 22 },
      { :year => '2010', :value => 25 },
      { :year => '2011', :value => 26 },
      { :year => '2012', :value => 56 }
    ]
  end

  def pie_chart_referres_data(json)
    channels = []
    channels << { :label => lorem(2) , :value =>  rand(100) }
    channels << { :label => lorem(2) , :value =>  rand(100) }
    channels << { :label => lorem(2) , :value =>  rand(100) }
    channels
  end

  def pie_chart_countries_data(json)
    channels = []
    channels << { :label => lorem(2) , :value =>  rand(100) }
    channels << { :label => lorem(2) , :value =>  rand(100) }
    channels << { :label => lorem(2) , :value =>  rand(100) }
    channels
  end
  
   def pie_chart_browsers_data(json)
    channels = []
    channels << { :label => lorem(2) , :value =>  rand(100) }
    channels << { :label => lorem(2) , :value =>  rand(100) }
    channels << { :label => lorem(2) , :value =>  rand(100) }
    channels
  end
  
   
  
end
