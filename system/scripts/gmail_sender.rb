#
#   THE SOCIAL MACHINE 
#   
#   SCRIPT      : GMAIL_SENDER
#   Description : Sends Emails via GMAIL from YML FILE.
#
#
#

# encoding: UTF-8
require 'rubygems'
require 'yaml'
#require 'FileUtils'
  

def email_sender(user,pwd,from_email,to_email,subject, email_file)
  begin
    cmd = "smtp-cli --verbose --host smtp.gmail.com --user='#{user}' --auth-plain --from='#{from_email}' --to='#{to_email}' --pass='#{pwd}' --subject='#{subject}' --body-html='#{email_file}' >> ~/logs/gmail_sender.log "
    puts "STATUS:: CALLING #{cmd}"
    sts = `#{cmd} `
    puts "RUNNING STATUS : #{sts}"
  rescue Exception => e
    puts "FAILED TO RUN THE smtp-cli. #{e.message} #{cmd}"
  end
end

 
#
#   MAIN 
#

if ARGV == [] then 
  puts " USAGE: gmail_sender   email.yml email.dta"
  exit
else
  puts " LOADING Email to open"
  begin
    yaml_file = ARGV[0]
    email = YAML::load(File.open( yaml_file ))
    data_file = ARGV[1]
  rescue Exception => e
    puts "ERROR RUNIING GMAIL_SENDER : #{e.message}"
    exit
  end
end


puts " STATUS:: SENDING GMAIL  "

# call send_email
begin
  
  status = email_sender(email[:user].gsub("@gmail.com",""),email[:pwd],email[:user],email[:to_email],email[:subject], data_file)
  #  status = send_email(email[:user], email[:pwd], email[:to_email], email[:subject], email[:html] )
  res_status = "" 
#  status.chars.each{| c| 
#    if c == " " then 
#      res_status << "%20"
#    else
#      res_status << c
#    end
#  }
rescue Exception => e
  puts "FAILED TO RUN SEND_EMAIL  #{e.message}" 
end

# update social machine 
begin
  uri = URI.parse(email[:url]+ "'#{res_status}'")
  response = Net::HTTP.get_response(uri)
  res = Net::HTTP.get(uri)
  
  # remove the file
  FileUtils.rm(yaml_file)
  puts "DONE !"
  
  
  #  puts res
rescue Exception => e
  puts "FAILED TO UPDATE SOCIALMACHINE EMAIL STATUS  #{e.message}" 
end


