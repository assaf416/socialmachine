
class GooglePlusScanner < GenericScanner
  require 'google_plus'
  
  #
  # google_plus Scan
  # 
  
  def scan(brand, url, file)
    
    #    client = google_plus::Client.new('y1ifejqt5oss', '0EI8fpBTNyOf1G0U')
    
    status "SCANNING google_plus"
    
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      error "FAILED THE google_plus SCAN #{e.message}"
    end
      
    
    token = plan[0][:google_plus_token]
    profile_id = plan[0][:google_plus_profile_id]
    
    GooglePlus.api_key = 'AIzaSyBkHTrd0iEB84Ije_1VehUnBWTx4j_CCk8'
    person = GooglePlus::Person.get(profile_id)#, :access_token => token)
    status person.display_name
    activites = person.list_activities.items
    
    channel_info = { :profile => person, :activities => activites}
    post_to_socialmachine(channel_info,file,url)
    
  end
end