def ps_scan
  res = `ps -fe | grep scanner_ctl`
  return "" if res.empty? 
  words = res.split("\n").first.split(" ")
  return "PROCESS ID: #{words[1]} (#{words[7]} #{words[8]})"
end


def ps_development_scan
  res = `ps -fe | grep  main.rb`
  return "" if res.empty? 
  begin
    words = res.split("\n").first.split(" ")
    return "Development scan: #{words[1]} (#{words[7]} #{words[8]})"
  rescue
    return ""
  end
end
def ps_production_scan
  res = `ps -fe | grep brands_production.rb`
  return "" if res.empty? 
  begin
    words = res.split("\n").first.split(" ")
    
    return "Production scan: #{words[1]} (#{words[7]} #{words[8]})"
  rescue
    return ""
  end
end


def start_scanner
  res = `../../lib/daemons/scanner_ctl start`
end

def stop_scanner
  res = `../../lib/daemons/scanner_ctl stop`
end

def scan_development
  res = `ruby main.rb &`
end

def scan_production
  res = `ruby brands_production.rb &`
end