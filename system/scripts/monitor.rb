# To change this template, choose Tools | Templates
# and open the template in the editor.


require 'rubygems'
require 'highline'
require "highline/import"
require 'net/http'
require './monitor_helper'



def get_last_scan
  return "23 minutes"
end


def mailing_menu
   
  puts " \n\n\n\n"
  puts " ! ----------------------------------------------------------------"
  puts " !                   SOCIAL MACHINE EMAIL QUEUE                   !"
  puts " ! ----------------------------------------------------------------"
  puts "     NEW EMAILS PENDING ..: 23 "
  puts "     FAILING EMAILS     ...: #{get_last_scan}"
  puts "     SENT EMAILS        ...: #{get_last_scan}"
  puts " ! ----------------------------------------------------------------"
    
  choose do |menu|
    menu.prompt = "Mailing Options  "

    menu.choice("START MAILER  DAEMON") { 
      mailing_menu
    }
    
    menu.choice("STOP MAILER DAEMON") { 
      mailing_menu
    }
    
    menu.choice("SEND ALL EMAILS IN DEVELOPMENT") { 
      mailing_menu
    }
    
    menu.choice("SEND ALL EMAILS IN PRODUCTION") { 
      mailing_menu
    }
    
        
    menu.choice("Back to Main menu") { 
     return
    }
    
  end
end




def scan_menu
   
  puts " \n\n\n\n"
  puts " ! ----------------------------------------------------------------"
  puts " !                   SOCIAL MACHINE SCAN OPTIONS                   !"
  puts " ! ----------------------------------------------------------------"
  puts "  #{ps_scan} "
  puts "  #{ps_development_scan} "
  puts "  #{ps_production_scan} "
  
  puts " ! ----------------------------------------------------------------"
  
  puts "     LAST SCAN FOR PRODUTION ...: #{get_last_scan}"
  puts "     LAST SCAN FOR DEV       ...: #{get_last_scan}"
  puts "     Errors                  ...: #{get_last_scan}"
  puts " ! ----------------------------------------------------------------"
    
  say("Scan menu")
  choose do |menu|
    menu.prompt = "Select brand to scan  "

    menu.choice(:all) { 
      say("scanning all brands") 
    }
    
    menu.choice("START SCANNER DAEMON") { 
      start_scanner 
      scan_menu
    }
    
    menu.choice("STOP SCANNER DAEMON") { 
      stop_scanner 
      scan_menu
    }
    
    menu.choice("Scan Development") { 
      scan_development 
      scan_menu
    }
    
    menu.choice("Scan New Requests") { 
      scan_production 
      scan_menu
    }
    menu.choice("Scan New Requests") { 
      scan_development 
      scan_menu
    }
    
    menu.choice("Back to Main menu") { 
     return
    }
    
  end
end

def post_to_social_machine(kind)
  the_url = "http://23.21.17.104/api/logs?kind=#{kind}"
  puts "POSTING TO #{the_url}"
  url = URI.parse(the_url)
  req = Net::HTTP::Get.new(url.path)
  res = Net::HTTP.start(url.host, url.port) {|http|  http.request(req) }
  puts res.body
  return res.body
  
end

def log_menu
   
  puts " \n\n\n\n"
  puts " ! ----------------------------------------------------------------"
  puts " !                   SOCIAL MACHINE LOG FILES                     !"
  puts " ! ----------------------------------------------------------------"
    
  say("Log menu")
  choose do |menu|
    menu.prompt = "Select log file to show  "

    menu.choice(:application) { 
      say("Showing Application Log file") 
      say post_to_social_machine("application")
    }
    
    menu.choices(:system) { 
      say("System Memory and Disk usage") 
      say post_to_social_machine("system")
    }
    menu.choices(:trackers) { 
      say("Showing latest trackers") 
      post_to_social_machine("trackers")
    }
    
  end
end



choose = []
while true do 
  choose do |menu|
    puts " \n\n\n\n"
    puts " ! ----------------------------------------------------------------"
    puts " !                   SOCIAL MACHINE MONITOR APP                   !"
    puts " ! ----------------------------------------------------------------"
    
    
    menu.prompt = "Ready to manage Socialmachine?  "

    menu.choice( "Manage Log Files") { 
      log_menu
    }
    menu.choices( "Manage brands scans") { 
      scan_menu
    }
    menu.choices( "Manage Email Queues") { 
      mailing_menu
    }
    menu.choices( "Export brand to YAML files") { 
      scan_menu
    }
    menu.choices("Quit Application.") { 
      exit
    }
  end
end