require 'json'
require './generic_scanner'
require './facebook_scanner'
require './twitter_scanner'
require './linkedin_scanner'
require './gmail_scanner'
require './rss_scanner'
require './google_plus_scanner'
require './youtube_scanner'
require './website_scanner'



t0 = Time.now
puts " =====  #{t0} ======= start"
# server_url = "http://localhost:3000" #"http://23.21.17.104"

server_url = "http://23.21.17.104"
brands = [1] # BRANDS TO PROCESS

scan_facebook = false
scan_twitter = false
scan_linkedin = false
scan_youtube = false
scan_gmail = false
scan_rss = true
scan_website = false

while true do 
  for brand in brands do 
    threads = []

    #
    # FACEBOOK
    #
    if scan_facebook
      threads << Thread.new() {  
        begin
          puts " -- @@-- BRAND #{brand} FACEBOOK SCAN"
          fb = FacebookScanner.new
          fb.scan(brand, server_url, "../logs/#{brand}.facebook-out.yaml")  
        rescue Exception => e
          puts " -- @@-- BRAND #{brand} FAILED ON FACEBOOK SCAN.. #{e.message}"
          puts e.backtrace
        end
      }
    end


    #
    # TWITTER
    #
    if scan_twitter
      threads << Thread.new() {  
        begin
          puts " -- @@-- BRAND #{brand} TWITTER SCAN"
          tw = TwitterScanner.new
          tw.scan(brand, server_url, "../logs/#{brand}.twitter.out.yaml")
        rescue Exception => e
          puts " -- @@-- BRAND #{brand}  FAILED ON TWITTER SCAN.."
          puts e.backtrace
        end
      }
    end

    #
    # Linkedin
    #
    if scan_linkedin
      threads << Thread.new() {  
        begin
          puts " -- @@-- BRAND #{brand} LINKEDIN SCAN"
          li = LinkedinScanner.new
          li.scan(brand, server_url, "../logs/#{brand}.linkedin.out.yaml")
        rescue Exception => e
          puts " -- @@-- BRAND #{brand}  FAILED ON Linkedin SCAN.. #{e.message}"
          puts e.backtrace
        end
      }
    end



    #
    # Gmail
    #
    if scan_gmail
      threads << Thread.new() {  
        begin
          puts " -- @@-- BRAND #{brand} GMAIL SCAN"
          gm = GmailScanner.new
          gm.scan(brand, server_url, "../logs/#{brand}.gmail.out.yaml")
        rescue Exception => e
          puts " -- @@-- BRAND #{brand}  FAILED ON GMAIL SCAN.. #{e.message}"
          puts e.backtrace
        end
      }
    end

    #
    # GOOGLE READER RSS
    #
    if scan_rss
      threads << Thread.new() {  
        begin
          puts " -- @@-- BRAND #{brand} RSS SCAN"
          gr = RSSScanner.new
          gr.scan(brand, server_url, "../logs/#{brand}.rss.out.yaml")
        rescue Exception => e
          puts " -- @@-- BRAND #{brand}  FAILED ON RSS SCAN.. #{e.message}"
          puts e.backtrace
        end
      }
    end


    #
    # YOUTUBE
    #
    if scan_youtube
      threads << Thread.new() {  
        begin
          yt = YoutubeScanner.new
          yt.scan(brand, server_url, "../logs/#{brand}.youtube.out.yaml")
        rescue Exception => e
          puts " -- @@-- BRAND #{brand}  FAILED ON Youtube SCAN.. #{e.message}"
          puts e.backtrace
        end
      }
    end


#
# WEBSITE SCAN
#
if scan_website
  threads << Thread.new() {  
    begin
      ws = WebsiteScanner.new
      ws.scan(brand, server_url, "../logs/website.json")  
    rescue Exception => e
      puts " -- @@-- FAILED ON WEBSITE  SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end


    #threads << Thread.new() {  
    #  begin
    #    gp = GooglePlusScanner.new
    #    gp.scan(brand, server_url, "tmpfile")
    #  rescue
    #    puts " -- @@-- FAILED ON GOOGLE PLUS SCAN.."
    #  end
    #}

    threads.each { |aThread|  aThread.join }
    t1 = Time.now
    puts " =====  #{t1} ======= finish"
    puts "PROCESS TOOK #{t1 - t0 } seconds"

  end
  puts " -- @@-- TAKING A NAP for 15 minitues .....zzzzz"
  sleep(900) # SLEEP FOR 15 minutes
  puts " -- @@-- RUNNING AGAIN..."
end