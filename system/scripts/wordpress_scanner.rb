
class WordpressScanner  < GenericScanner
  require 'feedzirra'
  # encoding: UTF-8 
  
  #
  # Wordpress Scan
  # 
  
  def scan(brand, url, file)
    
    t00 = Time.now
    status "SCANNING WORDPRESS.COM"
    
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      error "FAILED THE WORDPRESS SCAN #{e.message}"
    end
      
    feed_url  = plan[0][:wordpress_url]
    channel_id = plan[0][:wp_cid]
    
    debug "WORDPRESS FEED:#{feed_url}"
    
    channel_info = {}
    the_articles = []
    the_comments = []

    
    begin
      feedx = Feedzirra::Feed.fetch_and_parse(feed_url)
      feedx.entries.each do |entry|
        xentry = {
          :name         => entry.title,
          :summary      => entry.summary,
          :url          => entry.url,
          :published_at => entry.published,
          :guid         => entry.id
        }
          
        the_articles << xentry
      end
    rescue Exception => e
      puts "FAILED ON ENTRIES #{e.message} || FEED: #{feed_url}"
    end
    
    
    
    channel_info[:scan] = { :scan_start_time => t00, :scan_end_time => Time.now, :channel => channel_id}
    channel_info[:articles] = the_articles
    channel_info[:comments] = the_comments
      
    post_to_socialmachine(channel_info,file,url)
      
  end

 


  
end