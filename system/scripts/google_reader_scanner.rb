require "google_reader_api"
require 'feedzirra'

# encoding: UTF-8 
class GoogleReaderScanner < GenericScanner
 
  #
  # Google Reader Scan
  # 
  
  
  def scan(brand, url, file_name)
    status "SCANNING GOOGLE READER"
    t00 =Time.now
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      error "FAILED THE RSS  SCAN #{e.message}"
    end
      
    
    
    email = plan[0][:gmail_email]
    pwd = plan[0][:gmail_secret]
    search = plan[0][:content_search]
    job_search = plan[0][:job_search]
    channel_id = plan[0][:rs_cid]
    google_token = plan[0][:google_token]
  
    channel_info = {:name => "RSS"}
    ar_feeds = []
    failed = []
    recommended_articles = []
    recommended_jobs = []
    unread_articles = []
    unread_articles2 = {}
  
    
#    debug "GMAIL USER:#{email}::PWD:#{pwd}"
    user = GoogleReaderApi::User.new(:email => email, :password => pwd)
#    user = GoogleReaderApi::User.new(:auth => google_token)

    
    start_time = Time.now - 3.days
    
    for feed in user.feeds do 
      _count = 0
      feedx = Feedzirra::Feed.fetch_and_parse(feed.url)
      
      begin
        feedx.entries.each do |entry|
          if entry.published > start_time 
            xentry = {
              :feed         => feed.title,
              :name         => entry.title,
              :summary      => entry.summary,
              :url          => entry.url,
              :published_at => entry.published,
              :guid         => entry.id
            }
          
            unread_articles << xentry
            for keyword in search.split(",") do 
              unless xentry[:summary].nil?
                if xentry[:summary].include? keyword
                  recommended_articles <<  xentry 
                  _count = _count + 1
                  break
                end
              end
            end
            
            for keyword in job_search.split(",") do 
              unless xentry[:summary].nil?
                if xentry[:summary].include? keyword
                  recommended_jobs <<  xentry 
#                  _count = _count + 1
                  break
                end
              end
            end
          end
        end
      rescue Exception => e
        failed << { :namd => feed.title ,    :reason => e.message}
        puts "FAILED ON ENTRIES #{e.message} || FEED: #{feed.title}"
        
#        user.subscriptions.remove_if {|feed| feed.title =~ /#{feed.title}/i}
#        puts "REMOVED SUBSCRIPTION FROM #{feed.title}"
        next
      end
      
      ar_feeds << { :url =>  feed.url.to_s ,  :title => feed.title  , :found => _count.to_i}
    end
    
    #    for feed in user.feeds do 
    #      begin 
    #        feeds << {:title => feed.title , :url => feed.url }
    #        for x in feed.unread_items(10) do 
    #          #        x.entry.toggle_read
    #          unread_articles <<  x.entry
    #          unless x.entry.content.nil?
    #            ssss = { 
    #              :message => x.entry.content.content ,
    #              :posted_at => x.entry.published.content,
    #              :link => x.entry.link.href,
    #              :title  => x.entry.title.to_s,
    #              :feed => { :name => feed.title , :url => feed.url}
    #            } 
    #            unread_articles2[x.entry.link.href] = ssss
    #            for keyword in search.split(",") do 
    #              recommended_articles <<  ssss if ssss[:message].include? keyword
    #            end
    #          end
    #        end
    #      rescue Exception => e
    #        failed << {:feed => feed.title, :error => e.message}
    #        error "FAILED ON GOOGLE READER LOADING  #{feed.title}  #{e.message}"
    #        next
    #      end
    #      
    #      
    new_articles = []
    #      unread_articles2.each{ | key, value | new_articles << value }
    #    end

    channel_info[:scan] = { :scan_start_time => t00, :scan_end_time => Time.now, :channel => channel_id}
    channel_info[:feeds] = ar_feeds
    channel_info[:recommended] = {:search => search, :total => recommended_articles.size, :articles => recommended_articles , :jobs => recommended_jobs}
    channel_info[:unread] = unread_articles #= { :total => new_articles.size , :articles => new_articles}
    channel_info[:failed] = failed

    #
    # Update the Channel Status
    #
    post_to_socialmachine(channel_info,file_name,url)
    
  end

 

  
end