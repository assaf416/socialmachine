
class FacebookScanner  < GenericScanner
  # encoding: UTF-8 
  require 'koala'
  require 'yaml'
  require 'nokogiri'
  
  
  #
  # FACEBOOK  Scan
  # 
  
  def scan(brand, url, file)
    status("Starting facebook scan")
    t00 = Time.now
    
    
    ar_commenters = []
    def short_post(post)
      
      #      debug "POST:  #{post.inspect}\n\n"
      content = ""
      picture = ""
      
      content = post["story"] if post["story"]
      content = post["message"] if post["message"]
      
      
      if post["picture"]
        picture = post["picture"] 
      else
        picture = post["icon"] 
      end
      
      return  {
        :id => post["id"],
        :text => content,
        :type => post["type"],
        :likes => post["likes"],
        :shares => post["shares"],
        :posted_at  => post["created_time"],
        :picture => picture,
        :target_link => post["link"],
        :from_name => post["from"]["name"],
        :from_uid => post["from"]["id"],
        :from_image_url => ""
      }
    end
    
    def competitor_posts(graph,rest,list)
      status( "COMPETITOR MONITORING..")
      competitors = []
      for item in list
        posts =[]
        if item[:facebook_fan_page]
          page = graph.get_object(item[:facebook_fan_page])
          page_posts = graph.get_connections(item[:facebook_fan_page], "feed")
          comments = []
          for post in page_posts do 
            posts << short_post(post)
            if post['comments']['count'] > 0 then
              begin
                for comment in post['comments']['data'] do
                  comments << { :from_uid => comment['from']['id'] ,:from_name => comment['from']['name'] , :posted_at => comment['created_time'] ,  :message => comment['message'] }
                end
              rescue Exception => e
                puts "NO COMMENTS.."
                next
              end
            end
          end
        end
        
        info = ""
        
        user_search_results =[]
        #1. Get user info and posts if has #
        #2. if uid exists, get competitor posts 
        if page["cover"]
          the_image_url = page["cover"]["source"] 
        else
          the_image_url = ""
        end
        
        competitors << { :id => item[:id] ,
          :name => item[:name] ,
          :image_url => the_image_url,
          :description => page["about"],
          :user_search => user_search_results ,
          :website => page["website"],
          :likes => page["likes"] , :phone => page["phone"], :shares => page["talking_about_count"],
          :posts_count => posts.count, 
          :comments_count => comments.count,
          :posts => posts, 
          :comments => comments
        }
      end
      
      return competitors
      
    end
    
    def fb_page_views(page_name)
      # run cat | grep to trackers.log and load the page's trackers
    end
    
    def fb_messages(graph,rest,ar_commenters)
      ret = []
      messages = graph.get_connections("me", "threads")
      idss = []
      for item in messages do 
        ret << { :from => item['senders'] , :posted_at => item["updated_time"] , :message => item["snippet"] , :from_user_id => "" , :messages_count => item["message_count"], :uid => item["id"]}
        #        debug item['senders']['data'][0]['id']
        idss << item['senders']['data'][0]['id']
      end
      add_to_commenters(rest,idss,ar_commenters)
      return ret
    end

    def add_to_commenters(rest,ids,ar_commenters)
      idss ="0" ; ids.each{|id| idss << ",#{id}"}
      fql = fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
      users = []
      profiles = rest.fql_query(fql)
      for profile in profiles do 
        begin
          ar_commenters << profile
        rescue Exception => e2
          error " FAILED ON GETTING COMMENTERS INFO  #{e2.message}"
        end
      end
    end

    def fb_article_comments(graph,articles)
      urls = []
      list = {}
      
      for article in articles do 
        
        urls << article[:url] 
        list[article[:url]] = { :url => article[:url], :article_id => article[:id]}
      end
      comments = graph.get_comments_for_urls(urls)
      for item in comments do 
        begin
          list[item[0]][:comments] = item[1]["comments"]["data"]
        rescue
          next
        end
      end
      
      return list
    end
    
    
    def fb_fan_pages_posts(graph,rest,ar_commenters)
      status "Getting Fan pages posts"
      fan_pages = []
      accounts = graph.get_connections("me", "accounts")
      for account in accounts do
        unless account['category'].include? "Application"
      
      
          comments = []
      
      
          page = graph.get_object(account["id"])
          page_posts = graph.get_connections(account["id"], "feed")
          comenter_uids = []
          for post in page_posts do 
            if post['comments']['count'] > 0 then
              for comment in post['comments']['data'] do
                comments << { :from_uid => comment['from']['id'] ,:from_name => comment['from']['name'] , :posted_at => comment['created_time'] ,  :message => comment['message'] }
                comenter_uids <<  comment['from']['id']
              end
            end
          end
          
          add_to_commenters(rest, comenter_uids,ar_commenters)
          
          fan_pages << {:id => account["id"],:name => page['name'], 
            :page => page, :general_info => account,
            :posts => page_posts,
            :comments => comments , 
            
            :stats => {:posts => page_posts.size , :comments => comments.size, :likes => page["likes"] }}
        end
      end
      return fan_pages
    end




    def fb_group_posts(graph,rest)
      status "Getting Group posts.."
      res = []
      groups = graph.get_connections("me", "groups")
      for group in groups
        posts = []
        users = []
        begin
          group_feed = graph.get_connections(group["id"], "feed")
          for item in group_feed do 
            posts << item
          end
        rescue Exception => ex
          error "FAILED ON  fb_posts_and_comments. Error: #{ex.message}"
          next
        end
        res << {:group => group , :posts => posts , :posts_count => posts.size}
      end
      return res
    end


    def fb_groups_find(graph,rest,search)
      status "Searching Groups -  #{search}"
      ids = []
      pages = []
      for q in search.split(',') do 
        begin
          search_res = graph.search("#{q}",{:type => "group"})
          for item in search_res do 
            begin
              ids << item['id'] 
            rescue Exception => e
              error "FAILED on fb_groups_find #{e.message}"
              next
            end
          end
      
          idss ="0" ; ids.each{|id| idss << ",#{id}"}
          fql = "SELECT gid, name, pic,group_type,email,update_time, description,website  FROM group WHERE  gid IN(#{idss})"
          users = []
          profiles = rest.fql_query(fql)
          for profile in profiles do 
            begin
              users << profile
            rescue Exception => e2
              error " FAILED on fb_groups_find #{e2.message}"
            end
          end
          return users
        rescue Exception => e
          error " FAILED on fb_groups_find  (2) #{e.message}"
          return users
        end
      end
    end


    def fb_content_find(graph,rest,search)
      status "CONTENT SEARCH #{search}"
      ids = []
      posts = []
      for q in search.split(',') do 
        begin
          search_res = graph.search("#{q}",{:type => "post"})
          for item in search_res do 
            begin
              posts << item
              ids << item['from']['id'] 
            rescue Exception => e
              error "FAILED on fb_mentions_find #{e.message}"
              next
            end
          end
      
          idss ="0" ; ids.each{|id| idss << ",#{id}"}
          fql = fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
          users = []
          profiles = rest.fql_query(fql)
          for profile in profiles do 
            begin
              users << profile
            rescue Exception => e2
              error " FB_MENTIONS_FIND  #{e2.message}"
            end
          end
          return {:search => search, :users => users , :posts => posts , :posts_found => posts.size,  :users_found => users.size }
        rescue Exception => e
          error "FB_MENTIONS_FIND 2 #{e.message}"
          return {:search => search,  :posts => posts, :users => users }
        end
      end
    end
    
    
    
    def fb_prospects(graph,rest,search)
      status "PROSPECTS: #{search}"
      ids = []
      s_ids = {}
      ar_prospects = []
      for q in search.split(',') do 
        begin
          search_res = graph.search("#{q}",{:type => "user"})
          for item in search_res do 
            begin
              ids << item["id"]
              s_ids[item["id"]] = q
            rescue Exception => e
              error "FAILED on fb_sprospects #{e.message}"
              next
            end
          end
        rescue Exception => e
          error "FAILED ON FB PROSPECTS #{e.message}"
          #          return {:search => search,  :posts => posts, :users => users }
        end
      end
      
      idss ="0" ; ids.each{|id| idss << ",#{id}"}
      fql = fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
      users = []
      profiles = rest.fql_query(fql)
      for profile in profiles do 
        begin
          p_id = profile["uid"].to_s
          profile["reason"] = s_ids[p_id]
          users << profile
        rescue Exception => e2
          error " +++++++++++++ #{e2.message}"
        end
      end
      return {:search => search, :users => users }
      
    end

    def fb_pages_find(graph,rest,search)
      status "Pages search  #{search}"
      ids = []
      pages = []
      for q in search.split(',') do 
        begin
          search_res = graph.search("#{q}",{:type => "page"})
          for item in search_res do 
            begin
              ids << item['id'] 
            rescue Exception => e
              error "FAILED on fb_pages_find #{e.message}"
              next
            end
          end
      
          idss ="0" ; ids.each{|id| idss << ",#{id}"}
          fql = "SELECT  page_id ,name, username, description,bio,checkins,company_overview,general_info,hometown,location, page_url,website,phone , fan_count FROM page WHERE  page_id IN(#{idss})"
          users = []
          profiles = rest.fql_query(fql)
          for profile in profiles do 
            begin
              users << profile
            rescue Exception => e2
              error " +++++++++++++ #{e2.message}"
            end
          end
          return {:search => search, :users => users }
        rescue Exception => e
          error " !!!!!!!!!!!!!!!!!!!! #{e.message}"
          return {:search => search, :users => users }
        end
      end
    end




    def short_profile(q,profile)
      return {
        :uid => profile["uid"],
        :name => profile["name"],
        :image_url => profile["pic_small"],
        :website => profile["website"],
        :home_url => "",
        :email => "",
        :friends_count => "",
        :bio => "",
        :search_key => q,
        :wall_count => profile["wall_count"],
        :friend_count => profile["friend_count"],
        :likes_count => profile["likes_count"],
        :location => profile["hometown_location"]
      }
    end

    def fb_search_users(graph,rest,search,kind)
      status "Searching Users of kind #{kind}"
      ids = []
      users = []
      for q in search.split(',') do 
        begin
          search_res = graph.search("#{q}",{:type => "post"})
    
          for item in search_res do 
            begin
              #status " ----> #{item.to_yaml}"
          
              #          status " POST USER : #{item['from']}   ID: #{item['from']['id']}"
              ids << item['from']['id'] 
          
              #          users << {  :uid => "#{item['id']}",  :name => "#{item['name']}", :image_url => "https://graph.facebook.com/#{item['id']}/picture", :kind => kind   }
            rescue Exception => e
              error "FAILED on fb_search_users #{e.message}"
              next
            end

          end
      
          idss ="0" ; ids.each{|id| idss << ",#{id}"}
          fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
      
          users = []
          profiles = rest.fql_query(fql)
          for profile in profiles do 
            begin
              #              profile["search"] = q
              users << short_profile(q,profile)
              #          status "#{kind} profile ::#{profile.inspect}"
            rescue Exception => e2
              error " +++++++++++++ #{e2.message}"
            end
          end
          return users
        rescue Exception => e
          error " !!!!!!!!!!!!!!!!!!!! #{e.message}"
          return users
        end
      end
  
    end

    
    def fb_my_photos(graph,rest)
      photos = []
      albums = graph.get_connections("me", "albums")
      for album in albums do 
        for photo in  @graph.get_connections(album["id"], "photos") do 
          photos << photo
        end
      end
      photos
    end
    
    def fb_my_events(graph,rest)
      status "Getting events"
      movies = graph.get_connections("me", "events")
    end
    
    def fb_my_movies(graph,rest)
      status "Getting movies"
      movies = graph.get_connections("me", "videos/uploaded")
    end
    def fb_my_posts(graph,rest)
      status "Getting photos"
      graph.get_connections("me", "feed")
    end
    
    
    def fb_friends(graph,rest , uids)
      #
      # Load Friends
      #
  
      ids2 = []
      ids = []
      res_friends = []
      res_new_friends = []
      friends = graph.get_connections("me", "friends")
      for friend in friends do
        ids << friend["id"]
        ids2 << friend["id"] unless uids.include? friend["id"]
      end
  
      idss = ids.to_s.gsub!("[","").gsub!("]","")
      fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
 
      idss2 = ids2.to_s.gsub!("[","").gsub!("]","")
      fql2 = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss2})"
 
      profiles = rest.fql_query(fql)
      new_profiles = rest.fql_query(fql2)
 
      for profile in profiles do
        res_friends << profile
      end
  
      for profile in new_profiles do
        res_new_friends << profile
      end
      return res_friends, res_new_friends
    end



    def fb_profile(graph,rest)
      fbp = graph.get_object("me")
      profile = rest.fql_query("SELECT friend_count , about_me, name, contact_email,pic_with_logo , username, website , hometown_location , status ,political   FROM user WHERE uid=#{fbp['id']}")
      p = profile[0]
      res_profile =  { 
        :name => p["name"] ,
        :bio => p["about_me"] , 
        :website => p["website"] , 
        :email => p["contact_email"],
        :image_url => p["pic_with_logo"]
        
      }
    end
    
    



    
    # globals
    token = "" 
    secret = ""
    profile = ""
    uids     = ""
    search_1,search_2,search_3,search_4 = ""
    ar_commenters = []
    
    
    status " LOADING PARAMETERS.."
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
      
      
      
      token = plan[0][:facebook_token]
      secret = plan[0][:facebook_secret]
      uids = plan[0][:facebook_fan_uids]
      profile = plan[0][:facebook_profile_name]
      competitor_search = plan[0][:competitor_search]
      content_search = plan[0][:content_search]
      prospect_search = plan[0][:prospect_search]
      group_search = plan[0][:group_search]
      job_search = plan[0][:job_search]
      ignore_uids = plan[0][:facebook_ingore_ids]
      current_competitors = plan[0][:competitors]
      newsletter_articles = plan[0][:article_urls]

      
      
      debug "IGNORING UIDS : #{ignore_uids} "
      #      debug "IGNORING UIDS Array : #{ignore_uids.to_ary} "
      
      channel_id = plan[0][:fb_cid]
      #      status " STARTING #{token}|secret:#{secret}:  uids:#{uids}"
      
      
      channel_info = {}
      res_friends = []
      res_pages = []
      res_groups = []
      res_profile = {}
  
      ids = []
      @graph = Koala::Facebook::API.new(token)
      @rest = Koala::Facebook::RestAPI.new(token)


      notfications = []
      fbp = @graph.get_object("me")
      fql = "SELECT notification_id, sender_id, title_text, body_text,object_type,icon_url, href, created_time ,is_unread  FROM notification WHERE recipient_id=#{fbp['id']}"
      @rest.fql_query(fql).each{|n| notfications << n}
  
  
      friends , new_friends = fb_friends(@graph,@rest, uids)
  
  
  
      
      

      channel_info[:profile] = fb_profile(@graph,@rest)
      channel_info[:articles_comments] = fb_article_comments(@graph,newsletter_articles)
      channel_info[:monitored_competitors] = competitor_posts(@graph,@rest,current_competitors)
#      channel_info[:prospects] = fb_prospects(@graph,@rest,prospect_search)
      channel_info[:recommended_content] = fb_content_find(@graph,@rest,content_search)
      channel_info[:notifications] = notfications
      channel_info[:friends] = friends
      channel_info[:new_friends] = new_friends
#      channel_info[:recommended_competitors] = fb_pages_find(@graph,@rest,competitor_search)
      channel_info[:recommended_groups] = fb_pages_find(@graph,@rest,group_search)
      channel_info[:fan_pages] = fb_fan_pages_posts(@graph,@rest,ar_commenters)
      channel_info[:commenters] =  ar_commenters
      channel_info[:groups] = fb_group_posts(@graph,@rest)
      channel_info[:messages] = fb_messages(@graph,@rest,ar_commenters)
      channel_info[:feed] = fb_my_posts(@graph,@rest)
      channel_info[:events] = fb_my_events(@graph,@rest)
      channel_info[:photos] = fb_my_photos(@graph,@rest)
     
      pages_stats = []
      total_posts = 0
      total_comments = 0
      total_likes = 0
      for page in channel_info[:fan_pages]
        total_posts = total_posts + page[:stats][:posts] unless page[:stats][:posts].nil?
        total_comments = total_comments + page[:stats][:comments] unless page[:stats][:comments].nil?
        total_likes = total_likes + page[:stats][:likes] unless page[:stats][:likes].nil?
       
        
        pages_stats << { 
          :uid  => page[:id],
          :name  => page[:name],
          :cover_url  => page[:page]["cover"]["source"],
          :posts => page[:stats][:posts],
          :comments => page[:stats][:comments],
          :likes => page[:stats][:likes],
        }
      end
        
      
      stats_block = {
        :posts => total_posts ,
        :comments => total_comments ,
        :likes => total_likes, :messages => channel_info[:messages].count ,:pages_stats => pages_stats}
        
      
      channel_info[:scan] = { 
        :scan_start_time => t00, :scan_end_time => Time.now, :channel => channel_id,
        :new_friends => new_friends.size,
        :total_friends => friends.size,
        :no_longer_friends => 0,
        :fan_pages_stats => stats_block
      }  
         
      
      
      #  status channel_info.to_yaml
  
       
      #
      # Update the Channel Status
      #
      post_to_socialmachine(channel_info,file,url)
      
      
    rescue Exception => e
      error "ERROR RUNIING SCAN : #{e.message}"
      puts e.message
      puts e.backtrace
    end
    
    #    AAAEyg2j03cMBAEVH0Ya0yo8GdMthcmlY4u6dr94qiTVFt843Ew62Qvu1wJqiDiYiN4RREJONwKuDsgW1aPzPZBnvoWeAw0hZA6ZB5xQOAZDZD
    #    AAAEyg2j03cMBAEVH0Ya0yo8GdMthcmlY4u6dr94qiTVFt843Ew62Qvu1wJqiDiYiN4RREJONwKuDsgW1aPzPZBnvoWeAw0hZA6ZB5xQOAZDZD
    #   
    
    
    
    
  end

  
 
end