class TwitterScanner  < GenericScanner
  require 'yaml'
  require 'nokogiri'
  require 'twitter'
  # encoding: UTF-8 
  
  
  def clean_string(s)
    unless s.nil?
      return s.gsub(/\s+/, ' ').strip
      return s.strip
    end
  end

  def short_post(post)
    if post.class == Twitter::Tweet
      #      debug " SHORT-POST (TWEET) #{post.inspect}"
      return { :id => post.id, 
        #        :created_at => post["created_at"], 
        :from_user_id => post.user.id,
        :from_user_name => post.user.name,
        :from_user_url => post.user.url,
        :from_user_url => post.user.url,
        :from_user_description => post.user.description,
        :from_user_followers => post.user.followers_count,
        :from_user_following => post.user.friends_count,
        :from_user_profile_image_url => post.user.profile_image_url,
        :source => post.source,
        :posted_at => post.created_at,
        :message => post.text,
        :target_link => post.urls
      }
    else
      #      debug "POST ::::: #{post.class} ==> #{post.inspect}"
    end
      
  end

  def short_user(user)
    #debug "SHORT USER #{user.class} :: #{user.inspect}"
      
    if user.class == Hash 
      {
        :id => user["id"] ,
        :friends => user["friends_count"],
        :description => clean_string(user["description"]),
        :profile_image_url => user["profile_image_url"],
        :name => user["name"],
        :lang => user["lang"],
        :profile_background_image_url => user["profile_background_image_url"],
        :followers_count => user["followers_count"],
        :screen_name => user["screen_name"],
        :url => user["url"],
        :statuses_count => user["statuses_count"],
        :location => user["location"],
        :time_zone => user["time_zone"],
        :retweet_count => user["retweet_count"]
      }
    end
      
    if user.class == Twitter::User 
      {
        :id => user.id ,
        :following => user.friends_count,
        :description => clean_string(user.description),
        :profile_image_url => user.profile_image_url,
        :name => user.name,
        :lang => user.lang,
        :profile_background_image_url => user.profile_background_image_url,
        :followers_count => user.followers_count,
        :screen_name => user.screen_name,
        :url => user.url,
        :statuses_count => user.statuses_count,
        :location => user.location,
        :time_zone => user.time_zone
        #          :retweet_count => user.retweet_count
      }
    end
  end

  

  
  def scan( brand, url, file)
    status("Starting Scan")
    t00 = Time.now
    # globals
    token = "" 
    secret = ""
    profile = ""
    uids     = ""
    search_1,search_2,search_3,search_4 = ""
  
    
  
    
  
    #
    # Twitter Scan
    # 
  
    status " LOADING PARAMETERS.."
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
      
      
      token = plan[0][:twitter_token]
      secret = plan[0][:twitter_secret]
      uids = plan[0][:twitter_fan_uids]
      profile = plan[0][:twitter_uid]
      competitor_search = plan[0][:competitor_search]
      content_search = plan[0][:content_search]
      prospect_search = plan[0][:prospect_search]
      group_search = plan[0][:group_search]
      job_search = plan[0][:job_search]
      ignore_uids = plan[0][:twitter_ingore_ids]
      channel_id = plan[0][:tw_cid]
      current_competitors = plan[0][:competitors] 

    rescue Exception => e
      error "ERROR RUNIING SCAN : #{e.message}"
    end
    
    
    channel_info = {}
  
    api = Twitter.configure do |config|
      config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
      config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
      config.oauth_token = token
      config.oauth_token_secret = secret
    end
    status " TWITTER SCAN! ( version: #{Twitter::Version.to_s})"
    
    begin
      user = Twitter.user
    rescue Exception => e
      error "FAILED ON CONNECTING TO TWITTER : #{e.message}"
    end
    
  
    my_followers   = []
    followers_ids   = []
    new_followers  = []
    not_following  = []
    my_twits_ids = []
 
    #
    # Add my followers
    #
    begin
      followers = Twitter.followers
      for u in followers do 
        my_followers << short_user(u)
        followers_ids << u.id
      end
    rescue Exception => e
      error "FAILED TO RUN  TWITTER FOLLOWER CALL : #{e.message}"
      error e.backtrace
    end

    old_list = uids.split(",")
    new_followers = followers_ids - old_list
    not_following =  old_list - followers_ids
    #    status  " New Followers ( not in my curren ids ) #{}"
    #    status "\n\n\nOLD LIST : #{old_list}"
    #    status "\n\nNEW LIST : #{followers_ids}"
    #    status "\n\nNEW FOLLOWERS ==> #{followers_ids - old_list}"
    #    status "\n\nNOT FOLLOWING ==> #{ old_list - followers_ids}"
    # New followers that are not in my fan list.
  
   
    
    status  " TWITTER MY POSTS"
    my_posts = []
    begin
      search_key = user.screen_name
      res = Twitter.user_timeline(user.screen_name)
      res.each{|r| my_posts << short_post(r);my_twits_ids << r.id}
    rescue Exception => e
      error("FAILED ON SEARCHING MY POSTS #{e.message}")
      error e.backtrace
    end
  
    status  " TWITTER COMPETITOR POSTS"
    ar_competitors = []
    if current_competitors then
      for competitor  in current_competitors do 
        ar_posts = []
        unless competitor[:twitter_name].nil?
          competitor_posts = []
          begin
            cmp_usr = Twitter.user(competitor[:twitter_name])
            user_info = short_user(cmp_usr)
            res = Twitter.user_timeline(competitor[:twitter_name])
            res.each{|r| competitor_posts << short_post(r)}
            ar_competitors << {:id => competitor[:id], :info => user_info, :posts => competitor_posts}
          rescue Exception => e
            error("FAILED ON COMPETITOTR POSTS #{e.message}")
            error e.backtrace
          end
        end
      end
    end
    
    
    status  " TWITTER RETWEETS"
    retweets = []
    commenters = []
    api.retweets_of_me.each{|t| retweets << short_post(t) ; commenters << short_user(t.user)}
  
  
    #
    # Build the YAML tree
    #`
  

    channel_info[:profile] = short_user(user)
    #    channel_info[:mentions] = mentions
    channel_info[:statuses] = my_posts
    channel_info[:monitored_competitors] = ar_competitors
    #  channel_info[:messages] = messages
    channel_info[:followers] = my_followers
    channel_info[:new_followers] = new_followers
    channel_info[:not_following] = not_following
    channel_info[:retweets] = retweets
    channel_info[:commenters] = commenters
    channel_info[:scan] = { 
      :account_name => profile, 
      :scan_start_time => t00,
      :scan_end_time => Time.now, 
      :channel => channel_id,
      :posts => my_posts.size,
      :new_friends => new_followers.size,
      :total_friends => my_followers.size,
      :no_longer_friends => not_following.size,
      :comments => retweets.size,
      :messages => 0
    }
  
    
 
  
      
 
    
    
    #
    # Update the Channel Status
    #
    post_to_socialmachine(channel_info,file,url)
    #
    #
    #    File.open(file, 'w') { |file| file.write(channel_info.to_yaml) }
    #    status " UPDATING THE SOCIAL MACHINE .."
    #    params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
    #    x = Net::HTTP.post_form(URI.parse("#{url}/api/process_scan"), params)
    #    status " POSTED YAML TO SOCIALMACHINE"
    #    
    
    
   
  end
  
end