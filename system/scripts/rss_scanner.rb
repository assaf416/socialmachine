require 'feedzirra'

# encoding: UTF-8 
class RSSScanner < GenericScanner
 
  def feed_name(url)
     s1 = url.index("//") +2
     s2 = url.index("?") 
     s2 = url.length if s2 == nil
     return url[s1..s2-1]
  end
  
  
  
  def scan(brand, url, file_name)
    status "SCANNING RSS FEEDS"
    t00 =Time.now
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      error "FAILED THE RSS  SCAN #{e.message}"
    end
      
    
    
    search = plan[0][:content_search]
    job_search = plan[0][:job_search]
    channel_id = plan[0][:rs_cid]
    feeds  = plan[0][:rss_feeds]
    channel_info = {:name => "RSS"}
    ar_feeds = []
    failed = []
    recommended_articles = []
    recommended_jobs = []
    unread_articles = []
  
    start_time = Time.now - 3.days
    
    for feed in feeds do 
      _count = 0;_count2 =0
      feedx = Feedzirra::Feed.fetch_and_parse(feed[:url])
      begin
        feedx.entries.each do |entry|
          if entry.published > start_time 
            xentry = {
              :feed         => feed_name(feed[:url]),
              :name         => entry.title,
              :summary      => entry.summary,
              :url          => entry.url,
              :published_at => entry.published,
              :guid         => entry.id
            }
          
            unread_articles << xentry
            for keyword in search.split(",") do 
              unless xentry[:summary].nil?
                if xentry[:summary].include? keyword
                  recommended_articles <<  xentry 
                  _count = _count + 1
                  break
                end
              end
            end
            
            for keyword in job_search.split(",") do 
              unless xentry[:summary].nil?
                if xentry[:summary].include? keyword
                  recommended_jobs <<  xentry 
                  _count2 = _count2 + 1
                  break
                end
              end
            end
          end
        end
        ar_feeds << { :url =>  feed[:url].to_s   , :found => _count.to_i , :jobs => _count2.to_i , :name => feedx.title}
      rescue Exception => e
        failed << { :namd => feed[:url],    :reason => e.message}
        puts "FAILED ON ENTRIES #{e.message} || FEED: #{feed[:url]}"
        next
      end
      
      
    end

    channel_info[:scan] = { :scan_start_time => t00, :scan_end_time => Time.now, :channel => channel_id}
    channel_info[:feeds] = ar_feeds
    channel_info[:recommended] = {:search => search, :total => recommended_articles.size, :articles => recommended_articles , :jobs => recommended_jobs}
    channel_info[:unread] = unread_articles #= { :total => new_articles.size , :articles => new_articles}
    channel_info[:failed] = failed

    #
    # Update the Channel Status
    #
    post_to_socialmachine(channel_info,file_name,url)
    
  end

 

  
end