require 'json'
require './generic_scanner'
require './facebook_scanner'
require './twitter_scanner'
require './linkedin_scanner'
require './gmail_scanner'
require './rss_scanner'
require './google_plus_scanner'
#require './youtube_scanner'
require './website_scanner'
require './wordpress_scanner'
require './reddit_scanner'



t0 = Time.now
puts " =====  #{t0} ======= start"
server_url = "http://localhost:3000" 
#server_url = "http://23.21.17.104"
brand = 1

scan_website = false
scan_facebook = true
scan_twitter = true
scan_linkedin = true
scan_youtube = false
scan_gmail = true
scan_rss = true
scan_google_plus = false
scan_wordpress = false
scan_blogger = false
scan_reddit = false
#

threads = []



#
# WEBSITE SCAN
#
if scan_website
  threads << Thread.new() {  
    begin
      ws = WebsiteScanner.new
      ws.scan(brand, server_url, "../logs/website.json")  
    rescue Exception => e
      puts " -- @@-- FAILED ON WEBSITE  SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end


#
# FACEBOOK
#
if scan_facebook
  threads << Thread.new() {  
    begin
      fb = FacebookScanner.new
      fb.scan(brand, server_url, "../logs/facebook-out.yaml")  
     rescue Exception => e
      puts " -- @@-- FAILED ON FACEBOOK  SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end


#
# TWITTER
#
if scan_twitter
  threads << Thread.new() {  
    begin
      tw = TwitterScanner.new
      tw.scan(brand, server_url, "../logs/twitter.out.yaml")
    rescue Exception => e
      puts " -- @@-- FAILED ON TWITTER SCAN.."
      puts e.backtrace
    end
  }
end

#
# Linkedin
#
if scan_linkedin
  threads << Thread.new() {  
    begin
      li = LinkedinScanner.new
      li.scan(brand, server_url, "../logs/linkedin.out.yaml")
    rescue Exception => e
      puts " -- @@-- FAILED ON Linkedin SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end



#
# Gmail
#
if scan_gmail
  threads << Thread.new() {  
    begin
      gm = GmailScanner.new
      gm.scan(brand, server_url, "../logs/gmail.out.yaml")
    rescue Exception => e
      puts " -- @@-- FAILED ON GMAIL SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end

#
# RSS FEED
#
if scan_rss
  threads << Thread.new() {  
    begin
      gr = RSSScanner.new
      gr.scan(brand, server_url, "../logs/rss.out.yaml")
    rescue Exception => e
      puts " -- @@-- FAILED ON RSS  SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end


#
# YOUTUBE
#
if scan_youtube
  threads << Thread.new() {  
    begin
      yt = YoutubeScanner.new
      yt.scan(brand, server_url, "../logs/youtube.out.yaml")
    rescue Exception => e
      puts " -- @@-- FAILED ON Youtube SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end



#
# GOOGLE +
#
if scan_google_plus
  threads << Thread.new() {  
    begin
      gp = GooglePlusScanner.new
      gp.scan(brand, server_url, "../logs/google_plus.out.yaml")
    rescue Exception => e
      puts " -- @@-- FAILED ON GOOGLE PLUS SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end



#
# WORDPRESS 
#
if scan_wordpress
  threads << Thread.new() {  
    begin
      wp = WordpressScanner.new
      wp.scan(brand, server_url, "../logs/wordpress.out.yaml")
    rescue Exception => e
      puts " -- @@-- FAILED ON WORDPRESS.COM SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end



#
# BLOGGER 
#
if scan_wordpress
  threads << Thread.new() {  
    begin
      bl = WordpressScanner.new
      bl.scan(brand, server_url, "../logs/blogger.out.yaml")
    rescue Exception => e
      puts " -- @@-- FAILED ON BLOGGER SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end



#
# BLOGGER 
#
if scan_reddit
  threads << Thread.new() {  
    begin
      bl = RedditScanner.new
      bl.scan(brand, server_url, "../logs/reddit.out.yaml")
    rescue Exception => e
      puts " -- @@-- FAILED ON REDDIT SCAN.. #{e.message}"
      puts e.backtrace
    end
  }
end
threads.each { |aThread|  aThread.join }


t1 = Time.now
puts " =====  #{t1} ======= finish"
puts "PROCESS TOOK #{t1 - t0 } seconds"



