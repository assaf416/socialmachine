
class YoutubeScanner < GenericScanner
  require 'youtube_it'
  # encoding: UTF-8 
  #
  # Youtube Scan
  # 
  def short_video_url(vid)
    v1 = vid.index("video:")
    return vid[v1+6..100] unless v1.nil?
    return ""
  end
    
  def profile_info(profile)
    begin
      s = {
        :name => profile.username, 
        :views => profile.view_count,
        :description => profile.description,
        :uid => profile.user_id,
        :image_url => profile.avatar,
        :location =>  profile.location,
        :subscribers => profile.subscribers,
        :movies     => profile.upload_count
      } unless profile.nil?
      return s
    rescue Exception => e
      error "FAILED ON PROFILE_INFO: #{e.message}"
      return nil
    end
  end
  
  
  def video_info(video)
    
    s = {:title => video.title,
      :published => video.published_at ,
      :author_name => video.author.name, 
      :author_url => video.author.uri, 
      :player_url => video.player_url, 
      :views => video.view_count,
      :thumbnails => video.thumbnails[0].url,
      :keywords => video.keywords,
      :description => video.description,
      :category => video.categories[0].label,
      :comments_count => video.comment_count
    }
    return s
  end
  
  def comment_info(comment)
    return {  :title => comment.title , 
      :posted_at => comment.updated,
      :message =>comment.content, 
      :url => comment.url,
      :reply_to => comment.reply_to ,
      :author => comment.author.name,
      :author_home => comment.author.uri
    }
  end
  
  def scan(brand, url, file)
    t00 = Time.now
    
   
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      error "FAILED THE YOUTUBE SCAN #{e.message}"
      exit
    end
      
    youtube_channel = plan[0][:youtube_profile_name]
    search_query = plan[0][:content_search]
    channel_info = {}
    my_videos = []
    comments = []
    ar_competitors = []
    search_videos = []
    channel_id = plan[0][:yt_cid]
    user = plan[0][:youtube_user]
    pwd = plan[0][:youtube_pwd]
    
     
    status "SCANNING YOUTUBE #{YouTubeIt::VERSION}"
#    client = YouTubeIt::Client.new
    status "LOGIN WITH #{user} and #{pwd}"
    client = YouTubeIt::Client.new(:username => user, :password =>  pwd, :dev_key => "AI39si7WXSzgtG9l6tmfyTqHcK6RZXvGSnzke76XuiIovPfNkkvmynoLt2sqfcAgCujq_I8Qn9SVAqWSvTRaAe9yWDtilZj7Mw")
    
    
    status " MY YOUTUBE CHANNEL: #{youtube_channel}"
    my_profile = client.profile(user)
    
    for video in client.my_videos.videos do 

      v_id = short_video_url(video.video_id)
      my_videos << {:video => video_info(video)}
      
      if video.comment_count > 0
        status " GETTING COMMENTS for #{v_id}"
        #comments
        for comment in client.comments(video.unique_id) do 
          comments <<  comment_info( comment) 
        end
      end
    end
    
    # search for competitors and content
    status "COMPETITOR MOVIES for #{search_query}"
    competitors_names = []
    for word in search_query.split(",") do 
      for video in client.videos_by(:query => word).videos do 
        search_videos << { :video => video_info(video)}
        competitors_names << video.author.name 
      end
    end

    
    for profile in client.profiles(competitors_names[0..10])
      begin
        #        debug "COMPETITOR NAME : #{profile[1]} :: #{profile[1].class}"
        value = profile_info(profile[1]) 
        ar_competitors << value unless value.nil?
      rescue Exception => e
        error "FAILED ON COMPETITOR NAMES #{e.message}"
        next
      end
    end

 
    
    names = []
    ar_subscriptions = []
    ar_subscriptions_videos = []
    for item in client.subscriptions(youtube_channel)  do
      names << item.title.gsub!("Activity of: ","")
    end
    for profile in client.profiles(names)
      new_videos =[]
#      for video in client.videos_by(:fields => {:published  => ((Date.today - 14)..(Date.today)), :user => profile[0]["username"]} ).videos do 
      for video in client.new_subscription_videos.videos do 
        #        status " GETTING  SUBSCRIPTION  VIDEOS"
        v_id = short_video_url(video.video_id)
        new_videos << video_info(video)
        ar_subscriptions_videos << video_info(video)
      end
      ar_subscriptions <<  { :name => profile}
    end
    
   
    channel_info[:scan] =   { 
      :scan_start_time => t00, :scan_end_time => Time.now,
        
      :channel => plan[0][:yt_cid], :uid => youtube_channel,
      :total_friends => my_profile.subscribers.to_i, :no_longer_friends => my_profile.subscribers.to_i,
      :posts => my_videos.size, :comments => comments.size
        
    }
    channel_info[:profile] = my_profile
    channel_info[:my_videos] = my_videos
    channel_info[:search_videos] = search_videos
    channel_info[:competitors] = ar_competitors
    channel_info[:comments] = comments
    channel_info[:subscriptions] = ar_subscriptions
    channel_info[:subscriptions_videos] = ar_subscriptions_videos
    
    #
    # Update the Channel Status
    #
    post_to_socialmachine( channel_info, file,url)
    
    #    status " UPDATING THE SOCIAL MACHINE .."
    #    params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
    #    x = Net::HTTP.post_form(URI.parse("#{url}/api/process_scan"), params)
    #    status " POSTED YAML TO SOCIALMACHINE"
    #    
    
    
  
  end
end


