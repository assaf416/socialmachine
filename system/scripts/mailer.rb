require './generic_scanner'
require 'yaml'
require 'net/http'
require 'mail'
require 'json'
 
# encoding: UTF-8 
class Mailer < GenericScanner
  
  
  Mail.defaults do
    delivery_method :smtp, { :address   => "smtp.sendgrid.net",
      :port      => 587,
      :domain    => "socialmachine.biz",
      :user_name => "assaf416",
      :password  => "clipper",
      :authentication => 'plain',
      :enable_starttls_auto => true }
  end

  
  
  #status "SARTING MAILER"
  def initialize
    @stats = []
    @emails_sent = []
    @emails_failed = []
    @emails_opened = []
    @emails_clicked = []
  end
  
  def send_email(email)
    status "CALLING GMAIL SEND OR SENDGRID | MAILCHIMP  TO SEND EMAIL on #{email[:to_email]}"
    
    
    mail = Mail.deliver do
      to "#{email[:to_email]}"
      from 'The social machine <mailer@socialmachine.biz>'
      subject "#{email[:subject]}"
      text_part do
        body "#{email[:body_text]}"
      end
      html_part do
        content_type 'text/html; charset=UTF-8'
        body "#{email[:body_html]}"
      end
    end
    
    
    
    
    @emails_sent << email[:id]
  end
  
  
  def pending_emails(url)
    begin
      uri = URI.parse("#{url}/api/pending_emails")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      brands = YAML::load(res)
      emails = []
      for item in brands do 
        status "BRAND: #{item[:brand_name]}"
        for email in item[:emails] do 
          emails << email
          send_email(email)
        end
      end
    rescue Exception => e
      error "FAILED THE Mailer #{e.message}"
      puts e.backtrace
    end
  end
  
  
  def store_and_post
    begin
      File.open("../logs/mailer.log.yaml", 'w') { |file| file.write({:sent => @emails_sent,:stats => @stats}.to_yaml) }
      #    status " UPDATING THE SOCIAL MACHINE .."
      #    params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
    
      #      x = Net::HTTP.post_form(URI.parse("#{server_url}/api/process_scan"), params)
      #      status "RETURNED FROM SOCIAL MACHINE : #{x}"
    rescue Eexception => e
      error "FAILED ON POSTING TO SOCIAL MACHINE #{e.message}"  
      error e.backtrace 
    end
  end
  
  
  def mail_stats(usr,pwd)
    url = "http://sendgrid.com/api/stats.get.json?api_user=#{usr}&api_key=#{pwd}&days=2"
 
    res =  Net::HTTP.get(URI.parse(url))
    @stats = JSON.parse  res
  end

  
  def run
    status "RUNNING!"
    #    pending_emails("http://localhost:3000")
    mail_stats("assaf416","clipper")
  end
end


s = Mailer.new
s.run
s.store_and_post