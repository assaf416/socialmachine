class GenericScanner

  def status(s)
     puts " *** SCANNER-STATUS::  #{self.class} :: #{Time.now}  #{s}"
  end

  def error(s)
     puts " !!! SCANNER-ERROR::  #{self.class} :: #{Time.now}  #{s}"
  end

  def debug(s)
     puts " ??? SCANNER-DEBUG::  #{self.class} :: #{Time.now}  #{s}"
  end

  def post_to_socialmachine(channel_info,file_name,server_url)
    #
    # Update the Channel Status
    #
    File.open(file_name, 'w') { |file| file.write(channel_info.to_yaml) }
    status " UPDATING THE SOCIAL MACHINE .."
    params = {'yaml' => channel_info.to_yaml ,  'submit_yaml' => 'Submit'}
    begin
      x = Net::HTTP.post_form(URI.parse("#{server_url}/api/process_scan"), params)
      status "RETURNED FROM SOCIAL MACHINE : #{x}"
    rescue Exception => e
      error "FAILED ON POSTING TO SOCIAL MACHINE #{e.message}"  
      error e.backtrace 
    end
  end
  
end