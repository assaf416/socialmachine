#!/usr/bin/env ruby

require "rubygems"
require "highline/import"


def deploy
  # sudo rm -f  logs/development.log
  # sudo git pull
  # sudo rm -f  logs/development.log
end

def restart_rails
  # touch tmp/restart.txt
end

def log_file(file)
  " cat #{file}"
end

def log_files
  " cat #{file}"
end


def top_5_mem
  " cat #{file}"
end

def top_5_cpu
  " cat #{file}"
end

def top_status
  res = ""
  sts = `df -h | grep /dev/`
  res << "DISK: #{sts.split(" ")[2]} /#{sts.split(" ")[1]} #{sts.split(" ")[4]} | " 
  sts = `free -m  | grep Mem:`
  res 
end


def log_menu
  say("\nLOG FILES\n====================")
  choose do |menu|
    #  menu.index        = :letter
    menu.index_suffix = ") "

    menu.prompt = "select log file to view?  "

    menu.choice "Rails Development log file" do 
      say("Good choice!") 
    end
    menu.choice "NGIX files" do 
      say("Good choice!") 
    end
  
    menu.choice "Back" do 
      main_menu 
    end
  
  end
end


def cron_menu
  say("\n MANAGING CRON JOBS\n====================")
  choose do |menu|
    #  menu.index        = :letter
    menu.index_suffix = ") "

    menu.prompt = "select option  "

    menu.choice "Show current cron settings" do 
      say("Good choice!") 
    end
    
    menu.choice "Reload Rails project cron" do 
      say("Good choice!") 
    end
  
    menu.choice "Back" do 
      main_menu 
    end
  
  end
end


def db_menu
  say("\n MANAGING DATABASE\n====================")
  choose do |menu|
    #  menu.index        = :letter
    menu.index_suffix = ") "

    menu.prompt = "select option  "

    menu.choice "Show Table status" do 
      say("Good choice!") 
    end
    
    menu.choice "Import Database" do 
      say("Good choice!") 
    end
  
    menu.choice "Export Database" do 
      say("Good choice!") 
    end

    menu.choice "Migrate Database" do 
      say("Good choice!") 
    end
  
  
  
    menu.choice "Back" do 
      main_menu 
    end
  
  end
end



def main_menu
  
  say("\n #{top_status}RSYSTEM MONITOR\n====================")
  choose do |menu|
    #  menu.index        = :letter
    menu.index_suffix = ") "

    menu.prompt = "Select your option  "

    menu.choice "Show System info" do 
      say("Good choice!") 
    end
    
    menu.choice "Log Files " do 
      log_menu
    end

    menu.choice "Cron jobs " do 
      cron_menu
    end

    menu.choice "Manage Database  " do 
      db_menu
    end


    menu.choice "Restart Rails server  " do 
      log_menu
    end

    menu.choice "Social Machine tasks " do 
      log_menu
    end

  
    menu.choice "Quit" do 
      say("exit") 
    end
  
  end

end


main_menu