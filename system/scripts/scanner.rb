#encoding: UTF-8
require "rubygems"
require "multi_json"
require "koala"
require "youtube_it"
require 'yaml'
require 'nokogiri'
require 'twitter'
require 'gmail'
require 'mail'
#gem 'sax-machine-nokogiri-1.4.4-safe'
require 'open-uri'
require "google_reader_api"
require 'cgi'



@log_url = ""

#   -------------------------------------------------------------------------
#    logger
#   -------------------------------------------------------------------------
def log_status(msg)
  puts "#{Time.now} :: STATUS :: #{msg}"
end
def log_error(msg)
  puts "#{Time.now} :: FAILURE :: #{msg}"
end

#   -------------------------------------------------------------------------
#
#    GOOGLE SEARCH
#
#   -------------------------------------------------------------------------
def google_reader(brand_id,user, pwd,search)  
  channel_info = {:name => "RSS"}
  feeds = []
  failed = []
  recommended_articles = []
  unread_articles = []
  unread_articles2 = []
  

  user = GoogleReaderApi::User.new(:email => user, :password => pwd)

  for feed in user.feeds do 
    begin 
      feeds << feed
      for x in feed.unread_items(10) do 
        #        x.entry.toggle_read
        unread_articles <<  x.entry
        unless x.entry.content.nil?
          ssss = { 
            :message => x.entry.content.content ,
            :posted_at => x.entry.published.content,
            :link => x.entry.link.href,
            :title  => x.entry.title.to_s,
            :feed => { :name => feed.title , :url => feed.url}
          } 
          
          unread_articles2 <<  ssss
          for keyword in search.split(",") do 
            recommended_articles <<  ssss if ssss[:message].include? keyword
          end
        end
      end
    rescue Exception => e
      failed << {:feed => feed.title, :error => e.message}
      log_error "FAILED ON GOOGLE READER LOADING  #{feed.title}  #{e.message}"
      next
    end
  end

  channel_info[:scan_time] = Time.now  
  channel_info[:feeds] = {  :total => feeds.size, :feeds => feeds}
  channel_info[:recommended] = {:search => search, :total => recommended_articles.size, :articles => recommended_articles}
  channel_info[:unread] = { :total => unread_articles2.size , :articles => unread_articles2}
  channel_info[:failed] = failed

  
  #  channel_info[:unread_short] = unread_articles2
  
  File.open("#{@log_url}/#{brand_id}.rss.yml", "w") { |file| file.write(channel_info.to_yaml) }
  log_status "WROTE THE RSS FILE!"
end

#   -------------------------------------------------------------------------
#
#    GOOGLE SEARCH
#
#   -------------------------------------------------------------------------
def google_search(what)  
  #   return []
  new_search = what #CGI::escape(what)

  channel_info = {}
  
  
  begin    
    doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{new_search}&num=100"))
    
    # Do funky things with it using Nokogiri::XML::Node methods...

    ar_1 = []
    ar_2 = []
    ar_3 = []

    ####
    # Search for nodes by css
    doc.css('h3.r a').each do |link|
      ar_1 << link.content
    end

    
    doc.search("cite").each do |cite|
      link = cite.inner_text
      ar_2 << link
    end

    doc.css("li.g").each do |x|
      ar_3 <<  x.inner_text
    end
    
    
    res_google = []
    i = 0
    for item in ar_1 do
      description = ar_3[i]
      res_google << {:title => ar_1[i], :description => description, :url => ar_2[i]} 
      i = i + 1
    end
    puts " 111"
    res = []

    for item in res_google do
      begin
        title =  item[:title].encode('utf-8', :invalid => :replace, :undef => :replace)
        description =  item[:description].encode('utf-8', :invalid => :replace, :undef => :replace)
        link =  item[:url].encode('utf-8', :invalid => :replace, :undef => :replace)
        res << {:title => title, :description => description, :link => link}
      rescue Exception => e
        puts " BAD DATA #{e.message}"
        next
      end
    end
    
    puts " 2222"
    File.open("google.yml", "w") { |file| file.write(res.inspect) }
    puts " 3333333333"
    puts "Done searching in google for #{what}!"
    res_google
  rescue Exception => e
    puts " ERROR !!!!! #{e.message}"
  end
end

#   -------------------------------------------------------------------------
#
#    GMAIL
#
#   -------------------------------------------------------------------------
def scan_gmail(brand_id,email,pwd)
  puts "SCANNING GMAIL"
  channel_info = {}
  the_emails = []
  contacts = []
  begin
    gmail = Gmail.connect(email, pwd) 
    t = Time.now - 86400
    the_date = t.strftime("%Y-%m-%d")
    puts "READING MAIL FROM #{the_date}"
    emails =  gmail.inbox.mails(:after => Date.parse(the_date))
    for item in emails[0..10] do 
      begin
        mail = Mail.read_from_string item.message
        
        
        m = { :subject => item.subject , 
          :from_email => "#{item.envelope.sender[0].mailbox}@#{item.envelope.sender[0].host}" ,
          :posted_at => item.envelope.date,
          :uid => item.uid}
        
        m[:body] = mail.html_part.decoded if mail.html_part
        m[:body_text] = mail.text_part.decoded if mail.text_part
        #        m[:body] = mail.html_part.body.raw_source if mail.html_part
        
        the_emails << m
          
        puts "EMAIL : #{m[:subject]}"
        
        contacts << { :name => item.envelope.sender[0].name ,
          :email =>"#{item.envelope.sender[0].mailbox}@#{item.envelope.sender[0].host}" }
                    
        
      rescue Exception => e2
        puts "FAILED on email_reading #{e2.message}"
        if e2.message.include? "SCALAR, SEQUENCE-START"
          channel_info[:emails] = the_emails
          channel_info[:contacts] = contacts
    
          File.open("#{@log_url}/#{brand_id}.gmail.yml", 'w') { |file| file.write(channel_info.to_yaml) }
          puts "Done partial  loading gmail for #{email}"
        end
        next
      end
    end
    
    channel_info[:scan_time] = Time.now
    channel_info[:emails] = the_emails
    channel_info[:contacts] = contacts
    
    #    puts channel_info.to_yaml
    
    File.open("#{@log_url}/#{brand_id}.gmail.yml", 'w') { |file| file.write(channel_info.to_yaml) }
    puts "Done loading gmail for #{email}"
    
  end
  gmail.logout
  
  
end






#   -------------------------------------------------------------------------
#
#    Youtube
#
#   -------------------------------------------------------------------------


def video_id(video)
  v = video.reverse
  vv = ""
  for c in v.chars do 
    vv << c unless c == '/'
    return vv.reverse! if c == '/'
  end
end
  
def youtube_scan(brand_id,channel_name, competitor_search, prospect_search,content_search)
  
  
  log_status "YOUTUBE SCAN for channel #{channel_name}"
  channel_info = {}
  my_videos = []
  begin
    client = YouTubeIt::Client.new 
    profile =  client.profile(channel_name) # default: current user
    
   
        
    #
    # find competitors
    #
    competitor_videos = []
    competitors = []
    
    for search_key in competitor_search.split(",") do 
      for video in client.videos_by(:query => search_key).videos do 
        competitor_videos << video
        competitors <<  video.author.name
      end
    end

    profiles = client.profile(competitors)
    channel_info[:recommended_competitors] = {:search => competitor_search, :users => profiles}
    
    channel_info[:scan_time] = Time.now
    channel_info[:profile] = profile
    channel_info[:videos] = my_videos
    
    
    File.open("#{@log_url}/#{brand_id}.youtube.yml", 'w') { |file| file.write(channel_info.to_yaml) }
    puts "Done loading yourutbe for #{channel_name}"
    
  rescue Exception => e
    log_error " FAILED ON YOUTUBE_PROFILE_UPDATE #{e.message}"
  end
  
  
  
  
end



#   -------------------------------------------------------------------------
#
#    FACEBOOK 
#
#   -------------------------------------------------------------------------



def fb_messages(graph,rest)
  ret = []
  messages = graph.get_connections("me", "threads")
  for item in messages do 
    ret << { :from => item['senders'] , :posted_at => item["updated_time"] , :message => item["snippet"] , :from_user_id => "" , :messages_count => item["message_count"]}
  end
  
  return ret
end


def fb_fan_pages_posts(graph,rest)
  fan_pages = []
  accounts = graph.get_connections("me", "accounts")
  for account in accounts do
    unless account['category'].include? "Application"
      
      
      comments = []
      
      
      page = graph.get_object(account["id"])
      page_posts = graph.get_connections(account["id"], "feed")
      
      for post in page_posts do 
        if post['comments']['count'] > 0 then
          for comment in post['comments']['data'] do
            comments << { :from_uid => comment['from']['id'] ,:from_name => comment['from']['name'] , :posted_at => comment['created_time'] ,  :message => comment['message'] }
          end
        end
      end
      fan_pages << {:name => page['name'], :page => page, :general_info => account, :posts => page_posts, :comments => comments }
    end
  end
  return fan_pages
end




def fb_group_posts(graph,rest)
  
  res = []
  groups = graph.get_connections("me", "groups")
  for group in groups
    posts = []
    users = []
    begin
      group_feed = graph.get_connections(group["id"], "feed")
      for item in group_feed do 
        posts << item
      end
    rescue Exception => ex
      logger.error "FAILED ON  fb_posts_and_comments. Error: #{ex.message}"
      next
    end
    res << {:group => group , :posts => posts}
  end
  return res
end


def fb_groups_find(graph,rest,search)
  ids = []
  pages = []
  for q in search.split(',') do 
    begin
      search_res = graph.search("#{q}",{:type => "group"})
      for item in search_res do 
        begin
          ids << item['id'] 
        rescue Exception => e
          puts "FAILED on fb_groups_find #{e.message}"
          next
        end
      end
      
      idss ="0" ; ids.each{|id| idss << ",#{id}"}
      fql = "SELECT gid, name, pic,group_type,email,update_time, description,website  FROM group WHERE  gid IN(#{idss})"
      users = []
      profiles = rest.fql_query(fql)
      for profile in profiles do 
        begin
          users << profile
        rescue Exception => e2
          log_error " FAILED on fb_groups_find #{e2.message}"
        end
      end
      return users
    rescue Exception => e
      log_error " FAILED on fb_groups_find  (2) #{e.message}"
      return users
    end
  end
end


def fb_mentions_find(graph,rest,search)
  ids = []
  posts = []
  for q in search.split(',') do 
    begin
      search_res = graph.search("#{q}",{:type => "post"})
      for item in search_res do 
        begin
          posts << item
          ids << item['from']['id'] 
        rescue Exception => e
          log_error "FAILED on fb_mentions_find #{e.message}"
          next
        end
      end
      
      idss ="0" ; ids.each{|id| idss << ",#{id}"}
      fql = fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
      users = []
      profiles = rest.fql_query(fql)
      for profile in profiles do 
        begin
          users << profile
        rescue Exception => e2
          log_error " FB_MENTIONS_FIND  #{e2.message}"
        end
      end
      return {:search => search, :users => users , :posts => posts , :posts_found => posts.size,  :users_found => users.size }
    rescue Exception => e
      log_error "FB_MENTIONS_FIND 2 #{e.message}"
      return {:search => search,  :posts => posts, :users => users }
    end
  end
end

def fb_pages_find(graph,rest,search)
  ids = []
  pages = []
  for q in search.split(',') do 
    begin
      search_res = graph.search("#{q}",{:type => "page"})
      for item in search_res do 
        begin
          ids << item['id'] 
        rescue Exception => e
          log_error "FAILED on fb_pages_find #{e.message}"
          next
        end
      end
      
      idss ="0" ; ids.each{|id| idss << ",#{id}"}
      fql = "SELECT  page_id ,name, username, description, page_url,website,phone , fan_count FROM page WHERE  page_id IN(#{idss})"
      users = []
      profiles = rest.fql_query(fql)
      for profile in profiles do 
        begin
          users << profile
        rescue Exception => e2
          puts " +++++++++++++ #{e2.message}"
        end
      end
      return {:search => search, :users => users }
    rescue Exception => e
      puts " !!!!!!!!!!!!!!!!!!!! #{e.message}"
      return {:search => search, :users => users }
    end
  end
end






def fb_search_users(graph,rest,search,kind)
 
  ids = []
  users = []
  for q in search.split(',') do 
    begin
      search_res = graph.search("#{q}",{:type => "post"})
    
      for item in search_res do 
        begin
          #puts " ----> #{item.to_yaml}"
          
          #          puts " POST USER : #{item['from']}   ID: #{item['from']['id']}"
          ids << item['from']['id'] 
          
          #          users << {  :uid => "#{item['id']}",  :name => "#{item['name']}", :image_url => "https://graph.facebook.com/#{item['id']}/picture", :kind => kind   }
        rescue Exception => e
          puts "FAILED on fb_search_users #{e.message}"
          next
        end

      end
      
      idss ="0" ; ids.each{|id| idss << ",#{id}"}
      fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
      
      users = []
      profiles = rest.fql_query(fql)
      for profile in profiles do 
        begin
          users << profile
          #          puts "#{kind} profile ::#{profile.inspect}"
        rescue Exception => e2
          puts " +++++++++++++ #{e2.message}"
        end
      end
      return users
    rescue Exception => e
      puts " !!!!!!!!!!!!!!!!!!!! #{e.message}"
      return users
    end
  end
  
end


def fb_friends(graph,rest , uids)
  #
  # Load Friends
  #
  
  ids2 = []
  ids = []
  res_friends = []
  res_new_friends = []
  friends = graph.get_connections("me", "friends")
  for friend in friends do
    ids << friend["id"]
    ids2 << friend["id"] unless uids.include? friend["id"]
  end
  
  idss = ids.to_s.gsub!("[","").gsub!("]","")
  fql = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss})"
 
  idss2 = ids2.to_s.gsub!("[","").gsub!("]","")
  fql2 = "SELECT uid, name, first_name, last_name,middle_name,pic_small,affiliations,devices,sex,hometown_location,relationship_status,political,notes_count,wall_count,profile_url ,website,contact_email,email,work,education,likes_count,friend_count,mutual_friend_count FROM user WHERE  uid IN(#{idss2})"
 
  profiles = rest.fql_query(fql)
  new_profiles = rest.fql_query(fql2)
 
  for profile in profiles do
    res_friends << profile
  end
  
  for profile in new_profiles do
    res_new_friends << profile
  end
  return res_friends, res_new_friends
end



def fb_profile(graph,rest)
  fbp = graph.get_object("me")
  profile = rest.fql_query("SELECT friend_count , about_me, name, contact_email,pic_with_logo , username, website , hometown_location , status ,political   FROM user WHERE uid=#{fbp['id']}")
  p = profile[0]
  res_profile =  { :name => p["name"] , :bio => p["about_me"] , :website => p["website"] , :email => p["contact_email"]}
end

def facebook_scan(brand_id,token , content_search, competitor_search,group_search,mention_search, uids)
  channel_info = {}
  res_friends = []
  res_pages = []
  res_groups = []
  res_profile = {}
  
  ids = []
  @graph = Koala::Facebook::GraphAPI.new(token)
  @rest = Koala::Facebook::RestAPI.new(token)


  notfications = []
  fbp = @graph.get_object("me")
  fql = "SELECT notification_id, sender_id, title_text, body_text,object_type,icon_url, href, created_time ,is_unread  FROM notification WHERE recipient_id=#{fbp['id']}"
  @rest.fql_query(fql).each{|n| notfications << n}
  
  
  friends , new_friends = fb_friends(@graph,@rest, uids)
  
  
  
  #  channel_info[:groups] = fb_group_posts(@graph,@rest)
  
  channel_info[:scan_time] = Time.now
  channel_info[:profile] = fb_profile(@graph,@rest)
  channel_info[:notifications] = notfications
  channel_info[:friends] = friends
  channel_info[:new_friends] = new_friends
  channel_info[:recommended_competitors] = fb_pages_find(@graph,@rest,competitor_search)
  channel_info[:recommended_groups] = fb_pages_find(@graph,@rest,group_search)
  channel_info[:fan_pages] = fb_fan_pages_posts(@graph,@rest)
  #  channel_info[:groups_posts] = fb_fan_pages_posts(@graph,@rest)
  channel_info[:messages] = fb_messages(@graph,@rest)
  channel_info[:mentions] = fb_mentions_find(@graph,@rest,mention_search)
  
  puts " ------------ RESULT ---------"
  #  puts channel_info.to_yaml
  
  File.open("#{@log_url}/#{brand_id}.facebook.yml", 'w') { |file| file.write(channel_info.to_yaml) }
  puts "done with facebook!"
end



#   -------------------------------------------------------------------------
#
#    TWITTER
#
#   -------------------------------------------------------------------------


def clean_string(s)
  return s.gsub(/\s+/, ' ').strip
  return s.strip
end

def short_post(post)
  { :id => post["id"], 
    :created_at => post["created_at"], 
    :from_user_id => post["user"]["id"],
    :from_user_name => post["from_user_name"],
    :from_user_profile_image_url => post["profile_image_url"],
    :source => post["source"],
    :message => post["text"],
  }
end

def short_user(user)
  {
    :id => user["id"] ,
    :friends => user["friends_count"],
    :description => clean_string(user["description"]),
    :profile_image_url => user["profile_image_url"],
    :name => user["name"],
    :lang => user["lang"],
    :profile_background_image_url => user["profile_background_image_url"],
    :followers_count => user["followers_count"],
    :screen_name => user["screen_name"],
    :url => user["url"],
    :statuses_count => user["statuses_count"],
    :location => user["location"],
    :time_zone => user["time_zone"],
    :retweet_count => user["retweet_count"]
  }
end


def user_from_list(id,list)
  log_status " ===  FINDING USER IN LIST!"
  log_status " ==>  LIST: #{list.inspect}"
  log_status " ==>  ID: #{id.inspect}"
  for item in list do 
    if item[:id] == id.to_s
      puts "return: #{item.inspect}"
      return item 
    end
  end
end

def twitter_scan(brand_id,token, secret , id , content_search,competitors_search,prospect_search,mentions_search, uids)
  
  channel_info = {}
  
  api = Twitter.configure do |config|
    config.consumer_key = "LIQsuD0HPrVmEOqsYkCQA"
    config.consumer_secret = "0xwhCucXeS6HEQtuCJW6BkWC9X1ZxWin4cuB6C3CEM"
    config.oauth_token = token
    config.oauth_token_secret = secret
  end
  
 
  puts " TWITTER SCAN!"
  
  my_followers   = []
  new_followers  = []
  not_following  = []
 
  #
  # Add my followers
  #
  begin
    user_ids = "-1,"
    api.follower_ids.collection.each{|p | user_ids << ",#{p}"}
    search_url =  "http://api.twitter.com/1/users/lookup.json?user_id=#{user_ids}&include_entities=true"
    uri = URI.parse(search_url)
    response = Net::HTTP.get_response(uri)
    res = Net::HTTP.get(uri)
    json = JSON.parse(res)
    json.each{|p| my_followers << short_user(p) } 
  rescue
    puts "FAILED TO RUN  TWITTER FOLLOWER CALL"
  end

  log_status " BEFORE NEW FOLLOWERS !"
  # New followers that are not in my fan list.
  my_followers.each{|f|
    begin
      if uids.to_s.include? f[:id].to_s then
        log_status "#{f[:id]} exists.."
      else
        new_followers << user_from_list(f[:id],my_followers)
        log_status " NEW USER -> #{user_from_list(f[:id],my_followers)}"
      end
    rescue Exception => e
      log_status "RECOVER FROM follower #{e.message}"
      next
    end
    
  }
  
  log_status " NEW FOLLOWERS !"
  
  # add the people who left me
  current_followers_ids = [];  my_followers.each{|f| current_followers_ids << f[:id]}
  db_followers_ids = [] ;uids.split(",").each{| uid | db_followers_ids << uid unless uid.nil? or uid.empty? }
  for item in db_followers_ids do 
    if current_followers_ids.to_s.include? item
    else
      not_following <<  user_from_list(item,my_followers)
    end
  end
  
  log_status " TWITTER PROFILE"
  
  #
  # Add profile
  #
  sts = []
  user = api.user(id)
  
  log_status " TWITTER MY POSTS"
  my_posts = []
  Twitter.search("from:#{user.screen_name}").statuses.each{|r| my_posts << short_post(r)}
  
  log_status " TWITTER MENTIONS"
  #
  # mentions
  #
  mentions = []
  api.mentions.each{|m |  mentions << { :created_at => m.created_at , :text => m.text , :user => short_user(m.user)} }

  log_status " TWITTER RETWEETS"
  #
  # Add my retweets
  #
  retweets = []
  api.retweets_of_me.each{|t| retweets << short_post(t)}
  
  
  #
  # Build the YAML tree
  #
  
  channel_info[:scan_time] = Time.now
  channel_info[:profile] = short_user(user)
  channel_info[:mentions] = mentions
  channel_info[:statuses] = my_posts
  #  channel_info[:messages] = messages
  channel_info[:followers] = my_followers
  channel_info[:new_followers] = new_followers
  channel_info[:not_following] = not_following
  channel_info[:retweets] = retweets
  
  debug =2
  if debug == 2
  
    #
    # Search for content
    #
  
    log_status " TWITTER CONTENT SEARCH"
    uids = "-1"
    content_users = []
    posts = []
    for search_word in content_search.split(",") do 
      Twitter.search(search_word).statuses.each{|r|
        #        puts " --> #{r.inspect}"
        posts << short_post(r) 
        uids << ",#{ short_post(r)[:from_user_id] }"
        
      }
    end
  
    begin
      search_url =  "http://api.twitter.com/1/users/lookup.json?user_id=#{uids}&include_entities=true"
      uri = URI.parse(search_url)
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      json = JSON.parse(res)
      json.each{|p| content_users << short_user(p) } 
    rescue Exception => e
      puts "FAILED TO RUN CONTENT SEARCH CALL #{e.message}.   :::: URI => #{uri} res => #{res}"
    end
  
    channel_info[:recommended_content] = {:search => content_search, :posts_found => posts.size, :users_found => content_users.size,  :users => content_users, :posts => posts }
  
 
    #
    # Search for competitors
    #
    log_status " TWITTER COMPETITOR SEARCH"
    uids = "-1"
    competitors_users = []
    posts = []
    for search_word in competitors_search.split(",") do 
      Twitter.search(search_word).statuses.each{|r| posts << short_post(r) ; uids << ",#{short_post(r)[:from_user_id]}"}
    end
  
    begin
      search_url =  "http://api.twitter.com/1/users/lookup.json?user_id=#{uids}&include_entities=true"
      uri = URI.parse(search_url)
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      json = JSON.parse(res)
      json.each{|p| competitors_users << short_user(p) } 
    rescue Exception => e
      puts "FAILED TO RUN TWITTER COMPETITORS SEARCH CALL #{e.message}"
    end
  
    channel_info[:recommended_competitors] = {:search => competitors_search, :posts_found => posts.size, :users_found => competitors_users.size,  :users => competitors_users, :posts => posts }
  
 
    #
    # Search for prospects
    #
    log_status " TWITTER PROSPECTS SEARCH"
    uids = "-1"
    prospects = []
    posts = []
    for search_word in prospect_search.split(",") do 
      Twitter.search(search_word).statuses.each{|r| posts << short_post(r) ; uids << ",#{short_post(r)[:from_user_id]}"}
    end
  
    begin
      search_url =  "http://api.twitter.com/1/users/lookup.json?user_id=#{uids}&include_entities=true"
      uri = URI.parse(search_url)
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      json = JSON.parse(res)
      json.each{|p| prospects << short_user(p) } 
    rescue Exception => e
      puts "FAILED TO RUN PROSPECTS SEARCH CALL : #{e.message}"
    end
  
    channel_info[:recommended_prospects] = {:search => prospect_search, :posts_found => posts.size, :users_found => prospects.size,  :users => prospects, :posts => posts }
  
  
  end
  
  File.open("#{@log_url}/#{brand_id}.twitter.yml", 'w') { |file| file.write(channel_info.to_yaml) }
  puts "Finished twitter scan!"
end






#   -------------------------------------------------------------------------
#
#    MAIN
#
#   -------------------------------------------------------------------------

if ARGV == [] then 
  puts " USAGE: scanner  http://localhost:3000/api/scan_plan"  "/logs"
  exit
else
  puts " LOADING PARAMETERS.."
  begin
    plan_url = ARGV[0]
    @log_url = ARGV[1]
    uri = URI.parse(plan_url)
    response = Net::HTTP.get_response(uri)
    res = Net::HTTP.get(uri)
    puts res
    plan = YAML::load(res)
  rescue Exception => e
    puts "ERROR RUNIING SCAN : #{e.message}"
    exit
  end
end

puts " STATUS: TIME IS #{Time.now}"
puts " LOADING PLAN FROM SOCIAL MACHINE"

for brand in plan do 
  puts "BRAND : #{brand[:brand_name]}"

  
  
  t0 = Time.now 
  begin  
    twitter_scan(brand[:brand], brand[:twitter_token],brand[:twitter_secret], brand[:twitter_profile_name],
      brand[:twitter_search_1],brand[:twitter_search_2],brand[:twitter_search_3],brand[:twitter_search_4], brand[:twitter_fan_uids])
  rescue Exception => e
    puts " FAILED ON TWITTER SCAN for #{brand[:brand_name]}. error : #{e.message}"
  end
  puts " == TWITTER DONE IN #{Time.now - t0} "
     
  
  
  
  
  t0 = Time.now 
  begin 
    facebook_scan(brand[:brand],brand[:facebook_token] , brand[:facebook_search_1],  brand[:facebook_search_2], brand[:facebook_search_3], brand[:facebook_search_4],brand[:facebook_fan_uids])
  rescue Exception => e
    puts " FAILED ON FACEBOOK SCAN for #{brand[:brand_name]}. error : #{e.message}"
  end
  puts " == FACEBOOK DONE IN #{Time.now - t0} "


  
  t0 = Time.now 
  begin
    google_reader(brand[:brand],brand[:gmail_email], brand[:gmail_secret], "Apple,Samsung")
  rescue Exception => e
    puts " FAILED ON GOOGLE READER SCAN for #{brand[:brand_name]}. error : #{e.message}"
  end
  puts " == GOOGLE REAFER DONE  IN #{Time.now - t0} "
  


  
    
  t0 = Time.now 
  begin  
    youtube_scan(brand[:brand],brand[:youtube_profile_name], "social marketing" , "ipad,android", "another one")
  rescue Exception => e
    puts " FAILED ON YOUTUBE SCAN for #{brand[:brand_name]}. error : #{e.message}"
  end
  puts " == YOUTUBE DONE IN #{Time.now - t0} "
  
  begin
    scan_gmail(brand[:brand],brand[:gmail_email], brand[:gmail_secret])
  rescue Exception => e
    puts " FAILED ON GMAIL SCAN for #{brand[:brand_name]}. error : #{e.message}"
  end
  
  
  # CREATE COMPANY ROOT FILE 
  
  
  
end


