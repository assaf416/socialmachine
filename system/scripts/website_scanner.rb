require 'uri'
require 'cgi'
require 'json'
require 'nokogiri'
require 'open-uri'
require 'google_url_shortener'  


class WebsiteScanner  < GenericScanner
  @global_keywords = {}
  # encoding: UTF-8 
  
  #
  # Website SCAN
  # 
  
  
  def google_shortener(short_url)
    Google::UrlShortener::Base.api_key = "AIzaSyDQU97mjsTO_qfzAHNcfguvhcd2CNvv6Yw"
    url = Google::UrlShortener::Url.new(:short_url => short_url)
    url.expand! 
    stats = {}
    stats[:browsers] = url.analytics.all.browsers
    stats[:countries] = url.analytics.all.countries
    stats[:referrers] = url.analytics.all.referrers
    stats[:referrers] = url.analytics.all.referrers
    stats[:long_url_clicks] = url.analytics.all.long_url_clicks
    stats[:short_url_clicks] = url.analytics.all.short_url_clicks
    
    return stats
  end
  
  def add_to_global_keywords (keywords,list)
    for keyword in keywords.split(",") do
      if list[keyword].present?
        list[keyword] =list[keyword] + 1
      else
        list[keyword] = 1
      end
    end
  end
  
  
  def scan_results(results,list,competitor_keywords)
    for item in results do 
        if item[:url] =~ /facebook.com|twitter.com|linkedin.co|.org|wikipedia|.pdf/i then
           puts "IGNORING SOCIAL NETOWRK:  #{item[:url]}"
        else
          meta = ""
          begin
            meta  =  page_meta_data(item[:url],competitor_keywords)
            list << {:location => item[:location],:title => item[:title], :description => item[:description], :url => item[:url] , :keyword => item[:keyword], :meta => meta }
          rescue Exception => e
            puts " FAILED TO GET META DATA FOR #{item[:url]} REASON: #{e.message}"
            next
          end
        end
      end
      return list
  end
  
  
  def scan(brand, url, file)
    
    channel_info = {}
    site_info = {}
    begin
      uri = URI.parse("#{url}/api/scan_plan?id=#{brand}")
      response = Net::HTTP.get_response(uri)
      res = Net::HTTP.get(uri)
      plan = YAML::load(res)
    rescue Exception => e
      error "FAILED THE WEBSITE SCAN #{e.message}"
    end
      
    website = plan[0][:website_url]
    pwd = plan[0][:gmail_secret]
    channel_id = plan[0][:gm_cid]
    website_id = plan[0][:website_id]
    t00 = Time.now
    status "SCANNING WEBSITE URL: #{website}"
    
    
    
    begin
    
      competitor_keywords = {}
      res_list = []
      site_info[:meta] = page_meta_data(website,competitor_keywords)
      keywords = site_info[:meta][:keywords]
      keyword_searches = google_keywords(keywords,website) unless keywords.nil?
      
      res_list = scan_results(keyword_searches,res_list,competitor_keywords)
      
      
      
      channel_info[:scan] = { :scan_start_time => t00, :scan_end_time => Time.now, :channel => -1, :website_id => website_id}
      channel_info[:meta] = page_meta_data(website,competitor_keywords)
      channel_info[:results] = res_list
      channel_info[:searches] = keyword_searches
      channel_info[:competitors_keywords] = competitor_keywords
      channel_info[:website_stats] = google_shortener(website)
      File.open(file, 'w') { |file| file.write(channel_info.to_json) }
      
      status " UPDATING THE SOCIAL MACHINE .."
      params = {'json' => channel_info.to_json ,  'submit_yaml' => 'Submit'}
      begin
        x = Net::HTTP.post_form(URI.parse("#{url}/api/process_website"), params)
        status "RETURNED FROM SOCIAL MACHINE : #{x}"
      rescue Exception => e
        error "FAILED ON POSTING TO SOCIAL MACHINE #{e.message}"  
        error e.backtrace 
      end
    end
  end
  
  
  def page_views(page_name)
    # run cat | grep to trackers.log and load the page's trackers
  end
    
    
  def page_meta_data(the_url,competitor_keywords)
      
    if the_url.include? "http"
    else
      the_url = "http://" + the_url
    end
      
    status " READING #{the_url}"
    # grab page
    begin
      doc =  Nokogiri::HTML( open(URI.encode(the_url)))
    rescue Exception => e
      error "FAILED TO LOAD PAGE FOR ANALYSIS: #{the_url} , #{e.message}"
#      puts e.backtrace
      return  { :url => the_url, :keywords => "", :description => e.message}
    end
    

    # keywords
    keywords =[]
    begin
      doc.css('meta[name="keywords"]').each do |meta_tag|
        keywords << meta_tag['content'] 
        add_to_global_keywords(meta_tag['content'],competitor_keywords)
      end
    rescue Exception => e
      error "PAGE  #{the_url} HAS NO KEYWORDS. "
    end
    
    
    # META DESCRIPTION
    meta = ""
    begin
      doc.css('meta[name="description"]').each do |meta_tag|
        meta << meta_tag['content'] 
      end
    rescue Exception => e
      error "PAGE  #{the_url} HAS NO DESCRIPTION. "
    end
      
    
    # SOCIAL LINKS
    social_links = []
    begin
      doc.css('a').each do |link|
        social_links << link['href']  if link['href'].include? "facebook"
        social_links << link['href']  if link['href'].include? "twitter"
        social_links << link['href']  if link['href'].include? "linkedin"
        social_links << link['href']  if link['href'].include? "youtube"
      end
    rescue Exception => e
      error "PAGE  #{the_url} HAS NO DESCRIPTION. "
    end
    
    
    # EMAILS
    emails = []
    
    
    
    # IMAGES
    images = []
    begin
      doc.css('img').each do |img|
        images << img['src']  
      end
    rescue Exception => e
      error "PAGE  #{the_url} HAS NO DESCRIPTION. "
    end
      
    return { :url => the_url, :keywords => keywords, :description => meta , :social_links => social_links, :emails => emails, :images => images}
  end

 
  
  def call_google(what)
    status "CALLING GOOGLE SEARCH WITH  #{what}"
    new_search = CGI::escape(what)
    begin    
      doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{new_search}&num=25"))

      ar_1 = []
      ar_2 = []
      ar_3 = []

      doc.css('h3.r a').each do |link|
        begin
          ar_1 << link.content.encode
        rescue 
          next
        end
      end

      doc.search("cite").each do |cite|
        begin
          link = cite.inner_text.encode
          ar_2 << link
        rescue
          next
        end
      end

      doc.css("li.g").each do |x|
        begin
          ar_3 << x.inner_text.encode
        rescue
          next
        end
      end

      res_google = []
      i = 0
      for item in ar_1 do
        begin
          description = ar_3[i]
          res_google << {:title => ar_1[i], :description => description, :url => ar_2[i] , :keyword => what , :location => i} 
          i = i + 1
        rescue
          next
        end
      end
      return res_google
    end
    return []
  end
  
  def google_keywords(keywords,url)
    
    status " GOOGLE SEARCHING FOR #{keywords} of SITE:#{url}"
   
    
    results = []
    
    for what in keywords[0].split(",") do    
      call_google(what).each{ |item| results << item }
    end
      
    return results
  end
  
  
  
  
    
  def process_ccpage(url)
      
    i = 0
    for item in res_google do
      i = i + 1
      found_locations << i if item[:url].present? and item[:url].include? url
          
      begin
        link =  item[:url]
        if link.include? "/"
          pos = link.index("/")
          link = link[0..pos-1]
          #            puts " LINK : #{link}"
        end
          
            
            
        if filtered["#{link}"].nil?
          if link =~ /facebook.com|twitter.com|linkedin.co|.org|wikipedia|.pdf/i then
            #             puts "IGNORING SOCIAL NETOWRK:  #{link}"
          else
            #            puts "ADDED #{i}: #{link}"
            meta = ""
            begin
              meta  =  page_meta_data(item[:url],competitor_keywords)
            rescue Exception => e
              puts " FAILED TO GET META DATA FOR #{item[:url]} REASON: #{e.message}"
              next
            end
            filtered["#{link}"] =  {:location => i, :keyword => item[:keyword],:host => link , :title => item[:title], :description => item[:description] , :meta => meta}
          end
        end
  
      rescue Exception => e
        puts "ERROR #{e.message}"
        next
      end
    end
      
    filtered.keys.each{| k|  
      puts "#{filtered[k][:location]} ) #{filtered[k][:host]} | #{filtered[k][:title]}"
    }
          
    results << {:search => what, :locations => found_locations ,:resutls => filtered}
  rescue Exception => e
    error " *** #{e.message}"
    puts e.backtrace
    return results
  end
    
end
#    debug results.inspect


  